import os

def parse_env(settings, tenant_name):
    current = {}

    if settings['PLEIO_DB_HOST']:
        current['DB_HOST'] = settings['PLEIO_DB_HOST']

    if settings['PLEIO_DB_USER']:
        current['DB_USER'] = settings['PLEIO_DB_USER']

    if settings['PLEIO_DB_PASS']:
        current['DB_PASS'] = settings['PLEIO_DB_PASS']

    if settings['PLEIO_DATAROOT']:
        current['DATAROOT'] = os.path.join(settings['PLEIO_DATAROOT'], tenant_name, '') # '' adds trailing slash

    return {
        **os.environ,
        **current,
        'DB_NAME': tenant_name,
        'MEMCACHE_PREFIX': tenant_name,
        'ELASTIC_INDEX': tenant_name
    }
