# Pleio background
This folder contains scripts that are used for running Pleio background tasks. Tasks are handled using [Celery](http://www.celeryproject.org/) distributed task queue.

The runners are tenant-aware and run tasks for multiple tentants simultaneously. Celery also handles the periodic tasks (Elgg cron) with [beat](https://docs.celeryproject.org/en/latest/userguide/periodic-tasks.html).

Tasks can be created in PHP with the function `pleio_register_task` and can be scheduled with `pleio_schedule_task`.

## Call tasks from the CLI

To manually call commands from the CLI, use:

```bash
celery -A background.app call {taskname} --args='{args}'
```

Possible tasknames and arguments:

- background.dispatch_cron, ["{period}"]
- background.dispatch_command, ["{command}"]
- elgg.run_cron, ["{tenant_name}", "{period}"]
- elgg.run_command, ["{tenant_name}", "{command}"]

Some example commands:

- Run the daily cron on all tenants:
    ```bash
    celery -A background.app call background.dispatch_cron --args='["daily"]'
    ```
- Reset the Elasticsearch index on all tenants:
    ```bash
    celery -A background.app call background.dispatch_command --args='["es:index:reset"]'
    ```
- Run the daily cron on tenant *pleio*:
    ```bash
    celery -A background.app call elgg.run_cron --args='["pleio", "daily"]'
    ```

## Setup development environment

To setup a development environment with Python run the following commands from the root of the repository:

```bash
python -m venv .venv
source .venv/bin/activate
pip install -r background/requirements.txt
```

## Start a worker locally

To start a worker locally, run the following command from the root of the repository:

```bash
celery -A background.app worker -B -E -O fair --loglevel=info
```
