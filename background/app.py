import os
from celery import Celery
from celery.utils.log import get_task_logger
from sqlalchemy import create_engine
from elasticsearch import Elasticsearch
from .crontab import beat_schedule

logger = get_task_logger(__name__)

settings = {
    'AMQP_HOST': os.getenv('AMQP_HOST', ''),
    'AMQP_USER': os.getenv('AMQP_USER', ''),
    'AMQP_PASS': os.getenv('AMQP_PASS', ''),
    'AMQP_VHOST': os.getenv('AMQP_VHOST', '/'),
    'PLEIO_ENV': os.getenv('PLEIO_ENV', 'prod'),
    'PLEIO_DB_HOST': os.getenv('PLEIO_DB_HOST', ''),
    'PLEIO_DB_USER': os.getenv('PLEIO_DB_USER', ''),
    'PLEIO_DB_PASS': os.getenv('PLEIO_DB_PASS', ''),
    'PLEIO_DB_NAME': os.getenv('PLEIO_DB_NAME', ''),
    'PLEIO_DATAROOT': os.getenv('PLEIO_DATAROOT', ''),
    'ELASTIC_SERVER_1': os.getenv('ELASTIC_SERVER_1', '')
}

app = Celery('tasks', broker='amqp://{AMQP_USER}:{AMQP_PASS}@{AMQP_HOST}/{AMQP_VHOST}'.format(**settings))

app.settings = settings
app.logger = logger

app.elasticsearch = Elasticsearch([settings['ELASTIC_SERVER_1']])
app.engine = create_engine(
    'mysql+pymysql://{PLEIO_DB_USER}:{PLEIO_DB_PASS}@{PLEIO_DB_HOST}/{PLEIO_DB_NAME}'.format(**settings),
    pool_recycle=3600
)

app.conf.beat_schedule = beat_schedule

app.autodiscover_tasks(['background'])
