import os
import re
import json
import subprocess
import textract
from celery import shared_task
from .utils import parse_env

@shared_task(name='background.dispatch_cron', bind=True)
def dispatch_crons(self, period):
    connection = self.app.engine.connect()

    sites = connection.execute('SELECT * FROM instances WHERE env = %s AND enabled = 1', [self.app.settings['PLEIO_ENV']])
    for site in sites:
        self.app.logger.info('Schedule cron {} for {}'.format(period, site['name']))
        elgg_run_cron.delay(site['name'], period)

    connection.close()

@shared_task(name='elgg.run_cron', bind=True)
def elgg_run_cron(self, tenant_name, period):
    self.app.logger.info('Running {} cron for {}'.format(period, tenant_name))

    try:
        result = subprocess.run(
            ['php', 'console.php', 'cron', period],
            env=parse_env(self.app.settings, tenant_name),
            check=True
        )
    except subprocess.CalledProcessError as exception:
        self.app.logger.error('Cron {} for {} exits with a non-zero exit code, ignoring.'.format(period, tenant_name))

@shared_task(name='background.dispatch_command', bind=True)
def dispatch_command(self, command, argument=None):
    connection = self.app.engine.connect()

    sites = connection.execute('SELECT * FROM instances WHERE env = %s AND enabled = 1', [self.app.settings['PLEIO_ENV']])
    for site in sites:
        self.app.logger.info('Schedule command {} for {}'.format(command, site['name']))
        run_command.delay(site['name'], command, argument)

    connection.close()

@shared_task(name='elgg.run_command', bind=True)
def run_command(self, tenant_name, command, argument=None):
    self.app.logger.info('Run command {} for {}'.format(command, tenant_name))

    try:
        command = ['php', 'console.php', command]
        if argument:
            command.append(argument)

        result = subprocess.run(
            command,
            env=parse_env(self.app.settings, tenant_name),
            check=True
        )
    except subprocess.CalledProcessError as exception:
        self.app.logger.error('Running command {} for {} exits with a non-zero exit code, ignoring.'.format(command, tenant_name))

@shared_task(name='elgg.execute_task', bind=True)
def elgg_execute_task(self, tenant_name, task_name, arguments):
    self.app.logger.info('Executing task {} for {}'.format(task_name, tenant_name))

    try:
        result = subprocess.run(
            ['php', 'console.php', 'execute_task', task_name, json.dumps(arguments)],
            env=parse_env(self.app.settings, tenant_name),
            check=True
        )
    except subprocess.CalledProcessError as exception:
        self.app.logger.error('Executing task {} for {} exits with a non-zero exit code, ignoring.'.format(task_name, tenant_name))

@shared_task(name='elasticsearch.bulk', bind=True)
def elasticsearch_bulk(self, params):
    for b in params['body']:
        if b.get('file'):
            try:
                filename, file_extension = os.path.splitext(b['file'])
                if file_extension in ['.pdf', '.doc', '.docx', '.pptx', '.txt']:
                    file_contents = ''
                    file_contents = re.sub(r"\s+", " ", textract.process(b['file'], encoding='utf8').decode("utf-8"))
                    b['file_contents'] = file_contents
            except Exception as e:
                self.app.logger.error('Error occured while indexing file: {} - {}'.format(b['file'], e))

    try:
        self.app.elasticsearch.bulk(body=params['body'])
    except Exception as e:
        self.app.logger.error('Error occured while bulk updating in Elasticsearch: {}'.format(e))

@shared_task(name='elasticsearch.update', bind=True)
def elasticsearch_update(self, params):

    if 'file' in params['body']:
        filename, file_extension = os.path.splitext(params['body']['file'])
        if file_extension in ['.pdf', '.doc', '.docx', '.pptx', '.txt']:
            file_contents = ''
            try:
                file_contents = re.sub(r"\s+", " ", textract.process(params['body']['file'], encoding='utf8').decode("utf-8"))
                params['body']['file_contents'] = file_contents
            except Exception as e:
                self.app.logger.error('Error occured while indexing file: {} - {}'.format(params['body']['file'], e))
    try:
        self.app.elasticsearch.index(index=params['index'], doc_type=params['type'], id=params['id'], body=params['body'])
    except Exception as e:
        self.app.logger.error('Error occured while updating record in Elasticsearch: {}'.format(e))

@shared_task(name='elasticsearch.delete', bind=True)
def elasticsearch_delete(self, params):
    try:
        self.app.elasticsearch.delete(index=params['index'], doc_type=params['type'], id=params['id'])
    except Exception as e:
        self.app.logger.error('Error occured while deleting record in Elasticsearch: {}'.format(e))
