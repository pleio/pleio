FROM ubuntu:16.04
EXPOSE 80

# Packages
RUN apt-get update -y
RUN apt-get install -y software-properties-common python-software-properties language-pack-en-base
RUN LC_ALL=en_US.UTF-8 add-apt-repository -y ppa:ondrej/php
RUN apt-get update -y && apt-get install --no-install-recommends -y \
        antiword \
        apache2 \
        ca-certificates \
        gettext \
        inetutils-ping \
        libapache2-mod-php7.1 \
        locales \
        php7.1 \
        php7.1-bcmath \
        php7.1-curl \
        php7.1-gd \
        php7.1-mbstring \
        php7.1-memcached \
        php7.1-mysql \
        php7.1-xml \
        php7.1-zip \
        poppler-utils \
        python3-pip \
        python3-setuptools \
        ssmtp \
        swig \
        telnet \
        tesseract-ocr \
        curl \
        git \
        && rm -rf /var/lib/apt/lists/*

# Composer
COPY composer.lock /app/composer.lock
COPY composer.json /app/composer.json
RUN curl -o /usr/local/bin/composer https://getcomposer.org/download/1.6.5/composer.phar
RUN chmod +x /usr/local/bin/composer
RUN cd /app && composer install

# Scripts
COPY ./docker/initialize.sh /scripts/initialize.sh
COPY ./docker/apache.sh /scripts/apache.sh
COPY ./docker/celery.sh /scripts/celery.sh

# Configuration files
COPY ./docker/000-default.conf /etc/apache2/sites-available/000-default.conf
COPY ./docker/remoteip.conf /etc/apache2/conf-available/remoteip.conf
COPY ./docker/php.ini /etc/php/7.1/apache2/php.ini
COPY ./docker/php.ini /etc/php/7.1/cli/php.ini
COPY ./docker/ssmtp.conf /etc/ssmtp/ssmtp.conf

# Make ssmtp.conf writable as it is set on container runtime
RUN chown -R www-data:www-data /etc/ssmtp

# Apache configuration
RUN a2enmod rewrite remoteip headers expires
RUN a2enconf remoteip

# Python dependencies
COPY background/requirements.txt /app/background/requirements.txt
RUN pip3 install --upgrade pip
RUN pip3 install -r /app/background/requirements.txt

# Web application
COPY . /app
COPY docker/settings.php /app/engine/settings.php

RUN rm /app/.gitignore \
/app/.dockerignore \
/app/.gitlab-ci.yml \
/app/ChangeLog \
/app/engine/schema/mysql.sql \
/app/vendors/simpletest/docs/en/authentication_documentation.html \
/app/documentation/info/manifest.xml \
/app/js/tests/ElggLibTest.js \
/app/Dockerfile

# Create data-folder
RUN mkdir /app-data && chown -R www-data:www-data /app-data

# Generate locales for Dutch and French
RUN locale-gen nl_NL fr_FR

WORKDIR /app
CMD ["/scripts/apache.sh"]
