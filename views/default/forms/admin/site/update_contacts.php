<?php
/**
 * @todo cleanup
 */
$form_body = "";

if ($site = elgg_get_site_entity()) {
	if (!($site instanceof ElggSite)) {
		throw new InstallationException(elgg_echo('InvalidParameterException:NonElggSite'));
	}
    $form_body .= "<div class='dashboard-contact'>";
    foreach (array('communication','technical', 'responsible') as $field) {
        $form_body .= "<div>";
        $form_body .= elgg_echo("<strong>" . elgg_echo('contacts:' . $field) . "</strong><br />");
        $warning = elgg_echo('installation:warning:' . $field);
        $name_field = $field . '_contact_name';
        $name_value = $site->$name_field;
        $form_body .= elgg_echo("<label for=" . $name_field . ">" . elgg_echo('contacts:label:name') . "</label>");
        $form_body .= elgg_view("input/text",array('name' => $name_field, 'value' => $name_value));
        $name_field = $field . '_contact_tel';
        $name_value = $site->$name_field;
        $form_body .= elgg_echo("<label for=" . $name_field . ">" . elgg_echo('contacts:label:email') . "</label>");
        $form_body .= elgg_view("input/text",array('name' => $name_field, 'value' => $name_value));
        $name_field = $field . '_contact_email';
        $name_value = $site->$name_field;
        $form_body .= elgg_echo("<label for=" . $name_field . ">" . elgg_echo('contacts:label:tel') . "</label>");
        $form_body .= elgg_view("input/text",array('name' => $name_field, 'value' => $name_value));
        $form_body .= "</div>";
    }
    $form_body .= "</div>";

    $form_body .= '<div class="elgg-foot">';
    $form_body .= elgg_view('input/submit', array('value' => elgg_echo("save")));
    $form_body .= '</div>';

    echo $form_body;

}