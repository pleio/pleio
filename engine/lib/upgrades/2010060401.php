<?php

/**
 * Get each user's notify* relationships and confirm that they have a friend
 * or member relationship depending on type. This fixes the notify relationships
 * that were not updated to due to #1837
 */

$count = 0;

$dblink = get_db_link("write");

$user_guids = mysqli_query($dblink, "SELECT guid FROM {$CONFIG->dbprefix}users_entity");
while ($user = mysqli_fetch_object($user_guids)) {

	$query = "SELECT * FROM {$CONFIG->dbprefix}entity_relationships
		WHERE guid_one=$user->guid AND relationship LIKE 'notify%'";
	$relationships = mysqli_query($dblink, $query);
	if (mysqli_num_rows($relationships) == 0) {
		// no notify relationships for this user
		continue;
	}

	while ($obj = mysqli_fetch_object($relationships)) {
		$query = "SELECT type FROM {$CONFIG->dbprefix}entities WHERE guid=$obj->guid_two";
		$results = mysqli_query($dblink, $query);
		if (mysqli_num_rows($results) == 0) {
			// entity doesn't exist - shouldn't be possible
			continue;
		}

		$entity = mysqli_fetch_object($results);

		switch ($entity->type) {
			case 'user':
				$relationship_type = 'friend';
				break;
			case 'group':
				$relationship_type = 'member';
				break;
		}

		if (isset($relationship_type)) {
				$query = "SELECT * FROM {$CONFIG->dbprefix}entity_relationships
							WHERE guid_one=$user->guid AND relationship='$relationship_type'
							AND guid_two=$obj->guid_two";
				$results = mysqli_query($dblink, $query);

			if (mysqli_num_rows($results) == 0) {
				$query = "DELETE FROM {$CONFIG->dbprefix}entity_relationships WHERE id=$obj->id";
				mysqli_query($dblink, $query);
				$count++;
			}
		}
	}

}

if (is_callable('error_log')) {
	error_log("Deleted $count notify relationships in upgrade");
}
