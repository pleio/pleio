<?php

$CONSOLE_HANDLERS = array();
function pleio_register_console_handler($command, $description, $function) {
    global $CONSOLE_HANDLERS;

    if (!array_key_exists($command, $CONSOLE_HANDLERS)) {
        $CONSOLE_HANDLERS[$command] = array(
            "description" => $description,
            "function" => $function
        );
    }
}

function pleio_console_start($argv = array()) {
    pleio_console_init_host();

    pleio_register_console_handler("clear:cache", "Clear the system cache and simple cache", "pleio_console_clear_cache");
    pleio_register_console_handler("cron", "Run a cronjob", "pleio_console_cron");
    pleio_register_console_handler("test", "Run the unit tests.", "pleio_console_tests");
    pleio_register_console_handler("help", "Show an overview of the options.", "pleio_console_help");

    if ($command = pleio_console_get_command($argv[1])) {
        if (function_exists($command["function"])) {
            return call_user_func($command["function"], array_slice($argv, 2));
        }
    }

    return pleio_console_help();
}

function pleio_console_init_host() {
    if (php_sapi_name() !== "cli") {
      throw new Exception("This script must be run from the CLI.");
    }

    require_once(dirname(__FILE__) . "/../settings.php");

    // retrieve the first site (main site) from the database and set headers to boot the main ELGG site
    $link = mysqli_connect($CONFIG->dbhost, $CONFIG->dbuser, $CONFIG->dbpass, $CONFIG->dbname);

    $sql = "SELECT url FROM {$CONFIG->dbprefix}sites_entity ORDER BY guid LIMIT 1";
    $result = mysqli_query($link, $sql);
    $url = mysqli_fetch_row($result);
    mysqli_close($link);

    preg_match("/(https?):\/\/(.*)\//", $url[0], $output);

    if ($output[1] == "https") {
        $_SERVER["HTTPS"] = true;
    }

    $_SERVER["HTTP_HOST"] = $output[2];

    // start the ELGGG engine
    require_once(dirname(__FILE__) . "/../start.php");
}

function pleio_console_get_command($command) {
    global $CONSOLE_HANDLERS;

    if (array_key_exists($command, $CONSOLE_HANDLERS)) {
        return $CONSOLE_HANDLERS[$command];
    }

    return false;
}

function pleio_console_help() {
    global $CONSOLE_HANDLERS;

    echo "Usage: php console.php [option] [args...]" . PHP_EOL . PHP_EOL;

    foreach ($CONSOLE_HANDLERS as $command => $value) {
        echo str_pad($command, 20) . $value["description"] . PHP_EOL;
    }
}

function pleio_console_cron($args) {
    $allowed_periods = array(
        "minute", "fiveminute", "fifteenmin", "halfhour", "hourly",
        "daily", "weekly", "monthly", "yearly", "reboot"
    );

    if (!in_array($args[0], $allowed_periods)) {
        //throw new Exception("Use a valid period (" . implode($allowed_periods, ", ") . ")");
    }

    elgg_set_context("cron");
    $page = array($args[0]);
    cron_page_handler($page);
}

function pleio_console_clear_cache() {
    $system_cache = elgg_get_system_cache();
    $system_cache->clear();

    elgg_invalidate_simplecache();
    echo "Caches cleared successfully!" . PHP_EOL;
}

function pleio_console_tests() {
    global $CONFIG;

    $user = get_user_by_username("admin");
    if (!$user) {
        throw new Exception("Could not find admin user.");
    }

    login($user);

    $vendor_path = "$CONFIG->path/vendors/simpletest";
    $test_path = "$CONFIG->path/engine/tests";

    require_once("$vendor_path/unit_tester.php");
    require_once("$vendor_path/mock_objects.php");
    require_once("$vendor_path/reporter.php");
    require_once("$test_path/elgg_unit_test.php");

    // turn off system log
    elgg_unregister_event_handler("all", "all", "system_log_listener");
    elgg_unregister_event_handler("log", "systemlog", "system_log_default_logger");

    // Disable maximum execution time.
    // Tests take a while...
    set_time_limit(0);

    $suite = new TestSuite("Elgg Core Unit Tests");

    // emit a hook to pull in all tests
    $test_files = elgg_trigger_plugin_hook("unit_test", "system", null, array());
    foreach ($test_files as $file) {
        $suite->addTestFile($file);
    }

    exit ($suite->Run(new TextReporter()) ? 0 : 1 );
}