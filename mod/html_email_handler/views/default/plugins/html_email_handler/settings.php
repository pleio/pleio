<?php 

	$plugin = $vars["entity"];
	
	$noyes_options = array(
		"no" => elgg_echo("option:no"),
		"yes" => elgg_echo("option:yes")
	);

	// present settings
	echo "<div>";
	echo elgg_echo("html_email_handler:settings:notifications:description");
	echo "</div>";
	
	echo "<div>";
	echo elgg_echo("html_email_handler:settings:notifications");
	echo "&nbsp;" . elgg_view("input/dropdown", array("name" => "params[notifications]", "options_values" => $noyes_options, "value" => $plugin->notifications));
	echo "<div class='elgg-subtext'>" . elgg_echo("html_email_handler:settings:notifications:subtext") . "</div>";
	echo "</div>";