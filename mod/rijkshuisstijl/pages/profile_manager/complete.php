<?php

$unfilled_fields = profile_manager_get_unfilled_mandatory_fields();

// forward user to index when all mandatory fields are filled
if (count($unfilled_fields) == 0) {
    if ($redirect_uri = get_input('redirect_uri')) {
        forward($redirect_uri);
    } else {
        forward('/');
    }
}

$content = "<div class=\"rhs-splash\">";
$content .= "<h1 class=\"rhs-splash__title\">" . elgg_echo('profile_manager:complete:title') . "</h1>";
$content .= "<p>" . elgg_echo('profile_manager:complete:description') . "</p>";
$content .= elgg_view_form('profile_manager/complete', array(), array(
    'fields' => $unfilled_fields
));
$content .= "</div>";

echo elgg_view_page($title, $content, "empty");
