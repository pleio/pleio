<?php
elgg_load_css("splash");
elgg_load_js("splash");

if (elgg_is_logged_in()) {
    forward("");
}

$title = elgg_echo("login");

echo elgg_view_page($title, $vars["body"], "empty", array(
    "header" => elgg_view("rijkshuisstijl/elements/header")
));

