<?php
$site = elgg_get_site_entity();
$description = elgg_get_plugin_setting("walled_garden_description", "pleio");
if (!$description) {
    $description = "<p>" . elgg_echo("pleio:walled_garden_description") . "</p>";
}
$footer = elgg_get_plugin_setting("walled_garden_footer", "pleio");
?>
<div class="rhs-splash">
    <h1 class="rhs-splash__title"><?php echo elgg_echo("rijkshuisstijl:welcome_to"); ?> <?php echo $site->name; ?></h1>
    <?php echo $description; ?>

    <?php echo elgg_view_form("login", [
        "class" => "js-validateForm"
    ]); ?>

    <div style="text-align: center; font-size: 14px; margin-top: 50px;">
        <div class="content">
            <?php echo $footer; ?>
        </div>
    </footer>
</div>
