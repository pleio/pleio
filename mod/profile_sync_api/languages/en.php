<?php

$english = [
    "profile_sync_api:settings:secret" => "Secret",
    "profile_sync_api:settings:secret:description" => "Configure this secret in the profile sync client to access the API.",
    "admin:administer_utilities:profile_sync_api" => "Profile sync logs",
    "admin:administer_utilities:profile_sync_api:no_logs" => "There are no logs to display.",
];

add_translation("en", $english);
