<?php

$dutch = [
    "profile_sync_api:settings:secret" => "Geheime sleutel",
    "profile_sync_api:settings:secret:description" => "Configureer deze geheime sleutel in de profile sync client om verbinding te maken met de API.",
    "admin:administer_utilities:profile_sync_api" => "Profile sync logs",
    "admin:administer_utilities:profile_sync_api:no_logs" => "Er zijn geen logs om weer te geven.",
];

add_translation("nl", $dutch);
