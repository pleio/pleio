<?php
require_once(dirname(__FILE__) . "/../../vendor/autoload.php");

spl_autoload_register("profile_sync_api_autoloader");
function profile_sync_api_autoloader($class) {
    $filename = "classes/" . str_replace("\\", "/", $class) . ".php";
    if (file_exists(dirname(__FILE__) . "/" . $filename)) {
        include($filename);
    }
}

function profile_sync_api_init() {
    elgg_register_page_handler("profile_sync_api", "profile_sync_api_page_handler");
    elgg_register_page_handler("avatar", "profile_sync_api_avatar_page_handler");

    elgg_register_plugin_hook_handler("public_pages", "walled_garden", "profile_sync_api_public_pages");
    elgg_register_plugin_hook_handler("entity:icon:url", "user", "profile_sync_api_profile_icon", 10000); // high prio to overrule theme_haarlem_intranet and pleio

    elgg_register_admin_menu_item("administer", "profile_sync_api", "administer_utilities");
}

elgg_register_event_handler("init", "system", "profile_sync_api_init");

function profile_sync_api_page_handler($url) {
    $app = new ProfileSyncApi\Application();
    $app->run();

    return true;
}

function profile_sync_api_avatar_page_handler($url) {
    include(dirname(__FILE__) . "/pages/avatar/user.php");
	return true;
}

function profile_sync_api_public_pages($hook, $type, $return_value, $params) {
    $return_value[] = "profile_sync_api/.*";
    return $return_value;
}

function profile_sync_api_profile_icon($hook, $type, $return_value, $params) {
	$user = elgg_extract("entity", $params);
	if (empty($user) || !($user instanceof ElggUser)) {
		return;
    }

	if (!$user->profile_sync_api_icontime) {
		return;
    }

	$size = elgg_extract('size', $params);
	$icon_sizes = elgg_get_config('icon_sizes');
	if (!isset($icon_sizes[$size])) {
		return;
	}

	$fh = new ElggFile();
	$fh->owner_guid = $user->guid;

	$fh->setFilename("profile_sync_api/{$user->guid}{$size}.jpg");
	if (!$fh->exists()) {
		return;
    }

	return "/avatar?guid={$user->guid}&size={$size}&lastcache={$user->profile_sync_api_icontime}";
}
