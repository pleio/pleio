<?php $logs = ProfileSyncApi\Helpers::getLogs(); ?>

<?php if (count($logs) > 0): ?>
    <?php foreach ($logs as $log): ?>
        <h3><?php echo $log->uuid; ?></h3>
        <?php echo elgg_view("output/friendlytime", ["time" => $log->time_created]); ?><br />
        <pre><?php echo elgg_view("output/text", ["value" => $log->content]); ?></pre>
        <br />
    <?php endforeach; ?>
<?php else: ?>
    <?php echo elgg_echo("admin:administer_utilities:profile_sync_api:no_logs"); ?>
<?php endif; ?>