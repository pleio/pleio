<?php
$plugin = elgg_extract("entity", $vars);

echo "<div>";
echo elgg_echo("profile_sync_api:settings:secret");
echo elgg_view("input/password", array(
	"name" => "params[secret]",
	"value" => $plugin->secret,
	"style" => "width: 100%"
));
echo "<div class='elgg-subtext'>" . elgg_echo("profile_sync_api:settings:secret:description") . "</div>";
echo "</div>";
