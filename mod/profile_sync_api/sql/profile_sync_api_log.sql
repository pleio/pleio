CREATE TABLE IF NOT EXISTS profile_sync_api_log (
    `uuid` VARCHAR(36) NOT NULL,
    `content` TEXT,
    `time_created` DATETIME DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`uuid`),
    KEY `time_created` (`time_created`)
);