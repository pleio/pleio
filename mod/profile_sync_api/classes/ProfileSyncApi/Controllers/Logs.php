<?php
namespace ProfileSyncApi\Controllers;

class Logs {

    /**
     * @SWG\Post(
     *     path="/profile_api/logs",
     *     security={{"oauth2": {"all"}}},
     *     tags={"users"},
     *     summary="Create a new log entry or update an existing one.",
     *     description="Create a new log entry or update an existing one.",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Succesful operation."
     *     )
     * )
     */
    public function create_or_update($request, $response, $args) {
        $site = elgg_get_site_entity();

        $json = $request->getBody();
        $data = json_decode($json, true);

        $uuid = sanitize_string($data["uuid"]);
        $content = sanitize_string($data["content"]);

        if (!$data["uuid"]) {
            $json = [
                "status" => "400",
                "error" => "could_not_create",
                "pretty_error" => "Could not create the log entry, uuid is missing."
            ];

            return $response->withStatus(400)
                            ->withHeader("Content-type", "application/json")
                            ->write(json_encode($json, JSON_PRETTY_PRINT));
        }

        insert_data("
            INSERT INTO profile_sync_api_log (uuid, content)
            VALUES ('{$uuid}', '{$content}\n')
            ON DUPLICATE KEY UPDATE content = CONCAT(content, '{$content}\n');
        ");

        $json = [
            "status" => 200,
            "log" => [
                "uuid" => $uuid
            ]
        ];


        return $response->withHeader("Content-type", "application/json")
                        ->write(json_encode($json, JSON_PRETTY_PRINT));
    }
}