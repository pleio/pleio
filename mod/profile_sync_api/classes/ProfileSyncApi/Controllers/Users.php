<?php
namespace ProfileSyncApi\Controllers;
use ModPleio\Helpers as ModPleioHelpers;
use ProfileSyncApi\Helpers;

class Users {

    /**
     * @SWG\Get(
     *     path="/profile_api/users",
     *     security={{"oauth2": {"all"}}},
     *     tags={"users"},
     *     summary="Get a list of all users.",
     *     description="Get a list of all users.",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Succesful operation."
     *     )
     * )
     */
    public function list($request, $response, $args) {
        $site = elgg_get_site_entity();
        $dbprefix = elgg_get_config("dbprefix");

        $params = $request->getQueryParams();
        $limit = (int) $params["limit"] ?: 100;
        $cursor = (int) $params["cursor"] ?: null;

        $options = [
            "type" => "user",
            "limit" => $limit,
            "order_by" => "e.guid"
        ];

        if ($cursor) {
            $options["wheres"] = "e.guid > {$cursor}";
        }

        $users = [];
        foreach (elgg_get_entities($options) as $user) {
            $users[] = $this->parseUser($user);
        }

        $json = [
            "users" => $users
        ];

        return $response->withHeader("Content-type", "application/json")
                        ->write(json_encode($json, JSON_PRETTY_PRINT));
    }

    /**
     * @SWG\Post(
     *     path="/profile_api/users",
     *     security={{"oauth2": {"all"}}},
     *     tags={"users"},
     *     summary="Create a new user or update an existing user.",
     *     description="Create a new user or update an existing user.",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Succesful operation."
     *     )
     * )
     */
    public function create_or_update($request, $response, $args) {
        $site = elgg_get_site_entity();

        $json = $request->getBody();
        $data = json_decode($json, true);

        $name = $data["name"];
        $email = Helpers::lowerAndTrim($data["email"]);
        $external_id = Helpers::lowerAndTrim($data["external_id"]);

        if ($data["guid"]) {
            $user = Helpers::getUserByGuid($data["guid"]);
        } else {
            try {
                $user = Helpers::createUser($name, $email, $external_id);
            } catch (\Exception $e) {
                $json = [
                    "status" => "400",
                    "error" => "could_not_create",
                    "pretty_error" => "Could not create the user with these attributes ({$e->getMessage()}).",
                    "user" => []
                ];

                return $response->withStatus(400)
                                ->withHeader("Content-type", "application/json")
                                ->write(json_encode($json, JSON_PRETTY_PRINT));
            }
        }

        if (!$user) {
            $json = [
                "status" => 400,
                "error" => "could_not_create",
                "pretty_error" => "Could not find the specified user.",
                "user" => []
            ];

            return $response->withStatus(400)
                            ->withHeader("Content-type", "application/json")
                            ->write(json_encode($json, JSON_PRETTY_PRINT));
        }

        if ($user->isBanned()) {
            if ($user->unban()) {
                add_entity_relationship($user->guid, "member_of_site", $site->guid);

                // refetch user as cache is not updated correctly
                $user = get_entity($user->guid);
            }
        }

        if (!$user->isEnabled()) {
            $user->enable();
        }

        if (isset($external_id) && $user->external_id != $external_id) {
            if (Helpers::getUserFromExternalId($external_id)) {
                $json = [
                    "status" => 400,
                    "error" => "could_not_update",
                    "pretty_error" => "Could not change the external_id to {$external_id} as the id is already taken.",
                    "user" => []
                ];

                return $response->withStatus(400)
                                ->withHeader("Content-type", "application/json")
                                ->write(json_encode($json, JSON_PRETTY_PRINT));
            } else {
                $user->external_id = $external_id;
            }
        }

        if (isset($name) && $user->name != $name) {
            $user->name = $data["name"];
        }

        if (isset($email) && $user->email != $email) {
            if (Helpers::getUserFromEmail($email)) {
                $json = [
                    "status" => 400,
                    "error" => "could_not_update",
                    "pretty_error" => "Could not change the email to {$email} as the email is already taken.",
                    "user" => []
                ];

                return $response->withStatus(400)
                                ->withHeader("Content-type", "application/json")
                                ->write(json_encode($json, JSON_PRETTY_PRINT));
            } else {
                $user->email = $data["email"];
            }
        }

        if (isset($data["profile"])) {
            foreach (Helpers::getProfileFields() as $fieldKey => $fieldType) {
                if (!isset($data["profile"][$fieldKey]) || !$data["profile"][$fieldKey]) {
                    continue;
                }

                switch ($fieldType) {
                    case "tags":
                        $options = [
                            'guid' => $user->guid,
                            'metadata_name' => $fieldKey,
                            'limit' => false
                        ];

                        elgg_delete_metadata($options);

                        $values = explode(",", $data["profile"][$fieldKey]);
                        foreach ($values as $i => $value) {
                            create_metadata(
                                $user->guid,
                                $fieldKey,
                                $value,
                                "text",
                                $user->guid,
                                get_default_access(),
                                ($i > 0) ? true : false
                            );
                        }
                        break;
                    default:
                        create_metadata(
                            $user->guid,
                            $fieldKey,
                            $data["profile"][$fieldKey],
                            "text",
                            $user->guid,
                            get_default_access()
                        );
                        break;
                }
            }
        }

        if (isset($data["groups"])) {
            $make_member_of = explode(",", $data["groups"]);

            $current_memberships = get_users_membership($user->guid);
            if ($current_memberships) {
                foreach ($current_memberships as $current_membership) {
                    $key = array_search($current_membership->guid, $make_member_of);
                    if ($key) {
                        unset($make_member_of[$key]);
                    }
                }
            }

            foreach ($make_member_of as $group_guid) {
                join_group($group_guid, $user->guid);
            }
        }

        $user->save();

        $json = [
            "status" => 200,
            "user" => $this->parseUser($user)
        ];

        return $response->withHeader("Content-type", "application/json")
                        ->write(json_encode($json, JSON_PRETTY_PRINT));
    }

    /**
     * @SWG\Delete(
     *     path="/profile_api/users/{guid}",
     *     security={{"oauth2": {"all"}}},
     *     tags={"users"},
     *     summary="Delete the user.",
     *     description="Delete the user.",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Succesful operation."
     *     )
     * )
     */
    public function delete($request, $response, $args) {
        $site = elgg_get_site_entity();

        $guid = (int) $args["guid"];
        $user = get_entity($guid);
        if (!$user || !$user instanceof \ElggUser) {
            $json = [
                "status" => 404,
                "error" => "not_found",
                "pretty_error" => "Could not find user with this guid."
            ];

            return $response->withStatus(404)
                            ->withHeader("Content-type", "application/json")
                            ->write(json_encode($json, JSON_PRETTY_PRINT));
        }

        ModPleioHelpers::removeUser($user);

        $json = [
            "status" => 200
        ];

        return $response->withStatus(200)
                        ->withHeader("Content-type", "application/json")
                        ->write(json_encode($json, JSON_PRETTY_PRINT));
    }

    /**
     * @SWG\Post(
     *     path="/profile_api/users/{guid}/ban",
     *     security={{"oauth2": {"all"}}},
     *     tags={"users"},
     *     summary="Ban an existing user.",
     *     description="Ban an existing user.",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Succesful operation."
     *     )
     * )
     */
    public function ban($request, $response, $args) {
        $site = elgg_get_site_entity();

        $guid = (int) $args["guid"];
        $user = get_entity($guid);
        if (!$user || !$user instanceof \ElggUser) {
            $json = [
                "status" => 404,
                "error" => "not_found",
                "pretty_error" => "Could not find user with this guid."
            ];

            return $response->withStatus(404)
                            ->withHeader("Content-type", "application/json")
                            ->write(json_encode($json, JSON_PRETTY_PRINT));
        }

        if ($user->ban('banned')) {
            remove_entity_relationship($user->guid, "member_of_site", $site->guid);
        }

        // refetch user as cache is not updated correctly
        $user = get_entity($guid);

        $json = [
            "status" => 200,
            "user" => $this->parseUser($user)
        ];

        return $response->withStatus(200)
                        ->withHeader("Content-type", "application/json")
                        ->write(json_encode($json, JSON_PRETTY_PRINT));
    }

    /**
     * @SWG\Post(
     *     path="/profile_api/users/{guid}/unban",
     *     security={{"oauth2": {"all"}}},
     *     tags={"users"},
     *     summary="Unban an existing user.",
     *     description="Unban an existing user.",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Succesful operation."
     *     )
     * )
     */
    public function unban($request, $response, $args) {
        $site = elgg_get_site_entity();
        $response = $response->withHeader("Content-type", "application/json");

        $guid = (int) $args["guid"];
        $user = get_entity($guid);
        if (!$user || !$user instanceof \ElggUser) {
            $json = [
                "status" => 404,
                "error" => "not_found",
                "pretty_error" => "Could not find user with this guid."
            ];

            return $response->withStatus(404)
                            ->withHeader("Content-type", "application/json")
                            ->write(json_encode($json, JSON_PRETTY_PRINT));
        }

        $he = access_show_hidden_entities(true);

        if ($user->unban()) {
            add_entity_relationship($user->guid, "member_of_site", $site->guid);
        }

        access_show_hidden_entities($he);

        // refetch user as cache is not updated correctly
        $user = get_entity($guid);

        $json = [
            "status" => 200,
            "user" => $this->parseUser($user)
        ];


        return $response->withStatus(200)
                        ->withHeader("Content-type", "application/json")
                        ->write(json_encode($json, JSON_PRETTY_PRINT));
    }

    /**
     * @SWG\Post(
     *     path="/profile_api/users/{guid}/avatar",
     *     security={{"oauth2": {"all"}}},
     *     tags={"users"},
     *     summary="Update the avatar of an existing user.",
     *     description="Update the avatar of an existing user.",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Succesful operation."
     *     )
     * )
     */
    public function avatar($request, $response, $args) {
        $site = elgg_get_site_entity();
        $response = $response->withHeader("Content-type", "application/json");

        $guid = (int) $args["guid"];
        $user = get_entity($guid);
        if (!$user || !$user instanceof \ElggUser) {
            $json = [
                "status" => 404,
                "error" => "not_found",
                "pretty_error" => "Could not find user with this guid."
            ];

            return $response->withStatus(404)
                            ->withHeader("Content-type", "application/json")
                            ->write(json_encode($json, JSON_PRETTY_PRINT));
        }

        if ($_FILES['avatar']['error'] != 0) {
            $json = [
                "status" => 400,
                "error" => "invalid_upload",
                "pretty_error" => "An error occured while processing the uploaded avatar."
            ];
            return $response->withStatus(400)
                            ->withHeader("Content-type", "application/json")
                            ->write(json_encode($json, JSON_PRETTY_PRINT));
        }

        $icon_sizes = elgg_get_config('icon_sizes');

        // get the images and save their file handlers into an array
        // so we can do clean up if one fails.
        $files = array();
        foreach ($icon_sizes as $size => $size_info) {
            $resized = get_resized_image_from_uploaded_file('avatar', $size_info['w'], $size_info['h'], $size_info['square'], $size_info['upscale']);

            if ($resized) {
                //@todo Make these actual entities.  See exts #348.
                $file = new \ElggFile();
                $file->owner_guid = $user->guid;
                $file->setFilename("profile_sync_api/{$guid}{$size}.jpg");
                $file->open('write');
                $file->write($resized);
                $file->close();
                $files[] = $file;
            } else {
                // cleanup on fail
                foreach ($files as $file) {
                    $file->delete();
                }
            }
        }

        $user->profile_sync_api_icontime = time();

        $json = [
            "status" => 200,
            "user" => $this->parseUser($user)
        ];

        return $response->withStatus(200)
                        ->withHeader("Content-type", "application/json")
                        ->write(json_encode($json, JSON_PRETTY_PRINT));
    }

    private function parseUser(\ElggUser $user) {
        static $site;

        if (!$site) {
            $site = elgg_get_site_entity();
        }

        $profile = [];

        foreach (Helpers::getProfileFields() as $fieldKey => $fieldType) {
            $value = $user->$fieldKey;
            $profile[$fieldKey] = is_array($value) ? implode(",", $value) : $value;
        }

        return [
            "guid" => $user->guid,
            "external_id" => $user->external_id ?: null,
            "name" => $user->name,
            "email" => $user->email,
            "is_member" => $this->isMemberOfSite($user),
            "is_banned" => $user->isBanned(),
            "time_created" => date("c", $user->time_created),
            "time_updated" => date("c", $user->time_updated),
            "icontime" => $user->profile_sync_api_icontime ?: null,
            "profile" => $profile
        ];
    }

    private function isMemberOfSite(\ElggUser $user) {
        static $member_guids;

        if (!isset($member_guids)) {
            $site = elgg_get_site_entity();
            $dbprefix = elgg_get_config("dbprefix");
            $query = "SELECT guid_one FROM {$dbprefix}entity_relationships WHERE relationship = 'member_of_site' AND guid_two = '{$site->guid}'";
            $data = get_data($query) ?: [];

            $member_guids = [];
            foreach ($data as $row) {
                $member_guids[$row->guid_one] = true;
            }
        }

        return array_key_exists($user->guid, $member_guids);
    }

    private function isBanned(\ElggUser $user) {
        static $member_guids;

        if (!isset($member_guids)) {
            $site = elgg_get_site_entity();
            $dbprefix = elgg_get_config("dbprefix");
            $query = "SELECT guid FROM {$dbprefix}users_entity WHERE is_banned = 'yes'";
            $data = get_data($query) ?: [];

            $member_guids = [];
            foreach ($data as $row) {
                $member_guids[$row->guid] = true;
            }
        }

        return array_key_exists($user->guid, $member_guids);
    }
}