<?php
namespace ProfileSyncApi;

class Helpers {
    public function getUserByGuid($guid) {
        $dbprefix = elgg_get_config("dbprefix");

        $entity = get_entity($guid);
        if ($entity && $entity instanceof \ElggUser) {
            return $entity;
        }
    }

    public function createUser($name, $email, $external_id) {
        $dbprefix = elgg_get_config("dbprefix");

        $user = Helpers::getUserFromEmail($email);
        if ($user) {
            throw new \Exception("This e-mail is already taken by another user.");
        }

        if ($external_id) {
            $user = Helpers::getUserFromExternalId($external_id);
            if ($user) {
                throw new \Exception("This external_id is already taken by another user.");
            }
        }

        $guid = register_user(
            Helpers::generateUsername($email),
            generate_random_cleartext_password(),
            $name,
            $email
        );

        if ($guid) {
            $user = get_user($guid);

            if ($external_id) {
                $user->external_id = $external_id;
                $user->save();
            }

            return $user;
        }
    }

    public function lowerAndTrim($input) {
        if ($input) {
            return strtolower(trim($input));
        }
    }

    public function getUserFromEmail($email) {
        $dbprefix = elgg_get_config("dbprefix");

        $email = sanitise_string($email);

        $result = get_data_row("SELECT guid FROM {$dbprefix}users_entity WHERE email = '{$email}'");
        if ($result) {
            return get_entity($result->guid);
        }
    }

    public function getUserFromExternalId($external_id) {
        $dbprefix = elgg_get_config("dbprefix");

        $external_id = sanitise_string($external_id);

        $msn_id = get_metastring_id("external_id");
        $msv_id = get_metastring_id($external_id);

        if ($msn_id && $msv_id) {
            $result = get_data_row("
                SELECT guid FROM {$dbprefix}users_entity ue
                JOIN {$dbprefix}metadata md
                WHERE md.name_id = {$msn_id}
                AND md.value_id = {$msv_id}
                AND md.entity_guid = ue.guid
            ");
            if ($result) {
                return get_entity($result->guid);
            }
        }
    }

    public function getProfileFields() {
        $profileFields = elgg_get_config("profile_fields");

        if (isset($profileFields["custom_profile_type"])) {
            unset($profileFields["custom_profile_type"]);
        }

        return $profileFields;
    }

    public function getLogs($offset = 0, $limit = 10) {
        $offset = (int) $offset;
        $limit = (int) $limit;

        return get_data("
            SELECT uuid, UNIX_TIMESTAMP(time_created) AS time_created, content
            FROM profile_sync_api_log
            ORDER BY time_created DESC LIMIT {$offset},{$limit}
        ");
    }

    private function generateUsername($email) {
        $emailParts = explode("@", $email);
        $username = preg_replace("/[^a-zA-Z0-9]+/", "", $emailParts[0]);

        $hidden = access_show_hidden_entities(true);

        while (strlen($username) < 4) {
            $username .= "0";
        }

        if (get_user_by_username($username)) {
            $i = 1;

            while (get_user_by_username($username . $i)) {
                $i++;
            }

            $result = $username . $i;
        } else {
            $result = $username;
        }

        access_show_hidden_entities($hidden);
        return $result;
    }
}
