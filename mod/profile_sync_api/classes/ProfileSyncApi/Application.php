<?php
namespace ProfileSyncApi;

/**
 * @SWG\Swagger(
 *   schemes={"https"},
 *   host="www.pleio.nl"
 * )
 * @SWG\Info(
 *   version="1.0.0",
 *   title="Pleio profile sync API",
 *   description="This document describes the Pleio profile sync API.",
 *   termsOfService="https://www.pleio.nl",
 *   @SWG\Contact(
 *     email="support@pleio.nl"
 *   ),
 * )
 * @SWG\Tag(
 *   name="users",
 *   description="Retrieve a list of users."
 * )
 */

use \ProfileSyncApi\AuthenticationMiddleware;

class Application {
    public function run() {
        $c = new \Slim\Container();
        $c['notFoundHandler'] = function($c) {
            return function ($request, $response) use ($c) {
                return $c['response']->withStatus(404)
                                     ->withHeader('Content-type', 'application/json')
                                     ->write(json_encode(array(
                                        'status' => 404,
                                        'error' => 'not_found',
                                        'pretty_error' => 'Could not find the specified endpoint.'
                                    ), JSON_PRETTY_PRINT));
            };
        };

        $app = new \Slim\App($c);

        $app->add(new AuthenticationMiddleware());

        $app->get('/profile_sync_api/users', 'ProfileSyncApi\Controllers\Users:list');
        $app->post('/profile_sync_api/users', 'ProfileSyncApi\Controllers\Users:create_or_update');
        $app->delete('/profile_sync_api/users/{guid}', 'ProfileSyncApi\Controllers\Users:delete');
        $app->post('/profile_sync_api/users/{guid}/ban', 'ProfileSyncApi\Controllers\Users:ban');
        $app->post('/profile_sync_api/users/{guid}/unban', 'ProfileSyncApi\Controllers\Users:unban');
        $app->post('/profile_sync_api/users/{guid}/avatar', 'ProfileSyncApi\Controllers\Users:avatar');

        $app->post('/profile_sync_api/logs', 'ProfileSyncApi\Controllers\Logs:create_or_update');
        $app->run();
    }
}
