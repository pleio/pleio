<?php
namespace ProfileSyncApi;

class AuthenticationMiddleware {
    private function getToken($request) {
        $header = $request->getHeaderLine("Authorization");
        if (preg_match("/Bearer\s+(.*)$/i", $header, $matches)) {
            return $matches[1];
        }

        return null;
    }

    public function __invoke($request, $response, $next) {
        $valid_token = elgg_get_plugin_setting("secret", "profile_sync_api");
        $token_in_request = $this->getToken($request);

        if (!$valid_token || !$token_in_request || ($token_in_request !== $valid_token)) {
            $response = $response->withStatus(403);
            $response = $response->withHeader('Content-type', 'application/json');
            return $response->write(json_encode(array(
                'status' => 403,
                'error' => 'invalid_bearer_token',
                'pretty_error' => 'You did not supply a valid bearer token.'
            ), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
        }

        $ia = elgg_set_ignore_access(true);
        $response = $next($request, $response);
        elgg_set_ignore_access($ia);

        return $response;
    }
}