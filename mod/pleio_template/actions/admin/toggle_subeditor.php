<?php
$site = elgg_get_site_entity();

$user = get_entity(get_input("user_guid"));
if (!$user) {
    forward(REFERER);
}

if (check_entity_relationship($user->guid, "is_subeditor", $site->guid)) {
    remove_entity_relationship($user->guid, "is_subeditor", $site->guid);
    system_message(elgg_echo("pleio_template:toggle_subeditor:revoked"));
} else {
    add_entity_relationship($user->guid, "is_subeditor", $site->guid);
    system_message(elgg_echo("pleio_template:toggle_subeditor:granted"));
}