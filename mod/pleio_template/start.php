<?php
require_once(dirname(__FILE__) . "/lib/functions.php");
require_once(dirname(__FILE__) . "/lib/events.php");
require_once(dirname(__FILE__) . "/lib/hooks.php");

require_once(dirname(__FILE__) . "/../../vendor/autoload.php");
spl_autoload_register("pleio_template_autoloader");
function pleio_template_autoloader($class) {
    $filename = "classes/" . str_replace("\\", "/", $class) . ".php";
    if (file_exists(dirname(__FILE__) . "/" . $filename)) {
        include($filename);
    }
}

$CONFIG->pleio_subtypes = ["news", "blog", "question", "event", "wiki"];

function pleio_template_init() {
    switch (get_current_language()) {
        case "nl":
            setlocale(LC_TIME, "nl_NL");
            break;
        
        default:
            setlocale(LC_TIME, "en_GB");
    }

    elgg_register_action("odt_editor/upload", dirname(__FILE__) . "/actions/odt_editor/upload.php");
    elgg_register_action("odt_editor/upload_asnew", dirname(__FILE__) . "/actions/odt_editor/upload_asnew.php");

    elgg_register_action("admin/enable_email_notifications", dirname(__FILE__) . "/actions/admin/enable_email_notifications.php", "admin");
    elgg_register_action("admin/disable_email_notifications", dirname(__FILE__) . "/actions/admin/disable_email_notifications.php", "admin");

    elgg_register_action("admin/enable_email_overviews", dirname(__FILE__) . "/actions/admin/enable_email_overviews.php", "admin");
    elgg_register_action("admin/disable_email_overviews", dirname(__FILE__) . "/actions/admin/disable_email_overviews.php", "admin");

    elgg_register_action("admin/enable_auto_notifications", dirname(__FILE__) . "/actions/admin/enable_auto_notifications.php", "admin");
    elgg_register_action("admin/disable_auto_notifications", dirname(__FILE__) . "/actions/admin/disable_auto_notifications.php", "admin");

    elgg_register_action("admin/subscribe_users_to_auto_notification", dirname(__FILE__) . "/actions/admin/subscribe_users_to_auto_notification.php", "admin");
    elgg_register_action("admin/unsubscribe_users_to_auto_notification", dirname(__FILE__) . "/actions/admin/unsubscribe_users_to_auto_notification.php", "admin");

    elgg_register_action("admin/toggle_subeditor", dirname(__FILE__) . "/actions/admin/toggle_subeditor.php", "admin");

    elgg_register_plugin_hook_handler("index", "system", "pleio_template_index_hook");

    elgg_register_plugin_hook_handler("action", "plugins/settings/save", "pleio_template_plugins_settings_hook");
    elgg_register_plugin_hook_handler("permissions_check", "all", "pleio_template_permissions_check_hook");

    elgg_unregister_plugin_hook_handler("container_permissions_check", "all", "news_container_permissions_check");
    elgg_register_plugin_hook_handler("container_permissions_check", "object", "pleio_template_container_permissions_check_hook");

    elgg_register_plugin_hook_handler("register", "menu:user_hover", "pleio_template_user_hover_hook");
    elgg_register_plugin_hook_handler("register", "menu:user_hover", "pleio_template_user_personal_files");

    elgg_register_event_handler("create", "object", "pleio_template_create_object_handler");
    elgg_register_event_handler("create", "member_of_site", "pleio_template_create_member_of_site_handler");
    elgg_register_event_handler("join", "group", "pleio_template_join_group_handler");
    elgg_register_event_handler("leave", "group", "pleio_template_leave_group_handler");

    elgg_register_page_handler("activity", "pleio_template_page_handler");
    elgg_register_page_handler("blog", "pleio_template_page_handler");
    elgg_register_page_handler("news", "pleio_template_page_handler");
    elgg_register_page_handler("questions", "pleio_template_page_handler");
    elgg_register_page_handler("discussion", "pleio_template_page_handler");
    elgg_register_page_handler("admin2", "pleio_template_page_handler");
    elgg_register_page_handler("profile", "pleio_template_page_handler");
    elgg_register_page_handler("groups", "pleio_template_page_handler");
    elgg_register_page_handler("cms", "pleio_template_page_handler");
    elgg_register_page_handler("wiki", "pleio_template_page_handler");
    elgg_register_page_handler("members", "pleio_template_page_handler");
    elgg_register_page_handler("pages", "pleio_template_page_handler");
    elgg_register_page_handler("search", "pleio_template_page_handler");
    elgg_register_page_handler("saved", "pleio_template_page_handler");
    elgg_register_page_handler("trending", "pleio_template_page_handler");
    elgg_register_page_handler("events", "pleio_template_events_handler");
    elgg_register_page_handler("polls", "pleio_template_page_handler");
    elgg_register_page_handler("files", "pleio_template_page_handler");
    elgg_register_page_handler("file", "pleio_template_file_page_handler");
    elgg_register_page_handler("user", "pleio_template_page_handler");
    elgg_register_page_handler("users", "pleio_template_page_handler");

	elgg_register_page_handler("profile_manager", "pleio_template_profile_manager_handler");

    if (!elgg_is_active_plugin("pleio")) {
        elgg_register_page_handler("login", "pleio_template_page_handler");
        elgg_register_page_handler("register", "pleio_template_page_handler");
        elgg_register_page_handler("forgotpassword", "pleio_template_page_handler");
        elgg_register_page_handler("resetpassword", "pleio_template_page_handler");
    }

    elgg_register_page_handler("graphql", "pleio_template_graphql");
    elgg_register_page_handler("upload", "pleio_template_upload");

    elgg_register_page_handler("bulk_download", "pleio_template_bulk_download");

    elgg_register_page_handler("exporting", "pleio_template_export_handler");

    elgg_register_page_handler("sitemap.xml", "pleio_template_sitemap_page_handler");

    elgg_unregister_plugin_hook_handler("register", "user", "newsletter_register_user_handler");
    elgg_unregister_event_handler("create", "member_of_site", "newsletter_join_site_event_handler");

    elgg_unregister_plugin_hook_handler("entity:icon:url", "user", "profile_override_avatar_url");
    elgg_register_plugin_hook_handler("entity:icon:url", "user", "pleio_template_user_icon_url");

    elgg_unregister_plugin_hook_handler("route", "groups", "group_tools_route_groups_handler");

    elgg_register_plugin_hook_handler("cron", "hourly", "pleio_template_cron_notifications_handler");
    elgg_register_plugin_hook_handler("cron", "daily", "pleio_template_cron_email_overview_handler");
    elgg_register_plugin_hook_handler("cron", "weekly", "pleio_template_cron_email_overview_handler");
    elgg_register_plugin_hook_handler("cron", "monthly", "pleio_template_cron_email_overview_handler");

    elgg_unregister_plugin_hook_handler("email", "system", "html_email_handler_email_hook");
    elgg_register_plugin_hook_handler("email", "system", "pleio_template_email_handler");

    elgg_register_plugin_hook_handler("object:notifications", "object", "pleio_template_object_notifications_handler");

    elgg_extend_view("css/admin", "pleio_template/css/admin");

    elgg_register_simplecache_view("css/legacy");
    elgg_register_simplecache_view("css/web");
    elgg_register_simplecache_view("css/custom");
    elgg_register_simplecache_view("js/custom");

    elgg_register_admin_menu_item("administer", "notifications", "administer_utilities");

    if (!isset($_COOKIE["CSRF_TOKEN"])) {
        $token = md5(openssl_random_pseudo_bytes(32));
        $domain = ini_get("session.cookie_domain");
        setcookie("CSRF_TOKEN", $token, 0, "/; samesite=lax", $domain, _elgg_on_https());
    }

    if (function_exists('pleio_register_console_handler')) {
        pleio_register_console_handler('runscript', 'Run pleio_template console script', 'pleio_template_runscript');
    }

    elgg_register_entity_url_handler('user', 'all', 'pleio_template_profile_url');
}

elgg_register_event_handler("init", "system", "pleio_template_init");

function pleio_template_index_hook($hook, $type, $return_value, $params) {
    if ($return_value) {
        return;
    }

    include("pages/react.php");
    return true;
}

function pleio_template_user_icon_url($hook, $type, $return_value, $params) {
    // if someone already set this, quit
    if ($return_value) {
        return null;
    }

    $user = $params['entity'];
    $size = $params['size'];

    if (!elgg_instanceof($user, 'user')) {
        return null;
    }

    $user_guid = $user->getGUID();
    $icon_time = $user->icontime;

    if (!$icon_time) {
        return pleio_template_assets("images/user.png");
    }

    if ($user->isBanned()) {
        return null;
    }

    $filehandler = new ElggFile();
    $filehandler->owner_guid = $user_guid;
    $filehandler->setFilename("profile/{$user_guid}{$size}.jpg");

    try {
        if ($filehandler->exists()) {
            $join_date = $user->getTimeCreated();
            return "mod/profile/icondirect.php?lastcache=$icon_time&joindate=$join_date&guid=$user_guid&size=$size";
        }
    } catch (InvalidParameterException $e) {
        return pleio_template_assets("images/user.png");
    }

    return null;
}

function pleio_template_events_handler($page) {
    // rewrite some old event URL's
    switch ($page[0]) {
        case "event":
            forward("/events/view/{$page[2]}/{$page[3]}");
            return true;
        default:
            return pleio_template_page_handler($page);
    }
}

function pleio_template_page_handler($page) {
    set_input("page", $page);
    include("pages/react.php");
    return true;
}

function pleio_template_profile_manager_handler($page) {
    switch ($page[0]) {
        case "complete":
            include("pages/profile_manager/complete.php");
            return true;
        default:
            return profile_manager_page_handler($page);
    }
}

function pleio_template_file_page_handler($page) {
	if (!isset($page[0])) {
		$page[0] = "all";
	}

	$file_dir = elgg_get_plugins_path() . "file/pages/file";

	$page_type = $page[0];
	switch ($page_type) {
		case "owner":
		case "friends":
		case "add":
		case "edit":
		case "search":
		case "all":
            forward("/");
            break;

        case "view":
            $file = get_entity($page[1]);
            if ($file) {
                $container = $file->getContainerEntity();
                if ($container instanceof ElggGroup) {
                    forward(Pleio\Helpers::getURL($file));
                } else {
                    set_input("guid", $page[1]);
                    include "$file_dir/download.php";
                    break;
                }
            } else {
                forward("/");
            }
			break;
		case "group":
			break;
		case "download":
			set_input("guid", $page[1]);
			include "$file_dir/download.php";
			break;
		default:
			return false;
	}
	return true;
}

function pleio_template_export_handler($page) {
    switch ($page[0]) {
        case "group":
            set_input("group_guid", $page[1]);
            include("pages/exporting/group.php");
            return true;
        case "event":
            set_input("event_guid", $page[1]);
            include("pages/exporting/event.php");
            return true;
        case "calendar":
            include("pages/exporting/calendar.php");
            return true;
        default:
            return true;
    }
}

function pleio_template_graphql($page) {
    include("pages/graphql.php");
    return true;
}

function pleio_template_bulk_download($page) {
    include("pages/bulk_download.php");
    return true;
}

function pleio_template_assets($path) {
    return "/mod/pleio_template/assets/" . $path;
}

function webpack_dev_server_is_available() {
    global $CONFIG;

    if ($CONFIG->env == "prod") {
        return false;
    }

    $host = getenv('FRONTEND_DEV_HOST') ? getenv('FRONTEND_DEV_HOST') : "127.0.0.1";

    $fp = @fsockopen($host, "9001", $errno, $errstr, 0.25);

    if (is_resource($fp)) {
        fclose($fp);
        return true;
    } else {
        return false;
    }
}

function pleio_filetime($file) {
    $file = dirname(__FILE__) . "/" . $file;

    if (!file_exists($file)) {
        return time();
    }

    $filectime = filemtime($file);
    if ($filectime) {
        return $filectime;
    }

    return time();
}

function pleio_template_get_object($guid) {
    $object = get_entity($guid);

    if (!$object) {
        // trigger mod/rewrite hook
        elgg_trigger_plugin_hook('forward', '404', ['current_url' => $_SERVER['REQUEST_URI']]);

        return false;
    }

    if (!$object instanceof ElggObject) {
        return false;
    }

    if (!in_array($object->getSubtype(), ["blog", "question", "news"])) {
        return false;
    }

    return $object;
}

function pleio_template_cron_notifications_handler($hook, $period, $return, $params) {
    $ia = elgg_set_ignore_access(true);
    Pleio\NotificationsHandler::sendToAll();
    elgg_set_ignore_access($ia);
}

function pleio_template_cron_email_overview_handler($hook, $period, $return, $params) {
    $ia = elgg_set_ignore_access(true);
    Pleio\EmailOverviewHandler::sendToAll($period);
    elgg_set_ignore_access($ia);
}

function pleio_template_object_notifications_handler($hook, $type, $return, $params) {
    // always return to to make sure object notifications are not sent by the Elgg engine
    return true;
}

function pleio_template_email_handler($hook, $type, $return, $params) {
    global $CONFIG;
    $site = elgg_get_site_entity();

    $message_id = sprintf("<%s.%s@%s>", base_convert(microtime(), 10, 36), base_convert(bin2hex(openssl_random_pseudo_bytes(8)), 16, 36), get_site_domain($site->guid));

    $from = "=?UTF-8?B?" . base64_encode($site->name) . "?= ";

    if ($site->email && strtolower(substr($site->email, -9) == '@pleio.nl')) {
        $from .= "<" . $site->email . ">";
    } elseif (isset($CONFIG->email_from)) {
        $from .= "<{$CONFIG->email_from}>";
    } else {
        $from .= "<noreply@" . get_site_domain($site->guid) . ">";
    }

    // custom reply parameter is set
    if (is_array($params["params"]) && isset($params["params"]["reply_to"]) && strtolower(substr($params["params"]["reply_to"], -9) == '@pleio.nl')) {
        $reply_to = $params["params"]["reply_to"];
    } else {
        $reply_to = $from;
    }

    $headers = "Sender: {$from}\r\n"
        . "From: {$from}\r\n"
        . "Reply-To: {$reply_to}\r\n"
        . "Return-Path: {$reply_to}\r\n"
        . "Message-Id: {$message_id}\r\n"
        . "MIME-Version: 1.0\r\n"
        . "Content-Type: text/html; charset=UTF-8\r\n";

    // Sanitise subject by stripping line endings
    $subject = preg_replace("/(\r\n|\r|\n)/", " ", $params["subject"]);

    $body = $params["body"];
    $body = nl2br($body);

    $email_params = [
        "subject" => $subject,
        "body" => $body
    ];

    if (!is_array($params["params"])) {
        $params["params"] = [];
    }

	if (!isset($CONFIG->block_mail)) {
        return mail(
            $params["to"],
            $subject,
            elgg_view("emails/default", array_merge($email_params, $params["params"])),
            $headers
        );
	} else {
		return true;
	}
}

function pleio_template_format_date($datetime, $type = "default") {
    switch ($type) {
        case "event":
            return strftime('%e %B %Y', $datetime);
        default:
            return date("j-n-Y", $datetime);
    }
}

function pleio_template_sitemap_page_handler($page) {
    include(dirname(__FILE__) . "/pages/sitemap.php");
    return true;
}
