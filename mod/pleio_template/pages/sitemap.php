<?php
use Pleio\Helpers;
use Pleio\Resolver;

$indexingEnabled = elgg_get_config("enable_frontpage_indexing");
if(!$indexingEnabled) {
    http_response_code(404);
    exit();
}

header('Content-type: text/xml');

$base = elgg_get_site_url();
$xml = new SimpleXMLElement(
    '<?xml version="1.0" encoding="UTF-8" ?>' . PHP_EOL .
    '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" />'
);

// Start page
$url = $xml->addChild('url');
$url->addChild('loc', $base);
$url->addChild('lastmod', date("Y-m-d", time()));
$url->addChild('priority', '0.5');


// Entity pages of blogs and static pages
$batch = new ElggBatch('elgg_get_entities', array(
    'type' => 'object',
    'subtypes' => array('blog', 'page', 'wiki'),
    'limit' => 2000
));

foreach ($batch as $object) {
    $url = $xml->addChild('url');
    $url->addChild('loc', Helpers::getURL($object, true));
    $url->addChild('lastmod', date("Y-m-d", $object->time_updated));
    $url->addChild('priority', '0.5');
}

echo $xml->asXML();
