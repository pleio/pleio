<?php
$event_guid = (int) get_input("event_guid");

$event = get_entity($event_guid);
if (!$event || !$event instanceof ElggObject || !$event->getSubtype() === "event") {
    exit();
}

if (!$event->canEdit()) {
    exit();
}

if (elgg_get_plugin_setting("event_export", "pleio_template") !== "yes") {
    exit();
}

$relationship_types = [
    "event_attending",
    "event_maybe",
    "event_reject"
];

$relationships = get_entity_relationships($event->guid);

$is_admin = elgg_is_admin_logged_in();
$slug = pleio_template_slugify($event->title);

header("Content-Type: text/csv");
header("Content-Disposition: attachment; filename=\"{$slug}.csv\"");
$fp = fopen("php://output", "w");

$headers = [
    "guid",
	"name",
	"username",
	"email (only for admins)",
    "status",
    'datetime'
];

fputcsv($fp, $headers, ";");

foreach ($relationship_types as $relationship_type) {
    $users = elgg_get_entities_from_relationship([
        "relationship" => "$relationship_type",
        "relationship_guid" => $event->guid,
        "limit" => false
    ]);

    if (!$users) {
        continue;
    }

    foreach ($users as $user) {
        $time_created = get_time_created($relationships, $event->guid, $relationship_type, $user->guid);
        $date_formatted = date("d-m-Y H:i", $time_created);
        fputcsv($fp, [
            $user->guid,
            $user->name,
            $user->username,
            $is_admin ? $user->email : "",
            str_replace("event_", "", $relationship_type),
            $date_formatted
        ], ";");
    }
}

fclose($fp);

function get_time_created($relationships, $event_id, $relationship_type, $user_id) {
    foreach($relationships as $r) {
        if ($r->guid_one == $event_id && $r->relationship == $relationship_type && $r->guid_two == $user_id ) {
            return $r->time_created;
        }
    }
    return "";
}