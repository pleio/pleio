<?php
use GraphQL\Server\StandardServer;
use GraphQL\Upload\UploadMiddleware;
use Zend\Diactoros\ServerRequestFactory;
use GraphQL\Error\FormattedError;
use GraphQL\Error\Debug;

FormattedError::setInternalErrorMessage("unknown_error");

if ($_SERVER['REQUEST_METHOD'] !== "POST") {
    header('Content-Type: application/json');
    echo json_encode(["error" => "This endpoint only accepts POST requests."]);
    exit();
}

if (isset($_SERVER['HTTP_X_CSRF_TOKEN']) && $_SERVER['HTTP_X_CSRF_TOKEN'] !== $_COOKIE['CSRF_TOKEN']) {
    header('Content-Type: application/json');
    echo json_encode(["error" => "CSRF Token is invalid."]);
    exit();
}

try {
    $request = Zend\Diactoros\ServerRequestFactory::fromGlobals();

    if (isset($_SERVER['CONTENT_TYPE']) && $_SERVER['CONTENT_TYPE'] === 'application/json') {
        $rawBody = file_get_contents('php://input');
        $data = json_decode($rawBody ?: '', true);
    } else {
        $data = $_POST;
    }

    $request = $request->withParsedBody($data);

    // Process uploaded files
    $uploadMiddleware = new UploadMiddleware();
    $request = $uploadMiddleware->processRequest($request);

    $schema = Pleio\SchemaBuilder::build();

    $context = [
        "user" => elgg_get_logged_in_user_entity()
    ];

    $debug = $CONFIG->env !== 'prod' ? Debug::INCLUDE_DEBUG_MESSAGE | Debug::INCLUDE_TRACE : false;

    $server = new StandardServer([
        "schema" => $schema,
        "context" => $context,
        "debug" => $debug
    ]);

    $result = $server->executePsrRequest($request);

    global $START_MICROTIME, $DB_TIME, $MEMCACHE_TIME, $dbcalls;
    $totaltime = microtime(true) - $START_MICROTIME;
    $scripttime = $totaltime - $DB_TIME - $MEMCACHE_TIME;

    header("pleio-script-time: {$scripttime}");
    header("pleio-db-time: {$DB_TIME}");
    header("pleio-memcache-time: {$MEMCACHE_TIME}");
    header("pleio-db-queries: {$dbcalls}");

    $server->getHelper()->sendResponse($result);

} catch (\Exception $e) {
    StandardServer::send500Error($e);
    error_log($e);
}