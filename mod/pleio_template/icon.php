<?php
require_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");

$site = elgg_get_site_entity();

// If is the same ETag, content didn't changed.
$etag = $site->icontime . $site->guid;
if (isset($_SERVER['HTTP_IF_NONE_MATCH']) && trim($_SERVER['HTTP_IF_NONE_MATCH']) == "\"$etag\"") {
    header("HTTP/1.1 304 Not Modified");
    exit;
}

$success = false;

$filehandler = new ElggFile();
$filehandler->owner_guid = $site->guid;
$filehandler->setFilename("pleio_template/" . $site->guid . "_icon." . ($site->icon_extension ? $site->icon_extension : "jpg"));

$success = false;
if ($filehandler->open("read")) {
    if ($contents = $filehandler->read($filehandler->size())) {
        $success = true;
    }
}

if (!$success) {
    header("Location: " . pleio_template_assets("images/icon.svg"));
    exit();
}

switch($site->icon_extension) {
    case "svg":
        $mimetype = "image/svg+xml";
        break;
    case "png":
        $mimetype = "image/png";
        break;
    default:
        $mimetype = "image/jpeg";
}

header("Content-type: " . $mimetype);
header('Expires: ' . gmdate('D, d M Y H:i:s \G\M\T', strtotime("+6 months")), true);
header("Pragma: public");
header("Cache-Control: public");
header("Content-Length: " . strlen($contents));
echo $contents;
