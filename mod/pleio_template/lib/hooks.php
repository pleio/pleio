<?php
function pleio_template_plugins_settings_hook($hook, $type, $return_value, $params) {
    $site = elgg_get_site_entity();
    $plugin_id = get_input("plugin_id");

    if ($plugin_id !== "pleio_template") {
        return $return_value;
    }

    $tagCategory = get_input("tagCategory", []);
    $tagsByCategory = get_input("tagsByCategory", []);

    $menu = get_input("menu", []);

    $directLinksTitle = get_input("directLinksTitle", []);
    $directLinksLink = get_input("directLinksLink", []);

    $profileKey = get_input("profileKey", []);
    $profileName = get_input("profileName", []);
    $profileIsFilter = get_input("isFilter", []);

    $directLinks = [];
    foreach ($directLinksLink as $i => $link) {
        $directLinks[] = [
            "title" => $directLinksTitle[$i],
            "link" => $directLinksLink[$i]
        ];
    }

    $profile = [];
    foreach ($profileKey as $i => $key) {
        if (in_array($profileKey[$i], ["guid", "type", "subtype", "owner_guid", "site_guid", "container_guid", "access_id", "time_created", "time_updated", "last_action", "enabled", "name", "username", "password", "salt", "password_hash", "email", "language", "code", "banned", "admin", "last_action", "prev_last_action", "last_login", "prev_last_login"])) {
            continue;
        }

        if (preg_match("/^a-z/", $profileKey[$i])) {
            continue;
        }

        $profile[] = [
            "key" => $profileKey[$i],
            "name" => $profileName[$i],
            "isFilter" => $profileIsFilter[$i]
        ];
    }

    $tagCategories = [];
    foreach ($tagCategory as $i => $name) {
        if (!$tagCategory || !$tagsByCategory[$i]) {
            continue;
        }

        $values = [];

        foreach($tagsByCategory[$i] as $value) {
            if (!$value) {
                continue;
            }

            $values[] = $value;
        }

        $tagCategories[] = [
            "name" => $name,
            "values" => $values
        ];
    }

    $footerTitle = get_input("footerTitle", []);
    $footerLink = get_input("footerLink", []);

    $footer = [];
    foreach ($footerLink as $i => $link) {
        $footer[] = [
            "title" => $footerTitle[$i],
            "link" => $footerLink[$i]
        ];
    }

    $params = get_input("params");
    $params["menu"] = json_encode($menu);
    $params["directLinks"] = json_encode($directLinks);
    $params["profile"] = json_encode($profile);
    $params["tagCategories"] = json_encode($tagCategories);
    $params["footer"] = json_encode($footer);
    set_input("params", $params);

    pleio_template_process_uploaded_file($_FILES["logo"], "logo", get_input("remove_logo"));
    pleio_template_process_uploaded_file($_FILES["icon"], "icon", get_input("remove_icon"));
}

function pleio_template_permissions_check_hook($hook_name, $entity_type, $return_value, $parameters) {
    $user = $parameters['user'];
    $entity = $parameters['entity'];

    if (!$user | !$entity) {
        return $return_value;
    }

    if (!$entity instanceof ElggObject) {
        return $return_value;
    }

    $subtype = $entity->getSubtype();

    switch ($subtype) {
        case "news":
        case "page":
        case "row":
            return ($user->isAdmin() || pleio_template_is_subeditor($user));
        case "file":
        case "folder":
        case "page_widget":
            return pleio_template_extend_write_array($return_value, $entity, $user);
        case "wiki":
            return pleio_template_wiki_permissions($entity, $user);
        default:
            return $return_value;
    }
}

function pleio_template_container_permissions_check_hook($hook, $type, $return_value, $params) {
    $user = elgg_extract("user", $params);
    $container = elgg_extract("container", $params);
    $subtype = elgg_extract("subtype", $params);

    if (!$user) {
        return $return_value;
    }

    // Somehow this function get's called 2 times. The first time the container is the same
    // as the user. The second time the container is the real container.
    // http://learn.elgg.org/en/latest/appendix/upgrade-notes/1.x-to-2.0.html#container-permissions-hook

    switch ($subtype) {
        case "news":
        case "page":
        case "row":
            return ($user->isAdmin() || pleio_template_is_subeditor($user));
        case "page_widget":
            if ($container instanceof \ElggGroup) {
                return $container->canEdit();
            } else if ($container instanceof \ElggUser) {
                return $return_value;
            } else {
                return ($user->isAdmin() || pleio_template_is_subeditor($user));
            }
        default:
            return $return_value;
    }
}

function pleio_template_user_hover_hook($hook, $type, $return_value, $params) {
    $result = $return_value;

    $site = elgg_get_site_entity();
    $user = elgg_extract("entity", $params);

    if (check_entity_relationship($user->guid, "is_subeditor", $site->guid)) {
        $text = elgg_echo("pleio_template:toggle_subeditor:revoke");
    } else {
        $text = elgg_echo("pleio_template:toggle_subeditor:grant");
    }


    if ($user->isAdmin()) {
        // admins are always subeditor
        return $result;
    }

    $result[] = ElggMenuItem::factory(array(
        "name" => "toggle_subeditor",
        "text" => $text,
        "href" => "action/admin/toggle_subeditor?user_guid={$user->guid}",
        "confirm" => elgg_echo("pleio_template:toggle_subeditor:confirm")
    ));

    return $result;
}

function pleio_template_user_personal_files($hook, $type, $return_value, $params) {
    $result = $return_value;

    $site = elgg_get_site_entity();
    $user = elgg_extract("entity", $params);

    $result[] = ElggMenuItem::factory(array(
        "name" => "personal_files",
        "text" => elgg_echo("pleio_template:personal_files"),
        "href" => "user/{$user->username}/files"
    ));

    return $result;
}

