<?php
namespace Pleio;

class TypeRegistry {
    private $types = [];

    public function get($name) {
        if (!isset($this->types[$name])) {
            $this->types[$name] = $this->{$name}();
        }
        return $this->types[$name];
    }

    private function AccessId() {
        return new Types\AccessId($this);
    }

    private function Activity() {
        return new Types\Activity($this);
    }

    private function ActivityFilter() {
        return new Types\ActivityFilter($this);
    }

    private function Activitylist() {
        return new Types\ActivityList($this);
    }

    private function ActivityType() {
        return new Types\ActivityType($this);
    }

    private function AttendeesList() {
        return new Types\AttendeesList($this);
    }

    private function Blog() {
        return new Types\Blog($this);
    }

    private function Column() {
        return new Types\Column($this);
    }

    private function Comment() {
        return new Types\Comment($this);
    }

    private function Discussion() {
        return new Types\Discussion($this);
    }

    private function DirectLink() {
        return new Types\DirectLink($this);
    }

    private function EmailOverview() {
        return new Types\EmailOverview($this);
    }

    private function Entity() {
        return new Types\Entity($this);
    }

    private function EntityList() {
        return new Types\EntityList($this);
    }

    private function Event() {
        return new Types\Event($this);
    }

    private function EventFilter() {
        return new Types\EventFilter($this);
    }

    private function EventList() {
        return new Types\EventList($this);
    }

    private function Featured() {
        return new Types\Featured($this);
    }

    private function Frequency() {
        return new Types\Frequency($this);
    }

    private function FileFolder() {
        return new Types\FileFolder($this);
    }

    private function Filters() {
        return new Types\Filters($this);
    }

    private function Group() {
        return new Types\Group($this);
    }

    private function GroupFilter() {
        return new Types\GroupFilter($this);
    }

    private function GroupList() {
        return new Types\GroupList($this);
    }

    private function GroupNotificationItem() {
        return new Types\GroupNotificationItem($this);
    }

    private function Invite() {
        return new Types\Invite($this);
    }

    private function InviteList() {
        return new Types\InviteList($this);
    }

    private function KeyValueItem() {
        return new Types\KeyValueItem($this);
    }

    private function Member() {
        return new Types\Member($this);
    }

    private function MemberList() {
        return new Types\MemberList($this);
    }

    private function Membership() {
        return new Types\Membership($this);
    }

    private function MembershipRequestList() {
        return new Types\MembershipRequestList($this);
    }

    private function MenuItem() {
        return new Types\MenuItem($this);
    }

    private function News() {
        return new Types\News($this);
    }

    private function Notification() {
        return new Types\Notification($this);
    }

    private function NotificationList() {
        return new Types\NotificationList($this);
    }

    private function Option() {
        return new Types\Option($this);
    }

    private function OptionInteger() {
        return new Types\OptionInteger($this);
    }

    private function OrderBy() {
        return new Types\OrderBy($this);
    }

    private function OrderDirection() {
        return new Types\OrderDirection($this);
    }

    private function Page() {
        return new Types\Page($this);
    }

    private function Plugins() {
        return new Types\Plugins($this);
    }

    private function Poll() {
        return new Types\Poll($this);
    }

    private function PollChoice() {
        return new Types\PollChoice($this);
    }

    private function PredefinedTag() {
        return new Types\PredefinedTag($this);
    }

    private function UserFilter() {
        return new Types\UserFilter($this);
    }

    private function UserFilterCount() {
        return new Types\UserFilterCount($this);
    }

    private function UserFilterItemCount() {
        return new Types\UserFilterItemCount($this);
    }

    private function ProfileItem() {
        return new Types\ProfileItem($this);
    }

    private function ProfileOverviewItem() {
        return new Types\ProfileOverviewItem($this);
    }

    private function Question() {
        return new Types\Question($this);
    }

    private function Role() {
        return new Types\Role($this);
    }

    private function Row() {
        return new Types\Row($this);
    }

    private function SearchList() {
        return new Types\SearchList($this);
    }

    private function SearchTotal() {
        return new Types\SearchTotal($this);
    }

    private function SearchOrderBy() {
        return new Types\SearchOrderBy($this);
    }

    private function Site() {
        return new Types\Site($this);
    }

    private function SiteSettings() {
        return new Types\SiteSettings($this);
    }

    private function StatsItem() {
        return new Types\StatsItem($this);
    }

    private function StatusUpdate() {
        return new Types\StatusUpdate($this);
    }

    private function Style() {
        return new Types\Style($this);
    }

    private function Subgroup() {
        return new Types\Subgroup($this);
    }

    private function SubgroupList() {
        return new Types\SubgroupList($this);
    }

    private function TagCategory() {
        return new Types\TagCategory($this);
    }

    private function Task() {
        return new Types\Task($this);
    }

    private function TopItem() {
        return new Types\TopItem($this);
    }

    private function TrendingList() {
        return new Types\TrendingList($this);
    }

    private function Type() {
        return new Types\Type($this);
    }

    private function User() {
        return new Types\User($this);
    }

    private function UserList() {
        return new Types\UserList($this);
    }

    private function UserListOverviewItem() {
        return new Types\UserListOverviewItem($this);
    }

    private function Viewer() {
        return new Types\Viewer($this);
    }

    private function Widget() {
        return new Types\Widget($this);
    }

    private function WidgetSetting() {
        return new Types\WidgetSetting($this);
    }

    private function Wiki() {
        return new Types\Wiki($this);
    }

    private function DirectLinkInput() {
        return new InputTypes\DirectLinkInput($this);
    }

    private function FeaturedInput() {
        return new InputTypes\FeaturedInput($this);
    }

    private function FooterInput() {
        return new InputTypes\FooterInput($this);
    }

    private function FilterInput() {
        return new InputTypes\FilterInput($this);
    }

    private function InviteToGroupUser() {
        return new InputTypes\InviteToGroupUser($this);
    }

    private function MenuItemInput() {
        return new InputTypes\MenuItemInput($this);
    }

    private function ProfileItemInput() {
        return new InputTypes\ProfileItemInput($this);
    }

    private function TagCategoryInput() {
        return new InputTypes\TagCategoryInput($this);
    }

    private function WidgetSettingInput() {
        return new InputTypes\WidgetSettingInput($this);
    }

    /**
     * Upload file type
     *
     * https://github.com/Ecodev/graphql-upload
     */
    private function Upload() {
        return new \GraphQL\Upload\UploadType();
    }
}
