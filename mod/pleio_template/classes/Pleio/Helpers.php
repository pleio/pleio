<?php
namespace Pleio;
use DateTime;
use Psr\Http\Message\UploadedFileInterface;

class Helpers {
    static function generateUsername($email) {
        list($username, $dummy) = explode("@", $email);
        $username = preg_replace("/[^a-zA-Z0-9]+/", "", $username);

        $hidden = access_get_show_hidden_status();
        access_show_hidden_entities(true);

        while (strlen($username) < 4) {
            $username .= "0";
        }

        if (get_user_by_username($username)) {
            $i = 1;

            while (get_user_by_username($username . $i)) {
                $i++;
            }

            $result = $username . $i;
        } else {
            $result = $username . $i;
        }

        access_show_hidden_entities($hidden);

        return $result;
    }

    static function getUsernameByInput($username) {
        if (strpos($username, "@") !== false && ($users = get_user_by_email($username))) {
            $username = $users[0]->username;
        } else {
            $username = $username;
        }

        return $username;
    }

    static function getFeaturedEntity($options, $tags = []) {
        $tags = Helpers::renderArray($tags);

        $entities = elgg_get_entities_from_metadata(array_merge($options, [
            "metadata_name_value_pairs" => [
                "name" => "isFeatured",
                "value" => "1"
            ],
            "limit" => 1
        ]));

        if (!$entities) {
            return null;
        }

        if ($entities) {
            $entity = $entities[0];
        }

        if (!$tags || count($tags) === 0) {
            return $entity;
        }

        $entityTags = Helpers::renderArray($entity->tags);
        if (count(array_intersect($entityTags, $tags)) > 0) {
            return $entity;
        }

        return null;
    }

    static function renderArray($array) {
        if ($array) {
            if (!is_array($array)) {
                return [$array];
            } else {
                return $array;
            }
        } else {
            return [];
        }
    }

    static function getURL($entity, $absolute = false) {
        $site = elgg_get_site_entity();
        if ($absolute) {
            $root = $site->url;
        } else {
            $root = "/";
        }

        switch ($entity->type) {
            case "group":
                $friendlytitle = elgg_get_friendly_title($entity->name);
                return "{$root}groups/view/{$entity->guid}/{$friendlytitle}";
            case "user":
                return "{$root}user/{$entity->username}";
            case "object":
                $friendlytitle = elgg_get_friendly_title($entity->title);
                $entitySubtype = $entity->getSubtype();

                $container = $entity->getContainerEntity();
                if ($container instanceof \ElggGroup && $entitySubtype != 'file') {
                    $container_friendlytitle = elgg_get_friendly_title($container->name);
                    $root .= "groups/view/{$container->guid}/{$container_friendlytitle}/";
                }

                if ($container instanceof \ElggUser && $entitySubtype == 'folder') {
                    $container_friendlytitle = elgg_get_friendly_title($container->name);
                    $root .= "user/{$container->username}/";
                }

                switch ($entitySubtype) {
                    case "folder":
                        return "{$root}files/{$entity->guid}";
                        break;
                    case "file":
                        $extension = Helpers::getFileExtension($entity->originalfilename);
                        if ($extension) {
                            $extension = '.' . $extension;
                            $title = substr($entity->originalfilename, 0, -strlen($extension));
                        }
                        else {
                            $extension = '';
                            $title = $entity->title;
                        }
                        $friendlytitle = elgg_get_friendly_title($title) . $extension;
                        return "{$root}files/view/{$entity->guid}/{$friendlytitle}";
                        break;
                    case "news":
                        $root .= "news";
                        break;
                    case "question":
                        $root .= "questions";
                        break;
                    case "discussion":
                        $root .= "discussion";
                        break;
                    case "groupforumtopic":
                        $root .= "discussion";
                        break;
                    case "blog":
                        $root .= "blog";
                        break;
                    case "static":
                    case "page":
                        if ($container->guid != $site->guid) {
                            $container_friendlytitle = elgg_get_friendly_title($container->title);
                            return "{$root}cms/view/{$container->guid}/{$container_friendlytitle}/{$entity->guid}";
                        } else {
                            $root .= "cms";
                        }
                        break;
                    case "event":
                        $root .= "events";
                        break;
                    case "poll":
                        return "{$root}polls/view/{$entity->guid}/$friendlytitle";
                        break;
                    case "wiki":
                        // Check if wiki belongs to group and get root Wiki
                        $root_container = $container;
                        $root_wiki = $container;

                        while(elgg_instanceof($root_container, "object", "wiki")) {
                            $root_container = $root_container->getContainerEntity();
                            if (elgg_instanceof($root_container, "object", "wiki")) {
                                $root_wiki = $root_container;
                            }
                        }

                        $root_container_friendlytitle = elgg_get_friendly_title($root_container->name);
                        $root_wiki_friendlytitle = elgg_get_friendly_title($root_wiki->title);

                        // Are we in a group?
                        if ($root_container instanceof \ElggGroup && $container instanceof \ElggGroup) {
                            return "{$root}wiki/view/{$entity->guid}/{$friendlytitle}";
                        } else if($root_container instanceof \ElggGroup) {
                            return "{$root}groups/view/{$root_container->guid}/{$root_container_friendlytitle}/wiki/view/{$root_wiki->guid}/{$root_wiki_friendlytitle}/{$entity->guid}";
                        }

                        $root .= "wiki";
                        break;
                    case "thewire":
                        return $root . "#{$entity->guid}";
                        break;
                    default:
                        $root .= $entity->getSubtype();
                }

                return "{$root}/view/{$entity->guid}/$friendlytitle";
        }
    }


    static function deleteAdminImage($name) {
        $site = elgg_get_site_entity();
        $file = new \ElggFile();
        $file->owner_guid = $site->guid;
        $file->access_id = ACCESS_PUBLIC;
        $file->setFilename("pleio_template/{$site->guid}_{$name}.jpg");
        $file->delete();
        unset($site->{"{$name}time"});
        unset($site->{"{$name}_extension"});
        $site->save();
    }



    static function saveAdminImage(UploadedFileInterface $uploadFile, $name, $remove_file=False) {
        $site = elgg_get_site_entity();

        if (!$uploadFile) {
            throw new Exception("no_file");
        }

        if ($uploadFile->getError() !== UPLOAD_ERR_OK) {
            switch($uploadFile->getError()) {
                case UPLOAD_ERR_INI_SIZE:
                    throw new Exception("invalid_filesize");
                    break;
                default:
                    throw new Exception("no_file");
            }
        }

        $tempfile = tempnam(sys_get_temp_dir(), "logo_icon_");
        $uploadFile->moveTo($tempfile);

        $type = \ElggFile::detectMimeType($tempfile);
        switch($type) {
            case "image/svg+xml" :
                $file_contents = file_get_contents($tempfile);
                $file_extension = "svg";
                break;
            case "image/png" :
                $file_contents = file_get_contents($tempfile);
                $file_extension = "png";
                break;
            default:
                $file_contents = get_resized_image_from_existing_file($tempfile, 404, 231, false, 0, 0, 0, 0, true);
                $file_extension = "jpg";
        }

        unlink($tempfile);

        if ($file_contents) {
            $file = new \ElggFile();
            $file->owner_guid = $site->guid;
            $file->access_id = ACCESS_PUBLIC;
            $file->setFilename("pleio_template/{$site->guid}_{$name}.{$file_extension}");

            if ($remove_file) {
                $file->delete();
                unset($site->{"{$name}time"});
                unset($site->{"{$name}_extension"});
                $site->save();
            } else {
                $file->open("write");
                $file->write($file_contents);
                $file->close();
                $file->save();
                $site->{"{$name}time"} = time();
                $site->{"{$name}_extension"} = $file_extension;
                $site->save();
            }
        }
    }

    static function saveToIcon(UploadedFileInterface $uploadFile, $entity) {
        $icon_sizes = elgg_get_config("icon_sizes");

        if (!$uploadFile) {
            throw new Exception("no_file");
        }

        if ($uploadFile->getError() !== UPLOAD_ERR_OK) {
            switch($uploadFile->getError()) {
                case UPLOAD_ERR_INI_SIZE:
                    throw new Exception("invalid_filesize");
                    break;
                default:
                    throw new Exception("no_file");
            }
        }

        $class = get_class($entity);
        switch($class) {
            case "ElggGroup":
                $owner_guid = $entity->owner_guid;
                $target_filename = "groups/{$entity->guid}";
                break;
            case "ElggUser":
                $owner_guid = $entity->guid;
                $target_filename = "profile/{$entity->guid}";
                break;
            default:
                throw new Exception("Could not save an icon for this type of entity.");
        }

        // create temp file for image resizing
        $tempfile = tempnam(sys_get_temp_dir(), "saveToIcon_");
        $uploadFile->moveTo($tempfile);

        $files = array();
        foreach ($icon_sizes as $name => $size_info) {

            $resized = get_resized_image_from_existing_file($tempfile, $size_info["w"], $size_info["h"], $size_info["square"], 0, 0, 0, 0, true);

            if ($resized) {
                $file = new \ElggFile();
                $file->owner_guid = $owner_guid;
                $file->access_id = get_default_access();
                $file->setFilename("{$target_filename}{$name}.jpg");
                $file->open("write");
                $file->write($resized);
                $file->close();

                $files[] = $file;
            } else {
                // cleanup on fail
                foreach ($files as $file) {
                    $file->delete();
                }
            }
        }

        unlink($tempfile);
    }

    static function saveToFeatured(UploadedFileInterface $uploadFile, $owner) {

        if (!$uploadFile) {
            throw new Exception("no_file");
        }

        if ($uploadFile->getError() !== UPLOAD_ERR_OK) {
            switch($uploadFile->getError()) {
                case UPLOAD_ERR_INI_SIZE:
                    throw new Exception("invalid_filesize");
                    break;
                default:
                    throw new Exception("no_file");
            }
        }

        // create temp file for image resizing
        $tempfile = tempnam(sys_get_temp_dir(), "saveToFeatured_");
        $uploadFile->moveTo($tempfile);

        $resized = get_resized_image_from_existing_file($tempfile, 1400, 2000, false, 0, 0, 0, 0, false);
        unlink($tempfile);

        if ($resized) {
            $file = new \ElggFile();
            $file->owner_guid = $owner->guid;
            $file->access_id = get_default_access();
            $file->setFilename("featured/{$owner->guid}.jpg");
            $file->open("write");
            $file->write($resized);
            $file->close();
        } else {
            throw new Exception("image_resize_failed");
        }
    }

    static function saveToImage(UploadedFileInterface $uploadFile, $owner) {

        $time = time();

        if (!$uploadFile) {
            throw new Exception("no_file");
        }

        if ($uploadFile->getError() !== UPLOAD_ERR_OK) {
            switch($uploadFile->getError()) {
                case UPLOAD_ERR_INI_SIZE:
                    throw new Exception("invalid_filesize");
                    break;
                default:
                    throw new Exception("no_file");
            }
        }

        $tempfile = tempnam(sys_get_temp_dir(), "saveToImage_");
        $uploadFile->moveTo($tempfile);

        $resized = get_resized_image_from_existing_file($tempfile, 1200, 1200, false, 0, 0, 0, 0, false);
        unlink($tempfile);

        if ($resized) {
            $file = new \ElggFile();
            $file->owner_guid = $owner->guid;
            $file->access_id = get_default_access();
            $file->setFilename("image/{$time}.jpg");
            $file->open("write");
            $file->write($resized);
            $file->close();

            $file->originalfilename = "{$time}.jpg";
            $file->setMimeType("image/jpeg");
            $file->simpletype = file_get_simple_type("image/jpeg");

            $file->save();
            return $file;
        } else {
            throw new Exception("image_resize_failed");
        }
    }

    static function stringsToMetastrings($input) {
        $metastrings = [];

        if (!is_array($metastrings)) {
            return $metastrings;
        }

        foreach ($input as $tag) {
            $id = get_metastring_id($tag);
            if ($id) {
                $metastrings[] = $id;
            }
        }

        return $metastrings;
    }

    static function countAnnotations(\ElggUser $owner, $name, $value = null) {
        $options = array(
            "annotation_name" => $name,
            "annotation_owner_guid" => $owner->guid,
            "count" => true
        );

        if ($value) {
            $options["annnotation_value"] = $value;
        }

        return elgg_get_annotations($options);
    }

    static function sendPasswordChangeMessage(\ElggUser $user) {
        $site = elgg_get_site_entity();
        $subject = elgg_echo("security_tools:notify_user:password:subject");
        $message = elgg_echo("security_tools:notify_user:password:message", array(
            $user->name,
            html_entity_decode($site->name),
            $site->url
        ));

        notify_user($user->guid, $site->guid, $subject, $message, null, "email");
    }

    static function addView(\ElggEntity $entity) {
        $dbprefix = elgg_get_config("dbprefix");

        if (isset($_SERVER["HTTP_USER_AGENT"]) && preg_match('/bot|crawl|slurp|spider/i', $_SERVER["HTTP_USER_AGENT"])) {
            return true;
        }

        $user = elgg_get_logged_in_user_entity();
        if ($user && $user->guid == $entity->guid) {
            return true;
        }

        if (is_memcache_available()) {
            $cache = new \ElggMemcache('entity_view_counter');
            $key = "view_" . session_id() . "_" . $entity->guid;
            if ($cache->load($key)) {
                    return true;
            }
        }

        $guid = (int) $entity->guid;
        $user_guid = (int) elgg_get_logged_in_user_guid();
        $type = sanitise_string($entity->type);
        $subtype = (int) $entity->subtype;

        insert_data("
            INSERT INTO {$dbprefix}entity_views (guid, type, subtype, container_guid, site_guid, views)
            VALUES ({$guid}, '{$type}', {$subtype}, {$entity->container_guid}, {$entity->site_guid}, 1)
            ON DUPLICATE KEY UPDATE views = views + 1;
        ");

        $time = time();

        insert_data("
            INSERT INTO elgg_entity_views_log (entity_guid, type, subtype, container_guid, site_guid, performed_by_guid, time_created)
            VALUES ({$guid}, '${type}', {$subtype}, {$entity->container_guid}, {$entity->site_guid}, {$user_guid}, {$time});
        ");

        if (is_memcache_available()) {
            $cache = new \ElggMemcache('entity_view_counter');
            $key = "view_" . session_id() . "_" . $entity->guid;
            $cache->save($key, 1);
        }
    }

    static function getGroupFilterJoin($group_filter, $user) {
        $dbprefix = elgg_get_config("dbprefix");

        $open_groups = array();
        $my_groups = array();
        $closed_groups = array();

        $membership_id = get_metastring_id("membership");

        // if membership data does not exist, there are no groups, then return no filter
        if (!$membership_id) {
            return [[], []];
        }

        // only get data we need

        $query = "SELECT e.guid, ms.string AS membership FROM {$dbprefix}groups_entity as e JOIN {$dbprefix}metadata md ON md.entity_guid = e.guid JOIN {$dbprefix}metastrings ms on ms.id = md.value_id WHERE md.name_id = {$membership_id}";

        $all_groups = get_data($query);

        foreach ($all_groups as $group) {
            if ($group->membership == ACCESS_PUBLIC) {
                $open_groups[] = (int) $group->guid;
            } else {
                $closed_groups[] = $group->guid;
            }
            if (check_entity_relationship($user->guid,'member',$group->guid)) {
                $my_groups[] = $group->guid;
            }
        }

        if ($group_filter === "all") {
            $groups = array_unique(array_merge($open_groups, $my_groups));
            if (empty($groups)) {
                return [[], ["e.container_guid IN ('')"]];
            }
            $groups = implode(", ", $groups);
            return [
                [],
                ["e.container_guid IN ({$groups})"]
            ];
        } else if ($group_filter === "mine") {

            if (empty($my_groups)) {
                return [[], ["e.container_guid IN ('')"]];
            }
            $my_groups = implode(", ", $my_groups);

            return [
                [],
                ["e.container_guid IN ({$my_groups})"]
            ];
        } else {
            $groups = array_diff($closed_groups, $my_groups);
            $user = elgg_get_logged_in_user_entity();
            if (($user && $user->isAdmin()) || empty($groups)) {
                return [[], []];
            } else {
                $groups = implode(", ", $groups);
                return [
                    [],
                    ["e.container_guid NOT IN ({$groups})"]
                ];
            }
        }
    }

    static function getTagListFilterJoin($tag_lists) {
        $dbprefix = elgg_get_config("dbprefix");
        $tags_id = get_metastring_id("tags");

        if (!$tags_id || !$tag_lists) {
            // we are not filtering on tags or the tags list is empty
            return [[], []];
        }

        $joins = [];
        $wheres = [];
        $t = 0;

        if ($tag_lists) {
            foreach ($tag_lists as $tags) {
                if (!empty($tags)) {
                    $filtered_tags_ids = [];
                    foreach ($tags as $tag) {
                        $tag_id = get_metastring_id($tag);

                        if($tag_id) {
                            $filtered_tags_ids[] = $tag_id;
                        }
                    }

                    if (count($filtered_tags_ids) > 0) {
                        $metadata_table = "md{$t}";

                        $joins[] = "JOIN {$dbprefix}metadata {$metadata_table} ON {$metadata_table}.entity_guid = e.guid AND {$metadata_table}.enabled = 'yes'";
                        $wheres[] = "({$metadata_table}.name_id = {$tags_id} AND {$metadata_table}.value_id IN (".implode(", ", $filtered_tags_ids)."))";

                        $t++;
                    }
                }
            }
        }
        $where = implode(" AND ", $wheres);
        return [
            $joins,
            [$where]
        ];
    }

    static function getTagFilterJoin($tags) {
        $dbprefix = elgg_get_config("dbprefix");
        $tags_id = get_metastring_id("tags");

        if (!$tags_id || !$tags) {
            // we are not filtering on tags or the tags list is empty
            return [[], []];
        }

        $filtered_tags_ids = [];
        foreach ($tags as $tag) {
            $tag_id = get_metastring_id($tag, false);
            if ($tag_id && is_array($tag_id)) {
                foreach ($tag_id as $tag) {
                    $filtered_tags_ids[] = $tag;
                }
            } elseif ($tag_id){
                $filtered_tags_ids[] = $tag_id;
            }
        }

        if (!$filtered_tags_ids) {
            // return an empty list because we are filtering on tags that do not exist in the database yet
            return [[], ["(1 = 2)"]];
        }

        $count = count($tags);
        $filtered_tags_ids = implode(", ", $filtered_tags_ids);

        return [
            ["JOIN {$dbprefix}metadata md ON md.entity_guid = e.guid AND md.name_id = {$tags_id}"],
            ["md.value_id IN ({$filtered_tags_ids})"],
            "md.entity_guid HAVING COUNT(DISTINCT md.value_id) = {$count}"
        ];
    }

    static function isUser() {
        $user = elgg_get_logged_in_user_entity();
        if (!$user) {
            return false;
        }

        $site = elgg_get_site_entity();
        // method exposed in subsite_manager
        if (method_exists($site, "isUser")) {
            return $site->isUser();
        }

        $result = check_entity_relationship($user->guid, "member_of_site", $site->guid);

        if ($result) {
            return true;
        }

        return false;
    }

    static function canJoin() {
        // method exposed in subsite_manager
        if (method_exists($site, "canJoin")) {
            return $site->canJoin();
        }

        return elgg_get_config("allow_registration");
    }

    static function addUser() {
        $user_guid = elgg_get_logged_in_user_guid();
        if ($user_guid) {
            $site = elgg_get_site_entity();
            return $site->addUser($user_guid);
        }

        return false;
    }

    static function getGroupMembership(\ElggGroup $group) {
        $user = elgg_get_logged_in_user_entity();

        if ($group->isMember()) {
            return "joined";
        }

        $request = check_entity_relationship($user->guid, "membership_request", $group->guid);
        if ($request) {
            return "requested";
        }

        $invite = check_entity_relationship($user->guid, "invited", $group->guid);
        if ($invite) {
            return "invited";
        }

        return "not_joined";
    }

    static function getGroupIntroduction(\ElggGroup $group) {
        if ($group->isMember() || $group->canEdit() || $group->isIntroductionPublic) {
            return $group->introduction;
        }

        return "";
    }

    static function sendGroupMembershipRequestNotification(\ElggGroup $group, \ElggUser $user) {
        global $CONFIG;

        $url = "{$CONFIG->url}groups/requests/$group->guid";
        $hidden_email = Helpers::obfuscate_email($user->email);
        $subject = elgg_echo('groups:request:subject', array(
            $user->name,
            $group->name,
        ));

        $body = elgg_echo('groups:request:body', array(
            $group->getOwnerEntity()->name,
            $user->name,
            $hidden_email,
            $group->name,
            $user->getURL(),
            $url,
        ));

        return notify_user($group->owner_guid, $user->guid, $subject, $body);
    }

    static function isMembershipOnRequest($entity) {
        if ($entity->membership === ACCESS_PRIVATE) {
            return true;
        }
        if ($entity->isMembershipOnRequest) {
            return true;
        }
        return false;
    }

    static function getFolderContents($folder, $limit = 100, $offset = 0, $order_by = "filename", $direction = "asc", $filter = "all") {
        if ($folder) {
            $totalFolders = Helpers::getFolders($folder, $limit, $offset, true);
            $folders = Helpers::getFolders($folder, $limit, $offset, false, $order_by, $direction);
        } else {
            // when we are on site-level, we only have files.
            $totalFolders = 0;
            $folders = array();
        }

        $totalFiles = Helpers::getFiles($folder, 1, 0, true);

        if ($limit == 0) {
            $files = Helpers::getFiles($folder, 0, max(0, $offset-$totalFolders), false, $order_by, $direction);
        } elseif ($limit > count($folders)) {
            $files = Helpers::getFiles($folder, $limit-count($folders), max(0, $offset-$totalFolders), false, $order_by, $direction);
        } else {
            $files = array();
        }

        switch ($filter) {
            case "files":
                return array($totalFiles, $files);
            case "folders":
                return array($totalFolders, $folders);
            case "all":
            default:
                return array($totalFolders + $totalFiles, array_merge($folders, $files));
        }


    }

    static function getFolders($parent, $limit = 100, $offset = 0, $count = false, $order_by = "filename", $direction = "ASC") {
        $dbprefix = elgg_get_config("dbprefix");

        $options = array(
            'type' => 'object',
            'subtype' => 'folder',
            'limit' => $limit,
            'offset' => $offset
        );

        if (!$count) {
            switch ($order_by) {
                case "timeCreated":
                    $options['order_by'] = 'e.time_created';
                    break;
                case "timeUpdated":
                    $options['order_by'] = 'e.time_updated';
                    break;
                case "owner":
                    $options['joins'] = "JOIN {$dbprefix}users_entity ue ON e.owner_guid = ue.guid";
                    $options['order_by'] = 'ue.name';
                    break;
                default:
                    $options['joins'] = "JOIN {$dbprefix}objects_entity oe ON e.guid = oe.guid";
                    $options['order_by'] = 'oe.title';
            }

            switch ($direction) {
                case "desc":
                    $options['order_by'] .= ' DESC';
                    break;
                case "asc":
                    $options['order_by'] .= ' ASC';
                    break;
            }
        } else {
            $options['count'] = true;
        }

        if ($parent) {
            if ($parent instanceof \ElggUser | $parent instanceof \ElggGroup) {
                $options['container_guid'] = $parent->guid;
                $options['metadata_name_value_pairs'] = array(array(
                    'name' => 'parent_guid',
                    'value' => 0
                ));
            } else {
                $options['container_guid'] = $parent->container_guid;
                $options['metadata_name_value_pairs'] = array(array(
                    'name' => 'parent_guid',
                    'value' => $parent->guid
                ));
            }

            return elgg_get_entities_from_metadata($options);
        } else {
            if (!$count) {
                return array();
            } else {
                return 0;
            }
        }
    }

    static function getFileExtension($filename) {
        $file_parts = pathinfo($filename);
        return ($file_parts['extension']);
    }

    public static function sanitizeFilename($filename) {
        return preg_replace("/[^a-zA-Z0-9\-\._]/", '-', $filename);
    }

    static function endsWith($haystack, $needle)
    {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }

        return (substr($haystack, -$length) === $needle);
    }

    static function setRecursiveFileAccess($parent) {
        $count = Helpers::getFiles($parent, 1, 0, true);
        $files = Helpers::getFiles($parent, $count, 0);
        foreach($files as $file) {
            if($file->canEdit()) {
                $file->access_id = $parent->access_id;
                $file->write_access_id = $parent->write_access_id;
                $result = $file->save();
            }
        }

        $folderCount = Helpers::getFolders($parent, 1, 0, true);
        $folders = Helpers::getFolders($parent, $folderCount, 0);

        foreach($folders as $folder) {
            if ($folder->canEdit()) {
                $folder->access_id = $parent->access_id;
                $folder->write_access_id = $parent->write_access_id;
                $folder->save();

                Helpers::setRecursiveFileAccess($folder);
            }
        }
    }

    static function getFiles($parent, $limit = 100, $offset = 0, $count = false, $order_by = "filename", $direction = "asc") {
        $dbprefix = elgg_get_config("dbprefix");

        $options = array(
            'type' => 'object',
            'subtype' => 'file',
            'limit' => $limit,
            'offset' => $offset
        );

        if (!$count) {
            switch ($order_by) {
                case "timeCreated":
                    $options['order_by'] = 'e.time_created';
                    break;
                case "timeUpdated":
                    $options['order_by'] = 'e.time_updated';
                    break;
                case "owner":
                    $options['joins'] = "JOIN {$dbprefix}users_entity ue ON e.owner_guid = ue.guid";
                    $options['order_by'] = 'ue.name';
                    break;
                default:
                    $options['joins'] = "JOIN {$dbprefix}objects_entity oe ON e.guid = oe.guid";
                    $options['order_by'] = 'oe.title';
            }

            switch ($direction) {
                case "desc":
                    $options['order_by'] .= ' DESC';
                    break;
                case "asc":
                    $options['order_by'] .= ' ASC';
                    break;
            }
        } else {
            $options['count'] = true;
        }

        if ($parent) {
            if ($parent instanceof \ElggUser | $parent instanceof \ElggGroup) {
                $options['container_guid'] = $parent->guid;
                $options['wheres'] = "NOT EXISTS (
                        SELECT 1 FROM {$dbprefix}entity_relationships r
                        WHERE r.guid_two = e.guid AND
                        r.relationship = 'folder_of')";
            } else {
                $options['container_guid'] = $parent->container_guid;
                $options['relationship'] = "folder_of";
                $options['relationship_guid'] = $parent->guid;
            }
        }

        return elgg_get_entities_from_relationship($options);
    }

    static function generateThumbs($file) {
		$formats = array(
			"thumbnail" => 60,
			"smallthumb" => 153,
			"largethumb" => 600
		);

		$file->icontime = time();
        $filestorename = $file->getFilename();
        $filestorename = elgg_substr($filestorename, elgg_strlen("file/"));

		foreach ($formats as $name => $size) {
	        $thumbnail = get_resized_image_from_existing_file($file->getFilenameOnFilestore(), $size, $size, true);

	        if ($thumbnail) {
	        	$filename = "file/{$name}" . $filestorename;
	            $thumb = new \ElggFile();
	            $thumb->setFilename($filename);
	            $thumb->open("write");
	            $thumb->write($thumbnail);
	            $thumb->close();

	            $file->$name = $filename;
	            unset($thumbnail);
	        }
		}
    }

    static function getBreadcrumb($folder) {
        $path = array();

        if ($folder instanceof \ElggUser | $folder instanceof \ElggGroup) {
            return $path;
        }

        $path[] = $folder;

        $parent = get_entity($folder->parent_guid);
        while ($parent) {
            $path[] = $parent;
            $parent = get_entity($parent->parent_guid);
        }

        foreach ($path as $i => $entity) {
            $path[$i] = Mapper::getEntity($entity);
        }

        return array_reverse($path);
    }

    static function getInitialApolloState() {
        $site = Resolver::getSite(null, [], null);

        // Formatting is following the Apollo Client structure
        // You can extract it client site using: __APOLLO_CLIENT__.extract();
        $state = [
            "ROOT_QUERY" => [
                "site" => [
                    "__ref" => "Site:{\"guid\":\"{$site['guid']}\"}"
                ]
            ],
            "Site:{\"guid\":\"{$site['guid']}\"}" => [
                "guid" => $site["guid"],
                "footer" => $site["footer"],
                "icon" => $site["icon"],
                "iconAlt" => $site["iconAlt"],
                "language" => $site["language"],
                "showIcon" => $site["showIcon"],
                "logo" => $site["logo"],
                "logoAlt" => $site["logoAlt"],
                "name" => $site["name"],
                "theme" => $site["theme"],
                "achievementsEnabled" => $site["achievementsEnabled"],
                "cancelMembershipEnabled" => $site["cancelMembershipEnabled"],
                "customTagsAllowed" => $site["customTagsAllowed"],
                "showTagsInFeed" => $site["showTagsInFeed"],
                "showTagsInDetail" => $site["showTagsInDetail"],
                "style" => array_merge($site["style"], ["__typename" => "Style"]),
                "__typename" => "Site"
            ]
        ];

        return $state;
    }

    /**
     * Appears in body of page
     */
    static function getSettings() {
        $site = elgg_get_site_entity();
        $language = get_current_language();
        if (!$language) {
            $language = "nl";
        }
        return [
            "site" => [
                "guid" => $site->guid,
                "language" => $language,
                "name" => html_entity_decode($site->name),
                "accessIds" => Resolver::getAccessIds(["guid" => $site->guid]),
                "defaultAccessId" => (int)Resolver::getDefaultAccessId(["guid" => $site->guid]),
                "startPage" => elgg_get_plugin_setting("startpage", "pleio_template") ?: "activity",
                "startPageCms" => elgg_get_plugin_setting("startpage_cms", "pleio_template"),
                "likeIcon" => elgg_get_plugin_setting("like_icon", "pleio_template"),
                "newsletter" => elgg_get_plugin_setting("newsletter", "pleio_template") === "yes" ? true : false,
                "theme" => elgg_get_plugin_setting("theme", "pleio_template"),
                "isClosed" => elgg_get_config("walled_garden") ? true : false,
                "cookieConsent" => elgg_is_active_plugin("cookie_consent") ? true : false,
                "limitedGroupAdd" => elgg_get_plugin_setting("limited_groups", "groups")  === "no" ? false : true,
                "customTagsAllowed" => elgg_get_plugin_setting("custom_tags_allowed", "pleio_template") === "no" ? false : true
            ],
            "env" => elgg_get_config("env") ?: "prod",
            "odtEnabled" => elgg_is_active_plugin("odt_editor") ? true : false,
            "enableSharing" => elgg_get_plugin_setting("enable_sharing", "pleio_template") === "no" ? false : true,
            "showUpDownVoting" => elgg_get_plugin_setting("enable_up_down_voting", "pleio_template") === "no" ? false : true,
            "showViewsCount" => elgg_get_plugin_setting("enable_views_count", "pleio_template") === "no" ? false : true,
            "showLoginRegister" => elgg_get_plugin_setting("show_login_register", "pleio_template") === "no" ? false : true,
            "externalLogin" => elgg_is_active_plugin("pleio") ? true : false,
            "groupMemberExport" => elgg_get_plugin_setting("member_export", "pleio_template") === "yes" ? true : false,
            "showExcerptInNewsCard" => elgg_get_plugin_setting("show_excerpt_in_news_card", "pleio_template") === "yes" ? true : false,
            "numberOfFeaturedItems" => elgg_get_plugin_setting("number_of_featured_items", "pleio_template"),
            "enableFeedSorting" => elgg_get_plugin_setting("enable_feed_sorting", "pleio_template") === "no" ? false : true,
            "commentsOnNews" => elgg_get_plugin_setting("comments_on_news", "pleio_template") === "yes" ? true : false,
            "eventExport" => elgg_get_plugin_setting("event_export", "pleio_template") === "yes" ? true : false,
            "subgroups" => elgg_get_plugin_setting("subgroups", "pleio_template") === "yes" ? true : false,
            "statusUpdateGroups" => elgg_get_plugin_setting("status_update_groups", "pleio_template") === "no" ? false : true,
            "showExtraHomepageFilters" => elgg_get_plugin_setting("show_extra_homepage_filters", "pleio_template") === "yes" ? true : false,
            "backendVersion" => 1
        ];
    }

    static function getEventStartDate($entity) {
        $start_day = $entity->start_day;
        $start_time = $entity->start_time;

        if (!$start_day) {
            return null;
        }

        $date = mktime(
            date("H", $start_time),
            date("i", $start_time),
            date("s", $start_time),
            date("n", $start_day),
            date("j", $start_day),
            date("Y", $start_day)
        );

        return date("c", $date);
    }

    static function getEventEndDate($entity) {
        if (!$entity->end_ts) {
            return null;
        }

        return date("c", $entity->end_ts);
    }

    static function getEventDateString($event) {
        $d1 = new DateTime(Helpers::getEventStartDate($event));
        $d2 = new DateTime(Helpers::getEventEndDate($event));

        if ($d1->diff($d2)->days == 0) {
            if($d1 == $d2) {
                return $d1->format("d/m/Y H:i");
            } else {
                return $d1->format("d/m/Y H:i") . " - ". $d2->format("H:i");
            }
        } else {
            return $d1->format("d/m/Y H:i") . " - ". $d2->format("d/m/Y H:i");
        }
    }

    static function getFeaturedImage($entity) {
        if ($entity->featuredIcontime) {
            return "/mod/pleio_template/featuredimage.php?guid={$entity->guid}&lastcache={$entity->featuredIcontime}";
        }

        return "";
    }

    static function canClose($entity) {

        if ($entity->getSubtype() != 'question') {
            return false;
        }

        $site = elgg_get_site_entity();
        $user = elgg_get_logged_in_user_entity();

        if ($user && ($user->isAdmin() || check_entity_relationship($user->guid, "questions_expert", $site->guid))) {
            return true;
        }
        return false;
    }

    static function getChildren($entity, $subtype) {
        if (!$entity) {
            return [];
        }

        if (!$subtype) {
            throw new Exception("Subtype is required.");
            return [];
        }

        $result = elgg_get_entities([
            "type" => "object",
            "subtype" => $subtype,
            "container_guid" => $entity->guid,
            "limit" => false
        ]);

        if (!$result) {
            return [];
        }

        $children = [];
        foreach ($result as $child) {
            $children[] = $child;
        }

        return $children;
    }

    static function transferGroupOwnership($group, $new_owner) {
        $ia = elgg_set_ignore_access(true);

        $old_owner = $group->getOwnerEntity();

        $group->owner_guid = $new_owner->guid;
        $group->container_guid = $new_owner->guid;

        // make sure user is added to the group
        $group->join($new_owner);

        $group->save();

        // remove existing group administrator role for new owner
        remove_entity_relationship($new_owner->guid, "group_admin", $group->guid);
        add_entity_relationship($old_owner->guid, "group_admin", $group->guid);

        // check for group icon
        if (!empty($group->icontime)) {
            $prefix = "groups/" . $group->guid;

            $sizes = array("tiny", "small", "medium", "large");

            $ofh = new \ElggFile();
            $ofh->owner_guid = $old_owner->guid;

            $nfh = new \ElggFile();
            $nfh->owner_guid = $group->getOwnerGUID();

            foreach ($sizes as $size) {
                $ofh->setFilename($prefix . $size . ".jpg");
                $nfh->setFilename($prefix . $size . ".jpg");
                $ofh->open("read");
                $nfh->open("write");
                $nfh->write($ofh->grabFile());
                $ofh->close();
                $nfh->close();
                $ofh->delete();
            }

            $group->icontime = time();
        }

        $options = array(
            "guid" => $group->guid,
            "limit" => false
        );

        $metadata = elgg_get_metadata($options);
        if (!empty($metadata)) {
            foreach ($metadata as $md) {
                if ($md->owner_guid == $old_owner->guid) {
                    $md->owner_guid = $new_owner->guid;
                    $md->save();
                }
            }
        }

        elgg_set_ignore_access($ia);
    }

    static function orderByManual($entities) {
        $newArray = $entities;

        usort($newArray, function($a, $b) {
            if ($a->order === null && $b->order !== null) {
                return 1;
            }

            if ($a->order !== null && $b->order === null) {
                return -1;
            }

            if ($a->order == $b->order) {
                return 0;
            }

            return ($a->order < $b->order) ? -1 : 1;
        });

        return $newArray;
    }

    static function notifyMentions($object) {
        $dbprefix = elgg_get_config("dbprefix");

        $subtype = $object->getSubtype();

        $mentions = Helpers::renderArray($object->mentions);

        $performer = elgg_get_logged_in_user_entity();
        if (!$performer) {
            return;
        }

        $site = elgg_get_site_entity();

        $container = $object->getContainerEntity();
        $subtype = $object->getSubtype();

        $entity = $object;
        $container_guid = 0;

        if ($subtype == "comment") {
            $entity = $container;
            $container = $entity->getContainerEntity();
        }

        if ($container instanceof \ElggGroup) {
            $container_guid = $container->guid;
        }

        $time = time();

        foreach ($mentions as $guid) {
            if ($guid == $performer->guid) {
                // do not notify user of own actions
                continue;
            }

            $user = get_entity((int) $guid);
            if($user instanceof \ElggUser) {

                // check if alreay notified?
                $result = get_data_row("SELECT COUNT(*) AS total FROM {$dbprefix}notifications
                    WHERE user_guid = {$user->guid}
                    AND action = 'mentioned'
                    AND performer_guid = {$performer->guid}
                    AND entity_guid = {$entity->guid}
                    AND container_guid = {$container_guid}
                    AND site_guid = {$site->guid}");

                if ($result->total == 0) {

                    $res = insert_data("INSERT INTO {$dbprefix}notifications (user_guid, action, performer_guid, entity_guid, container_guid, site_guid, time_created)
                        VALUES
                        ({$user->guid}, 'mentioned', {$performer->guid}, {$entity->guid}, {$container_guid}, {$site->guid}, {$time})");
                }
            }
        }
    }

    static function getMetadataByName($name) {
        $metadata = elgg_get_entities_from_metadata(array(
            "metadata_name_value_pairs" => [
                "name" => "metadata_name",
                "value" => $name
            ],
            "limit" => 1
        ));
        return $metadata;
    }

    static function getProfileCategory($name) {
        $profileFields = Helpers::getMetadataByName($name);

        if (is_array($profileFields) && count($profilefields > 0)) {
            if ($profileFields[0]->category_guid) {
                $category = get_entity($profileFields[0]->category_guid);
                if ($category->metadata_label) {
                    return $category->metadata_label;
                } else {
                    return $category->metadata_name;
                }
            }
        }
        return null;
    }

    static function getProfileFieldType($name) {
        $profileFields = Helpers::getMetadataByName($name);

        $internalType = is_array($profileFields) && count($profilefields > 0) ? $profileFields[0]->metadata_type : '';

        switch($internalType) {
            case 'dropdown':
            case 'radio':
            case 'pm_rating':
                $type = 'selectField';
                break;
            case 'date':
            case 'birthday':
            case 'calendar':
            case 'pm_datepicker':
                $type = 'dateField';
                break;
            case 'longtext':
                $type = 'htmlField';
                break;
            case 'multiselect':
                $type = 'multiSelectField';
                break;
            default:
                $type = 'textField';
        }

        return $type;
    }

    static function getProfileFieldOptions($name) {
        $profileField = Helpers::getMetadataByName($name);

        $options = [];

        if (is_array($profileField) && count($profileField > 0)) {

            if (get_class($profileField[0]) === 'ProfileManagerCustomProfileField') {
                $options =  $profileField[0]->getOptions()?:[];
            } else {
                $options = [];
            }
        }

        return $options;
    }

    static function isProfileFieldEditable($name) {
        $profileFields = elgg_get_entities_from_metadata(array(
            "metadata_name_value_pairs" => [
                "name" => "metadata_name",
                "value" => $name
            ],
            "limit" => 1
        ));

        if (is_array($profileFields) && count($profilefields > 0)) {
            if ($profileFields[0]->user_editable) {
                return filter_var($profileFields[0]->user_editable, FILTER_VALIDATE_BOOLEAN);
            }
        }
        return true;
    }

    static function isProfileFieldFilter($name, $profiles) {
        foreach ($profiles as $profile) {
            if ($profile['key'] == $name && $profile['isFilter']) {
                return TRUE;
            }
        }
        return FALSE;
    }

    static function getFieldsInOverview($profiles) {
        $fields = [];
        foreach ($profiles as $profile) {
            if ($profile['isInOverview']) {
                $fields[] = array(
                    'key' => $profile['key'],
                    'label' => $profile['name']
                );
            }
        }
        return $fields;
    }

    static function getFilterLabel($name, $profiles) {
        foreach ($profiles as $profile) {
            if ($profile['key'] == $name) {
                return $profile['name'];
            }
        }
        return '';
    }

    static function filterHtml($html) {
        $html = Helpers::replaceLinks($html);

        $config = \HTMLPurifier_Config::createDefault();
        // allowed tags (excluded div's because of deep nesting in old editor)
        $config->set('HTML.AllowedElements','p,ul,ol,li,img,a,blockquote,pre,h2,h3,h4,h5,h6,strong,br,div');

        // disabled because it also removes div with br...
        $config->set('AutoFormat.RemoveEmpty', true);
        $config->set('Cache.SerializerPath', '/tmp');

        // transform h1 to h2
        $def = $config->getHTMLDefinition(true);
        $def->info_tag_transform['h1'] = new \HTMLPurifier_TagTransform_Simple('h2');

        $purifier = new \HTMLPurifier($config);

        return $purifier->purify($html);
    }

    /**
     * Fix to replace routing links
     */
    static function replaceLinks($html) {
        $links = array(
            "/pages/view/" => "/wiki/view/"
        );

        foreach($links as $old => $new) {
            $html = str_replace($old, $new, $html);
        }

        return $html;
    }

    static function validateDate($date, $format = 'Y-m-d') {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }

    static function createRandomCode() {

        $chars = "abcdefghijkmnopqrstuvwxyz023456789";
        srand((double)microtime()*1000000);
        $i = 0;
        $code = '' ;

        while ($i <= 21) {
            $num = rand() % 33;
            $tmp = substr($chars, $num, 1);
            $code = $code . $tmp;
            $i++;
        }

        return $code;

    }

    static function getConfirmationURL($guid, $code, $email) {
        $site = elgg_get_site_entity();
        return "{$site->url}events/confirm/{$guid}?code={$code}&email={$email}";
    }

    static function arrSortObjsByKey($key, $order = 'DESC') {
        return function($a, $b) use ($key, $order) {
            // Swap order if necessary
            if ($order == 'ASC') {
                list($a, $b) = array($b, $a);
            }
            // Check data type
            if (is_numeric($a->$key)) {
                return $a->$key - $b->$key; // compare numeric
            } else {
                return strnatcasecmp($a->$key, $b->$key); // compare string
            }
        };
    }

    // Get entities with same parent_guid, order them by position (integer)
    // IE if in array obj1->position = 1, obj2->position = 5, obj3->position = 8, this function will alter it to obj1->position = 0, obj2->position = 1, obj3->position = 2
    public static function orderEntitiesWithSameParentGuidByPosition($parent_guid, $subtype, $limit=50) {

        $options = array(
            'type' => 'object',
            'subtype' => $subtype,
            'metadata_name_value_pairs' => array(
                array(
                    'name' => 'parent_guid',
                    'value' => $parent_guid,
                ),
            ),
            'limit' => $limit
        );
        $entities = elgg_get_entities_from_metadata($options);

        usort($entities, Helpers::arrSortObjsByKey('position'));

        foreach ($entities as $e) {
            $key = array_search($e, $entities);
            $e->position = $key;
            $e->save();
        }
    }

    // Get entities with same parent_guid, order them by position (integer)
    // IE if in array obj1->position = 1, obj2->position = 5, obj3->position = 8, this function will alter it to obj1->position = 0, obj2->position = 1, obj3->position = 2
    public static function orderEntitiesWithSameContainerGuidByPosition($container_guid, $subtype, $limit=50) {

        $options = array(
            'type' => 'object',
            'subtype' => $subtype,
            'container_guid' => $container_guid,
            'limit' => $limit
        );
        $entities = elgg_get_entities($options);

        usort($entities, Helpers::arrSortObjsByKey('position'));

        foreach ($entities as $e) {
            $key = array_search($e, $entities);
            $e->position = $key;
            $e->save();
        }
    }


    // Get entities with same parent_guid and add 1 to position if position is the same or higer
    public static function raisePositionOfSiblingsWithSameParentGuid ($entity, $oldPosition, $limit=50) {
        if (!$entity->parent_guid || !isset($entity->position)) {
            return;
        }

        $options = array(
            'type' => 'object',
            'subtype' => $entity->getSubtype(),
            'metadata_name_value_pairs' => array(
                array(
                    'name' => 'parent_guid',
                    'value' => $entity->parent_guid,
                ),
            ),
            'limit' => $limit
        );
        $entities = elgg_get_entities_from_metadata($options);

        foreach ($entities as $e) {
            // if position of changed entity is moving up in row
            if ($entity->position > $oldPosition && ($entity->position >= $e->position) && ($e->guid !== $entity->guid)) {
                // if higer then old position lower position in row
                if ((($e->position - 1) >= $oldPosition)) {
                    $e->position = $e->position - 1;
                    $e->save();
                }
            }
            // if position of changed entity is moving down in row
            else if ($entity->position <= $oldPosition && ($entity->position <= $e->position) && ($e->guid !== $entity->guid)) {
                $e->position = $e->position + 1;
                $e->save();
            }
        }
    }

    // Get entities with same container_guid and add 1 to position if position is the same or higer
    public static function raisePositionOfSiblingsWithSameContainerGuid ($entity, $oldPosition, $limit=50) {
        if (!$entity->container_guid || !isset($entity->position)) {
            return;
        }

        $options = array(
            'type' => 'object',
            'subtype' => $entity->getSubtype(),
            'container_guid' => $entity->container_guid,
            'limit' => $limit
        );
        $entities = elgg_get_entities($options);

        foreach ($entities as $e) {
            // if position of changed entity is moving up in row
            if ($entity->position > $oldPosition && ($entity->position >= $e->position) && ($e->guid !== $entity->guid)) {
                // if higer then old position lower position in row
                if ((($e->position - 1) >= $oldPosition)) {
                    $e->position = $e->position - 1;
                    $e->save();
                }
            }
            // if position of changed entity is moving down in row
            else if ($entity->position <= $oldPosition && ($entity->position <= $e->position) && ($e->guid !== $entity->guid)) {
                $e->position = $e->position + 1;
                $e->save();
            }
        }
    }


    public static function deleteCmsObjectsWithParentGuid ($parent_guid, $limit=12) {
        if (!$parent_guid) {
            return;
        }

        $options = array(
            'type' => 'object',
            'metadata_name_value_pairs' => array(
                array(
                    'name' => 'parent_guid',
                    'value' => $parent_guid,
                ),
            ),
            'limit' => $limit
        );

        $entities = elgg_get_entities_from_metadata($options);
        foreach ($entities as $e) {
            if ($e->getSubtype() == "column") {
                Helpers::deleteCmsObjectsWithParentGuid($e->guid);
            }
            if (in_array($e->getSubtype(), array("row", "column", "page_widget"))) {
                $e->delete();
            } else {
                throw new Exception("deleting_invalid_subtype");
            }
        }
    }

    /**
     * Generate a unique code to be used in email invitations
     *
     * @param int    $group_guid the group GUID
     * @param string $email      the email address
     *
     * @return boolean|string the invite code, or false on failure
     */
    function group_tools_generate_email_invite_code($group_guid, $email) {
        $result = false;

        if (!empty($group_guid) && !empty($email)) {
            // get site secret
            $site_secret = get_site_secret();

            // generate code
            $result = md5($site_secret . strtolower($email) . $group_guid);
        }

        return $result;
    }

    /**
     * Check if a invitation code results in a group
     *
     * @param string $invite_code the invite code
     * @param int    $group_guid  (optional) the group to check
     *
     * @return boolean|ElggGroup a group for the invitation or false
     */
    function group_tools_check_group_email_invitation($invite_code, $group_guid = 0) {
        $result = false;

        if (!empty($invite_code)) {

            // note not using elgg_get_entities_from_annotations
            // due to performance issues with LIKE wildcard search
            // prefetch metastring ids for use in lighter joins instead
            $name_id = add_metastring('email_invitation');
            $code_id = add_metastring($invite_code);
            $sanitized_invite_code = sanitize_string($invite_code);
            $options = array(
                'limit' => 1,
                'wheres' => array(
                    "n_table.name_id = {$name_id} AND (n_table.value_id = {$code_id} OR v.string LIKE '{$sanitized_invite_code}|%')"
                )
            );

            if (!empty($group_guid)) {
                $options["annotation_owner_guids"] = array($group_guid);
            }

            $annotations = elgg_get_annotations($options);

            if (!$annotations) {
                return $result;
            }

            // find hidden groups
            $ia = elgg_set_ignore_access(true);

            $group = $annotations[0]->getEntity();

            if ($group) {
                $result = $group;
            }

            // restore access
            elgg_set_ignore_access($ia);
        }

        return $result;
    }

    function setParentFoldersTimeUpdated($container) {
        if ($container instanceof \ElggObject) {
            $container->time_updated = time();
            $container->save();
            $parent_container = get_entity($container->parent_guid);
            if ($parent_container) {
                Helpers::setParentFoldersTimeUpdated($parent_container);
            }

        }
    }

    static function get_yes_no($input) {
        if ($input) {
            return "yes";
        } else {
            return "no";
        }

    }

    static function get_menu_children($menu, $parentId) {
        $children = [];
        foreach ($menu as $page) {
            if ($page["parentId"] == $parentId) {
                array_push($children, array(
                    "title" => $page["title"], "link" => $page["link"], "children" => Helpers::get_menu_children($menu, $page["id"])
                    )
                );
            }
        }
        return $children;
    }

    # check if email and alter it from example@domain.com -> e******@domain.com
    static function obfuscate_email($email) {
        if (!validate_email_address($email)) {
            return '';
        }
        $email = preg_split("/[@]+/", $email);
        $nr_char = strlen($email[0]) - 1;

        return $email[0][0] . str_repeat('*', $nr_char) . '@' . $email[1];
    }
}
