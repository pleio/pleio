<?php
namespace Pleio;

class Resolver {
    static function getSite($a, $args, $c) {
        $site = elgg_get_site_entity();
        $user = elgg_get_logged_in_user_entity();

        $language = get_current_language();
        if (!$language) {
            $language = "nl";
        }
        $logoAlt = html_entity_decode(elgg_get_plugin_setting("logo_alt", "pleio_template"), ENT_COMPAT | ENT_QUOTES, 'UTF-8') ?: elgg_echo("pleio_template:logo_alt:default");
        $iconAlt = html_entity_decode(elgg_get_plugin_setting("icon_alt", "pleio_template"), ENT_COMPAT | ENT_QUOTES, 'UTF-8') ?: elgg_echo("pleio_template:icon_alt:default");
        $showIcon = (elgg_get_plugin_setting("show_icon", "pleio_template") === "yes") ? true : false;
        $showLeader = (elgg_get_plugin_setting("show_leader", "pleio_template") === "yes") ? true : false;
        $showLeaderButtons = (elgg_get_plugin_setting("show_leader_buttons", "pleio_template") === "no") ? false : true;

        $subtitle = html_entity_decode(elgg_get_plugin_setting("subtitle", "pleio_template"), ENT_COMPAT | ENT_QUOTES, 'UTF-8');
        if (!$subtitle) {
            $subtitle = "";
        }

        $leaderImage = elgg_get_plugin_setting("leader_image", "pleio_template");
        if (!$leaderImage) {
            $leaderImage = "";
        }

        $showInitiative = (elgg_get_plugin_setting("show_initiative", "pleio_template") === "yes") ? true : false;
        $initiativeImage = elgg_get_plugin_setting("initiative_image", "pleio_template") ?: "";
        $initiativeImageAlt = html_entity_decode(
            elgg_get_plugin_setting("initiative_image_alt", "pleio_template"), ENT_COMPAT | ENT_QUOTES, 'UTF-8'
        ) ?: elgg_echo("pleio_template:initiative_image_alt:default");
        $initiativeTitle = elgg_get_plugin_setting("initiative_title", "pleio_template") ?: "";
        $initiativeDescription = elgg_get_plugin_setting("initiative_description", "pleio_template") ?: "";
        $initiatorLink = elgg_get_plugin_setting("initiator_link", "pleio_template") ?: "";

        $profiles = json_decode(html_entity_decode(elgg_get_plugin_setting("profile", "pleio_template"), ENT_COMPAT | ENT_QUOTES, 'UTF-8'));
        $profile = [];

        if (is_array($profiles)) {
            foreach ($profiles as $p) {
                $fieldType = Helpers::getProfileFieldType($p->key);
                $isEditable = Helpers::isProfileFieldEditable($p->key);

                switch($fieldType) {
                    case "dateField":
                    case "htmlField":
                        $isFilterable = false;
                        break;
                    case "textField":
                        $isFilterable = !$isEditable;
                        break;
                    default:
                        $isFilterable = true;
                }

                $p->fieldType = $fieldType;
                $p->isFilterable = $isFilterable;
                $profile[] = $p;
            }
        }

        $achievementsEnabled = (elgg_get_plugin_setting("achievements_enabled", "pleio_template") === "yes") ? true : false;

        $cancelMembershipEnabled = (elgg_get_plugin_setting("cancel_membership_enabled", "pleio_template") === "no") ? false : true;

        $customTagsAllowed = (elgg_get_plugin_setting("custom_tags_allowed", "pleio_template") === "no") ? false : true;

        $tagCategories = json_decode(html_entity_decode(elgg_get_plugin_setting("tagCategories", "pleio_template"), ENT_COMPAT | ENT_QUOTES, 'UTF-8'));
        if (!$tagCategories) {
            $tagCategories = [];
        }

        $showExtraHomepageFilters = (elgg_get_plugin_setting("show_extra_homepage_filters", "pleio_template") === "yes") ? true : false;

        $theme = elgg_get_plugin_setting("theme", "pleio_template") ?: "leraar";
        $startpage = elgg_get_plugin_setting("startpage", "pleio_template") ?: "activity";

        $directLinks = json_decode(html_entity_decode(elgg_get_plugin_setting("directLinks", "pleio_template"), ENT_COMPAT | ENT_QUOTES, 'UTF-8')) ?: [];
        $footer = json_decode(html_entity_decode(elgg_get_plugin_setting("footer", "pleio_template"), ENT_COMPAT | ENT_QUOTES, 'UTF-8')) ?: [];

        if ($user && Helpers::isUser()) {
            if (Helpers::canJoin()) {
                Helpers::addUser();
            }
        }

        return [
            "guid" => $site->guid,
            "name" => html_entity_decode($site->name),
            "menu" => Resolver::getMenu(),
            "directLinks" => $directLinks,
            "profile" => $profile,
            "achievementsEnabled" => $achievementsEnabled,
            "cancelMembershipEnabled" => $cancelMembershipEnabled,
            "theme" => $theme,
            "footer" => $footer,
            "language" => $language,
            "logo" => $site->logotime ? "/mod/pleio_template/logo.php?lastcache={$site->logotime}" : "",
            "logoAlt" => $logoAlt,
            "icon" => "/mod/pleio_template/icon.php?lastcache={$site->icontime}",
            "iconAlt" => $iconAlt,
            "showIcon" => $showIcon,
            "startpage" => $startpage,
            "initiatorLink" => $initiatorLink,
            "showLeader" => $showLeader,
            "showLeaderButtons" => $showLeaderButtons,
            "subtitle" => $subtitle,
            "leaderImage" => $leaderImage,
			"showInitiative" => $showInitiative,
            "initiativeTitle" => $initiativeTitle,
            "initiativeImage" => $initiativeImage,
            "initiativeImageAlt" => $initiativeImageAlt,
            "initiativeDescription" => $initiativeDescription,
            "customTagsAllowed" => $customTagsAllowed,
            "tagCategories" => $tagCategories,
            "showTagsInFeed" => (elgg_get_plugin_setting("show_tags_in_feed", "pleio_template") === "yes") ? true : false,
            "showTagsInDetail" => (elgg_get_plugin_setting("show_tags_in_detail", "pleio_template") === "yes") ? true : false,
            "showExtraHomepageFilters" => $showExtraHomepageFilters,
            "style" => Resolver::getStyle()
        ];
    }

    static function getSiteSettings() {
        if (!elgg_is_admin_logged_in()) {
            throw new Exception("could_not_find");
        }
        $site = elgg_get_site_entity();
        $language = get_current_language();
        if (!$language) {
            $language = "nl";
        }

        if (elgg_get_plugin_setting("theme", "pleio_template") === "rijkshuisstijl") {
            $default_header = "#99d8f4";
        } else {
            $default_header = elgg_get_plugin_setting("color_primary", "pleio_template") ?: "#01689b";
        }

        $logoAlt = html_entity_decode(elgg_get_plugin_setting("logo_alt", "pleio_template"), ENT_COMPAT | ENT_QUOTES, 'UTF-8') ?: elgg_echo("pleio_template:logo_alt:default");
        $isClosed = elgg_get_config("walled_garden") ? true : false;
        $defaultAccessIdOptions = [["value" => 0, "label" => elgg_echo("PRIVATE")], ["value" => 1, "label" => elgg_echo("LOGGED_IN") ]];
        if (!$isClosed) {
            array_push($defaultAccessIdOptions, ["value" => 2, "label" => elgg_echo("PUBLIC") ]);
        }

        $startPageCmsOptions = [];
        $cms_pages = pleio_template_get_cms_pages();
        foreach ($cms_pages as $guid => $title) {
            array_push($startPageCmsOptions, array("value" => $guid, "label" => $title));
        }

        $profiles = json_decode(html_entity_decode(elgg_get_plugin_setting("profile", "pleio_template"), ENT_COMPAT | ENT_QUOTES, 'UTF-8'));
        $profile = [];

        if (is_array($profiles)) {
            foreach ($profiles as $p) {
                $fieldType = Helpers::getProfileFieldType($p->key);
                $isEditable = Helpers::isProfileFieldEditable($p->key);

                switch($fieldType) {
                    case "dateField":
                    case "htmlField":
                        $isFilterable = false;
                        break;
                    case "textField":
                        $isFilterable = !$isEditable;
                        break;
                    default:
                        $isFilterable = true;
                }

                $p->fieldType = $fieldType;
                $p->isFilterable = $isFilterable;
                $profile[] = $p;
            }
        }



        return [
            "language" => $language,
            "languageOptions" => [["value" => "en", "label" => elgg_echo("en")], ["value" => "nl", "label" => elgg_echo("nl") ]],
            "name" => html_entity_decode($site->name),
            "description" => html_entity_decode($site->description),
            "isClosed" => $isClosed,
            "allowRegistration" => elgg_get_config("allow_registration") ? true : false,
            "defaultAccessId" => Resolver::getDefaultAccessId(["guid" => $site->guid]),
            "defaultAccessIdOptions" => $defaultAccessIdOptions,
            "googleAnalyticsId" => elgg_get_plugin_setting("google_analytics", "pleio_template"),
            "googleSiteVerification" => elgg_get_plugin_setting("google_site_verification", "pleio_template"),
            "piwikUrl" => elgg_get_plugin_setting("piwik_url", "pleio_template") ?: "https://stats.pleio.nl/",
            "piwikId" => elgg_get_plugin_setting("piwik", "pleio_template"),
            "font" => elgg_get_plugin_setting("font", "pleio_template") ?: "Rijksoverheid Sans",
            "fontOptions" => [
                ["value" => "Arial", "label" => "Arial"],
                ["value" => "Open Sans", "label" => "Open Sans"],
                ["value" => "PT Sans", "label" => "PT Sans"],
                ["value" => "Rijksoverheid Sans", "label" => "Rijksoverheid Sans"],
                ["value" => "Roboto", "label" => "Roboto"],
                ["value" => "Source Sans Pro", "label" => "Source Sans Pro"]
              ],

            "colorPrimary" => elgg_get_plugin_setting("color_primary", "pleio_template") ?: "#01689b",
            "colorSecondary" => elgg_get_plugin_setting("color_secondary", "pleio_template") ?: "#009ee3",
            "colorHeader" => elgg_get_plugin_setting("color_header", "pleio_template") ?: $default_header,
            "theme" => elgg_get_plugin_setting("theme", "pleio_template"),
            "themeOptions" => [
                ["value" => "leraar", "label" => elgg_echo("pleio_template:minimalistic")],
                ["value" => "rijkshuisstijl", "label" => elgg_echo("pleio_template:rijkshuisstijl")]
              ],
            "logo" => $site->logotime ? "/mod/pleio_template/logo.php?lastcache={$site->logotime}" : "",
            "logoAlt" => $logoAlt,
            "likeIcon" => elgg_get_plugin_setting("like_icon", "pleio_template"),

            "startPage" => elgg_get_plugin_setting("startpage", "pleio_template") ?: "activity",
            "startPageOptions" => [
                ["value" => "activity", "label" => elgg_echo("pleio_template:activity")],
                ["value" => "cms", "label" => elgg_echo("pleio_template:cms")]
              ],
            "startPageCms" => elgg_get_plugin_setting("startpage_cms", "pleio_template"),
            "startPageCmsOptions" => $startPageCmsOptions,
            "icon" => "/mod/pleio_template/icon.php?lastcache={$site->icontime}",
            "showIcon" => (elgg_get_plugin_setting("show_icon", "pleio_template") === "yes") ? true : false,
            "menu" => Resolver::getMenu(),
            "numberOfFeaturedItems" => elgg_get_plugin_setting("number_of_featured_items", "pleio_template"),
            "enableFeedSorting" => (elgg_get_plugin_setting("enable_feed_sorting", "pleio_template") === "no") ? false : true,
            "showExtraHomepageFilters" => (elgg_get_plugin_setting("show_extra_homepage_filters", "pleio_template") === "yes") ? true : false,
            "showLeader" => (elgg_get_plugin_setting("show_leader", "pleio_template") === "yes") ? true : false,
            "showLeaderButtons" => (elgg_get_plugin_setting("show_leader_buttons", "pleio_template") === "no") ? false : true,
            "subtitle" => html_entity_decode(elgg_get_plugin_setting("subtitle", "pleio_template"), ENT_COMPAT | ENT_QUOTES, 'UTF-8') ?: "",
            "leaderImage" => elgg_get_plugin_setting("leader_image", "pleio_template")  ?: "",
			"showInitiative" => (elgg_get_plugin_setting("show_initiative", "pleio_template") === "yes") ? true : false,
            "initiativeTitle" => elgg_get_plugin_setting("initiative_title", "pleio_template") ?: "",
            "initiativeImage" => elgg_get_plugin_setting("initiative_image", "pleio_template") ?: "",
            "initiativeImageAlt" => html_entity_decode(
                elgg_get_plugin_setting("initiative_image_alt", "pleio_template"), ENT_COMPAT | ENT_QUOTES, 'UTF-8'
            ) ?: elgg_echo("pleio_template:initiative_image_alt:default"),
            "initiativeDescription" =>  elgg_get_plugin_setting("initiative_description", "pleio_template") ?: "",
            "initiativeLink" =>  elgg_get_plugin_setting("initiator_link", "pleio_template") ?: "",
            "directLinks" => json_decode(html_entity_decode(elgg_get_plugin_setting("directLinks", "pleio_template"), ENT_COMPAT | ENT_QUOTES, 'UTF-8')) ?: [],
            "footer" => json_decode(html_entity_decode(elgg_get_plugin_setting("footer", "pleio_template"), ENT_COMPAT | ENT_QUOTES, 'UTF-8')) ?: [],

            "tagCategories" => json_decode(html_entity_decode(elgg_get_plugin_setting("tagCategories", "pleio_template"), ENT_COMPAT | ENT_QUOTES, 'UTF-8')) ?: [],
            "showTagsInFeed" => (elgg_get_plugin_setting("show_tags_in_feed", "pleio_template") === "yes") ? true : false,
            "showTagsInDetail" => (elgg_get_plugin_setting("show_tags_in_detail", "pleio_template") === "yes") ? true : false,

            "profile" => $profile,

            "defaultEmailOverviewFrequency" => elgg_get_plugin_setting("default_email_overview", "pleio_template") ?: "",
            "defaultEmailOverviewFrequencyOptions" => [
                                                        ["value" => "daily", "label" => elgg_echo("option:daily")],
                                                        ["value" => "weekly", "label" => elgg_echo("option:weekly")],
                                                        ["value" => "monthly", "label" => elgg_echo("option:monthly")],
                                                        ["value" => "never", "label" => elgg_echo("option:never")]
                                                      ],
            "emailOverviewSubject" => elgg_get_plugin_setting("email_overview_subject", "pleio_template") ?: "",
            "emailOverviewTitle" => elgg_get_plugin_setting("email_overview_title", "pleio_template") ?: "",
            "emailOverviewIntro" => elgg_get_plugin_setting("email_overview_intro", "pleio_template") ?: "",
            "emailOverviewEnableFeatured" => (elgg_get_plugin_setting("email_overview_enable_featured", "pleio_template") === "yes") ? true : false,
            "emailOverviewFeaturedTitle" => elgg_get_plugin_setting("email_overview_featured_title", "pleio_template") ?: "",
            "emailNotificationShowExcerpt" => (elgg_get_plugin_setting("email_notification_show_excerpt", "pleio_template") === "yes") ? true : false,

            "showLoginRegister" => (elgg_get_plugin_setting("show_login_register", "pleio_template") === "no") ? false : true,
            "customTagsAllowed" => (elgg_get_plugin_setting("custom_tags_allowed", "pleio_template") === "no") ? false : true,
            "showUpDownVoting" => elgg_get_plugin_setting("enable_up_down_voting", "pleio_template") === "no" ? false : true,
            "enableSharing" => (elgg_get_plugin_setting("enable_sharing", "pleio_template") === "no") ? false : true,
            "showViewsCount" => (elgg_get_plugin_setting("enable_views_count", "pleio_template") === "no") ? false : true,
            "newsletter" => (elgg_get_plugin_setting("newsletter", "pleio_template") === "yes") ? true : false,
            "cancelMembershipEnabled" => (elgg_get_plugin_setting("cancel_membership_enabled", "pleio_template") === "no") ? false : true,
            "showExcerptInNewsCard" => (elgg_get_plugin_setting("show_excerpt_in_news_card", "pleio_template") === "yes") ? true : false,
            "commentsOnNews" => (elgg_get_plugin_setting("comments_on_news", "pleio_template") === "yes") ? true : false,
            "eventExport" => (elgg_get_plugin_setting("event_export", "pleio_template") === "yes") ? true : false,
            "questionerCanChooseBestAnswer" => (elgg_get_plugin_setting("questioner_can_choose_best_answer", "pleio_template") === "no") ? false : true,
            "statusUpdateGroups" => (elgg_get_plugin_setting("status_update_groups", "pleio_template") === "no") ? false : true,
            "subgroups" => (elgg_get_plugin_setting("subgroups", "pleio_template") === "yes") ? true : false,
            "groupMemberExport" => (elgg_get_plugin_setting("member_export", "pleio_template") === "yes") ? true : false,
        ];
    }

    static function getStyle() {
        if (elgg_get_plugin_setting("theme", "pleio_template") === "rijkshuisstijl") {
            $default_header = "#99d8f4";
        } else {
            $default_header = elgg_get_plugin_setting("color_primary", "pleio_template") ?: "#01689b";
        }

        return [
            "font" => elgg_get_plugin_setting("font", "pleio_template") ?: "Rijksoverheid Sans",
            "colorPrimary" => elgg_get_plugin_setting("color_primary", "pleio_template") ?: "#01689b",
            "colorSecondary" => elgg_get_plugin_setting("color_secondary", "pleio_template") ?: "#009ee3",
            "colorHeader" => elgg_get_plugin_setting("color_header", "pleio_template") ?: $default_header,
        ];
    }

    static function getMenu() {
        $menu = json_decode(html_entity_decode(elgg_get_plugin_setting("menu", "pleio_template"), ENT_COMPAT | ENT_QUOTES, 'UTF-8'));

        if (!$menu) {
            $menu = [
                ["title" => "Blog", "link" => "/blog", "children" => []],
                ["title" => "Nieuws", "link" => "/news", "children" => []],
                ["title" => "Discussies", "link" => "/discussion", "children" => []],
                ["title" => "Agenda", "link" => "/events", "children" => []],
                ["title" => "Groepen", "link" => "/groups", "children" => []]
            ];
        }

        foreach ($menu as $i => $value) {
            if (!$menu[$i]->children) {
                $menu[$i]->children = [];
            }
        }

        return $menu;
    }

    static function getNotifications($a, $args, $c) {
        $dbprefix = elgg_get_config("dbprefix");
        $site = elgg_get_site_entity();

        $user = elgg_get_logged_in_user_entity();
        if (!$user) {
            return [
                "total" => 0,
                "totalUnread" => 0,
                "notifications" => []
            ];
        }

        $limit = (int) ($args["limit"] ?: 20);
        $offset = (int) ($args["offset"] ?: 0);

        $unreadFilter = "";
        if (isset($args["unread"])) {
            if ($args["unread"]) {
                $unreadFilter = 'AND unread = "yes"';
            } else {
                $unreadFilter = 'AND unread = "no"';
            }
        }

        $totalUnread = get_data("SELECT count(id) FROM {$dbprefix}notifications WHERE user_guid = {$user->guid} AND site_guid = {$site->guid} AND unread = \"yes\"")[0]->{'count(id)'};
        $total = get_data("SELECT count(id) FROM {$dbprefix}notifications WHERE user_guid = {$user->guid} AND site_guid = {$site->guid} $unreadFilter")[0]->{'count(id)'};

        $sql = "SELECT * FROM {$dbprefix}notifications WHERE user_guid = {$user->guid} AND site_guid = {$site->guid} "
                . $unreadFilter
                . " ORDER BY id DESC LIMIT {$offset}, {$limit}";

        $notifications = [];

        foreach (get_data($sql) as $notification) {
            $notification = Mapper::getNotification($notification);

            if ($notification) {
                $notifications[] = $notification;
            }
        }

        return [
            "total" => $total,
            "totalUnread" => $totalUnread,
            "edges" => $notifications
        ];
    }

    static function canChangeOwnership($object) {
        $entity = get_entity($object["guid"]);
        if (!$entity) {
            return false;
        }

        $logged_in_user = elgg_get_logged_in_user_entity();
        if (!$logged_in_user) {
            return false;
        }

        if ($logged_in_user->guid == $entity->owner_guid) {
            return true;
        }

        if ($logged_in_user->isAdmin()) {
            return true;
        }

        return false;
    }

    static function getAccessIds($object) {
        $old_guid = elgg_set_page_owner_guid($object["guid"]);

        $container = get_entity($object["guid"]);

        $accessIds = [];
        foreach (get_write_access_array() as $id => $description) {
            if ($id === -2) {
                continue;
            }

            // don't allow public content in private group
            if ($container instanceof \ElggGroup &&
                $container->membership === ACCESS_PRIVATE &&
                ($id === ACCESS_LOGGED_IN || $id === ACCESS_PUBLIC)) {
                continue;
            }

            $accessIds[] = [
                "id" => $id,
                "description" => $description
            ];
        }

        elgg_set_page_owner_guid($old_guid);

        return $accessIds;
    }

    static function getDefaultAccessId($container) {
        $container = get_entity($container["guid"]);

        if ($container instanceof \ElggGroup && $container->membership === ACCESS_PRIVATE && $container->group_acl) {
            $default_access = $container->group_acl;
        } else {
            $default_access = get_default_access();
        }

        return $default_access;
    }

    static function getActivities($a, $args, $c) {
        global $CONFIG;

        // deprecated?
        $tags = $args["tags"];

        $tag_lists = $args["tagLists"];
        $group_filter = $args["groupFilter"];
        $subtypes = $args["subtypes"];

        if (!is_array($group_filter) || empty($group_filter)) {
            $group_filter = "";
        } else {
            $group_filter = $group_filter[0];
        }

        if (!is_array($subtypes) || empty($subtypes)) {
            $subtypes = ["news", "blog", "question", "discussion", "thewire", "event", "wiki"];
        }

        // depreacted?
        $user = elgg_get_logged_in_user_entity();
        if ($user && $tags == ["mine"]) {
            if ($user->tags) {
                if (is_array($user->tags)) {
                    $tags = $user->tags;
                } else {
                    $tags = [$user->tags];
                }
            } else {
                $tags = [];
            }
        }

        // deprecated?
        list ($tag_joins, $tag_wheres, $group_by) = Helpers::getTagFilterJoin($tags);

        list ($tag_lists_joins, $tag_list_wheres) = Helpers::getTagListFilterJoin($tag_lists);
        list ($group_joins, $group_wheres) = Helpers::getGroupFilterJoin($group_filter, $user);

        $joins = array_merge($tag_joins, $tag_lists_joins, $group_joins);
        $wheres = array_merge($tag_wheres, $tag_list_wheres, $group_wheres);

        $options = [
            "type" => "object",
            "subtype" => $subtypes,
            "offset" => (int) $args["offset"],
            "limit" => (int) $args["limit"],
            "joins" => $joins,
            "wheres" => $wheres,
            "group_by" => $group_by
        ];

        if ($args["containerGuid"]) {
            $options["container_guid"] = (int) $args["containerGuid"];
        }

        switch ($args["orderDirection"]) {
            case "asc":
                $direction = "ASC";
                break;
            default:
                $direction = "DESC";
                break;
        }

        switch ($args["orderBy"]) {
            case "timeCreated":
                $orderBy = "e.time_created {$direction}";
                break;
            case "timeUpdated":
                $orderBy = "e.time_updated {$direction}";
                break;
            case "lastAction":
                $orderBy = "e.last_action {$direction}";
                break;
        }

        if (isset($orderBy)) {
            $options["order_by"] = $orderBy;
        }

        $result = [
            "total" => elgg_get_entities(array_merge($options, ["count" => true])),
            "entities" => elgg_get_entities($options)
        ];

        $activities = [];

        foreach ($result["entities"] as $entity) {
            $subject = $entity->getOwnerEntity();

            if (!$entity || !$subject) {
                continue;
            }

            $activities[] = array(
                "guid" => "activity:" . $entity->guid,
                "type" => "create",
                "entity" => $entity
            );
        }

        return [
            "total" => $result["total"],
            "edges" => $activities
        ];
    }

    static function getRecommended($a, $args, $c) {
        $options = [
            "type" => "object",
            "metadata_name" => "isRecommended",
            "metadata_value" => "1",
            "offset" => (int) $args["offset"],
            "limit" => (int) $args["limit"]
        ];

        $entities = [];
        foreach (elgg_get_entities_from_metadata($options) as $entity) {
            $entities[] = [
                "guid" => $entity->guid,
                "type" => $entity->type,
                "subtype" => $entity->getSubtype(),
                "title" => $entity->title,
                "description" => $entity->description,
                "ownerGuid" => $entity->owner_guid,
                "url" => Helpers::getURL($entity),
                "timeCreated" => $entity->time_created,
                "timeUpdated" => $entity->time_updated
            ];
        }

        return [
            "total" => elgg_get_entities_from_metadata(array_merge($options, ["count" => true])),
            "edges" => $entities
        ];
    }

    static function getTrending($a, $args, $c) {
        $options = array(
            "annotation_name" => "vote",
            "annotation_value" => 1,
            "order_by" => "n_table.time_created desc",
            "limit" => 250
        );

        $tagLikes = [];
        $annotations = elgg_get_annotations($options);
        foreach ($annotations as $annotation) {
            $tags = $annotation->getEntity()->tags;
            if (!$tags) {
                continue;
            }

            if (!is_array($tags)) {
                $tags = [$tags];
            }

            foreach ($tags as $tag) {
                if ($tagLikes[$tag]) {
                    $tagLikes[$tag] += 1;
                } else {
                    $tagLikes[$tag] = 1;
                }
            }
        }

        arsort($tagLikes);
        $tagLikes = array_slice($tagLikes, 0, 3);

        $return = [];
        foreach ($tagLikes as $tag => $likes) {
            $return[] = [
                "tag" => $tag,
                "likes" => $likes
            ];
        }

        return $return;
    }

    static function getTop($a, $args, $c) {
        $options = array(
            "annotation_name" => "vote",
            "annotation_value" => 1,
            "order_by" => "n_table.time_created desc",
            "limit" => 250
        );

        $topUsers = [];
        $annotations = elgg_get_annotations($options);
        foreach ($annotations as $annotation) {
            $ownerGuid = $annotation->getEntity()->owner_guid;
            if (!$ownerGuid || $ownerGuid == 0) {
                continue;
            }

            if ($topUsers[$ownerGuid]) {
                $topUsers[$ownerGuid] += 1;
            } else {
                $topUsers[$ownerGuid] = 1;
            }
        }

        arsort($topUsers);

        $topUsers = array_slice($topUsers, 0, 3, true);

        $return = [];
        foreach ($topUsers as $userGuid => $likes) {
            $entity = get_entity($userGuid);

            if (!$entity) {
                continue;
            }

            $return[] = [
                "user" => Mapper::getUser($entity),
                "likes" => $likes
            ];
        }


    }

    static function getUsers($site, $args) {

        if (!elgg_is_logged_in()) {
            return [
                "total" => 0,
                "filterCount" => [],
                "edges" => []
            ];
        }

        $es = \ESInterface::get();

        $results = array();
        $filters = array();

        $q = $args["q"];
        if (!$q) {
            $sort = "name.raw";
            $order = "asc";
        }

        $es_results = $es->search(
            $q,
            null,
            "user",
            $args["subtype"] ?: null,
            $args["limit"] ?: null,
            $args["offset"] ?: null,
            $sort ?: null,
            $order ?: null,
            $site->guid,
            $args["filters"]
        );
        foreach ($es_results["hits"] as $hit) {
            $results[] = Mapper::getEntity($hit);
        }

        $profileFields = elgg_get_plugin_setting("profile", "pleio_template") ? json_decode(html_entity_decode(elgg_get_plugin_setting("profile", "pleio_template"), ENT_COMPAT | ENT_QUOTES, 'UTF-8'), true) : [];
        $profileFieldKeys = array_column($profileFields, 'key');

        foreach ($es_results["count_per_metadata_name"] as $name => $values) {
            $isEditable = Helpers::isProfileFieldEditable($name);
            $isFilter = Helpers::isProfileFieldFilter($name, $profileFields);
            $fieldType = Helpers::getProfileFieldType($name);
            if (in_array($name, $profileFieldKeys)) {
                if (($isEditable && $fieldType == 'textField') || !$isFilter) {
                    continue;
                }
                if (in_array($fieldType, ['dateField', 'htmlField'])) {
                    continue;
                }
                $filters[] = [
                    "name" => $name,
                    "fieldType" => $fieldType,
                    "values" => $values
                ];
            }
        }
        $fieldsInOverview = Helpers::getFieldsInOverview($profileFields);

        return [
            "total" => $es_results["count"],
            "filterCount" => $filters,
            "edges" => $results,
            "fieldsInOverview" => $fieldsInOverview
        ];
    }

    static function getFilterValues($fields) {
        $values = [];
        foreach($fields as $value) {
            if (is_array($value)) {
                foreach($value as $v) {
                    $values[] = $v;
                }
            }
        }
        return $values;
    }

    static function getFilters($site, $args) {

        $es = \ESInterface::get();

        $es_results = $es->search(
            "",
            null,
            "user",
            $args["subtype"] ?: null,
            $args["limit"] ?: null,
            $args["offset"] ?: null,
            $sort ?: null,
            $order ?: null,
            $site->guid,
            $args["filters"]
        );


        $profileFields = elgg_get_plugin_setting("profile", "pleio_template") ? json_decode(html_entity_decode(elgg_get_plugin_setting("profile", "pleio_template"), ENT_COMPAT | ENT_QUOTES, 'UTF-8'), true) : [];
        $profileFieldKeys = array_column($profileFields, 'key');

        $user_filters = [];
        foreach ($es_results["count_per_metadata_name"] as $name => $values) {
            $keys = [];
            $isEditable = Helpers::isProfileFieldEditable($name);
            $isFilter = Helpers::isProfileFieldFilter($name, $profileFields);
            $filterLabel = Helpers::getFilterLabel($name, $profileFields);
            $fieldType = Helpers::getProfileFieldType($name);
            if (in_array($name, $profileFieldKeys)) {
                if (($isEditable && $fieldType == 'textField') || !$isFilter) {
                    continue;
                }
                if (in_array($fieldType, ['dateField', 'htmlField'])) {
                    continue;
                }

                // is leading if profile field has defined options else use elasticsearch results
                $profileFieldOptions = Helpers::getProfileFieldOptions($name);
                if (count($profileFieldOptions) > 0 ) {
                    // keep order
                    $keys = $profileFieldOptions;
                } else {
                    foreach ($values as $value) {
                        $keys[] = $value['key'];
                    }

                    // order on alfabet
                    sort($keys);
                }

                $user_filters[] = [
                    "name" => $name,
                    "fieldType" => $fieldType,
                    "label" => $filterLabel,
                    "keys" => $keys
                ];
            }
        }

        return [
            "users" => $user_filters
        ];
    }

    static function getUsersOnline($site) {
        global $CONFIG;

        $time = time() - 600;
        $site = elgg_get_site_entity();

        return elgg_get_entities(array(
            "type" => "user",
            "count" => true,
            "joins" => [
                    "join {$CONFIG->dbprefix}users_entity u on e.guid = u.guid",
                    "join {$CONFIG->dbprefix}entity_relationships r ON e.guid = r.guid_one AND relationship = 'member_of_site'"
            ],
            "wheres" => [
                "u.last_action >= {$time}",
                "r.guid_two = {$site->guid}"
            ]
        ));
    }

    static function getEntity($a, $args, $c) {
        $guid = (int) $args["guid"];
        $username = $args["username"];

        if ($guid) {
            $entity = get_entity($guid);
        } elseif ($username) {
            $entity = get_user_by_username($username);
        }

        if (!$entity) {
            return null;
        }

        if ($entity instanceof \ElggGroup) {
            return Mapper::getGroup($entity);
        }

        if ($entity instanceof \ElggUser) {
            return Mapper::getUser($entity);
        }

        if ($entity->container_guid) {
            $container = get_entity($entity->container_guid);
            if ($container instanceof \ElggGroup && $container->membership === ACCESS_PRIVATE && Helpers::getGroupMembership($container) != 'joined') {
                $user = elgg_get_logged_in_user_entity();
                if (!$user->isAdmin()) {
                    throw new Exception("not_member_of_closed_group");
                }
            }
        }

        if ($entity instanceof \ElggObject) {
            Helpers::addView($entity);

            $subtype = $entity->getSubtype();
            switch ($subtype) {
                case "wiki":
                    return Mapper::getWiki($entity);
                case "page":
                case "static":
                    return Mapper::getPage($entity);
                case "page_widget":
                    return Mapper::getWidget($entity);
                default:
                    return Mapper::getObject($entity);
            }
        }
    }

    static function canWriteToContainer($a, $args, $c) {
        if ($args["containerGuid"]) {
            $container = get_entity($args["containerGuid"]);
            if (!$container) {
                throw new Exception("could_not_find");
            }

            return $container->canWriteToContainer(0, $args["type"], $args["subtype"]);
        }

        $user = elgg_get_logged_in_user_entity();
        if (!$user) {
            return false;
        }

        return $user->canWriteToContainer(0, $args["type"], $args["subtype"]);
    }

    static function countComments($object) {
        $total_comments = elgg_get_entities([
            "type" => "object",
            "subtypes" => ["comment", "answer"],
            "container_guid" => (int) $object["guid"],
            "count" => true
        ]);

        $total_annotations = elgg_get_annotations([
            "guid" => (int) $object["guid"],
            "annotation_names" => ["group_topic_post", "generic_comment"],
            "count" => true
        ]);

        return $total_comments + $total_annotations;
    }

    static function getGroupMemberCount($group) {
        $options = [
            "relationship" => "member",
            "relationship_guid" => $group['guid'],
            "inverse_relationship" => true,
            "wheres" => [],
            "type" => "user",
            "count" => true
        ];

        return elgg_get_entities_from_relationship($options);
    }

    static function getGroupMembers($a, $args, $c) {
        $dbprefix = elgg_get_config("dbprefix");
        $group = get_entity($a["guid"]);

        if (!$group || !$group instanceof \ElggGroup) {
            throw new Exception("could_not_find");
        }

        $offset = (int) $args["offset"] ? (int) $args["offset"] : 0;
        $limit = (int) $args["limit"] ? (int) $args["limit"] : 10;

        $inSubgroupId = (int) $args["inSubgroupId"];
        $notInSubgroupId = (int) $args["notInSubgroupId"];

        $admins = elgg_get_entities_from_relationship([
            "relationship" => "group_admin",
            "relationship_guid" => $group->guid,
            "inverse_relationship" => true,
            "type" => "user",
            "limit" => 0
        ]);
        $admin_guids = array($group->owner_guid);
        foreach ($admins as $admin) {
            array_push($admin_guids, $admin->guid);
        }


        $guids = implode(", ", $admin_guids);
        $options = [
            "relationship" => "member",
            "relationship_guid" => $group->guid,
            "inverse_relationship" => true,
            "joins" => ["JOIN {$dbprefix}users_entity ue ON e.guid = ue.guid"],
            "order_by" => "FIELD(e.guid, {$guids}) desc, ue.name",
            "wheres" => [],
            "type" => "user",
            "limit" => $limit,
            "offset" => $offset
        ];

        if ($inSubgroupId) {
            $member_guids = get_members_of_access_collection($inSubgroupId, true);
            if ($member_guids) {
                $set = "(" . implode(',', $member_guids) . ")";
                $options["wheres"][] = "e.guid IN {$set}";
            } else {
                $options["wheres"][] = "FALSE";
            }
        }

        if ($notInSubgroupId) {
            $member_guids = get_members_of_access_collection($notInSubgroupId, true);
            if ($member_guids) {
                $set = "(" . implode(',', $member_guids) . ")";
                $options["wheres"][] = "e.guid NOT IN {$set}";
            }
        }

        if ($args["q"]) {
            $q = sanitize_string($args["q"]);
            $options["wheres"][] = "ue.name LIKE '%{$q}%'";
        }

        $total = elgg_get_entities_from_relationship(array_merge(
            $options,
            ["count" => true]
        ));

        if ($group->membership === ACCESS_PRIVATE && !$group->canEdit() && !$group->isMember())  {
            return [
                "total" => $total,
                "edges" => []
            ];
        }

        $members = [];
        foreach (elgg_get_entities_from_relationship($options) as $member) {
            if ($member->guid == $group->owner_guid) {
                $role = "owner";
            } else if (in_array($member, $admins)) {
                $role = "admin";
            } else {
                $role = "member";
            }

            $members[] = [
                "role" => $role,
                "user" => Mapper::getUser($member)
            ];
        }

        return [
            "total" => $total,
            "edges" => $members
        ];
    }

    static function getAttendees($a, $args, $c) {
        $user = elgg_get_logged_in_user_entity();
        $entity = get_entity($a["guid"]);
        if (!$entity || !$entity->getSubtype() === "event" || !$user) {
            return [
                "total" => 0,
                "totalMaybe" => 0,
                "totalReject" => 0,
                "edges" => []
            ];
        }

        switch ($args["state"]) {
            case "accept":
                $relationship = "event_attending";
                break;
            case "maybe":
                $relationship = "event_maybe";
                break;
            case "reject":
                $relationship = "event_reject";
                break;
            default:
                $relationship = "event_attending";
                break;
        }

        $options = [
            "type" => "user",
            "relationship" => $relationship,
            "relationship_guid" => $entity->guid,
            "offset" => (int) $args["offset"],
            "limit" => (int) $args["limit"]
        ];

        $totalMaybe = elgg_get_entities_from_relationship(array_merge($options, ["relationship" => "event_maybe", "count" => true]));
        $totalReject = elgg_get_entities_from_relationship(array_merge($options, ["relationship" => "event_reject", "count" => true]));

        $attendees = elgg_get_entities_from_relationship($options);

        $options = [
            "relationship" => "event_attending",
            "relationship_guid" => $entity->guid,
            "limit" => false
        ];

        $total = elgg_get_entities_from_relationship(array_merge($options, ["count" => true]));

        $edges = [];
        foreach ($attendees as $attendee) {
            $edges[] = Mapper::getUser($attendee);
        }

        return [
            "total" => $total,
            "totalMaybe" => $totalMaybe,
            "totalReject" => $totalReject,
            "edges" => $edges
        ];
    }

    static function getAttending($a, $args, $c) {
        $entity = get_entity($a["guid"]);
        if (!$entity || !$entity->getSubtype() === "event") {
            return null;
        }

        $user = elgg_get_logged_in_user_entity();
        if (!$user) {
            return null;
        }

        if (check_entity_relationship($entity->guid, "event_attending", $user->guid)) {
            return "accept";
        }

        if (check_entity_relationship($entity->guid, "event_maybe", $user->guid)) {
            return "maybe";
        }

        if (check_entity_relationship($entity->guid, "event_reject", $user->guid)) {
            return "reject";
        }
    }

    static function getAttendeesWithoutAccount($entity) {
        $user = elgg_get_logged_in_user_entity();

        $entity = get_entity($entity["guid"]);
        if (!$entity || !$entity->getSubtype() === "event" || !$user) {
            return null;
        }
        $options = [
            "relationship" => "event_attending",
            "relationship_guid" => $entity->guid,
            "limit" => false
        ];

        $total = elgg_get_entities_from_relationship(array_merge($options, ["count" => true]));

        $options = [
            "type" => "user",
            "relationship" => "event_attending",
            "relationship_guid" => $entity->guid,
            "limit" => false
        ];

        $total_users = elgg_get_entities_from_relationship(array_merge($options, ["count" => true]));

        $total_without_account = $total - $total_users;

        return $total_without_account;
    }

    static function getInvite($a, $args, $c) {
        $dbprefix = elgg_get_config("dbprefix");
        $site = elgg_get_site_entity();

        $offset = (int) $args["offset"] || 0;
        $limit = (int) $args["limit"] || 10;

        $group = get_entity($a["guid"]);

        if (!$group->canEdit()) {
            return [ "total" => 0, "edges" => [] ];
        }

        $options = [
            "type" => "user",
            "site_guids" => false,
            "relationship" => "member_of_site",
            "relationship_guid" => $site->guid,
            "inverse_relationship" => true
        ];

        if ($args["q"]) {
            $q = sanitize_string($args["q"]);
            $options["joins"] = "JOIN {$dbprefix}users_entity ue ON e.guid = ue.guid";
            $options["wheres"] = "ue.name LIKE '%{$q}%'";
        }

        $total = elgg_get_entities_from_relationship(array_merge($options, ["count" => true]));
        $users = elgg_get_entities_from_relationship($options);

        $invite = [];
        foreach ($users as $user) {
            $invite[] = [
                "invited" => check_entity_relationship($group->guid, "invited", $user->guid),
                "user" => Mapper::getUser($user)
            ];
        }

        return [
            "total" => $total,
            "edges" => $invite
        ];
    }

    static function getInvited($a, $args, $c) {
        $dbprefix = elgg_get_config("dbprefix");
        $site = elgg_get_site_entity();

        $offset = (int) $args["offset"] || 0;
        $limit = (int) $args["limit"] || 10;

        $group = get_entity($a["guid"]);

        if (!$group || !$group->canEdit()) {
            return [ "total" => 0, "edges" => [] ];
        }

        $options = [
            "guid" => $group->guid,
            "annotation_names" => "email_invitation",
            "offset" => $args["offset"] ? (int) $args["offset"] : 0,
            "limit" => $args["limit"] ? (int) $args["limit"] : 0,
            "order_by" => "id DESC"
        ];

        $total = elgg_get_annotations(array_merge($options, ["count" => true]));

        $invites = [];
        foreach (elgg_get_annotations($options) as $invite) {
            $code = explode("|", $invite->value);

            $user = get_user_by_email($code[1]);
            if ($user) {
                $user = Mapper::getUser($user[0]);
                $email = null;
            } else {
                $user = null;
                $email = $code[1];
            }

            $invites[] = [
                "id" => $invite->id,
                "invited" => true,
                "timeCreated" => date("c", $invite->time_created),
                "user" => $user,
                "email" => $email
            ];
        }

        return [
            "total" => $total,
            "edges" => $invites
        ];
    }

    static function getMembershipRequests($a, $args, $c) {
        $group = get_entity($a["guid"]);

        if (!$group || !$group->canEdit()) {
            return [ "total" => 0, "edges" => [] ];
        }

        $options = [
            "type" => "user",
            "relationship_guid" => $group->guid,
            "relationship" => "membership_request",
            "inverse_relationship" => true
        ];

        $edges = [];
        foreach (elgg_get_entities_from_relationship($options) as $user) {
            $edges[] = Mapper::getUser($user);
       }

        return [
            "total" => elgg_get_entities_from_relationship(array_merge($options, ["count" => true])),
            "edges" => $edges
        ];
    }

    static function getComments($object) {
        $entities = elgg_get_entities([
            "type" => "object",
            "subtypes" => ["comment", "answer"],
            "container_guid" => (int) $object["guid"],
            "limit" => 0
        ]);

        $annotations = elgg_get_annotations([
            "guid" => (int) $object["guid"],
            "annotation_names" => ["group_topic_post", "generic_comment"],
            "limit" => 0
        ]);

        if (!$entities) {
            $entities = [];
        }

        if (!$annotations) {
            $annotations = [];
        }

        $comments = array_merge($entities, $annotations);

        usort($comments, function($a, $b) {
            return ($a->time_created > $b->time_created) ? -1 : 1;
        });

        $mapped_comments = [];
        foreach ($comments as $comment) {
            $mapped_comment =  Mapper::getComment($comment);
            if (Resolver::isBestAnswer($comment)) {
                array_unshift($mapped_comments, $mapped_comment);
            } else {
                $mapped_comments[] = $mapped_comment;
            }
        }

        return $mapped_comments;
    }

    static function getRows($entity) {
        $options = [
            "container_guid" => (int) $entity["guid"],
            "type" => "object",
            "subtype" => "row",
            "limit" => false
        ];

        $rows = [];
        foreach (elgg_get_entities($options) as $row) {
            $rows[] = Mapper::getRow($row);
        }

        return $rows;
    }

    static function getColumns($entity) {
        $options = [
            "container_guid" => (int) $entity["guid"],
            "type" => "object",
            "subtype" => "column",
            "limit" => false
        ];

        $columns = [];
        foreach (elgg_get_entities($options) as $column) {
            $columns[] = Mapper::getColumn($column);
        }

        return $columns;
    }

    static function getWidgets($entity) {
        $options = array(
            "container_guid" => (int) $entity["guid"],
            "type" => "object",
            "subtype" => "page_widget",
            "limit" => false
        );

        $widgets = [];
        foreach (elgg_get_entities($options) as $widget) {
            $widgets[] = Mapper::getWidget($widget);
        }

        return $widgets;
    }

    static function getUser($guid) {
        $user = get_entity($guid);
        if (!$user) {
            return [
                "guid" => 0,
                "name" => "Unknown user"
            ];
        }

        return [
            "guid" => $user->guid,
            "username" => $user->username,
            "name" => $user->name,
            "icon" => $user->getIconURL("large"),
            "url" => Helpers::getURL($user),
            "requestDelete" => $user->requestDelete ? true : false
        ];
    }

    static function getStats($user) {
        $user = get_entity($user["guid"]);
        if (!$user || !$user instanceof \ElggUser) {
            return [];
        }

        $answers = elgg_get_entities([
            "type" => "object",
            "subtype" => "comment",
            "owner_guid" => $user->guid,
            "count" => true
        ]);

        $upvotes = elgg_get_annotations([
            "annotation_name" => "vote",
            "annotation_value" => 1,
            "annotation_owner_guid" => $user->guid,
            "count" => true
        ]);

        $downvotes = elgg_get_annotations([
            "annotation_name" => "vote",
            "annotation_value" => -1,
            "annotation_owner_guid" => $user->guid,
            "count" => true
        ]);

        $items = [
            [
                "key" => "answers",
                "name" => "Antwoorden",
                "value" => $answers ? $answers : 0
            ],
            [
                "key" => "upvotes",
                "name" => "Stemmen omhoog",
                "value" => $upvotes ? $upvotes : 0
            ],
            [
                "key" => "downvotes",
                "name" => "Stemmen omlaag",
                "value" => $downvotes ? $downvotes : 0
            ]
        ];

        return $items;
    }

    static function getUserFieldsInOverview($user) {
        $user = get_entity($user["guid"]);

        $site = elgg_get_site_entity();

        if (!$user || !$user instanceof \ElggUser) {
            return [];
        }

        $profileFields = elgg_get_plugin_setting("profile", "pleio_template") ? json_decode(html_entity_decode(elgg_get_plugin_setting("profile", "pleio_template"), ENT_COMPAT | ENT_QUOTES, 'UTF-8'), true) : [];

        $fieldsInOverview = Helpers::getFieldsInOverview($profileFields);

        $raw_results = elgg_get_metadata([
            "guid" => $user->guid,
            "site_guids" => $site->guid,
            "limit" => 50,
            "metadata_names" => array_map(function($f) { return $f["key"]; }, $fieldsInOverview)
        ]);


        $metadata_by_key = [];
        foreach ($raw_results as $result) {
            if (array_key_exists($result["name"], $metadata_by_key)) {
                if (!is_array($metadata_by_key[$result["name"]])) {
                    $metadata_by_key[$result["name"]] = [$metadata_by_key[$result["name"]], $result];
                } else {
                    array_push($metadata_by_key[$result["name"]], $result);
                }
            } else {
                $metadata_by_key[$result["name"]] = $result;
            }
        }

        $result = [];
        foreach ($fieldsInOverview as $item) {
            if (isset($metadata_by_key[$item["key"]])) {
                if (is_array($metadata_by_key[$item["key"]])) {
                    // return multi value fields comma separated
                    $str = implode(',', array_map(function($item) {
                        return $item->value;
                    }, $metadata_by_key[$item["key"]]));

                    $item["value"] = $str;
                } else {
                    $item["value"] = $metadata_by_key[$item["key"]]->value;
                }
            } else {
                $item["value"] = null;
            }

            $result[] = $item;
        }

        return $result;
    }

    static function getProfile($user) {
        $user = get_entity($user["guid"]);

        $site = elgg_get_site_entity();

        if (!$user || !$user instanceof \ElggUser) {
            return [];
        }

        $profileFields = elgg_get_plugin_setting("profile", "pleio_template") ? json_decode(html_entity_decode(elgg_get_plugin_setting("profile", "pleio_template"), ENT_COMPAT | ENT_QUOTES, 'UTF-8'), true) : [];

        $raw_results = elgg_get_metadata([
            "guid" => $user->guid,
            "site_guids" => $site->guid,
            "limit" => 50,
            "metadata_names" => array_map(function($f) { return $f["key"]; }, $profileFields)
        ]);

        $metadata_by_key = [];
        foreach ($raw_results as $result) {
            if (array_key_exists($result["name"], $metadata_by_key)) {
                if (!is_array($metadata_by_key[$result["name"]])) {
                    $metadata_by_key[$result["name"]] = [$metadata_by_key[$result["name"]], $result];
                } else {
                    array_push($metadata_by_key[$result["name"]], $result);
                }
            } else {
                $metadata_by_key[$result["name"]] = $result;
            }
        }

        if (!isset($metadata_by_key["emailaddress"]) && $user->canEdit()) {
            $emailaddress = new \stdClass();
            $emailaddress->name = "emailaddress";
            $emailaddress->value = $user->email;
            $emailaddress->access_id = ACCESS_PRIVATE;
            $metadata_by_key["emailaddress"] = $emailaddress;
        }

        if (!isset($metadata_by_key["aboutme"]) && $user->canEdit()) {
            $aboutme = new \stdClass();
            $aboutme->name = "aboutme";
            $aboutme->value = $user->description;
            $aboutme->access_id = ACCESS_PRIVATE;
            $metadata_by_key["aboutme"] = $aboutme;
        }

        $result = [];
        foreach ($profileFields as $item) {
            if (isset($metadata_by_key[$item["key"]])) {
                if (is_array($metadata_by_key[$item["key"]])) {
                    // return multi value fields comma separated
                    $str = implode(',', array_map(function($item) {
                        return $item->value;
                    }, $metadata_by_key[$item["key"]]));

                    $item["value"] = $str;
                    $item["accessId"] = $metadata_by_key[$item["key"]][0]->access_id;
                } else {
                    $item["value"] = $metadata_by_key[$item["key"]]->value;
                    $item["accessId"] = $metadata_by_key[$item["key"]]->access_id;
                }
            } else {
                $item["value"] = null;
                $item["accessId"] = 1;
            }

            $item["category"] = Helpers::getProfileCategory($item['key']);
            $item["isEditable"] = Helpers::isProfileFieldEditable($item['key']);
            $item["fieldType"] = Helpers::getProfileFieldType($item['key']);
            $item["fieldOptions"] = Helpers::getProfileFieldOptions($item['key']);

            $result[] = $item;
        }

        return $result;
    }

    static function getGroupNotificationsForUser($user) {
        $loggedInUser = elgg_get_logged_in_user_entity();

        $user = get_entity($user["guid"]);
        $site = elgg_get_site_entity();

        if (!$user || !$user instanceof \ElggUser) {
            return [];
        }

        // only return if currentUser is admin or self!
        if ($user->guid !== $loggedInUser->guid && !$loggedInUser->isAdmin()) {
            return [];
        }

        $options = [
            "type" => "group",
            "relationship_guid" => $user->guid,
            "relationship" => "member",
            "limit" => 0
        ];

        $groups = [];

        foreach (elgg_get_entities_from_relationship($options) as $group) {
            $groups[] = [
                "guid" => $group->guid,
                "name" => $group->name,
                "getsNotifications" => check_entity_relationship($user->guid, "subscribed", $group->guid) ? true : false,
            ];
        }

        return $groups;
    }

    static function getGroups($a, $args, $c) {
        $dbprefix = elgg_get_config("dbprefix");
        $user = elgg_get_logged_in_user_entity();

        $options = [
            "type" => "group",
            "limit" => (int) $args["limit"],
            "offset" => (int) $args["offset"],
            "joins" => [],
            "wheres" => []
        ];

        $msid = get_metastring_id("isFeatured");
        if ($msid) {
            $options["joins"][] = "LEFT JOIN {$dbprefix}metadata mdf INNER JOIN {$dbprefix}metastrings ms on mdf.value_id = ms.id ON e.guid = mdf.entity_guid AND mdf.name_id = {$msid}";
            $options["order_by"] = "ms.string DESC, ge.name";
        } else {
            $options["order_by"] = "ge.name";
        }

        if ($args["tags"]) {
            list ($tag_joins, $tag_wheres, $group_by) = Helpers::getTagFilterJoin($args["tags"]);

            $options["joins"] = array_merge($options["joins"] , $tag_joins);
            $options["wheres"] = array_merge($options["wheres"], $tag_wheres);
            $options["group_by"] = $group_by;
        }

        if ($args["q"]) {
            $q = sanitize_string($args["q"]);
            $ampersant_string = "";
            if (strpos($q, '&') !== false) {
                $q_replaced = str_replace('&', '&amp;', $q);
                $ampersant_string = " OR ge.name LIKE '%{$q_replaced}%' OR ge.description LIKE '%{$q_replaced}%'";
            }
            $options["wheres"][] = "(ge.name LIKE '%{$q}%' OR ge.description LIKE '%{$q}%'" .  $ampersant_string . ")";
        }

        if ($user && $args["filter"] === "mine") {
            $options["relationship"] = "member";
            $options["relationship_guid"] = $user->guid;
            $options["joins"][] = "JOIN {$dbprefix}groups_entity ge ON e.guid = ge.guid";

            $total = elgg_get_entities_from_relationship(array_merge($options, array( "count" => true )));
            $entities = elgg_get_entities_from_relationship($options);
        } else {
            $options["joins"][] = "JOIN {$dbprefix}groups_entity ge ON e.guid = ge.guid";

            $total = elgg_get_entities(array_merge($options, array( "count" => true )));
            $entities = elgg_get_entities($options);
        }


        $edges = [];
        foreach ($entities as $entity) {
            $edges[] = Mapper::getGroup($entity);
        }

        return [
            "total" => $total,
            "edges" => $edges
        ];
    }

    static function getEvents($a, $args, $c) {
        $user = elgg_get_logged_in_user_entity();

        $options = array(
            "type" => "object",
            "subtype" => "event",
            "limit" => (int) $args["limit"],
            "offset" => (int) $args["offset"]
        );

        if ($args["containerGuid"]) {
            if ($args["containerGuid"] == "1") {
                $container = elgg_get_site_entity();
            } else {
                $container = get_entity($args["containerGuid"]);
            }
        }

        if ($container) {
            $options["container_guid"] = $container->guid;
        }

        if (get_metastring_id("end_ts")) {
            $msid = get_metastring_id("end_ts");
        } else {
            $msid = get_metastring_id("start_day");
        }

        if ($msid) {
            $dbprefix = elgg_get_config("dbprefix");

            $options["joins"] = [
                "JOIN {$dbprefix}metadata md ON e.guid = md.entity_guid",
                "JOIN {$dbprefix}metastrings msv ON md.value_id = msv.id",
                "JOIN {$dbprefix}objects_entity oe ON e.guid = oe.guid"
            ];

            $today = strtotime('23:59:59');
            $yesterday = strtotime('-1 day', $today);

            switch ($args["filter"]) {
                case "previous":
                    $options["wheres"] = [
                        "md.name_id = {$msid}",
                        "msv.string <= $yesterday"
                    ];
                    $options["order_by"] = "msv.string DESC, oe.title";
                    break;
                case "upcoming":
                default:
                    $options["wheres"] = [
                        "md.name_id = {$msid}",
                        "msv.string > $yesterday"
                    ];
                    $options["order_by"] = "msv.string ASC, oe.title";
                    break;
            }
        }

        $total = elgg_get_entities(array_merge($options, array( "count" => true )));

        $edges = [];
        foreach (elgg_get_entities($options) as $entity) {
            $edges[] = Mapper::getObject($entity);
        }

        return [
            "total" => $total,
            "edges" => $edges
        ];
    }

    static function getEntities($a, $args, $c) {
        $dbprefix = elgg_get_config("dbprefix");

        if (!$args["type"] || !in_array($args["type"], ["group", "object"])) {
            $type = "object";
        } else {
            $type = $args["type"];
        }

        if ($type == "object") {
            if ($args["subtypes"]) {
                $subtypes = $args["subtypes"];
            } elseif (!$args["subtype"] || $args["subtype"] == "all") {
                $subtypes = ["blog", "news", "question", "discussion", "groupforumtopic"];
            } elseif ($args["subtype"] === "file|folder") {
                $subtypes = ["file", "folder"];
            } elseif ($args["subtype"] === "discussion") {
                $subtypes = ["discussion", "groupforumtopic"];
            } elseif ($args["subtype"] === "page") {
                $subtypes = ["page", "static"];
            } elseif ($args["subtype"]) {
                $subtypes = $args["subtype"];
            }
        }

        $tags = $args["tags"];
        $tag_lists = $args["tagLists"];

        $user = elgg_get_logged_in_user_entity();
        if ($user && $tags == ["mine"]) {
            if ($user->tags) {
                if (is_array($user->tags)) {
                    $tags = $user->tags;
                } else {
                    $tags = [$user->tags];
                }
            } else {
                $tags = [];
            }
        }

        list ($tag_joins, $tag_wheres, $group_by) = Helpers::getTagFilterJoin($tags);

        $joins = array_merge($tag_joins);
        $wheres = array_merge($tag_wheres);

        $options = [
            "type" => $type,
            "subtypes" => $subtypes,
            "offset" => (int) $args["offset"],
            "limit" => (int) $args["limit"],
            "joins" => $joins ? $joins : [],
            "wheres" => $wheres,
            "group_by" => $group_by
        ];

        $options["metadata_name_value_pairs"] = [];

        if ($tag_lists) {
            foreach ($tag_lists as $tags) {
                if (is_array($tags)) {
                    $options["metadata_name_value_pairs"][] = [
                        "name" => "tags",
                        "value" => $tags
                    ];
                }
            }
        }

        if ($args["isFeatured"]) {
            $options["metadata_name_value_pairs"][] = ["name" => "isFeatured", "value" => "1"];
        }

        if ($args["containerGuid"]) {
            if ($args["containerGuid"] == "1") {
                $container = elgg_get_site_entity();
            } else {
                $container = get_entity($args["containerGuid"]);
            }
        }

        if ($container) {
            $options["container_guid"] = $container->guid;
        }

        switch ($args["orderDirection"]) {
            case "asc":
                $direction = "ASC";
                break;
            default:
                $direction = "DESC";
                break;
        }

        switch ($args["orderBy"]) {
            case "timeCreated":
                $orderBy = "e.time_created {$direction}";
                break;
            case "timeUpdated":
                $orderBy = "e.time_updated {$direction}";
                break;
            case "lastAction":
                $orderBy = "e.last_action {$direction}";
                break;
            case "title":
                $options["joins"][] = "JOIN {$dbprefix}objects_entity oe ON e.guid = oe.guid";
                $orderBy = "oe.title {$direction}";
        }

        if (isset($orderBy)) {
            $options["order_by"] = $orderBy;
        }

        if ($type == "object" && $args["subtype"] == "page" && $args["orderBy"] != "title") {
            $options["joins"][] = "JOIN {$dbprefix}objects_entity oe ON e.guid = oe.guid";
            $options["order_by"] = "oe.title";
        }

        $total = elgg_get_entities_from_metadata(array_merge($options, ["count" => true]));
        $entities = elgg_get_entities_from_metadata($options);

        $result = [
            "total" => $total,
            "entities" => []
        ];

        foreach ($entities as $entity) {
            $result["entities"][] = Mapper::getEntity($entity);
        }

        $user = elgg_get_logged_in_user_entity();

        return [
            "total" => $result["total"],
            "edges" => $result["entities"]
        ];
    }

    static function getFiles($a, $args, $c) {
        $container = get_entity($args["containerGuid"]);
        if ($container) {
            list($total, $entities) = Helpers::getFolderContents($container, $args["limit"], $args["offset"], $args["orderBy"], $args["orderDirection"], $args["filter"]);

            $edges = [];
            if ($entities) {
                foreach ($entities as $entity) {
                    $edges[] = Mapper::getObject($entity);
                }
            }
        } else {
            $total = 0;
            $edges = [];
        }

        $user = elgg_get_logged_in_user_entity();

        return [
            "total" => $total,
            "edges" => $edges
        ];
    }

    static function emailOverview($user) {
        $site = elgg_get_site_entity();

        $user = get_entity($user["guid"]);
        if (!$user || !$user instanceof \ElggUser) {
            return [
                "frequency" => null,
                "tags" => null
            ];
        }

        if (!$user->canEdit()) {
            return [
                "frequency" => null,
                "tags" => null
            ];
        }

        $frequency = $user->getPrivateSetting("email_overview_{$site->guid}");

        if (!$frequency || !in_array($frequency, ["daily", "weekly", "twoweekly", "monthly", "never"])) {
            $frequency = "never";
        }

        return [
            "frequency" => $frequency,
            "tags" => Helpers::renderArray($user->editEmailOverviewTags)
        ];
    }

    static function getEmailNotifications($user) {
        $user = get_entity($user["guid"]);
        if (!$user || !$user instanceof \ElggUser) {
            return false;
        }

        if (!$user->canEdit()) {
            return false;
        }

        $settings = get_user_notification_settings($user->guid);
        if ($settings && $settings->email) {
            return ($settings->email === "1") ? true : false;
        }

        return false;
    }

    static function getsNewsletter($user) {
        $user = get_entity($user["guid"]);
        if (!$user || !$user instanceof \ElggUser) {
            return false;
        }

        if (!$user->canEdit()) {
            return false;
        }

        $site = elgg_get_site_entity();
        return check_entity_relationship($user->guid, "subscribed", $site->guid);
    }

    static function getEmail($user) {
        $user = get_entity($user["guid"]);
        if (!$user || !$user instanceof \ElggUser) {
            return "";
        }

        if (!$user->canEdit()) {
            return "";
        }

        return $user->email;
    }

    static function isBookmarked($object) {
        if (!elgg_is_logged_in()) {
            return false;
        }

        $user = elgg_get_logged_in_user_entity();
        if (check_entity_relationship($user->guid, "bookmarked", $object['guid'])) {
            return true;
        }

        return false;
    }

    static function isFollowing($object) {
        if (!elgg_is_logged_in()) {
            return false;
        }

        $user = elgg_get_logged_in_user_entity();
        if (check_entity_relationship($user->guid, "content_subscription", $object['guid'])) {
            return true;
        }

        return false;
    }

    static function isRecommended($object) {
        $object = get_entity($object["guid"]);
        return $object->isRecommended && ($object->isRecommended == 1) ? true : false;
    }

    static function canBookmark($object) {
        if (elgg_is_logged_in()) {
            return true;
        }

        if ($object->isClosed) {
            return false;
        }

        return false;
    }

    static function canComment($object) {
        if ($object->isClosed) {
            return false;
        }

        if (elgg_is_logged_in()) {
            return true;
        }

        return false;
    }

    static function canVote($object) {
        if ($object->isClosed) {
            return false;
        }

        if (elgg_is_logged_in()) {
            return true;
        }

        return false;
    }

    static function search($a, $args, $c) {
        $es = \ESInterface::get();

        if (!$args["q"]) {
            return [
                "total" => 0,
                "totals" => [],
                "edges" => []
            ];
        }

        $results = array();

        $search_only_in_subtypes = ['user', 'group', 'file', 'folder', 'blog', 'discussion', 'event', 'news', 'question', 'wiki', 'page'];

        if ($args["subtype"] && !in_array($args["subtype"], $search_only_in_subtypes)) {
            throw new Exception("search_invalid_subtype");
        }

        if (!elgg_is_logged_in()) {
            $type = "object"; // only search for type object when not logged in!
        } else {
            $type = $args["type"];
        }

        $dateFilters = [];

        if ($args['dateFrom']) {
            $dateFilters['dateFrom'] = strtotime($args['dateFrom']);
        }

        if ($args['dateTo']) {
            $dateFilters['dateTo'] = strtotime($args['dateTo']);
        }

        $orderBy = "";

        if ($args["orderBy"]) {
            // map to elasticsearch fields
            switch($args["orderBy"]) {
                case "title":
                    $orderBy = "title.raw"; // or it will used the analyzed data
                break;
                case "timeCreated":
                    $orderBy = "time_created";
                break;
                case "timeUpdated":
                    $orderBy = "time_updated";
                break;
            }
        }

        $es_results = $es->search(
            $args["q"],
            null,
            $type ?: null,
            $args["subtype"] ?: $search_only_in_subtypes,
            $args["limit"],
            $args["offset"],
            $orderBy,
            $args["orderDirection"],
            $args["containerGuid"],
            [], // profileFields
            0, // accessId
            $dateFilters
        );


        foreach ($es_results["hits"] as $hit) {
            $results[] = Mapper::getEntity($hit);
        }

        $searchTotals = [];

        $type_totals = null;
        if (!elgg_is_logged_in()) {
            $type_totals = 'object';
        }
        $totals = $es->search(
            $args["q"],
            null,
            $type_totals,
            $search_only_in_subtypes,
            null,
            null,
            "",
            "",
            $args["containerGuid"],
            [], // profileFields
            0, // accessId
            $dateFilters
        );

        foreach ($totals["count_per_type"] as $type => $total) {
            if ($type === "object") {
                continue;
            }

            $searchTotals[] = [
                "subtype" => $type,
                "total" => $total
            ];
        }

        foreach ($totals["count_per_subtype"] as $subtype => $total) {
            $searchTotals[] = [
                "subtype" => $subtype,
                "total" => $total
            ];
        }

        return [
            "total" => $es_results["count"],
            "totals" => $searchTotals,
            "edges" => $results
        ];
    }

    static function getFile($a, $args, $c) {
        $guid = (int) $args["guid"];
        $file = get_entity($guid);

        return Mapper::getFile($file);
    }

    static function getViewer() {
        $user = elgg_get_logged_in_user_entity();
        if ($user) {
            $tags = $user->tags;
        } else {
            $tags = [];
        }

        return [
            "guid" => 0,
            "loggedIn" => elgg_is_logged_in(),
            "isBanned" => isset($_SESSION['pleio_user_is_banned']),
            "isSubEditor" => (($user && $user->isAdmin()) || pleio_template_is_subeditor($user)),
            "isAdmin" => $user ? $user->isAdmin() : false,
            "tags" => $tags
        ];
    }

    static function getViews($entity) {
        $dbprefix = elgg_get_config("dbprefix");
        $guid = (int) $entity["guid"];
        $result = get_data_row("SELECT views FROM {$dbprefix}entity_views WHERE guid={$guid}");

        if ($result) {
            return $result->views;
        }

        return 0;
    }

    static function getVotes($entity) {
        $result = elgg_get_annotations(array(
            "guid" => (int) $entity["guid"],
            "annotation_name" => "vote",
            "annotation_calculation" => "sum",
            "limit" => false
        ));

        if ($result) {
            return (int) $result;
        }

        return 0;
    }

    static function hasVoted($entity) {
        if (!elgg_is_logged_in()) {
            return false;
        }

        $user = elgg_get_logged_in_user_entity();

        $past_vote = elgg_get_annotations(array(
            "guid" => (int) $entity["guid"],
            "annotation_name" => "vote",
            "annotation_owner_guid" => (int) $user->guid,
            "limit" => 1
        ));

        if ($past_vote) {
            return true;
        }

        return false;
    }

    static function getBookmarks($a, $args, $c) {
        $user = elgg_get_logged_in_user_entity();
        $tags = $args["tags"];
        $subtype = $args["subtype"];

        if ($user) {
            $options = [
                "relationship_guid" => $user->guid,
                "relationship" => "bookmarked",
                "offset" => (int) $args["offset"],
                "limit" => (int) $args["limit"],
                "order_by" => "r.id DESC"
            ];

            if ($subtype && in_array($subtype, ["news", "question", "blog", "discussion", "thewire", "wiki", "event"])) {
                $options["type"] = "object";
                $options["subtype"] = $subtype;
            }

            // @todo: Elgg will generate a query that will definately not scale for large amounts of items.
            // Think we will need a seperate table to speed up tag matching
            if ($tags) {
                $options["metadata_name_value_pairs"] = [];
                foreach ($tags as $tag) {
                    $options["metadata_name_value_pairs"][] = [
                        "name" => "tags",
                        "value" => $tag
                    ];
                }
            }

            $total = elgg_get_entities_from_relationship(array_merge($options, ["count" => true]));

            $entities = [];
            foreach (elgg_get_entities_from_relationship($options) as $entity) {
                $entities[] = Mapper::getObject($entity);
            }

        } else {
            $total = 0;
            $entities = [];
        }

        return [
            "total" => $total,
            "edges" => $entities
        ];
    }

    static function inGroup($object) {
        $entity = get_entity($object["guid"]);
        if (!$entity) {
            return false;
        }

        if ($entity->getContainerEntity() instanceof \ElggGroup) {
            return true;
        } else if ($entity->getContainerEntity() instanceof \ElggSite) {
            return false;
        } else {
            return Resolver::inGroup($entity->getContainerEntity());
        }
    }

    static function getGroup($object) {
        $entity = get_entity($object["guid"]);
        if (!$entity) {
            return null;
        }

        $container = $entity->getContainerEntity();

        if ($container instanceof \ElggGroup) {
            return $container;
        } else if ($container instanceof \ElggSite) {
            return null;
        } else {
            return Resolver::getGroup($container);
        }
    }

    static function getBreadcrumb($a, $args, $c) {
        if (!$args["guid"]) {
            return [];
        }

        $entity = get_entity($args["guid"]);
        if (!$entity) {
            return [];
        }

        return Helpers::getBreadcrumb($entity);
    }

    static function hasChildren($entity) {
        $options = [
            "type" => "object",
            "subtype" => $entity->subtype,
            "container_guid" => $entity["guid"],
            "count" => true
        ];

        $result = elgg_get_entities($options);

        if ($result !== 0) {
            return true;
        }

        return false;
    }

    static function getSubgroups($group) {
        $group = get_entity($group["guid"]);
        if (!$group || !$group->canEdit()) {
            return [
                "total" => 0,
                "edges" => []
            ];
        }

        if (!$group->subpermissions) {
            return [
                "total" => 0,
                "edges" => []
            ];
        }

        $subgroups = [];
        foreach (unserialize($group->subpermissions) as $subpermission) {
            $access_collection = get_access_collection($subpermission);
            if (!$access_collection) {
                continue;
            }

            $members = [];
            foreach (get_members_of_access_collection($access_collection->id) as $member) {
                $members[] = Mapper::getUser($member);
            }

            $subgroups[] = [
                "id" => $access_collection->id,
                "name" => $access_collection->name,
                "members" => $members
            ];
        }

        usort($subgroups, function($a, $b) {
            return ($a["name"] > $b["name"]) ? -1 : 1;
        });

        return [
            "total" => count($subgroups),
            "edges" => $subgroups
        ];
    }

    static function getsNotifications($group) {
        $user = elgg_get_logged_in_user_entity();

        if (!$user) {
            return false;
        }

        $group = get_entity($group["guid"]);
        if (!$group) {
            return false;
        }

        return check_entity_relationship($user->guid, "subscribed", $group->guid) ? true : false;
    }

    static function isBestAnswer($entity) {
        $entity = get_entity($entity["guid"]);
        if (!$entity) {
            return false;
        }

        $question = $entity->getContainerEntity();
        if (!$question) {
            return false;
        }

        return check_entity_relationship($question->guid, "correctAnswer", $entity->guid) ? true : false;
    }

    static function canChooseBestAnswer($entity) {
        $entity = get_entity($entity["guid"]);

        if ($entity->isClosed) {
            return false;
        }
        $site = elgg_get_site_entity();
        $user = elgg_get_logged_in_user_entity();
        if (!$user) {
            return false;
        }

        $is_owner = $entity->owner_guid == $user->guid;
        $questioner_can_hoose_best_answer = (elgg_get_plugin_setting("questioner_can_choose_best_answer", "pleio_template") === "no") ? false : true;

        if ($user->isAdmin() || check_entity_relationship($user->guid, "questions_expert", $site->guid) || pleio_template_is_subeditor($user)) {
            return true;
        }
        elseif ($is_owner && $questioner_can_hoose_best_answer) {
            return true;
        }
        else {
            return false;
        }
    }

    static function getParent($object) {
        $entity = get_entity($object["guid"]);

        if (!$entity) {
            return null;
        }

        $parent = $entity->getContainerEntity();

        switch ($parent->getSubtype()) {
            case "page":
                return  Mapper::getPage($entity->getContainerEntity());
            case "wiki":
                return Mapper::getWiki($entity->getContainerEntity());
        }

        return null;
    }


    static function getParentFolder($object) {
        $entity = get_entity($object["guid"]);
        if (!$entity) {
            return null;
        }

        $options = [];
        $options['relationship'] = "folder_of";
        $options['relationship_guid'] = $entity->guid;
        $options['inverse_relationship'] = true;

        $folders = elgg_get_entities_from_relationship($options);

        if (is_array($folders) && count($folders) === 1) {
            return $folders[0];
        }

        return null;
    }

    static function getChildren($entity) {
        $entity = get_entity($entity["guid"]);
        switch ($entity->getSubtype()) {
            case "page":
            case "static":
                $subtypes = ["static", "page"];
                break;
            case "wiki":
                $subtypes = ["wiki"];
                break;
            default:
                return [];
                break;
        }

        $options = [
            "type" => "object",
            "subtypes" => $subtypes,
            "container_guid" => $entity->guid,
            "limit" => 100 // this is a safeguard
        ];

        $children = elgg_get_entities($options);
        if (!$children) {
            return [];
        }

        $children = Helpers::orderByManual($children);

        $return = [];
        foreach ($children as $child) {
            $return[] = Mapper::getPage($child);
        }

        return $return;
    }

    static function getChoices($entity) {
        $options = [
            "type" => "object",
            "subtype" => "poll_choice",
            "relationship" => "poll_choice",
            "relationship_guid" => $entity["guid"],
            "inverse_relationship" => true,
            "order_by" => "e.guid"
        ];

        $choices = [];
        foreach(elgg_get_entities_from_relationship($options) as $choice) {
            $votes = elgg_get_annotations([
                "guid" => (int) $entity["guid"],
                "annotation_name" => "vote",
                "annotation_value" => $choice->text,
                "count" => true
            ]);

            $choices[] = [
                "guid" => $choice->guid,
                "text" => $choice->text,
                "votes" => $votes
            ];
        }

        return $choices;
    }

    static function hasVotedOnPoll($entity) {
        $user = elgg_get_logged_in_user_entity();
        if (!$user) {
            return false;
        }

        $entity = get_entity($entity["guid"]);
        if (!$entity) {
            return false;
        }

        $options = [
            "annotation_name" => "vote",
            "annotation_owner_guid" => $user->guid,
            "guid" => $entity->guid,
            "limit" => 1
        ];

        return elgg_get_annotations($options) ? true : false;
    }

    static function getGroupWelcomeMessage($object) {
        $group = get_entity($object["guid"]);
        if (!$group || !$group->canEdit()) {
            return "";
        }

        $message = $group->getPrivateSetting("group_tools:welcome_message");

        return ($message) ? $message : "";
    }

    static function getActivityFilter($containeGuid = false) {
        return [
            "contentTypes" => Resolver::getActivityFilterContentTypes($containeGuid)
        ];
    }

    static function getActivityFilterContentTypes($containeGuid) {
        $dbprefix = elgg_get_config("dbprefix");

        $site = elgg_get_site_entity();

        $availableSubtypes = [
            "blog" => elgg_echo("pleio_template:subtype:blog"),
            "discussion" => elgg_echo("pleio_template:subtype:discussion"),
            "event" => elgg_echo("pleio_template:subtype:event"),
            "news" => elgg_echo("pleio_template:subtype:news"),
            "question" => elgg_echo("pleio_template:subtype:question"),
            "thewire" => elgg_echo("pleio_template:subtype:thewire"),
            "wiki" => elgg_echo("pleio_template:subtype:wiki")
        ];

        $subtypes_query = "SELECT id, subtype as name FROM elgg_entity_subtypes WHERE type = 'object' AND subtype IN (" . sprintf("'%s'", join("','", array_keys($availableSubtypes))) . ") ";

        $in_container = $containeGuid ? " AND container_guid = {$containerGuid} " : "";

        $subtypes = [];
        foreach (get_data($subtypes_query) as $type) {
            $found = get_data("SELECT guid FROM {$dbprefix}entities WHERE subtype = {$type->id} AND site_guid = {$site->guid} {$in_container} LIMIT 1");

            if (count($found) > 0) {
                $subtypes[] = ["key" => $type->name, "value" => $availableSubtypes[$type->name] ];
            }
        }

        $value = array_column($subtypes, 'value');
        array_multisort($value, SORT_ASC, $subtypes);

        return $subtypes;
    }
}
