<?php
namespace Pleio;

use GraphQL\Type\Definition\EnumType;
use GraphQL\Type\Definition\InterfaceType;
use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Schema;

class SchemaBuilder {
    static function build() {
        $registry = new TypeRegistry();

        $queryType = new ObjectType([
            "name" => "Query",
            "fields" => [
                "viewer" => [
                    "type" => $registry->get("Viewer"),
                    "resolve" => "Pleio\Resolver::getViewer"
                ],
                "entity" => [
                    "type" => $registry->get("Entity"),
                    "args" => [
                        "guid" => [ "type" => Type::string() ],
                        "username" => [ "type" => Type::string() ]
                    ],
                    "resolve" => "Pleio\Resolver::getEntity"
                ],
                "search" => [
                    "type" => $registry->get("SearchList"),
                    "args" => [
                        "q" => [ "type" => Type::nonNull(Type::string()) ],
                        "containerGuid" => [ "type" => Type::string() ],
                        "type" => [ "type" => $registry->get("Type") ],
                        "subtype" => [ "type" => Type::string() ],
                        "dateFrom" => [ "type" => Type::string() ],
                        "dateTo" => [ "type" => Type::string() ],
                        "offset" => [ "type" => Type::int() ],
                        "limit" => [ "type" => Type::int() ],
                        "tagLists" => [ "type" => Type::listOf(Type::listOf(Type::string())) ],
                        "orderBy" => [ "type" => $registry->get("SearchOrderBy") ],
                        "orderDirection" => [ "type" => $registry->get("OrderDirection") ]
                    ],
                    "resolve" => "Pleio\Resolver::search"
                ],
                "recommended" => [
                    "type" => $registry->get("EntityList"),
                    "args" => [
                        "offset" => [ "type" => Type::int() ],
                        "limit" => [ "type" => Type::int() ]
                    ],
                    "resolve" => "Pleio\Resolver::getRecommended"
                ],
                "trending" => [
                    "type" => Type::listOf($registry->get("TrendingList")),
                    "resolve" => "Pleio\Resolver::getTrending"
                ],
                "top" => [
                    "type" => Type::listOf($registry->get("TopItem")),
                    "resolve" => "Pleio\Resolver::getTop"
                ],
                "breadcrumb" => [
                    "type" => Type::listOf($registry->get("Entity")),
                    "args" => [
                        "guid" => [ "type" => Type::string() ]
                    ],
                    "resolve" => "Pleio\Resolver::getBreadcrumb"
                ],
                "files" => [
                    "type" => $registry->get("EntityList"),
                    "args" => [
                        "containerGuid" => [ "type" => Type::string() ],
                        "filter" => [ "type" => Type::string() ],
                        "orderBy" => [ "type" => Type::string() ],
                        "orderDirection" => [ "type" => Type::string() ],
                        "offset" => [ "type" => Type::int() ],
                        "limit" => [ "type" => Type::int() ]
                    ],
                    "resolve" => "Pleio\Resolver::getFiles"
                ],
                "groups" => [
                    "type" => $registry->get("GroupList"),
                    "args" => [
                        "q" => [ "type" => Type::string() ],
                        "filter" => [ "type" => $registry->get("GroupFilter") ],
                        "tags" => [ "type" => Type::listOf(Type::string()) ],
                        "offset" => [ "type" => Type::int() ],
                        "limit" => [ "type" => Type::int() ]
                    ],
                    "resolve" => "Pleio\Resolver::getGroups"
                ],
                "events" => [
                    "type" => $registry->get("EventList"),
                    "args" => [
                        "filter" => [
                            "type" => $registry->get("EventFilter")
                        ],
                        "containerGuid" => [ "type" => Type::string() ],
                        "offset" => [ "type" => Type::int() ],
                        "limit" => [ "type" => Type::int() ]
                    ],
                    "resolve" => "Pleio\Resolver::getEvents"
                ],
                "users" => [
                    "type" => $registry->get("UserList"),
                    "args" => [
                        "q" => [ "type" => Type::nonNull(Type::string()) ],
                        "offset" => [ "type" => Type::int() ],
                        "limit" => [ "type" => Type::int() ],
                        "filters" => [ "type" => Type::listOf($registry->get("FilterInput")) ]
                    ],
                    "resolve" => "Pleio\Resolver::getUsers"
                ],
                "entities" => [
                    "type" => $registry->get("EntityList"),
                    "args" => [
                        "offset" => [ "type" => Type::int() ],
                        "limit" => [ "type" => Type::int() ],
                        "type" => [ "type" => $registry->get("Type") ],
                        "subtype" => [ "type" => Type::string() ],
                        "subtypes" => [ "type" => Type::listOf(Type::string()) ],
                        "containerGuid" => [ "type" => Type::string() ],
                        "tags" => [ "type" => Type::listOf(Type::string()) ],
                        "tagLists" => [ "type" => Type::listOf(Type::listOf(Type::string())) ],
                        "orderBy" => [ "type" => $registry->get("OrderBy") ],
                        "orderDirection" => [ "type" => $registry->get("OrderDirection") ],
                        "isFeatured" => [ "type" => Type::boolean() ],
                        "sortPinned" => [ "type" => Type::boolean() ],
                    ],
                    "resolve" => "Pleio\Resolver::getEntities"
                ],
                "notifications" => [
                    "type" => $registry->get("NotificationList"),
                    "args" => [
                        "offset" => [ "type" => Type::int() ],
                        "limit" => [ "type" => Type::int() ],
                        "unread" => [ "type" => Type::boolean() ]

                    ],
                    "resolve" => "Pleio\Resolver::getNotifications"
                ],
                "activities" => [
                    "type" => $registry->get("ActivityList"),
                    "args" => [
                        "containerGuid" => [ "type" => Type::string() ],
                        "offset" => [ "type" => Type::int() ],
                        "limit" => [ "type" => Type::int() ],
                        "tags" => [ "type" => Type::listOf(Type::string()) ],
                        "tagLists" => [ "type" => Type::listOf(Type::listOf(Type::string())) ],
                        "groupFilter" => [ "type" => Type::listOf(Type::string()) ],
                        "subtypes" => [ "type" => Type::listOf(Type::string()) ],
                        "orderBy" => [ "type" => $registry->get("OrderBy") ],
                        "orderDirection" => [ "type" => $registry->get("OrderDirection") ],
                        "sortPinned" => [ "type" => Type::boolean() ],
                    ],
                    "resolve" => "Pleio\Resolver::getActivities"
                ],
                "bookmarks" => [
                    "type" => $registry->get("EntityList"),
                    "args" => [
                        "offset" => [ "type" => Type::int() ],
                        "limit" => [ "type" => Type::int() ],
                        "subtype" => [ "type" => Type::string() ]
                    ],
                    "resolve" => "Pleio\Resolver::getBookmarks"
                ],
                "filters" => [
                    "type" => $registry->get("Filters"),
                    "resolve" => "Pleio\Resolver::getFilters"
                ],
                "site" => [
                    "type" => $registry->get("Site"),
                    "resolve" => "Pleio\Resolver::getSite"
                ],
                "siteSettings" => [
                    "type" => $registry->get("SiteSettings"),
                    "resolve" => "Pleio\Resolver::getSiteSettings"
                ]
            ]
        ]);

        $mutationType = new ObjectType([
            "name" => "Mutation",
            "fields" => [
                    "editSiteSetting" => Mutations\EditSiteSetting::getMutation($registry),
                    "addEntity" => Mutations\AddEntity::getMutation($registry),
                    "editEntity" => Mutations\EditEntity::getMutation($registry),
                    "deleteEntity" => Mutations\DeleteEntity::getMutation($registry),
                    "addFile" => Mutations\AddFile::getMutation($registry),
                    "editFileFolder" => Mutations\EditFileFolder::getMutation($registry),
                    "moveFileFolder" => Mutations\MoveFileFolder::getMutation($registry),
                    "addPage" => Mutations\AddPage::getMutation($registry),
                    "editPage" => Mutations\EditPage::getMutation($registry),
                    "addRow" => Mutations\AddRow::getMutation($registry),
                    "editRow" => Mutations\EditRow::getMutation($registry),
                    "deleteRow" => Mutations\DeleteRow::getMutation($registry),
                    "addColumn" => Mutations\AddColumn::getMutation($registry),
                    "editColumn" => Mutations\EditColumn::getMutation($registry),
                    "addColumn" => Mutations\AddColumn::getMutation($registry),
                    "editColumn" => Mutations\EditColumn::getMutation($registry),
                    "deleteColumn" => Mutations\DeleteColumn::getMutation($registry),
                    "addWidget" => Mutations\AddWidget::getMutation($registry),
                    "editWidget" => Mutations\EditWidget::getMutation($registry),
                    "deleteWidget" => Mutations\DeleteWidget::getMutation($registry),
                    "editNotifications" => Mutations\EditNotifications::getMutation($registry),
                    "editGroupNotifications" => Mutations\EditGroupNotifications::getMutation($registry),
                    "editEmailOverview" => Mutations\EditEmailOverview::getMutation($registry),
                    "bookmark" => Mutations\Bookmark::getMutation($registry),
                    "vote" => Mutations\Vote::getMutation($registry),
                    "follow" => Mutations\Follow::getMutation($registry),
                    "editAvatar" => Mutations\EditAvatar::getMutation($registry),
                    "editProfileField" => Mutations\EditProfileField::getMutation($registry),
                    "addImage" => Mutations\AddImage::getMutation($registry),
                    "addGroup" => Mutations\AddGroup::getMutation($registry),
                    "editGroup" => Mutations\EditGroup::getMutation($registry),
                    "addSubgroup" => Mutations\AddSubgroup::getMutation($registry),
                    "editSubgroup" => Mutations\EditSubgroup::getMutation($registry),
                    "deleteSubgroup" => Mutations\DeleteSubgroup::getMutation($registry),
                    "joinGroup" => Mutations\JoinGroup::getMutation($registry),
                    "leaveGroup" => Mutations\LeaveGroup::getMutation($registry),
                    "inviteToGroup" => Mutations\InviteToGroup::getMutation($registry),
                    "resendGroupInvitation" => Mutations\ResendGroupInvitation::getMutation($registry),
                    "deleteGroupInvitation" => Mutations\DeleteGroupInvitation::getMutation($registry),
                    "acceptMembershipRequest" => Mutations\AcceptMembershipRequest::getMutation($registry),
                    "rejectMembershipRequest" => Mutations\RejectMembershipRequest::getMutation($registry),
                    "sendMessageToGroup" => Mutations\SendMessageToGroup::getMutation($registry),
                    "acceptGroupInvitation" => Mutations\AcceptGroupInvitation::getMutation($registry),
                    "changeGroupRole" => Mutations\ChangeGroupRole::getMutation($registry),
                    "editTask" => Mutations\EditTask::getMutation($registry),
                    "attendEvent" => Mutations\AttendEvent::getMutation($registry),
                    "attendEventWithoutAccount" => Mutations\AttendEventWithoutAccount::getMutation($registry),
                    "confirmAttendEventWithoutAccount" => Mutations\ConfirmAttendEventWithoutAccount::getMutation($registry),
                    "markAsRead" => Mutations\MarkAsRead::getMutation($registry),
                    "markAllAsRead" => Mutations\MarkAllAsRead::getMutation($registry),
                    "toggleBestAnswer" => Mutations\ToggleBestAnswer::getMutation($registry),
                    "toggleIsClosed" => Mutations\ToggleIsClosed::getMutation($registry),
                    "toggleRequestDeleteUser" => Mutations\ToggleRequestDeleteUser::getMutation($registry),
                    "reorder" => Mutations\Reorder::getMutation($registry),
                    "voteOnPoll" => Mutations\VoteOnPoll::getMutation($registry),
                    "addPoll" => Mutations\AddPoll::getMutation($registry),
                    "editPoll" => Mutations\EditPoll::getMutation($registry),
                    "sendMessageToUser" => Mutations\SendMessageToUser::getMutation($registry),
                    "addGroupWidget" => Mutations\AddGroupWidget::getMutation($registry),
                    "editGroupWidget" => Mutations\EditGroupWidget::getMutation($registry),
            ]
        ]);

        $schema = new Schema([
            "query" => $queryType,
            "mutation" => $mutationType,
            "types" => [
                $registry->get("User"),
                $registry->get("Poll"),
                $registry->get("Group"),
                $registry->get("Page"),
                $registry->get("Wiki"),
                $registry->get("Blog"),
                $registry->get("Event"),
                $registry->get("News"),
                $registry->get("Task"),
                $registry->get("FileFolder"),
                $registry->get("Question"),
                $registry->get("Discussion"),
                $registry->get("StatusUpdate"),
                $registry->get("Comment"),
            ],
        ]);

        return $schema;
   }
}
