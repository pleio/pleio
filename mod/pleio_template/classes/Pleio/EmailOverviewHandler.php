<?php
namespace Pleio;

class EmailOverviewHandler {
    static function sendToAll($interval = "daily") {
        $dbprefix = elgg_get_config("dbprefix");
        $site = elgg_get_site_entity();

        $site_guid = (int) $site->guid;

        if (!in_array($interval, ["daily", "weekly", "monthly"])) {
            throw new Exception("Invalid interval specified.");
        }

        $dblink = get_db_link("read");
        $interval = mysqli_real_escape_string($dblink, $interval);

        $sql = "SELECT r.guid_one, ps.value AS period FROM {$dbprefix}entity_relationships r
            JOIN
                {$dbprefix}private_settings ps ON r.guid_one = ps.entity_guid
            WHERE r.relationship = 'member_of_site'
                AND r.guid_two = {$site_guid}
                AND ps.name = 'email_overview_{$site_guid}'
                AND ps.value = '{$interval}'
        ";

        $rows = get_data($sql);

        foreach ($rows as $row) {
            EmailOverviewHandler::sendOverview($row->guid_one, $site->guid, $interval);
        }
    }

    static function sendOverview($user_guid, $site_guid, $interval) {
        global $CONFIG;
        static $subtypes;

        if (!$subtypes) {
            $subtypes = EmailOverviewHandler::getSubtypes();
        }

        $user = get_entity($user_guid);
        if (!$user || $user->isBanned()) {
            // do not send a mail to banned users
            return;
        }

        $site = get_entity($site_guid);
        if (!$site) {
            return;
        }

        $dbprefix = elgg_get_config("dbprefix");
        $tags_name_id = get_metastring_id("tags");

        $upper_bound = time();
        $lower_bound = $upper_bound - 3600*1500;

        $latest_email_overview = (int) $user->getPrivateSetting("latest_email_overview_{$site->guid}");
        if ($latest_email_overview > $lower_bound) {
            $lower_bound = $latest_email_overview;
        }

        $access = implode(",", pleio_template_get_access_array($user->guid));

        // select featured "news" and recommended "blog" items

        $is_enabled_featured = elgg_get_plugin_setting("email_overview_enable_featured", "pleio_template") === "yes";
        $featured_entities = [];

        if ($is_enabled_featured && $interval !== "monthly") {
            $featured_subtypes = EmailOverviewHandler::getFeaturedSubtypes();
            $is_featured_name_id = get_metastring_id("isFeatured") ?: 0;
            $is_recommended_name_id = get_metastring_id("isRecommended") ?: 0;
            $value_id_one = get_metastring_id("1") ?: 0;

            $query = "SELECT e.guid FROM elgg_entities as e
                LEFT JOIN elgg_metadata md1 ON md1.entity_guid = e.guid AND md1.name_id = {$is_featured_name_id} AND md1.value_id = '{$value_id_one}'
                LEFT JOIN elgg_metadata md2 ON md2.entity_guid = e.guid AND md2.name_id = {$is_recommended_name_id} AND md2.value_id = '{$value_id_one}'
                WHERE e.time_created > {$lower_bound}
                AND e.time_created <= {$upper_bound}
                AND e.type = 'object'
                AND e.subtype IN ({$featured_subtypes})
                AND e.access_id IN ({$access})
                AND (md1.id IS NOT NULL OR md2.id IS NOT NULL)
                ORDER BY e.guid DESC";

            $featured_entities = EmailOverviewHandler::getColumn($query, "guid");
        }

        $filtered_tags_ids = [0]; // Added dummy ID for when user has no tags configured
        if ($user->editEmailOverviewTags) {
            $userTags = is_array($user->editEmailOverviewTags) ? $user->editEmailOverviewTags : [$user->editEmailOverviewTags];

            foreach ($userTags as $tag) {
                $tag_id = get_metastring_id($tag, false);
                if ($tag_id && is_array($tag_id)) {
                    foreach ($tag_id as $tag) {
                        $filtered_tags_ids[] = $tag;
                    }
                } elseif ($tag_id){
                    $filtered_tags_ids[] = $tag_id;
                }
            }
        }

        $filtered_tags_ids = implode(", ", $filtered_tags_ids);

        $new_entities = EmailOverviewHandler::getColumn("
            SELECT e.guid, count(md.id) as tags FROM elgg_entities as e
            LEFT JOIN elgg_metadata md ON md.entity_guid = e.guid AND md.name_id = {$tags_name_id} AND md.value_id IN ({$filtered_tags_ids})
            WHERE e.time_created > {$lower_bound}
            AND e.time_created <= {$upper_bound}
            AND e.type = 'object'
            AND e.subtype IN ({$subtypes})
            AND e.owner_guid != {$user->guid}
            AND e.site_guid = {$site->guid}
            AND e.access_id IN ({$access})
            GROUP By e.guid
            ORDER BY tags DESC, e.guid DESC",
            "guid"
        );

        $already_viewed = EmailOverviewHandler::getColumn("
            SELECT DISTINCT(entity_guid) FROM {$dbprefix}entity_views_log
            WHERE
            performed_by_guid = {$user->guid}
            AND time_created > {$lower_bound}",
            "entity_guid"
        );

        $selected_entities = array_diff($new_entities, $already_viewed);

        // remove featured_entities from selected_entities
        $selected_entities = array_diff($selected_entities, $featured_entities);

        if (count($selected_entities) === 0 && count($featured_entities) === 0) {
            return;
        }

        $subject = elgg_get_plugin_setting("email_overview_subject", "pleio_template") ?: elgg_echo("pleio_template:periodical:overview:send:subject", [ $site->name ]);

        $result = elgg_send_email(
            $site->email ? $site->email : "noreply@" . get_site_domain($site->guid),
            $user->email,
            html_entity_decode($subject),
            "",
            [
                "to" => $user,
                "overview" => true,
                "entities" => EmailOverviewHandler::getEntities($selected_entities, 5),
                "featured" => EmailOverviewHandler::getEntities($featured_entities, 3)
            ]
        );

        if ($result) {
            $user->setPrivateSetting("latest_email_overview_{$site->guid}", $upper_bound);
        }
    }

    private static function getSubtypes() {
        global $CONFIG;

        $subtypes = array_filter(array_map(function($v) { return get_subtype_id("object", $v); }, $CONFIG->pleio_subtypes));
        $sanitized_subtypes = array_map(function($v) { return sanitize_int($v); }, $subtypes);
        return implode(",", $sanitized_subtypes);
    }

    private static function getFeaturedSubtypes() {
        global $CONFIG;

        $subtypes = array_filter(array_map(function($v) { return get_subtype_id("object", $v); }, ["news", "blog"]));
        $sanitized_subtypes = array_map(function($v) { return sanitize_int($v); }, $subtypes);
        return implode(",", $sanitized_subtypes);
    }

    private static function getColumn($query, $column) {
        $result = get_data($query);
        if (!$result) {
            return [];
        }

        return array_map(function($row) use ($column) { return $row->$column; }, $result);
    }

    private static function getEntities($guids, $limit = 5) {
        $guids = array_slice($guids, 0, $limit);
        $entities = [];

        foreach ($guids as $guid) {
            $entities[] = get_entity($guid);
        }
        return $entities;
    }
}