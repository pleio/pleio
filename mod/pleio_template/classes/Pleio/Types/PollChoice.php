<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class PollChoice extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "PollChoice",
            "fields" => [
                "guid" => [ "type" => Type::nonNull(Type::string()) ],
                "text" => [ "type" => Type::string() ],
                "votes" => [ "type" => Type::int() ]
            ]
        ]);
    }
}
