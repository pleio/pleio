<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class Featured extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "fields" => [
                "video" => [ "type" => Type::string() ],
                "image" => [ "type" => Type::string() ],
                "positionY" => [ "type" => Type::int() ],
                "alt" => ["type" => Type::string(), "deprecated"=> "Only resolves in backend2"]
            ]
        ]);
    }
}
