<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class NotificationList extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "NotificationsList",
            "fields" => [
                "total" => [ "type" => Type::int() ],
                "totalUnread" => [ "type" => Type::int() ],
                "edges" => [ "type" => Type::listOf($registry->get("Notification")) ]
            ]
        ]);
    }
}
