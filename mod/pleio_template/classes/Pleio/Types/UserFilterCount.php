<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;
use Pleio\Resolver;

class UserFilterCount extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "UserFilterCount",
            "fields" => [
                "name" => [
                    "type" => Type::nonNull(Type::string())
                ],
                "values" => [
                    "type" => Type::listOf($registry->get("UserFilterItemCount")),
                    "resolve" => function($values) {
                        return Resolver::getFilterValues($values);
                    }
                ],
            ]
        ]);
    }
}

