<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class StatsItem extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "fields" => [
                "key" => [ "type" => Type::nonNull(Type::string()) ],
                "name" => [ "type" => Type::nonNull(Type::string()) ],
                "value" => [ "type" => Type::string() ]
            ]
        ]);
    }
}
