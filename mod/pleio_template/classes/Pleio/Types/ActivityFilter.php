<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class ActivityFilter extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "ActivityFilter",
            "fields" => [
                "contentTypes" => [
                    "type" => Type::nonNull(Type::listOf($registry->get("KeyValueItem")))
                ],
            ]
        ]);
    }
}

