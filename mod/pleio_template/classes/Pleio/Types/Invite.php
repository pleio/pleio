<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class Invite extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "fields" => [
                "id" => [ "type" => Type::int() ],
                "timeCreated" => [ "type" => Type::string() ],
                "invited" => [ "type" => Type::nonNull(Type::boolean()) ],
                "user" => [ "type" => $registry->get("User") ],
                "email" => [ "type" => Type::string() ]
            ]
        ]);
    }
}
