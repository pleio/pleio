<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class Option extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "Option",
            "fields" => [
                "value" => [ "type" => Type::nonNull(Type::string()) ],
                "label" => [ "type" => Type::string() ]
            ]
        ]);
    }
}
