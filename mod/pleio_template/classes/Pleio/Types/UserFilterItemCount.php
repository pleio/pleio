<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class UserFilterItemCount extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "fields" => [
                "key" => [ "type" => Type::nonNull(Type::string()) ],
                "count" => [ "type" => Type::int() ]
            ]
        ]);
    }
}
