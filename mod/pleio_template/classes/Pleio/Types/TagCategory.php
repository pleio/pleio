<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class TagCategory extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "TagCategory",
            "fields" => [
                "name" => [
                    "type" => Type::nonNull(Type::string())
                ],
                "values" => [
                    "type" => Type::listOf(Type::string())
                ]
            ]
        ]);
    }
}
