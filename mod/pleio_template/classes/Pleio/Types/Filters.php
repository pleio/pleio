<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use Pleio\TypeRegistry;
use Pleio\Resolver;

class Filters extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "Filters",
            "description" => "Available Filters",
            "fields" => [
                "users" => [
                    "type" => Type::listOf($registry->get("UserFilter"))
                ]
            ]
        ]);
    }
}
