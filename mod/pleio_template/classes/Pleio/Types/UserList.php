<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class UserList extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "UserList",
            "fields" => [
                "total" => [
                    "type" => Type::nonNull(Type::int())
                ],
                "filterCount" => [
                    "type" => Type::listOf($registry->get("UserFilterCount"))
                ],
                "edges" => [
                    "type" => Type::listOf($registry->get("User"))
                ],
                "fieldsInOverview" => [
                    "type" => Type::listOf($registry->get("UserListOverviewItem"))
                ],
            ]
        ]);
    }
}
