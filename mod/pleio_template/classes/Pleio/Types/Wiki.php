<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Pleio\Resolver;
use Pleio\TypeRegistry;
use Pleio\Helpers;
use Pleio\Mapper;

class Wiki extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "interfaces" => [$registry->get("Entity")],
            "fields" => function() use ($registry) {
                return [
                    "guid" => [
                        "type" => Type::nonNull(Type::string())
                    ],
                    "status" => [
                        "type" => Type::int()
                    ],
                    "subtype" => [
                        "type" => Type::string()
                    ],
                    "canEdit" => [
                        "type" => Type::nonNull(Type::boolean())
                    ],
                    "title" => [
                        "type" => Type::string()
                    ],
                    "description" => [
                        "type" => Type::string()
                    ],
                    "hasChildren" => [
                        "type" => Type::boolean(),
                        "resolve" => function($object, array $args, $context, ResolveInfo $info) {
                            return Resolver::hasChildren($object);
                        }
                    ],
                    "children" => [
                        "type" => Type::listOf($registry->get("Wiki")),
                        "resolve" => function($object, array $args, $context, ResolveInfo $info) {
                            return Resolver::getChildren($object);
                        }
                    ],
                    "parent" => [
                        "type" => $registry->get("Wiki"),
                        "resolve" => function($object, array $args, $context, ResolveInfo $info) {
                            return Resolver::getParent($object);
                        }
                    ],
                    "richDescription" => [
                        "type" => Type::string()
                    ],
                    "excerpt" => [ "type" => Type::string() ],
                    "url" => [
                        "type" => Type::string()
                    ],
                    "timeCreated" => [
                        "type" => Type::string()
                    ],
                    "timeUpdated" => [
                        "type" => Type::string()
                    ],
                    "accessId" => [
                        "type" => Type::int()
                    ],
                    "writeAccessId" => [
                        "type" => Type::int()
                    ],
                    "isBookmarked" => [
                        "type" => Type::boolean(),
                        "resolve" => function($object, array $args, $context, ResolveInfo $info) {
                            return Resolver::isBookmarked($object);
                        }
                    ],
                    "isFeatured" => [ "type" => Type::boolean() ],
                    "canBookmark" => [
                        "type" => Type::boolean(),
                        "resolve" => function($object, array $args, $context, ResolveInfo $info) {
                            return Resolver::canBookmark($object);
                        }
                    ],
                    "tags" => [
                        "type" => Type::listOf(Type::string())
                    ],
                    "inGroup" => [
                        "type" => Type::boolean(),
                        "resolve" => function($object, array $args, $context, ResolveInfo $info) {
                            return Resolver::inGroup($object);
                        }
                    ],
                    "group" => [
                        "type" => $registry->get("Group"),
                        "resolve" => function($object, array $args, $context, ResolveInfo $info) {
                            $object = get_entity($object["guid"]);

                            if (!$object) {
                                return null;
                            }

                            $container = $object->getContainerEntity();
                            if (!$container) {
                                return null;
                            }

                            if ($container instanceof \ElggGroup) {
                                return Mapper::getGroup($container);
                            }

                            return null;
                        }
                    ],
                    "owner" => [
                        "type" => $registry->get("User"),
                        "resolve" => function($object, array $args, $context, ResolveInfo $info) {
                            return Resolver::getUser($object["ownerGuid"]);
                        }
                    ],
                    "isPinned" => [
                        "type" => Type::nonNull(Type::boolean()),
                        "resolve" => function($object, array $args, $context, ResolveInfo $info) {
                            return false;
                        }
                    ],
                ];
            }
        ]);
    }
}
