<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use Pleio\TypeRegistry;
use Pleio\Resolver;

class Viewer extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "Viewer",
            "description" => "The current viewer",
            "fields" => [
                "guid" => [
                    "type" => Type::nonNull(Type::string())
                ],
                "loggedIn" => [
                    "type" => Type::nonNull(Type::boolean())
                ],
                "isSubEditor" => [
                    "type" => Type::nonNull(Type::boolean())
                ],
                "isAdmin" => [
                    "type" => Type::nonNull(Type::boolean())
                ],
                "isBanned" => [
                    "type" => Type::nonNull(Type::boolean())
                ],
                "tags" => [
                    "type" => Type::listOf(Type::string())
                ],
                "canWriteToContainer" => [
                    "type" => Type::nonNull(Type::boolean()),
                    "args" => [
                        "containerGuid" => [
                            "type" => Type::string()
                        ],
                        "type" => [
                            "type" => $registry->get("Type")
                        ],
                        "subtype" => [
                            "type" => Type::string()
                        ]
                    ],
                    "resolve" => function($viewer, array $args, $context, ResolveInfo $info) {
                        return Resolver::canWriteToContainer($viewer, $args, $info);
                    }
                ],
                "user" => [
                    "type" => $registry->get("User"),
                    "resolve" => function($viewer, array $args, $context, ResolveInfo $info) {
                        $entity = elgg_get_logged_in_user_entity();
                        if ($entity) {
                            return Resolver::getUser($entity->guid);
                        }
                    }
                ]
            ]
        ]);
    }
}
