<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class PredefinedTag extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "PredefinedTag",
            "fields" => [
                "tag" => [ "type" => Type::string() ]
            ]
        ]);
    }
}
