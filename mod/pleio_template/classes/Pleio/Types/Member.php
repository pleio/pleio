<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class Member extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "Member",
            "fields" => [
                "role" => [ "type" => $registry->get("Role") ],
                "user" => [ "type" => $registry->get("User") ],
                "email" => [ "type" => Type::string() ]
            ]
        ]);
    }
}
