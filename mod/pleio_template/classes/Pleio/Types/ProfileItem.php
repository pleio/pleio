<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class ProfileItem extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "fields" => [
                "key" => [ "type" => Type::nonNull(Type::string()) ],
                "name" => [ "type" => Type::nonNull(Type::string()) ],
                "value" => [ "type" => Type::string() ],
                "category" => [ "type" => Type::string() ],
                "isEditable" => [ "type" => Type::boolean() ],
                "isFilterable" => [ "type" => Type::boolean() ],
                "isFilter" => [ "type" => Type::boolean() ],
                "isInOverview" => [ "type" => Type::boolean() ],
                "accessId" => [ "type" => Type::int() ],
                "fieldType" => [ "type" => Type::string() ],
                "fieldOptions" => [ "type" => Type::listOf(Type::string()) ]
            ]
        ]);
    }
}
