<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\EnumType;
use Pleio\TypeRegistry;

class OrderBy extends EnumType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "OrderBy",
            "description" => "Configure the way the entity list is ordered. lastAction is the last action that is performed on the object, e.g. the latest comment that is placed.",
            "values" => [
                "timeCreated" => [ "value" => "timeCreated" ],
                "timeUpdated" => [ "value" => "timeUpdated" ],
                "lastAction" => [ "value" => "lastAction" ],
                "title" => [ "value" => "title" ]
            ]
        ]);
    }
}
