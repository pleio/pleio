<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class WidgetSetting extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "WidgetSetting",
            "fields" => [
                "key" => [ "type" => Type::nonNull(Type::string()) ],
                "value" => [ "type" => Type::string() ]
            ]
        ]);
    }
}
