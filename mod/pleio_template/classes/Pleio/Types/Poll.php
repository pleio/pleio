<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use Pleio\TypeRegistry;
use Pleio\Resolver;

class Poll extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "Poll",
            "interfaces" => [$registry->get("Entity")],
            "fields" => [
                "guid" => [ "type" => Type::nonNull(Type::string()) ],
                "status" => [ "type" => Type::int() ],
                "canEdit" => [ "type" => Type::nonNull(Type::boolean()) ],
                "title" => [ "type" => Type::string() ],
                "url" => [ "type" => Type::string() ],
                "timeCreated" => [ "type" => Type::string() ],
                "timeUpdated" => [ "type" => Type::string() ],
                "accessId" => [ "type" => Type::int() ],
                "choices" => [
                    "type" => Type::ListOf($registry->get("PollChoice")),
                    "resolve" => function($object, array $args, $context, ResolveInfo $info) {
                        return Resolver::getChoices($object);
                    }
                ],
                "hasVoted" => [
                    "type" => Type::boolean(),
                    "resolve" => function($object, array $args, $context, ResolveInfo $info) {
                        return Resolver::hasVotedOnPoll($object);
                    }
                ],
                "isPinned" => [
                    "type" => Type::nonNull(Type::boolean()),
                    "resolve" => function($object, array $args, $context, ResolveInfo $info) {
                        return false;
                    }
                ],
            ]
        ]);
    }
}
