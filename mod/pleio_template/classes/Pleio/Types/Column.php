<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use Pleio\TypeRegistry;
use Pleio\Resolver;

class Column extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "Column",
            "fields" => [
                "guid" => [
                    "type" => Type::nonNull(Type::string())
                ],
                "containerGuid" => [
                    "type" => Type::nonNull(Type::string())
                ],
                "width" => [
                    "type" => Type::listOf(Type::int())
                ],
                "parentGuid" => [
                    "type" => Type::nonNull(Type::string())
                ],
                "position" => [
                    "type" => Type::nonNull(Type::int())
                ],
                "canEdit" => [
                    "type" => Type::nonNull(Type::boolean())
                ]
            ]
        ]);
    }
}
