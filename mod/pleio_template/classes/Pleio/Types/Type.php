<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\EnumType;
use Pleio\TypeRegistry;

class Type extends EnumType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "Type",
            "description" => "The type of entity",
            "values" => [
                "user" => [ "value" => "user" ],
                "group" => [ "value" => "group" ],
                "object" => [ "value" => "object" ],
                "page" => [ "value" => "page" ],
                "wiki" => [ "value" => "wiki" ]
            ]
        ]);
    }
}
