<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use Pleio\TypeRegistry;
use Pleio\Resolver;

class Row extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "Row",
            "fields" => [
                "guid" => [
                    "type" => Type::nonNull(Type::string())
                ],
                "containerGuid" => [
                    "type" => Type::nonNull(Type::string())
                ],
                "position" => [
                    "type" => Type::nonNull(Type::int())
                ],
                "parentGuid" => [
                    "type" => Type::nonNull(Type::string())
                ],
                "isFullWidth" => [
                    "type" => Type::nonNull(Type::boolean())
                ],
                "canEdit" => [
                    "type" => Type::nonNull(Type::boolean())
                ]
            ]
        ]);
    }
}
