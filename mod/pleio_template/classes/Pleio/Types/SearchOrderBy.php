<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\EnumType;
use Pleio\TypeRegistry;

class SearchOrderBy extends EnumType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "SearchOrderBy",
            "description" => "Configure the way the search results are ordered. When not provided, sorting is done on search score (most relevant first).",
            "values" => [
                "title" => [ "value" => "title" ],
                "timeCreated" => [ "value" => "timeCreated" ],
            ]
        ]);
    }
}
