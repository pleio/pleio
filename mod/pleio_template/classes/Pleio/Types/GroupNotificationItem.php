<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class GroupNotificationItem extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "fields" => [
                "guid" => [ "type" => Type::nonNull(Type::string()) ],
                "name" => [ "type" => Type::nonNull(Type::string()) ],
                "getsNotifications" => [ "type" => Type::nonNull(Type::boolean()) ]
            ]
        ]);
    }
}
