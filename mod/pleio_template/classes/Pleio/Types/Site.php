<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use Pleio\TypeRegistry;
use Pleio\Resolver;

class Site extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "Site",
            "description" => "The current site",
            "fields" => [
                "guid" => [
                    "type" => Type::nonNull(Type::string())
                ],
                "name" => [
                    "type" => Type::nonNull(Type::string())
                ],
                "theme" => [
                    "type" => Type::nonNull(Type::string())
                ],
                "menu" => [
                    "type" => Type::listOf($registry->get("MenuItem"))
                ],
                "profile" => [
                    "type" => Type::listOf($registry->get("ProfileItem"))
                ],
                "achievementsEnabled" => [
                    "type" => Type::nonNull(Type::boolean())
                ],
                "cancelMembershipEnabled" => [
                    "type" => Type::nonNull(Type::boolean())
                ],
                "footer" => [
                    "type" => Type::listOf($registry->get("MenuItem"))
                ],
                "directLinks" => [
                    "type" => Type::listOf($registry->get("DirectLink"))
                ],
                "accessIds" => [
                    "type" => Type::listOf($registry->get("AccessId"))
                ],
                "defaultAccessId" => [
                    "type" => Type::nonNull(Type::int())
                ],
                "language" => [
                    "type" => Type::nonNull(Type::string())
                ],
                "logo" => [
                    "type" => Type::string()
                ],
                "logoAlt" => [
                    "type" => Type::string()
                ],
                "icon" => [
                    "type" => Type::string()
                ],
                "iconAlt" => [
                    "type" => Type::string()
                ],
                "showIcon" => [
                    "type" => Type::nonNull(Type::boolean())
                ],

                "startpage" => [
                    "type" => Type::string()
                ],
                "showLeader" => [
                    "type" => Type::nonNull(Type::boolean())
                ],
                "showLeaderButtons" => [
                    "type" => Type::nonNull(Type::boolean())
                ],
                "subtitle" => [
                    "type" => Type::string()
                ],
                "leaderImage" => [
                    "type" => Type::string()
                ],
                "showInitiative" => [
                    "type" => Type::nonNull(Type::boolean())
				],
                "initiativeTitle" => [
                    "type" => Type::string()
                ],
                "initiativeImage" => [
                    "type" => Type::string()
                ],
                "initiativeImageAlt" => [
                    "type" => Type::string()
                ],
                "initiativeDescription" => [
                    "type" => Type::string()
                ],
                "initiatorLink" => [
                    "type" => Type::string()
                ],
                "style" => [
                    "type" => Type::nonNull($registry->get("Style"))
                ],
                "customTagsAllowed" => [
                    "type" => Type::nonNull(Type::boolean())
                ],
                "tagCategories" => [
                    "type" => Type::nonNull(Type::listOf($registry->get("TagCategory")))
                ],
                "activityFilter" => [
                    "type" => $registry->get("ActivityFilter"),
                    "resolve" => function($site, array $args, $context, ResolveInfo $info) {
                        return Resolver::getActivityFilter();
                    }
                ],
                "showExtraHomepageFilters" => [
                    "type" => Type::nonNull(Type::boolean())
                ],
                "showTagsInFeed" => [
                    "type" => Type::nonNull(Type::boolean())
                ],
                "showTagsInDetail" => [
                    "type" => Type::nonNull(Type::boolean())
                ],
                "usersOnline" => [
                    "type" => Type::nonNull(Type::int()),
                    "resolve" => function($site, array $args, $context, ResolveInfo $info) {
                        return Resolver::getUsersOnline($site);
                    }
                ]
            ]
        ]);
    }
}
