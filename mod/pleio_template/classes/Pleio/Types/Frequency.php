<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\EnumType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class Frequency extends EnumType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "values" => [
                "daily" => [ "value" => "daily" ],
                "weekly" => [ "value" => "weekly" ],
                "twoweekly" => [ "value" => "twoweekly" ],
                "monthly" => [ "value" => "monthly" ],
                "never" => [ "value" => "never" ]
            ]
        ]);
    }
}
