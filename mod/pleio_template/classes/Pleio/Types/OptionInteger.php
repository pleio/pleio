<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class OptionInteger extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "OptionInteger",
            "fields" => [
                "value" => [ "type" => Type::nonNull(Type::int()) ],
                "label" => [ "type" => Type::string() ]
            ]
        ]);
    }
}
