<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class MenuItem extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "fields" => function() use ($registry) {
                return [
                    "title" => [
                        "type" => Type::nonNull(Type::string())
                    ],
                    "link" => [
                        "type" => Type::string()
                    ],
                    "children" => [
                        "type" => Type::listOf($registry->get("MenuItem"))
                    ]
                ];
            }
        ]);
    }
}
