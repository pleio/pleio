<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class TopItem extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "TopItem",
            "fields" => [
                "user" => [ "type" => $registry->get("User") ],
                "likes" => [ "type" => Type::int() ]
            ]
        ]);
    }
}