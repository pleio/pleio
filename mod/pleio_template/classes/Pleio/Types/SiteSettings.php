<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class SiteSettings extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "SiteSettings",
            "fields" => [
                "name" => [ "type" => Type::nonNull(Type::string()) ],
                "description" => [ "type" => Type::string() ],
                "language" => [ "type" => Type::nonNull(Type::string()) ],
                "languageOptions" => ["type" => Type::listOf($registry->get("Option"))],
                "isClosed" => [ "type" => Type::nonNull(Type::boolean()) ],
                "allowRegistration" => [ "type" => Type::nonNull(Type::boolean()) ],
                "defaultAccessId" => [ "type" => Type::nonNull(Type::int()) ],
                "defaultAccessIdOptions" => ["type" => Type::listOf($registry->get("OptionInteger"))],
                "googleAnalyticsId" => [ "type" => Type::string() ],
                "googleSiteVerification" => [ "type" => Type::string() ],
                "piwikUrl" => [ "type" => Type::string() ],
                "piwikId" => [ "type" => Type::string() ],
                "font" => [ "type" => Type::string() ],
                "fontOptions" => ["type" => Type::listOf($registry->get("Option"))],
                "colorPrimary" => [ "type" => Type::string() ],
                "colorSecondary" => [ "type" => Type::string() ],
                "colorHeader" => [ "type" => Type::string() ],
                "theme" => [ "type" => Type::string() ],
                "themeOptions" => ["type" => Type::listOf($registry->get("Option"))],
                "logo" => [ "type" => Type::string() ],
                "logoAlt" => [ "type" => Type::string() ],
                "likeIcon" => [ "type" => Type::string() ],
                "startPage" => [ "type" => Type::string() ],
                "startPageOptions" => ["type" => Type::listOf($registry->get("Option"))],
                "startPageCms" => [ "type" => Type::string() ],
                "startPageCmsOptions" => ["type" => Type::listOf($registry->get("Option"))],
                "icon" => [ "type" => Type::string() ],
                "showIcon" => [ "type" => Type::nonNull(Type::boolean()) ],
                "menu" => ["type" => Type::listOf($registry->get("MenuItem"))],
                "numberOfFeaturedItems" => [ "type" => Type::int() ],
                "enableFeedSorting" => [ "type" => Type::nonNull(Type::boolean()) ],
                "showExtraHomepageFilters" => [ "type" => Type::nonNull(Type::boolean()) ],
                "showLeader" => [ "type" => Type::nonNull(Type::boolean()) ],
                "showLeaderButtons" => [ "type" => Type::nonNull(Type::boolean()) ],
                "subtitle" => [ "type" => Type::string() ],
                "leaderImage" => [ "type" => Type::string() ],
                "showInitiative" => ["type" => Type::nonNull(Type::boolean())],
                "initiativeTitle" => ["type" => Type::string()],
                "initiativeImage" => ["type" => Type::string()],
                "initiativeImageAlt" => ["type" => Type::string()],
                "initiativeDescription" => ["type" => Type::string()],
                "initiativeLink" => ["type" => Type::string()],
                "directLinks" => ["type" => Type::listOf($registry->get("DirectLink"))],
                "footer" => ["type" => Type::listOf($registry->get("MenuItem"))],

                "defaultEmailOverviewFrequency" => ["type" => Type::string()],
                "defaultEmailOverviewFrequencyOptions" => ["type" => Type::listOf($registry->get("Option"))],
                "emailOverviewSubject" => ["type" => Type::string()],
                "emailOverviewTitle" => ["type" => Type::string()],
                "emailOverviewIntro" => ["type" => Type::string()],
                "emailOverviewEnableFeatured" => ["type" => Type::nonNull(Type::boolean())],
                "emailOverviewFeaturedTitle" => ["type" => Type::string()],
                "emailNotificationShowExcerpt" => [ "type" => Type::nonNull(Type::boolean()) ],

                "tagCategories" => ["type" => Type::nonNull(Type::listOf($registry->get("TagCategory")))],
                "showTagsInFeed" => [ "type" => Type::nonNull(Type::boolean()) ],
                "showTagsInDetail" => [ "type" => Type::nonNull(Type::boolean()) ],

                "profile" => ["type" => Type::listOf($registry->get("ProfileItem"))],

                "showLoginRegister" => [ "type" => Type::nonNull(Type::boolean()) ],
                "customTagsAllowed" => [ "type" => Type::nonNull(Type::boolean()) ],
                "showUpDownVoting" => [ "type" => Type::nonNull(Type::boolean()) ],
                "enableSharing" => [ "type" => Type::nonNull(Type::boolean()) ],
                "showViewsCount" => [ "type" => Type::nonNull(Type::boolean()) ],
                "newsletter" => [ "type" => Type::nonNull(Type::boolean()) ],
                "cancelMembershipEnabled" => [ "type" => Type::nonNull(Type::boolean()) ],
                "showExcerptInNewsCard" => [ "type" => Type::nonNull(Type::boolean()) ],
                "commentsOnNews" => [ "type" => Type::nonNull(Type::boolean()) ],
                "eventExport" => [ "type" => Type::nonNull(Type::boolean()) ],
                "questionerCanChooseBestAnswer" => [ "type" => Type::nonNull(Type::boolean()) ],
                "statusUpdateGroups" => [ "type" => Type::nonNull(Type::boolean()) ],
                "subgroups" => [ "type" => Type::nonNull(Type::boolean()) ],
                "groupMemberExport" => [ "type" => Type::nonNull(Type::boolean()) ],
            ]
        ]);
    }
}
