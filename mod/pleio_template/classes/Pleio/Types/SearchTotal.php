<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class SearchTotal extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "SearchTotal",
            "fields" => [
                "subtype" => [
                    "type" => Type::nonNull(Type::string())
                ],
                "total" => [
                    "type" => Type::nonNull(Type::int())
                ]
            ]
        ]);
    }
}
