<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;
use Pleio\Resolver;

class UserFilter extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "UserFilter",
            "fields" => [
                "name" => [
                    "type" => Type::nonNull(Type::string())
                ],
                "fieldType" => [
                    "type" => Type::nonNull(Type::string())
                ],
                "label" => [
                    "type" => Type::nonNull(Type::string())
                ],
                "keys" => [
                    "type" => Type::listOf(Type::nonNull(Type::string()) )
                ],
            ]
        ]);
    }
}

