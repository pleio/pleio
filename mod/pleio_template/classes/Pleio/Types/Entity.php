<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\InterfaceType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class Entity extends InterfaceType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "fields" => [
                "guid" => [ "type" => Type::nonNull(Type::string()) ],
                "status" => [ "type" => Type::int() ]
            ],
            "resolveType" => function($object) use (&$registry) {
                switch ($object["type"]) {
                    case "user":
                        return $registry->get("User");
                    case "group":
                        return $registry->get("Group");
                    case "object":
                        switch ($object["subtype"]) {
                            case "news":
                                return $registry->get("News");
                            case "blog":
                                return $registry->get("Blog");
                            case "event":
                                return $registry->get("Event");
                            case "task":
                                return $registry->get("Task");
                            case "question":
                                return $registry->get("Question");
                            case "discussion":
                                return $registry->get("Discussion");
                            case "file":
                            case "folder":
                                return $registry->get("FileFolder");
                            case "static":
                            case "page":
                                return $registry->get("Page");
                            case "wiki":
                                return $registry->get("Wiki");
                            case "poll":
                                return $registry->get("Poll");
                            case "comment":
                            case "answer":
                                return $registry->get("Comment");
                            case "thewire":
                                return $registry->get("StatusUpdate");
                            default:
                                return null;
                        }
                }
            }
        ]);
    }
}
