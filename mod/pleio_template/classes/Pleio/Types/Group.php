<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\Resolver;
use Pleio\TypeRegistry;

class Group extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "interfaces" => [$registry->get("Entity")],
            "fields" => function() use ($registry) {
                return [
                    "guid" => [ "type" => Type::nonNull(Type::string()) ],
                    "status" => [ "type" => Type::int() ],
                    "name" => [ "type" => Type::string() ],
                    "description" => [ "type" => Type::string() ],
                    "richDescription" => [ "type" => Type::string() ],
                    "excerpt" => [ "type" => Type::string() ],
                    "introduction" => [ "type" => Type::string() ],
                    "isIntroductionPublic"  => [ "type" => Type::boolean() ],
                    "icon" => [ "type" => Type::string() ],
                    "featured" => [ "type" => $registry->get("Featured") ],
                    "url" => [ "type" => Type::string() ],
                    "canEdit" => [ "type" => Type::nonNull(Type::boolean()) ],
                    "canChangeOwnership" => [
                        "type" => Type::boolean(),
                        "resolve" => function($group, array $args, $context, ResolveInfo $info) {
                            return Resolver::canChangeOwnership($group, $args, $context, $info);
                        }
                    ],
                    "isClosed" => [ "type" => Type::boolean() ],
                    "isMembershipOnRequest" => [ "type" => Type::boolean() ],
                    "isFeatured" => [ "type" => Type::boolean() ],
                    "autoNotification" => [ "type" => Type::boolean() ],
                    "isLeavingGroupDisabled"  => [ "type" => Type::boolean() ],
                    "isAutoMembershipEnabled"  => [ "type" => Type::boolean() ],
                    "membership" => [ "type" => $registry->get("Membership") ],
                    "accessIds" => [
                        "type" => Type::listOf($registry->get("AccessId")),
                        "resolve" => function($group, array $args, $context, ResolveInfo $info) {
                            return Resolver::getAccessIds($group, $args, $context, $info);
                        }
                    ],
                    "defaultAccessId" => [
                        "type" => Type::int(),
                        "resolve" => function($group, array $args, $context, ResolveInfo $info) {
                            return Resolver::getDefaultAccessId($group, $args, $context, $info);
                        }
                    ],
                    "getsNotifications" => [
                        "type" => Type::boolean(),
                        "resolve" => function($group, array $args, $context, ResolveInfo $info) {
                            return Resolver::getsNotifications($group);
                        }
                    ],
                    "tags" => [
                        "type" => Type::listOf(Type::string())
                    ],
                    "memberCount" => [
                        "type" => Type::nonNull(Type::int()),
                        "resolve" => function($group, array $args, $context, ResolveInfo $info) {
                            return Resolver::getGroupMemberCount($group);
                        }
                    ],
                    "members" => [
                        "type" => $registry->get("MemberList"),
                        "args" => [
                            "q" => [ "type" => Type::string() ],
                            "offset" => [ "type" => Type::int() ],
                            "limit" => [ "type" => Type::int() ],
                            "inSubgroupId" => [
                                "type" => Type::int(),
                                "description" => "Filter resultset on a specific subgroup."
                            ],
                            "notInSubgroupId" => [
                                "type" => Type::int(),
                                "description" => "Filter resultset on users who are not in a specific subgroup."
                            ]
                        ],
                        "resolve" => function($group, array $args, $context, ResolveInfo $info) {
                            return Resolver::getGroupMembers($group, $args, $context, $info);
                        }
                    ],
                    "invite" => [
                        "type" => $registry->get("InviteList"),
                        "args" => [
                            "q" => [ "type" => Type::string() ],
                            "offset" => [ "type" => Type::int() ],
                            "limit" => [ "type" => Type::int() ]
                        ],
                        "resolve" => function($group, array $args, $context, ResolveInfo $info) {
                            return Resolver::getInvite($group, $args, $context, $info);
                        }
                    ],
                    "invited" => [
                        "type" => $registry->get("InviteList"),
                        "args" => [
                            "q" => [ "type" => Type::string() ],
                            "offset" => [ "type" => Type::int() ],
                            "limit" => [ "type" => Type::int() ]
                        ],
                        "resolve" => function($group, array $args, $context, ResolveInfo $info) {
                            return Resolver::getInvited($group, $args, $context, $info);
                        }
                    ],
                    "membershipRequests" => [
                        "type" => $registry->get("MembershipRequestList"),
                        "resolve" => function($group, array $args, $context, ResolveInfo $info) {
                            return Resolver::getMembershipRequests($group, $args, $context, $info);
                        }
                    ],
                    "plugins" => [
                        "type" => Type::listOf($registry->get("Plugins"))
                    ],
                    "subgroups" => [
                        "type" => $registry->get("SubgroupList"),
                        "resolve" => function($group, array $args, $context, ResolveInfo $info) {
                            return Resolver::getSubgroups($group, $args, $context, $info);
                        }
                    ],
                    "welcomeMessage" => [
                        "type" => Type::string(),
                        "resolve" => function($group, array $args, $context, ResolveInfo $info) {
                            return Resolver::getGroupWelcomeMessage($group, $args, $context, $info);
                        }
                    ],
                    "widgets" => [
                        "type" => Type::listOf($registry->get("Widget")),
                        "resolve" => function($object, array $args, $context, ResolveInfo $info) {
                            return Resolver::getWidgets($object);
                        }
                    ],
                ];
            }
        ]);
    }
}
