<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\EnumType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use Pleio\TypeRegistry;
use Pleio\Helpers;
use Pleio\Resolver;

class Comment extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "Comment",
            "interfaces" => [$registry->get("Entity")],
            "fields" => function() use ($registry) {
                return [
                    "guid" => [ "type" => Type::nonNull(Type::string()) ],
                    "status" => [ "type" => Type::int() ],
                    "subtype" => [ "type" => Type::string() ],
                    "title" => [ "type" => Type::string() ],
                    "description" => [ "type" => Type::string() ],
                    "richDescription" => [ "type" => Type::string() ],
                    "excerpt" => [ "type" => Type::string() ],
                    "url" => [ "type" => Type::string() ],
                    "tags" => [ "type" => Type::listOf(Type::string()) ],
                    "timeCreated" => [ "type" => Type::string() ],
                    "timeUpdated" => [ "type" => Type::string() ],
                    "canEdit" => [ "type" => Type::nonNull(Type::boolean()) ],
                    "canVote" => [
                        "type" => Type::boolean(),
                        "resolve" => function($object, array $args, $context, ResolveInfo $info) {
                            return Resolver::canVote($object);
                        }
                    ],
                    "accessId" => [ "type" => Type::int() ],
                    "writeAccessId" => [ "type" => Type::int() ],
                    "hasVoted" => [
                        "type" => Type::boolean(),
                        "resolve" => function($object, array $args, $context, ResolveInfo $info) {
                            return Resolver::hasVoted($object);
                        }
                    ],
                    "votes" => [
                        "type" => Type::int(),
                        "resolve" => function($object, array $args, $context, ResolveInfo $info) {
                            return Resolver::getVotes($object);
                        }
                    ],
                    "isBestAnswer" => [
                        "type" => Type::boolean(),
                        "resolve" => function($object, array $args, $context, ResolveInfo $info) {
                            return Resolver::isBestAnswer($object);
                        }
                    ],
                    "owner" => [
                        "type" => $registry->get("User"),
                        "resolve" => function($object, array $args, $context, ResolveInfo $info) {
                            return Resolver::getUser($object["ownerGuid"]);
                        }
                    ]
                ];
            }
        ]);
    }
}
