<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\EnumType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class Role extends EnumType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "description" => "The type of role.",
            "values" => [
                "owner" => [ "value" => "owner" ],
                "admin" => [ "value" => "admin" ],
                "member" => [ "value" => "member" ],
                "removed" => [ "value" => "removed" ],
            ]
        ]);
    }
}
