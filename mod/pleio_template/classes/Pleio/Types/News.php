<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\EnumType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use Pleio\TypeRegistry;
use Pleio\Helpers;
use Pleio\Resolver;

class News extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "News",
            "interfaces" => [$registry->get("Entity")],
            "fields" => function() use ($registry) {
                return [
                    "guid" => [ "type" => Type::nonNull(Type::string()) ],
                    "status" => [ "type" => Type::int() ],
                    "subtype" => [ "type" => Type::string() ],
                    "title" => [ "type" => Type::string() ],
                    "description" => [ "type" => Type::string() ],
                    "richDescription" => [ "type" => Type::string() ],
                    "excerpt" => [ "type" => Type::string() ],
                    "url" => [ "type" => Type::string() ],
                    "tags" => [ "type" => Type::listOf(Type::string()) ],
                    "timeCreated" => [ "type" => Type::string() ],
                    "timeUpdated" => [ "type" => Type::string() ],
                    "startDate" => [ "type" => Type::string() ],
                    "endDate" => [ "type" => Type::string() ],
                    "source" => [ "type" => Type::string() ],
                    "isFeatured" => [ "type" => Type::boolean() ],
                    "isHighlighted" => [ "type" => Type::boolean() ],
                    "isRecommended" => [
                        "type" => Type::boolean(),
                        "resolve" => function($object, array $args, $context, ResolveInfo $info) {
                            return Resolver::isRecommended($object);
                        }
                    ],
                    "featured" => [ "type" => $registry->get("Featured") ],
                    "canEdit" => [ "type" => Type::nonNull(Type::boolean()) ],
                    "canComment" => [
                        "type" => Type::boolean(),
                        "resolve" => function($object, array $args, $context, ResolveInfo $info) {
                            return Resolver::canComment($object);
                        }
                    ],
                    "canVote" => [
                        "type" => Type::boolean(),
                        "resolve" => function($object, array $args, $context, ResolveInfo $info) {
                            return Resolver::canVote($object);
                        }
                    ],
                    "accessId" => [ "type" => Type::int() ],
                    "writeAccessId" => [ "type" => Type::int() ],
                    "isBookmarked" => [
                        "type" => Type::boolean(),
                        "resolve" => function($object, array $args, $context, ResolveInfo $info) {
                            return Resolver::isBookmarked($object);
                        }
                    ],
                    "isFollowing" => [
                        "type" => Type::boolean(),
                        "resolve" => function($object, array $args, $context, ResolveInfo $info) {
                            return Resolver::isFollowing($object);
                        }
                    ],
                    "canBookmark" => [
                        "type" => Type::boolean(),
                        "resolve" => function($object, array $args, $context, ResolveInfo $info) {
                            return Resolver::canBookmark($object);
                        }
                    ],
                    "hasVoted" => [
                        "type" => Type::boolean(),
                        "resolve" => function($object, array $args, $context, ResolveInfo $info) {
                            return Resolver::hasVoted($object);
                        }
                    ],
                    "votes" => [
                        "type" => Type::int(),
                        "resolve" => function($object, array $args, $context, ResolveInfo $info) {
                            return Resolver::getVotes($object);
                        }
                    ],
                    "isBestAnswer" => [
                        "type" => Type::boolean(),
                        "resolve" => function($object, array $args, $context, ResolveInfo $info) {
                            return Resolver::isBestAnswer($object);
                        }
                    ],
                    "views" => [
                        "type" => Type::int(),
                        "resolve" => function($object, array $args, $context, ResolveInfo $info) {
                            return Resolver::getViews($object);
                        }
                    ],
                    "owner" => [
                        "type" => $registry->get("User"),
                        "resolve" => function($object, array $args, $context, ResolveInfo $info) {
                            return Resolver::getUser($object["ownerGuid"]);
                        }
                    ],
                    "comments" => [
                        "type" => Type::listOf($registry->get("Comment")),
                        "resolve" => function($object, array $args, $context, ResolveInfo $info) {
                            return Resolver::getComments($object);
                        }
                    ],
                    "commentCount" => [
                        "type" => Type::int(),
                        "resolve" => function($object, array $args, $context, ResolveInfo $info) {
                            return Resolver::countComments($object);
                        }
                    ],
                    "isPinned" => [
                        "type" => Type::nonNull(Type::boolean()),
                        "resolve" => function($object, array $args, $context, ResolveInfo $info) {
                            return false;
                        }
                    ],
                ];
            }
        ]);
    }
}
