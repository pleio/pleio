<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class Widget extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "Widget",
            "fields" => [
                "guid" => [ "type" => Type::nonNull(Type::string()) ],
                "containerGuid" => [
                    "type" => Type::nonNull(Type::string())
                ],
                "type" => [ "type" => Type::nonNull(Type::string()) ],
                "position" => [ "type" => Type::nonNull(Type::int()) ],
                "settings" => [ "type" => Type::listOf($registry->get("WidgetSetting")) ],
                "canEdit" => [ "type" => Type::nonNull(Type::boolean()) ],
                "parentGuid" => [
                    "type" => Type::nonNull(Type::string())
                ],
            ]
        ]);
    }
}

