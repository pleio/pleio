<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class UserListOverviewItem extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "UserListOverviewItem",
            "fields" => [
                "key" => [ "type" => Type::nonNull(Type::string()) ],
                "label" => [ "type" => Type::string() ]
            ]
        ]);
    }
}
