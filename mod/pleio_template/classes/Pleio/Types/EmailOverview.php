<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use Pleio\TypeRegistry;
use Pleio\Resolver;

class EmailOverview extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "EmailOverview",
            "fields" => [
                "frequency" => [
                    "type" => $registry->get("Frequency"),
                ],
                "tags" => [
                    "type" => Type::listOf(Type::string())
                ]
            ]
        ]);
    }
}
