<?php
namespace Pleio;

class Mapper {
    static function getEntity($entity) {
        if ($entity instanceof \ElggUser) {
            return Mapper::getUser($entity);
        } elseif ($entity instanceof \ElggGroup) {
            return Mapper::getGroup($entity);
        } elseif ($entity instanceof \ElggObject) {
            return Mapper::getObject($entity);
        }
    }

    static function getUser($entity) {
        return [
            "guid" => $entity->guid,
            "status" => 200,
            "ownerGuid" => $entity->owner_guid,
            "type" => $entity->type,
            "username" => $entity->username,
            "name" => html_entity_decode($entity->name, ENT_COMPAT | ENT_QUOTES, 'UTF-8'),
            "url" => Helpers::getURL($entity),
            "icon" => $entity->getIconURL("large"),
            "timeCreated" => $entity->time_created,
            "timeUpdated" => $entity->time_updated,
            "canEdit" => $entity->canEdit(),
            "requestDelete" => $entity->requestDelete ? true : false
        ];
    }

    static function getGroup($entity) {

        // If there is no richDescription this content is probably created
        // with the old template and we want to cleanup the html
        if (!$entity->richDescription) {
            $cleanDescription = Helpers::filterHtml(elgg_autop($entity->description));
        }

        return [
            "guid" => $entity->guid,
            "status" => 200,
            "type" => $entity->type,
            "name" => html_entity_decode($entity->name, ENT_COMPAT | ENT_QUOTES, 'UTF-8'),
            "isClosed" => ($entity->membership === ACCESS_PRIVATE) ? true : false,
            "isMembershipOnRequest" => Helpers::isMembershipOnRequest($entity),
            "isFeatured" => $entity->isFeatured ? true : false,
            "membership" => Helpers::getGroupMembership($entity),
            "description" => $cleanDescription?:$entity->description,
            "richDescription" => $entity->richDescription,
            "autoNotification" => $entity->autoNotification ? true : false,
            "isIntroductionReadableNonMembers" => $entity->isIntroductionReadableNonMembers ? true : false,
            "isLeavingGroupDisabled" => $entity->isLeavingGroupDisabled ? true : false,
            "isAutoMembershipEnabled" => $entity->isAutoMembershipEnabled ? true : false,
            "excerpt" => elgg_get_excerpt(html_entity_decode($entity->description, ENT_COMPAT | ENT_QUOTES, 'UTF-8'), 150),
            "introduction" => Helpers::getGroupIntroduction($entity),
            "isIntroductionPublic" => $entity->isIntroductionPublic,
            "plugins" => Helpers::renderArray($entity->plugins),
            "url" => Helpers::getURL($entity),
            "icon" => $entity->getIconURL("large"),
            "featured" => [
                "image" => $entity->featuredIcontime ? "/mod/pleio_template/featuredimage.php?guid={$entity->guid}&lastcache={$entity->featuredIcontime}" : "",
                "positionY" => isset($entity->featuredPositionY) ? $entity->featuredPositionY : 50,
                "video" => $entity->featuredVideo ? $entity->featuredVideo : ""
            ],
            "timeCreated" => $entity->time_created,
            "timeUpdated" => $entity->time_updated,
            "canEdit" => $entity->canEdit(),
            "tags" => Helpers::renderArray($entity->tags),
        ];
    }

    static function getObject($entity, $is_highlighted = false) {
        $subtype = $entity->getSubtype();
        if ($subtype === "groupforumtopic") {
            $subtype = "discussion";
        }

        // If there is no richDescription this content is probably created
        // with the old template and we want to cleanup the html
        if (!$entity->richDescription) {
            $cleanDescription = Helpers::filterHtml(elgg_autop($entity->description));
        }

        return [
            "guid" => $entity->guid,
            "status" => 200,
            "ownerGuid" => $entity->owner_guid,
            "type" => $entity->type,
            "subtype" => $subtype,
            "source" => $entity->source,
            "location" => $entity->location,
            "maxAttendees" => $entity->maxAttendees,
            "rsvp" => isset($entity->rsvp) ? $entity->rsvp : true,
            "isFeatured" => $entity->isFeatured,
            "attendEventWithoutAccount" => $entity->attend_event_without_account ? true : false,
            "isHighlighted" => $is_highlighted ? true : false,
            "isClosed" => $entity->isClosed ? true : false,
            "isFeatured" => $entity->isFeatured ? true : false,
            "featured" => [
                "image" => Helpers::getFeaturedImage($entity),
                "positionY" => isset($entity->featuredPositionY) ? $entity->featuredPositionY : 50,
                "video" => $entity->featuredVideo ? $entity->featuredVideo : ""
            ],
            "title" => html_entity_decode($entity->title, ENT_COMPAT | ENT_QUOTES, 'UTF-8'),
            "url" => Helpers::getURL($entity),
            "thumbnail" => $entity->getIconURL(),
            "description" => $cleanDescription?:$entity->description,
            "richDescription" => $entity->richDescription,
            "mimeType" => $entity->mimetype,
            "state" => $entity->state ? $entity->state : "NEW",
            "excerpt" => elgg_get_excerpt(html_entity_decode($entity->description, ENT_COMPAT | ENT_QUOTES, 'UTF-8')),
            "timeCreated" => date("c", $entity->time_created),
            "timeUpdated" => date("c", $entity->time_updated),
            "startDate" => Helpers::getEventStartDate($entity),
            "endDate" => Helpers::getEventEndDate($entity),
            "canEdit" => $entity->canEdit(),
            "canClose" => Helpers::canClose($entity),
            "accessId" => $entity->access_id,
            "writeAccessId" => $entity->write_access_id ? $entity->write_access_id : ACCESS_PRIVATE,
            "tags" => Helpers::renderArray($entity->tags),
            "pageType" => $entity->pageType ? $entity->pageType : "text",
        ];
    }

    static function getComment($entity) {
        return [
            "guid" => $entity->guid ? $entity->guid : "annotation:" . $entity->id,
            "ownerGuid" => $entity->owner_guid,
            "description" => $entity->description ?: $entity->value,
            "richDescription" => $entity->richDescription,
            "canEdit" => $entity->canEdit(),
            "timeCreated" => date("c", $entity->time_created),
            "timeUpdated" => date("c", $entity->time_updated)
        ];
    }

    static function getPage($entity) {

        // If there is no richDescription this content is probably created
        // with the old template and we want to cleanup the html
        if (!$entity->richDescription) {
            $cleanDescription = Helpers::filterHtml(elgg_autop($entity->description));
        }

        return [
            "guid" => $entity->guid,
            "status" => 200,
            "ownerGuid" => $entity->owner_guid,
            "canEdit" => $entity->canEdit(),
            "type" => $entity->type,
            "subtype" => "page",
            "pageType" => $entity->pageType ? $entity->pageType : "text",
            "title" => html_entity_decode($entity->title, ENT_COMPAT | ENT_QUOTES, 'UTF-8'),
            "description" => $cleanDescription?:$entity->description,
            "richDescription" => $entity->richDescription,
            "excerpt" => elgg_get_excerpt(html_entity_decode($entity->description, ENT_COMPAT | ENT_QUOTES, 'UTF-8')),
            "timeCreated" => $entity->timeCreated,
            "timeUpdated" => $entity->timeUpdated,
            "accessId" => $entity->access_id,
            "url" => Helpers::getURL($entity),
            "tags" => Helpers::renderArray($entity->tags)
        ];
    }

    static function getWiki($entity) {

        // If there is no richDescription this content is probably created
        // with the old template and we want to cleanup the html
        if (!$entity->richDescription) {
            $cleanDescription = Helpers::filterHtml(elgg_autop($entity->description));
        }

        return [
            "guid" => $entity->guid,
            "status" => 200,
            "ownerGuid" => $entity->owner_guid,
            "canEdit" => $entity->canEdit(),
            "type" => $entity->type,
            "subtype" => "wiki",
            "title" => html_entity_decode($entity->title, ENT_COMPAT | ENT_QUOTES, 'UTF-8'),
            "description" => $cleanDescription?:$entity->description,
            "richDescription" => $entity->richDescription,
            "excerpt" => elgg_get_excerpt(html_entity_decode($entity->description, ENT_COMPAT | ENT_QUOTES, 'UTF-8')),
            "timeCreated" => $entity->timeCreated,
            "timeUpdated" => $entity->timeUpdated,
            "accessId" => $entity->access_id,
            "writeAccessId" => $entity->write_access_id ? $entity->write_access_id : ACCESS_PRIVATE,
            "url" => Helpers::getURL($entity),
            "tags" => Helpers::renderArray($entity->tags),
            "isFeatured" => $entity->isFeatured,
        ];
    }

    static function getFile($entity) {
        $path = pathinfo($entity->getFilenameOnFilestore());

        return [
            "guid" => $entity->guid,
            "url" => "/file/download/{$entity->guid}/{$path['basename']}"
        ];
    }

    static function getRow($entity) {
        return [
            "guid" => $entity->guid,
            "containerGuid" => $entity->container_guid,
            "parentGuid" => $entity->parent_guid,
            "type" => "object",
            "subtype" => "row",
            "position" => is_array($entity->position) ? $entity->position[0] : $entity->position,
            "isFullWidth" => $entity->is_full_width ? true : false,
            "canEdit" => $entity->canEdit()
        ];
    }

    static function getColumn($entity) {
        # TODO: some columns have a array as position. How is this possible? Create a fix....
        return [
            "guid" => $entity->guid,
            "containerGuid" => $entity->container_guid,
            "parentGuid" => $entity->parent_guid,
            "type" => "object",
            "subtype" => "column",
            "position" => is_array($entity->position) ? $entity->position[0] : $entity->position,
            "canEdit" => $entity->canEdit(),
            "width" => Helpers::renderArray($entity->width),
        ];
    }

    static function getWidget($entity) {
        return [
            "guid" => $entity->guid,
            "containerGuid" => $entity->container_guid,
            "parentGuid" => $entity->parent_guid,
            "type" => $entity->widget_type ? $entity->widget_type : "Empty",
            "position" => is_array($entity->position) ? $entity->position[0] : $entity->position,
            "canEdit" => $entity->canEdit(),
            "settings" => $entity->getPrivateSetting("settings") ? json_decode($entity->getPrivateSetting("settings")) : []
        ];
    }

    static function getNotification($notification) {
        $performer = get_entity($notification->performer_guid);
        $entity = get_entity($notification->entity_guid);
        $container = get_entity($notification->container_guid);

        return [
            "id" => $notification->id,
            "action" => $notification->action,
            "performer" => $performer ? Mapper::getUser($performer) : null,
            "entity" => $entity ? Mapper::getEntity($entity) : null,
            "container" => $container ? Mapper::getEntity($container) : null,
            "isUnread" => $notification->unread === "yes" ? true : false,
            "timeCreated" => date("c", $notification->time_created)
        ];
    }
}
