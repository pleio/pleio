<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class EditProfileField {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "editProfileFieldPayload",
                "fields" => [
                    "user" => [
                        "type" => $registry->get("User"),
                        "resolve" => function($entity) {
                            return Resolver::getEntity(null, $entity, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "editProfileFieldInput",
                        "fields" => [
                            "guid" => [
                                "type" => Type::nonNull(Type::string())
                            ],
                            "accessId" => [
                                "type" => Type::nonNull(Type::int())
                            ],
                            "key" => [
                                "type" => Type::nonNull(Type::string())
                            ],
                            "value" => [
                                "type" => Type::nonNull(Type::string())
                            ],
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $site = elgg_get_site_entity();
        $entity = get_entity(((int) $input["guid"]));
        if (!$entity) {
            throw new Exception("could_not_find");
        }

        if (!$entity->canEdit()) {
            throw new Exception("could_not_save");
        }

        if (!$entity instanceof \ElggUser) {
            throw new Exception("not_a_user");
        }

        $key = $input["key"];
        $value = $input["value"] === null ? '' : $input["value"];

        if (!Helpers::isProfileFieldEditable($key)) {
            throw new Exception("field_not_editable");
        }

        $entityFields = [
            [ "key" => "name", "name" => "Naam" ],
        ];

        $configuredFields = elgg_get_plugin_setting("profile", "pleio_template") ? json_decode(elgg_get_plugin_setting("profile", "pleio_template"), true) : [];
        $profileFields = array_merge($entityFields, $configuredFields);

        if (!in_array($key, array_map(function($f) { return $f["key"]; }, $profileFields))) {
            throw new Exception("invalid_key");
        }

        // configured profile fields
        if (in_array($key, array_map(function($f) { return $f["key"]; }, $configuredFields))) {

            $accessId = isset($input["accessId"]) ? $input["accessId"] : ACCESS_LOGGED_IN;

            $fieldType = Helpers::getProfileFieldType($key);

            // Validate input
            if ($fieldType === 'dateField' && $value) {
                if (!Helpers::validateDate($value)) {
                    throw new Exception("invalid_date");
                }
            }

            if ($fieldType === 'multiSelectField') {
                $entity->deleteMetadata($key);

                $i = 0;
                $values = explode(',', $value);
                foreach($values as $v) {
                    $i++;
                    $multiple = ($i != 1);
                    $result = create_metadata($entity->guid, $key, trim($v), "", 0, $accessId, $multiple);
                }
            } else {
                $entity->deleteMetadata($key);
                $result = create_metadata($entity->guid, $key, $value, "", 0, $accessId, false);
            }

        } else { // save entity fields

            $entity->$key = $value;
            $result = $entity->save();

            if ($key === "name" && $value) {

                if ($result && elgg_is_active_plugin("pleio")) {
                   try {
                        $profile_handler = new \ModPleio\ProfileHandler($entity);
                        $profile_handler->changeName($value)->getBody()->getContents();
                    } catch (\Exception $e) {
                        // silently fail
                        error_log("Failed to update profile name: ".$e->getMessage());
                    }
                }
            }
        }

        if ($result) {
            elgg_trigger_event('update', 'user', $entity);

            return [
                "guid" => $entity->guid
            ];
        }

        throw new Exception("could_not_save");
    }
}
