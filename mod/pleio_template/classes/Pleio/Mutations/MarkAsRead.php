<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;
use Pleio\Mapper;

class MarkAsRead {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "markAsReadPayload",
                "fields" => [
                    "success" => [ "type" => Type::boolean() ],
                    "notification" => [ "type" => $registry->get("Notification") ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "markAsReadInput",
                        "fields" => [
                            "id" => [ "type" => Type::int() ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $dbprefix = elgg_get_config("dbprefix");

        $user = elgg_get_logged_in_user_entity();
        if (!$user) {
            return [
                "success" => false,
                "notification" => null
            ];
        }

        $id = (int) $input["id"];
        if (!$id) {
            throw new Exception("could_not_find");
        }

        $sql = "SELECT * FROM {$dbprefix}notifications WHERE id = {$id} AND user_guid = {$user->guid}";
        $notification = get_data_row($sql);

        if (!$notification) {
            throw new Exception("could_not_find");
        }

        $result = update_data("UPDATE {$dbprefix}notifications SET unread = 'no' WHERE id = {$id} AND user_guid = {$user->guid}");
        if ($result) {
            $notification = get_data_row($sql);

            return [
                "success" => true,
                "notification" => Mapper::getNotification($notification)
            ];
        }

        return [
            "success" => false,
            "notification" => null
        ];
    }
}
