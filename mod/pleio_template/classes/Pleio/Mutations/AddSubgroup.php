<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class AddSubgroup {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "addSubgroupPayload",
                "fields" => [
                    "success" => [ "type" => Type::boolean() ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "addSubgroupInput",
                        "fields" => [
                            "name" => [ "type" => Type::string() ],
                            "groupGuid" => [ "type" => Type::string() ],
                            "members" => [ "type" => Type::listOf(Type::string()) ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $group = get_entity($input["groupGuid"]);
        if (!$group || !$group instanceof \ElggGroup) {
            throw new Exception("could_not_find");
        }

        if (!$group->canEdit()) {
            throw new Exception("could_not_save");
        }

        $id = create_access_collection($input["name"], $group->guid);

        if ($group->subpermissions) {
            $subpermissions = unserialize($group->subpermissions);
        }

        if (!is_array($subpermissions)) {
            $subpermissions = array();
        }

        array_push($subpermissions, $id);

        $group->subpermissions = serialize($subpermissions);
        $group->save();

        update_access_collection($id, $input["members"]);

        return [
            "success" => true
        ];
    }
}
