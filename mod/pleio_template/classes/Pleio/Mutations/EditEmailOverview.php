<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class EditEmailOverview {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "editEmailOverviewPayload",
                "fields" => [
                    "user" => [
                        "type" => $registry->get("User"),
                        "resolve" => function($entity) {
                            return Resolver::getEntity(null, $entity, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "editEmailOverviewInput",
                        "fields" => [
                            "guid" => [ "type" => Type::string() ],
                            "frequency" => [ "type" => $registry->get("Frequency") ],
                            "tags" => [ "type" => Type::listOf(Type::string()) ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $site = elgg_get_site_entity();
        $entity = get_entity(((int) $input["guid"]));


        if (!$entity) {
            throw new Exception("could_not_find");
        }

        if (!$entity->canEdit()) {
            throw new Exception("could_not_save");
        }

        if (!$entity instanceof \ElggUser) {
            throw new Exception("not_a_user");
        }

        if (isset($input["frequency"])) {
            if (!in_array($input["frequency"], ["daily", "weekly", "twoweekly", "monthly", "never"])) {
                throw new Exception("invalid_value");
            }
    
            if ($updates === "never") {
                $entity->removePrivateSetting("email_overview_{$site->guid}");
            } else {
                $entity->setPrivateSetting("email_overview_{$site->guid}", $input["frequency"]);
            }
        }

        if (isset($input["tags"])) {
            $tags = filter_tags($input["tags"]);
            $entity->editEmailOverviewTags = $tags;
            $entity->save();        
        }

        return [
            "guid" => $entity->guid
        ];
    }
}
