<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class RejectMembershipRequest {
    public static function getMutation($registry) {
        return [
            "description" => "Reject a membership request to a group.",
            "type" => new ObjectType([
                "name" => "rejectMembershipRequestPayload",
                "fields" => [
                    "group" => [
                        "type" => $registry->get("Group"),
                        "resolve" => function($group) {
                            return Resolver::getEntity(null, $group, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "rejectMembershipRequestInput",
                        "fields" => [
                            "userGuid" => [
                                "type" => Type::string(),
                                "description" => "The guid of the user."
                            ],
                            "groupGuid" => [
                                "type" => Type::string(),
                                "description" => "The guid of the group."
                            ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $site = elgg_get_site_entity();
        $logged_in_user = elgg_get_logged_in_user_entity();

        $group = get_entity($input["groupGuid"]);
        $user = get_entity($input["userGuid"]);

        if (!$group || !$group instanceof \ElggGroup) {
            throw new Exception("could_not_find_group");
        }

        if (!$user || !$user instanceof \ElggUser) {
            throw new Exception("could_not_find_group");
        }

        if (!$group->canEdit()) {
            throw new Exception("could_not_save");
        }

        remove_entity_relationship($user->guid, "membership_request", $group->guid);

        $result = elgg_send_email(
            $site->email ? $site->email : "noreply@" . get_site_domain($site->guid),
            $user->email,
            elgg_echo("pleio_template:group:membership:reject_request:subject", [ $group->name ]),
            elgg_echo("pleio_template:group:membership:reject_request:body", [ $logged_in_user->name, $group->name ])
        );

        return [
            "guid" => $group->guid
        ];
    }
}
