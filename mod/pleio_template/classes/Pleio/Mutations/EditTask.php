<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class EditTask {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "editTaskPayload",
                "fields" => [
                    "entity" => [
                        "type" => $registry->get("Task"),
                        "resolve" => function($entity) {
                            return Resolver::getEntity(null, $entity, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "editTaskInput",
                        "fields" => [
                            "guid" => [ "type" => Type::string() ],
                            "state" => [ "type" => Type::string() ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $task = get_entity((int) $input["guid"]);
        if (!$task) {
            throw new Exception("could_not_find");
        }

        if (!$task->canEdit() || !$task instanceof \ElggObject || $task->getSubtype() !== "task") {
            throw new Exception("could_not_save");
        }

        $task->state = $input["state"];

        $result = $task->save();

        if ($result) {
            return [
                "guid" => $task->guid
            ];
        }

        throw new Exception("could_not_save");
    }
}
