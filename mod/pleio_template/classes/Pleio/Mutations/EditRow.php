<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class EditRow {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "editRowPayload",
                "fields" => [
                    "row" => [
                        "type" => $registry->get("Row"),
                        "resolve" => function($entity) {
                            return Resolver::getEntity(null, $entity, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "editRowInput",
                        "fields" => [
                            "guid" => [ "type" => Type::nonNull(Type::string()) ],
                            "parentGuid" => [ "type" => Type::string() ],
                            "position" => [ "type" => Type::int() ],
                            "isFullWidth" => [ "type" => Type::boolean() ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        if ($input["parentGuid"]) {
            $container = get_entity($input["parentGuid"]);
            if (!$container || !in_array($container->getSubtype(), array("page", "column"))) {
                throw new Exception("could_not_find");
            }
        }

        $row = get_entity((int) $input["guid"]);
        $parentGuidIsEdited = false;
        $positionIsEdited = false;
        $oldPosition = NULL;
        if (!$row) {
            throw new Exception("could_not_find");
        }

        if (!$row->canEdit()) {
            throw new Exception("could_not_save");
        }

        if ($input["parentGuid"]) {
            if ($input['parentGuid'] != $row->parent_guid) {
                $previousParentGuid = $row->parent_guid;
                $row->parent_guid = $input["parentGuid"];
                $parentGuidIsEdited = true;
            }
        }

        if (isset($input["position"])) {
            if ($input['position'] != $row->position) {
                $oldPosition = $row->position;
                $row->position = $input["position"];
                $positionIsEdited = true;
            }
        }

        if ($input["isFullWidth"]) {
            $row->is_full_width = $input["isFullWidth"];
        }
       
        $result = $row->save();

        // Reorder the objects with parentGuid of row
        if ($positionIsEdited || $parentGuidIsEdited) {
            Helpers::raisePositionOfSiblingsWithSameParentGuid($row, $oldPosition);
            Helpers::orderEntitiesWithSameParentGuidByPosition($row->parent_guid, $row->getSubtype());
        }

        // Reorder position of the objects with previous parentGuid
        if ($parentGuidIsEdited) {
            Helpers::orderEntitiesWithSameParentGuidByPosition($previousParentGuid, $row->getSubtype());
        }


        if ($result) {
            return [
                "guid" => $row->guid
            ];
        }

        throw new Exception("could_not_save");
    }
}
