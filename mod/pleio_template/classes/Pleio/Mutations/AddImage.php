<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class AddImage {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "addImagePayload",
                "fields" => [
                    "file" => [
                        "type" => $registry->get("FileFolder"),
                        "resolve" => function($file) {
                            return Resolver::getFile(null, $file, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "addImageInput",
                        "fields" => [
                            "image" => [
                                "type" => Type::nonNull($registry->get("Upload"))
                            ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $user = elgg_get_logged_in_user_entity();
        if (!$user) {
            throw new Exception("could_not_save");
        }

        $result = Helpers::saveToImage($input["image"], $user);

        if ($result) {
            return [
                "guid" => $result->guid
            ];
        }

        throw new Exception("could_not_save");
    }
}
