<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class EditGroupNotifications {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "editGroupNotificationsPayload",
                "fields" => [
                    "group" => [
                        "type" => $registry->get("Group"),
                        "resolve" => function($entity) {
                            return Resolver::getEntity(null, $entity, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "editGroupNotificationsInput",
                        "fields" => [
                            "guid" => [
                                "type" => Type::nonNull(Type::string())
                            ],
                            "getsNotifications" => [
                                "type" => Type::nonNull(Type::boolean())
                            ],
                            "userGuid" => [
                                "type" => Type::string()
                            ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $currentUser = elgg_get_logged_in_user_entity();
        if (!$currentUser) {
            throw new Exception("not_logged_in");
        }

        if ($input["userGuid"]) {
            $user = get_entity($input["userGuid"]);

            if (!$user || !$user instanceof \ElggUser) {
                throw new Exception("invalid_user");
            }

            // only return if currentUser is admin or self!
            if ($user->guid !== $currentUser->guid && !$currentUser->isAdmin()) {
                throw new Exception("access_denied");
            }
        } else {
            $user = $currentUser;
        }

        $group = get_entity(((int) $input["guid"]));
        if (!$group || !$group instanceof \ElggGroup) {
            throw new Exception("could_not_find");
        }

        if (!$group->isMember($user)) {
            throw new Exception("user_not_member_of_group");
        }

        $getsNotifications = $input["getsNotifications"];
        if ($getsNotifications) {
            add_entity_relationship($user->guid, "subscribed", $group->guid);
        } else {
            remove_entity_relationship($user->guid, "subscribed", $group->guid);
        }

        return  [
            "guid" => $group->guid
        ];
    }
}
