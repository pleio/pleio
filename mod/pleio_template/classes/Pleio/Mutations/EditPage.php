<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class EditPage {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "editPagePayload",
                "fields" => [
                    "entity" => [
                        "type" => $registry->get("Entity"),
                        "resolve" => function($entity) {
                            return Resolver::getEntity(null, $entity, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "editPageInput",
                        "fields" => [
                            "guid" => [ "type" => Type::nonNull(Type::string()) ],
                            "title" => [ "type" => Type::string() ],
                            "description" => [ "type" => Type::nonNull(Type::string()) ],
                            "richDescription" => [ "type" => Type::string() ],
                            "accessId" => [ "type" => Type::int() ],
                            "tags" => [ "type" => Type::listOf(Type::string()) ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $entity = get_entity((int) $input["guid"]);
        if (!$entity) {
            throw new Exception("could_not_find");
        }

        if (!$entity->canEdit()) {
            throw new Exception("could_not_save");
        }

        $entity->title = $input["title"];

        $entity->description = $input["description"];

        // throw error when mysql TEXT limit is reached
        if (strlen($input["richDescription"]) > 65000) {
            throw new Exception("rich_description_limit_reached");
        }

        $entity->richDescription = $input["richDescription"];
        
        if (isset($input["accessId"]) && $input["accessId"] != $entity->access_id) {
            $entity->access_id = (int) $input["accessId"];

            foreach (Helpers::getChildren($entity, "row") as $row) {
                $row->access_id = (int) $input["accessId"];
                $row->save();
            }

            foreach (Helpers::getChildren($entity, "column") as $column) {
                $column->access_id = (int) $input["accessId"];
                $column->save();
            }

            foreach (Helpers::getChildren($entity, "page_widget") as $widget) {
                $widget->access_id = (int) $input["accessId"];
                $widget->save();
            }
        }

        $entity->tags = filter_tags($input["tags"]);

        $result = $entity->save();

        if ($result) {
            return [
                "guid" => $entity->guid
            ];
        }

        throw new Exception("could_not_save");
    }
}
