<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class DeleteWidget {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "deleteWidgetPayload",
                "fields" => [
                    "success" => [ "type" => Type::boolean() ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "deleteWidgetInput",
                        "fields" => [
                            "guid" => [
                                "type" => Type::nonNull(Type::string())
                            ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $entity = get_entity((int) $input["guid"]);
        if (!$entity) {
            throw new Exception("could_not_find");
        }
        $subtype = $entity->getSubtype();
        if (!in_array($entity->type, array("group", "object"))) {
            throw new Exception("invalid_object_type");
        }

        if (!in_array($subtype, array("page_widget"))) {
            throw new Exception("invalid_object_type");
        }

        $parent_guid = $entity->parent_guid;
        Helpers::deleteCmsObjectsWithParentGuid($entity->guid);

        $result = $entity->delete();

        if ($parent_guid) {
            Helpers::orderEntitiesWithSameParentGuidByPosition($parent_guid, $subtype);
        }

        if ($result) {
            return [
                "success" => true
            ];
        }

        throw new Exception("could_not_delete");
    }
}
