<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class AddColumn {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "addColumnPayload",
                "fields" => [
                    "column" => [
                        "type" => $registry->get("Column"),
                        "resolve" => function($entity) {
                            return Resolver::getEntity(null, $entity, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "addColumnInput",
                        "fields" => [
                            "position" => [ "type" => Type::nonNull(Type::int()) ],
                            "containerGuid" => [ "type" => Type::nonNull(Type::string()) ],
                            "parentGuid" => [ "type" => Type::nonNull(Type::string()) ],
                            "width" => [ "type" => Type::listOf(Type::int())],
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $page = get_entity($input["containerGuid"]);
        $container = get_entity($input["parentGuid"]);
        if (!$container || !$page || $page->getSubtype() != 'page' || !in_array($container->getSubtype(), array("row"))) {
            throw new Exception("could_not_find");
        }

        if (!$container->canEdit()) {
            throw new Exception("could_not_save");
        }
    
        $column = new \ElggObject();
        $column->subtype = "column";
        $column->parent_guid = $input["parentGuid"];
        $column->position = $input["position"];
        $column->width = $input["width"];
        $column->container_guid = $input["containerGuid"];
        $column->access_id = $container->access_id;
        $result = $column->save();

        Helpers::raisePositionOfSiblingsWithSameParentGuid($column, $input["position"]);
        Helpers::orderEntitiesWithSameParentGuidByPosition($column->parent_guid, $column->getSubtype());

        if ($result) {
            return [
                "guid" => $column->guid
            ];
        }

        throw new Exception("could_not_save");
    }
}
