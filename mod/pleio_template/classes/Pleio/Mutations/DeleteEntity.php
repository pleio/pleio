<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class DeleteEntity {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "deleteEntityPayload",
                "fields" => [
                    "success" => [ "type" => Type::boolean() ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "deleteEntityInput",
                        "fields" => [
                            "guid" => [
                                "type" => Type::nonNull(Type::string())
                            ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $entity = get_entity((int) $input["guid"]);
        if (!$entity) {
            throw new Exception("could_not_find");
        }
        $subtype = $entity->getSubtype();
        if (!in_array($entity->type, array("group", "object"))) {
            throw new Exception("invalid_object_type");
        }

        if (in_array($subtype, array("row", "column", "page_widget"))) {
            throw new Exception("invalid_object_type");
        }

        if (in_array($subtype, array("file", "folder"))) {
            $container = Resolver::getParentFolder($entity);
            if ($container && $container instanceof \ElggObject) {
                Helpers::setParentFoldersTimeUpdated($container);
            }
        }

        $result = $entity->delete();

        if ($result) {
            return [
                "success" => true
            ];
        }

        throw new Exception("could_not_delete");
    }
}
