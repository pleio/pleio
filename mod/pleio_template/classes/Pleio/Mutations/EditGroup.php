<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class EditGroup {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "editGroupPayload",
                "fields" => [
                    "group" => [
                        "type" => $registry->get("Group"),
                        "resolve" => function($group) {
                            return Resolver::getEntity(null, $group, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "editGroupInput",
                        "fields" => [
                            "guid" => [ "type" => Type::string() ],
                            "name" => [ "type" => Type::string() ],
                            "icon" => [ "type" => $registry->get("Upload") ],
                            "featured" => [ "type" => $registry->get("FeaturedInput") ],
                            "isClosed" => [ "type" => Type::boolean(), "description" => "True when membership has to be requested by the user, False when every user can join the group." ],
                            "isMembershipOnRequest" => [ "type" => Type::boolean(), "description" => "True when membership has to be requested by the user, False when every user can join the group." ],
                            "isFeatured" => [ "type" => Type::boolean() ],
                            "autoNotification" => [ "type" => Type::boolean() ],
                            "isLeavingGroupDisabled" => [ "type" => Type::boolean() ],
                            "isAutoMembershipEnabled" => [ "type" => Type::boolean() ],
                            "description" => [ "type" => Type::string() ],
                            "richDescription" => [ "type" => Type::string() ],
                            "introduction" => [ "type" => Type::string() ],
                            "isIntroductionPublic" => [ "type" => Type::boolean() ],
                            "welcomeMessage" => [ "type" => Type::string() ],
                            "tags" => [ "type" => Type::listOf(Type::string()) ],
                            "plugins" => [ "type" => Type::listOf($registry->get("Plugins")) ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $group = get_entity((int) $input["guid"]);
        if (!$group) {
            throw new Exception("could_not_find");
        }

        if (!$group->canEdit() || !$group instanceof \ElggGroup) {
            throw new Exception("could_not_save");
        }

        if ($input["icon"]) {
            Helpers::saveToIcon($input["icon"], $group);
            $group->icontime = time();
        } else {
            unset($group->icontime);
        }

        // throw error when mysql TEXT limit is reached
        if (strlen($input["richDescription"]) > 65000) {
            throw new Exception("rich_description_limit_reached");
        }

        $group->name = $input["name"];
        $group->membership = $input["isClosed"] ? ACCESS_PRIVATE : ACCESS_PUBLIC;
        $group->isMembershipOnRequest = $input["isMembershipOnRequest"];
        $group->access_id = ACCESS_PUBLIC;
        $group->description = $input["description"];
        $group->richDescription = $input["richDescription"];
        $group->introduction = $input["introduction"];

        $group->plugins = array_unique($input["plugins"]);

        if (isset($input["tags"])) {
            $group->tags = filter_tags($input["tags"]);
        }

        if (elgg_is_admin_logged_in() && isset($input["isFeatured"])) {
            $group->isFeatured = $input["isFeatured"];
        }

        if (isset($input["autoNotification"])) {
            if ($input["autoNotification"]) {
                $group->autoNotification = $input["autoNotification"];
            } else {
                unset($group->autoNotification);
            }
        }

        if (isset($input["isIntroductionPublic"])) {
            $group->isIntroductionPublic = $input["isIntroductionPublic"];
        }

        if (elgg_is_admin_logged_in() && isset($input["isLeavingGroupDisabled"])) {
            if ($input["isLeavingGroupDisabled"]) {
                $group->isLeavingGroupDisabled = $input["isLeavingGroupDisabled"];
            } else {
                unset($group->isLeavingGroupDisabled);
            }
        }

        if (elgg_is_admin_logged_in() && isset($input["isAutoMembershipEnabled"])) {
            if ($input["isAutoMembershipEnabled"]) {
                $group->isAutoMembershipEnabled = $input["isAutoMembershipEnabled"];
            } else {
                unset($group->isAutoMembershipEnabled);
            }
        }

        if ($input["featured"]) {
            if ($input["featured"]["image"]) {
                Helpers::saveToFeatured($input["featured"]["image"], $group);
                $group->featuredIcontime = time();
            }

            if ($input["featured"]["video"]) {
                $group->featuredVideo = $input["featured"]["video"];
            } else {
                unset($group->featuredVideo);
            }

            if (isset($input["featured"]["positionY"])) {
                $group->featuredPositionY = (int) $input["featured"]["positionY"];
            } else {
                unset($group->featuredPositionY);
            }
        } else {
            unset($group->featuredIcontime);
            unset($group->featuredVideo);
            unset($group->featuredPositionY);
        }

        if (!empty(trim(strip_tags($input["welcomeMessage"])))) {
            $group->setPrivateSetting("group_tools:welcome_message", $input["welcomeMessage"]);
        } else {
            $group->removePrivateSetting("group_tools:welcome_message");
        }

        $result = $group->save();

        if ($result) {
            return [
                "guid" => $group->guid
            ];
        }

        throw new Exception("could_not_save");
    }
}
