<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class AcceptGroupInvitation {
    public static function getMutation($registry) {
        return [
            "description" => "Accept a group invitation.",
            "type" => new ObjectType([
                "name" => "acceptGroupInvitationPayload",
                "fields" => [
                    "group" => [
                        "type" => $registry->get("Group"),
                        "resolve" => function($group) {
                            return Resolver::getEntity(null, $group, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "acceptGroupInvitationInput",
                        "fields" => [
                            "code" => [
                                "type" => Type::string(),
                                "description" => "The unique invite code."
                            ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $user = elgg_get_logged_in_user_entity();
        if (!$user) {
            throw new Exception("not_logged_in");
        }

        $group = Helpers::group_tools_check_group_email_invitation($input["code"]);
        if (!$group) {
            throw new Exception("invalid_code");
        }

        groups_join_group($group, $user);

        $options = array(
            "guid" => $group->guid,
            "annotation_name" => "email_invitation",
            "wheres" => array("(v.string = '" . sanitize_string($input["code"]) . "' OR v.string LIKE '" . sanitize_string($input["code"]) . "|%')"),
            "annotation_owner_guid" => $group->guid,
            "limit" => 1
        );

        $annotations = elgg_get_annotations($options);
        if (!empty($annotations)) {
            $ia = elgg_set_ignore_access(true);
            $annotations[0]->delete();
            elgg_set_ignore_access($ia);
        }

        return [
            "guid" => $group->guid
        ];
    }
}
