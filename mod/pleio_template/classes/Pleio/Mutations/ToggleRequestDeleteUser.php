<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class ToggleRequestDeleteUser {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "toggleRequestDeleteUserPayload",
                "fields" => [
                    "viewer" => [
                        "type" => $registry->get("Viewer"),
                        "resolve" => "Pleio\Resolver::getViewer"
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "toggleRequestDeleteUserInput",
                        "fields" => [
                            "guid" => [
                                "type" => Type::nonNull(Type::string())
                            ]   
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $user = elgg_get_logged_in_user_entity();
        $site = elgg_get_site_entity();
        $guid = $input["guid"];

        // Can only request deletion for own account
        if ($user->guid != $guid) {
            throw new Exception("could_not_save");
        }

        if ($user->requestDelete) {
            unset($user->requestDelete);
            elgg_send_email(
                $site->email ? $site->email : "noreply@" . get_site_domain($site->guid),
                $user->email,
                elgg_echo("pleio_template:account:request:cancel:delete:subject"),
                elgg_echo("pleio_template:account:request:cancel:delete:message:user", [ $user->name ])
            );
            $admins = elgg_get_admins();
            foreach ($admins as $admin) {
                elgg_send_email(
                    $site->email ? $site->email : "noreply@" . get_site_domain($site->guid),
                    $admin->email,
                    elgg_echo("pleio_template:account:request:cancel:delete:subject"),
                    elgg_echo("pleio_template:account:request:cancel:delete:message:admin", [ $user->name, $site->url ])
                );
            }
        } else {
            $user->requestDelete = true;
            elgg_send_email(
                $site->email ? $site->email : "noreply@" . get_site_domain($site->guid),
                $user->email,
                elgg_echo("pleio_template:account:request:delete:subject"),
                elgg_echo("pleio_template:account:request:delete:message:user", [ $user->name ])
            );
            $admins = elgg_get_admins();
            foreach ($admins as $admin) {
                elgg_send_email(
                    $site->email ? $site->email : "noreply@" . get_site_domain($site->guid),
                    $admin->email,
                    elgg_echo("pleio_template:account:request:delete:subject"),
                    elgg_echo("pleio_template:account:request:delete:message:admin", [ $user->name, $site->url ])
                );
            }
        }
    }
}
