<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class LeaveGroup {
    public static function getMutation($registry) {
        return [
            "description" => "Leave a group.",
            "type" => new ObjectType([
                "name" => "leaveGroupPayload",
                "fields" => [
                    "group" => [
                        "type" => $registry->get("Group"),
                        "resolve" => function($group) {
                            return Resolver::getEntity(null, $group, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "leaveGroupInput",
                        "fields" => [
                            "guid" => [
                                "type" => Type::string(),
                                "description" => "The guid of the group to leave."
                            ],
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $group = get_entity((int) $input["guid"]);
        if (!$group || !$group instanceof \ElggGroup) {
            throw new Exception("could_not_find");
        }

        if ($group->isLeavingGroupDisabled) {
            throw new Exception("leaving_group_is_disabled");
        }

        $user = elgg_get_logged_in_user_entity();
        if (!$user) {
            throw new Exception("not_logged_in");
        }

        if ($group->owner_guid == $user->guid) {
            throw new Exception("could_not_leave");
        }

        if (!$group->leave($user)) {
            throw new Exception("could_not_leave");
        }

        if ($group->group_acl) {
            remove_user_from_access_collection($user->guid, $group->group_acl);
        }

        remove_entity_relationship($user->guid, "membership_request", $group->guid);
        remove_entity_relationship($user->guid, "invited", $group->guid);

        return [
            "guid" => $group->guid
        ];
    }
}
