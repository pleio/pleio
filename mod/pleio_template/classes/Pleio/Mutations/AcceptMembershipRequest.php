<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class AcceptMembershipRequest {
    public static function getMutation($registry) {
        return [
            "description" => "Accept a membership request to a group.",
            "type" => new ObjectType([
                "name" => "acceptMembershipRequestPayload",
                "fields" => [
                    "group" => [
                        "type" => $registry->get("Group"),
                        "resolve" => function($group) {
                            return Resolver::getEntity(null, $group, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "acceptMembershipRequestInput",
                        "fields" => [
                            "userGuid" => [
                                "type" => Type::string(),
                                "description" => "The guid of the user."
                            ],
                            "groupGuid" => [
                                "type" => Type::string(),
                                "description" => "The guid of the group."
                            ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $site = elgg_get_site_entity();
        $logged_in_user = elgg_get_logged_in_user_entity();

        $group = get_entity($input["groupGuid"]);
        $user = get_entity($input["userGuid"]);

        if (!$group || !$group instanceof \ElggGroup) {
            throw new Exception("could_not_find_group");
        }

        if (!$user || !$user instanceof \ElggUser) {
            throw new Exception("could_not_find_group");
        }

        if (!$group->canEdit()) {
            throw new Exception("could_not_save");
        }

        $relationship = check_entity_relationship($user->guid, "membership_request", $group->guid);
        if (!$relationship) {
            throw new Exception("could_not_find_membership_request");
        }

        groups_join_group($group, $user);
        remove_entity_relationship($user->guid, "membership_request", $group->guid);

        $link = Helpers::getURL($group, true);

        $result = elgg_send_email(
            $site->email ? $site->email : "noreply@" . get_site_domain($site->guid),
            $user->email,
            elgg_echo("pleio_template:group:membership:accept_request:subject", [ $group->name ]),
            elgg_echo("pleio_template:group:membership:accept_request:body", [ $logged_in_user->name, $group->name, $link, $link ])
        );

        return [
            "guid" => $group->guid
        ];
    }
}
