<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class SendMessageToUser {
    public static function getMutation($registry) {
        return [
            "description" => "Send a message to a user.",
            "type" => new ObjectType([
                "name" => "sendMessageToUserPayload",
                "fields" => [
                    "success" => [ "type" => Type::boolean() ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "sendMessageToUserInput",
                        "fields" => [
                            "guid" => [
                                "type" => Type::string(),
                                "description" => "The guid of the user to send the message to."
                            ],
                            "subject" => [
                                "type" => Type::string(),
                                "description" => "The subject of the message."
                            ],
                            "message" => [
                                "type" => Type::string(),
                                "description" => "The message to send."
                            ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        set_time_limit(0);

        $site = elgg_get_site_entity();
        $current_user = elgg_get_logged_in_user_entity();

        $user = get_entity((int) $input["guid"]);

        if (!$user) {
            throw new Exception("could_not_find");
        }

        if (!$user instanceof \ElggUser) {
            throw new Exception("could_not_send");
        }

        $result = elgg_send_email(
            $site->email ? $site->email : "noreply@" . get_site_domain($site->guid),
            $user->email,
            elgg_echo("pleio_template:message:subject", [ $current_user->name, $input['subject'] ]),
            elgg_echo("pleio_template:message:body", [ $input['message'], elgg_get_site_url($site->guid) , Helpers::getURL($current_user, true), $current_user->name ])
        );

        return [
            "success" => $result
        ];
    }
}
