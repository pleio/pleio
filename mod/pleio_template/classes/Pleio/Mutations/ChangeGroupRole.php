<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class ChangeGroupRole {
    public static function getMutation($registry) {
        return [
            "description" => "Change the role of a user in a group.",
            "type" => new ObjectType([
                "name" => "changeGroupRolePayload",
                "fields" => [
                    "group" => [
                        "type" => $registry->get("Group"),
                        "resolve" => function($group) {
                            return Resolver::getEntity(null, $group, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "changeGroupRoleInput",
                        "fields" => [
                            "guid" => [
                                "type" => Type::string(),
                                "description" => "The group guid."
                            ],
                            "userGuid" => [
                                "type" => Type::string(),
                                "description" => "The user guid."
                            ],
                            "role" => [
                                "type" => $registry->get("Role"),
                                "description" => "The new role for the user."
                            ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $site = elgg_get_site_entity();

        $current_user = elgg_get_logged_in_user_entity();
        if (!$current_user) {
            throw new Exception("not_logged_in");
        }

        $group = get_entity((int) $input["guid"]);
        if (!$group || !$group instanceof \ElggGroup) {
            throw new Exception("could_not_find_group");
        }

        if (!$group->canEdit()) {
            throw new Exception("could_not_save");
        }

        $user = get_entity((int) $input["userGuid"]);
        if (!$user || !$user instanceof \ElggUser) {
            throw new Exception("could_not_find_user");
        }

        if (!$group->isMember($user)) {
           throw new Exception("user_not_member_of_group");
        }

        $role = $input["role"];

        switch ($role) {
            case "owner":
                if (!$current_user->isAdmin() && $current_user->guid != $group->owner_guid) {
                    throw new Exception("user_not_group_owner_or_site_admin");
                }

                Helpers::transferGroupOwnership($group, $user);

                $link = Helpers::getURL($group, true);

                $result = elgg_send_email(
                    $site->email ? $site->email : "noreply@" . get_site_domain($site->guid),
                    $user->email,
                    elgg_echo("pleio_template:group:membership:change_role:subject", [ $group->name ]),
                    elgg_echo("pleio_template:group:membership:change_role:body", [ $current_user->name, $group->name, $link, $link ])
                );

                break;
            case "admin":
                add_entity_relationship($user->guid, "group_admin", $group->guid);
                break;
            case "member":
                remove_entity_relationship($user->guid, "group_admin", $group->guid);
                break;
            case "removed":
                remove_entity_relationship($user->guid, "group_admin", $group->guid);
                leave_group($group->guid, $user->guid);
                break;
        }

        return [
            "guid" => $group->guid
        ];
    }
}
