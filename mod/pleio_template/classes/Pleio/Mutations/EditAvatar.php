<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;
use Psr\Http\Message\UploadedFileInterface;

class EditAvatar {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "editAvatarPayload",
                "fields" => [
                    "user" => [
                        "type" => $registry->get("User"),
                        "resolve" => function($entity) {
                            return Resolver::getEntity(null, $entity, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "editAvatarInput",
                        "fields" => [
                            "guid" => [
                                "type" => Type::nonNull(Type::string())
                            ],
                            "avatar" => [
                                "type" => Type::nonNull($registry->get("Upload")),
                                "description" => "UploadFile"
                            ],
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $entity = get_entity(((int) $input["guid"]));
        if (!$entity) {
            throw new Exception("could_not_find");
        }

        if (!$entity->canEdit()) {
            throw new Exception("could_not_save");
        }

        $file = $input["avatar"];

        if (!$file instanceof UploadedFileInterface) {
            throw new Exception("no_file");
        }

        if ($file->getError() !== UPLOAD_ERR_OK) {
            switch($file->getError()) {
                case UPLOAD_ERR_INI_SIZE:
                    throw new Exception("invalid_filesize");
                    break;
                default:
                    throw new Exception("no_file");
            }
        }

        if (elgg_is_active_plugin("pleio")) {
            $profile_handler = new \ModPleio\ProfileHandler($entity);
            $profile_handler->changeAvatar($file);
        } else {
            Helpers::saveToIcon($file, $entity);
        }

        $entity->icontime = time();
        $result = $entity->save();

        if ($result) {
            return [
                "guid" => $entity->guid
            ];
        }

        throw new Exception("could_not_save");
    }
}
