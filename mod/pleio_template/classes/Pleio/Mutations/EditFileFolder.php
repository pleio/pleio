<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;
use Psr\Http\Message\UploadedFileInterface;

class EditFileFolder {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "editFileFolderPayload",
                "fields" => [
                    "entity" => [
                        "type" => $registry->get("Entity"),
                        "resolve" => function($entity) {
                            return Resolver::getEntity(null, $entity, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "editFileFolderInput",
                        "fields" => [
                            "guid" => [ "type" => Type::string() ],
                            "title" => [ "type" => Type::string() ],
                            "file" => [ "type" => $registry->get("Upload") ],
                            "accessId" => [ "type" => Type::int() ],
                            "writeAccessId" => [ "type" => Type::int() ],
                            "isAccessRecursive" => [ "type" => Type::boolean() ],
                            "tags" => [ "type" => Type::listOf(Type::string()) ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $entity = get_entity((int) $input["guid"]);
        if (!$entity) {
            throw new Exception("could_not_find");
        }

        if (!$entity->canEdit()) {
            throw new Exception("could_not_save");
        }

        if (!in_array($entity->getSubtype(), array("file", "folder"))) {
            throw new Exception("invalid_object_subtype");
        }

        if (isset($input["title"])) {
            $entity->title = $input["title"];
            if ($entity->getSubtype() === "file") {

                $extension = Helpers::getFileExtension($entity->originalfilename);
                $fullExtension = "." . Helpers::getFileExtension($entity->originalfilename);

                $filename = Helpers::sanitizeFilename($input["title"]);

                if (Helpers::endsWith($filename, $fullExtension) || !$extension) {
                    $entity->originalfilename = $filename;
                }
                else {
                    $entity->originalfilename = $filename . $fullExtension;
                }
            }
        }

        if (isset($input["accessId"])) {
            $entity->access_id = $input["accessId"];
        }

        if (isset($input["writeAccessId"])) {
            $entity->write_access_id = $input["writeAccessId"];
        }

        if (isset($input["isAccessRecursive"]) && $entity->getSubtype() == "folder" && $input["isAccessRecursive"] == true) {
            Helpers::setRecursiveFileAccess($entity);
        }

        if (isset($input["tags"])) {
            $entity->tags = filter_tags($input["tags"]);
        }

        if ($entity->getSubtype() === "file" && $input["file"]) {
            $file = $input["file"];

            if (!$file instanceof UploadedFileInterface) {
                throw new Exception("no_file");
            }

            if ($file->getError() !== UPLOAD_ERR_OK) {
                switch($file->getError()) {
                    case UPLOAD_ERR_INI_SIZE:
                        throw new Exception("invalid_filesize");
                        break;
                    default:
                        throw new Exception("no_file");
                }
            }

            $file->moveTo($entity->getFilenameOnFilestore());
            $entity->originalfilename = $file->getClientFilename();
            $mime_type = \ElggFile::detectMimeType($entity->getFilenameOnFilestore(), $file->getClientMediaType());
            $entity->setMimeType($mime_type);
            $entity->simpletype = file_get_simple_type($mime_type);
        }

        $result = $entity->save();


        $container = Resolver::getParentFolder($entity);
        if ($container instanceof \ElggObject) {
            Helpers::setParentFoldersTimeUpdated($container);
        }

        if ($result) {
            return [
                "guid" => $entity->guid
            ];
        }

        throw new Exception("could_not_save");
    }
}
