<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class AddRow {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "addRowPayload",
                "fields" => [
                    "row" => [
                        "type" => $registry->get("Row"),
                        "resolve" => function($entity) {
                            return Resolver::getEntity(null, $entity, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "addRowInput",
                        "fields" => [
                            "position" => [ "type" => Type::nonNull(Type::int()) ],
                            "containerGuid" => [ "type" => Type::nonNull(Type::string()) ],
                            "parentGuid" => [ "type" => Type::nonNull(Type::string()) ],
                            "isFullWidth" => [ "type" => Type::nonNull(Type::boolean()) ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $page = get_entity($input["containerGuid"]);
        $container = get_entity($input["parentGuid"]);
        if (!$container || !$page || $page->getSubtype() != 'page' || !in_array($container->getSubtype(), array("page", "column"))) {
            throw new Exception("could_not_find");
        }

        if (!$container->canEdit()) {
            throw new Exception("could_not_save");
        }

        $row = new \ElggObject();
        $row->subtype = "row";
        $row->parent_guid = $input["parentGuid"];
        $row->position = $input["position"];
        $row->is_full_width = $input["isFullWidth"];
        $row->container_guid = $input["containerGuid"];
        $row->access_id = $container->access_id;
        $result = $row->save();

        Helpers::raisePositionOfSiblingsWithSameParentGuid($row, $input["position"]);
        Helpers::orderEntitiesWithSameParentGuidByPosition($row->parent_guid, $row->getSubtype());

        if ($result) {
            return [
                "guid" => $row->guid
            ];
        }

        throw new Exception("could_not_save");
    }
}
