<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class ToggleIsClosed {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "toggleIsClosedPayload",
                "fields" => [
                    "entity" => [
                        "type" => Type::nonNull($registry->get("Question")),
                        "resolve" => function($entity, array $args, $context, ResolveInfo $info) {
                            return Resolver::getEntity(null, $entity, null);
                        }
                    ],
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "toggleIsClosedInput",
                        "fields" => [
                            "guid" => [ "type" => Type::string() ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $site = elgg_get_site_entity();
        $user = elgg_get_logged_in_user_entity();

        
        if (!$user || !check_entity_relationship($user->guid, "questions_expert", $site->guid)) {
            if (!$user->isAdmin()) {
                return [
                    "guid" => $input["guid"]
                ];
            }
        }

        $entity = get_entity($input["guid"]);
        if (!$entity || !in_array($entity->getSubtype(), ["question"])) {
            throw new Exception("could_not_save");
        }

        if ($entity->isClosed) {
            unset($entity->isClosed);
        } else {
            $entity->isClosed = true;
        }

        return [
            "guid" => $entity->guid
        ];
    }
}
