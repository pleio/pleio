<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class AddEntity {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "addEntityPayload",
                "fields" => [
                    "entity" => [
                        "type" => $registry->get("Entity"),
                        "resolve" => function($entity) {
                            return Resolver::getEntity(null, $entity, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "addEntityInput",
                        "fields" => [
                            "type" => [
                                "type" => $registry->get("Type"),
                                "description" => "deprecated: use subtype",
                                "deprecationReason" => "Only subtype is used in this query"
                            ],
                            "subtype" => [ "type" => Type::nonNull(Type::string()) ],
                            "title" => [ "type" => Type::string() ],
                            "description" => [ "type" => Type::nonNull(Type::string()) ],
                            "richDescription" => [ "type" => Type::string() ],
                            "isRecommended" => [ "type" => Type::boolean() ],
                            "isFeatured" => [ "type" => Type::boolean() ],
                            "attendEventWithoutAccount" => [ "type" => Type::boolean() ],
                            "featured" => [ "type" => $registry->get("FeaturedInput") ],
                            "startDate" => [ "type" => Type::string() ],
                            "endDate" => [ "type" => Type::string() ],
                            "source" => [ "type" => Type::string() ],
                            "location" => [ "type" => Type::string() ],
                            "maxAttendees" => [ "type" => Type::string() ],
                            "rsvp" => [ "type" => Type::boolean() ],
                            "containerGuid" => [ "type" => Type::string() ],
                            "accessId" => [ "type" => Type::int() ],
                            "writeAccessId" => [ "type" => Type::int() ],
                            "tags" => [ "type" => Type::listOf(Type::string()) ],
                            "mentions" => [ "type" => Type::listOf(Type::string())],
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        if (!elgg_is_logged_in()) {
            throw new Exception("not_logged_in");
        }

        $site = elgg_get_site_entity();
        if (!Helpers::isUser()) {
            if (Helpers::canJoin()) {
                Helpers::addUser();
            } else {
                throw new Exception("not_member_of_site");
            }
        }
        $user = elgg_get_logged_in_user_entity();

        if ($input["subtype"] === "event") {
            $startDate = strtotime($input["startDate"]);
            $endDate = strtotime($input["endDate"]);
            if ($startDate > $endDate) {
                throw new Exception("startdate_after_enddate");
            }
        }

        if ($input["subtype"] === "comment" && (int) $input["containerGuid"]) {
            $content = get_entity((int) $input["containerGuid"]);
            $container = get_entity((int) $content->container_guid);
            if ($container instanceof \ElggGroup && !check_entity_relationship($user->guid,'member',$container->guid)) {
                throw new Exception("not_member_of_group");
            }
        }

        if (!in_array($input["subtype"], array("file", "folder", "news", "blog", "question", "discussion", "comment","page", "wiki", "event", "task", "thewire", "poll"))) {
            throw new Exception("invalid_subtype");
        }

        $entity = new \ElggObject();
        $entity->title = $input["title"];
        $entity->subtype = $input["subtype"];

        $entity->description = $input["description"];

        if ($input["richDescription"]) {
            // throw error when mysql TEXT limit is reached
            if (strlen($input["richDescription"]) > 65000) {
                throw new Exception("rich_description_limit_reached");
            }
            $entity->richDescription = $input["richDescription"];
        }

        if ((int) $input["containerGuid"]) {
            $container = get_entity((int) $input["containerGuid"]);
            if ($container instanceof \ElggGroup && $container->membership === ACCESS_PRIVATE && $container->group_acl) {
                $defaultAccessId = $container->group_acl;
            } elseif ($input["subtype"] === "comment") {
                $defaultAccessId = $container->access_id;
            } else {
                $defaultAccessId = get_default_access();
            }
        } else {
            $defaultAccessId = get_default_access();
        }

        if (isset($input["accessId"])) {
            $entity->access_id = (int) $input["accessId"];
        } else {
            $entity->access_id = $defaultAccessId;
        }

        if (isset($input["writeAccessId"])) {
            $entity->write_access_id = (int) $input["writeAccessId"];
        } else {
            $entity->write_access_id = ACCESS_PRIVATE;
        }

        $entity->tags = filter_tags($input["tags"]);

        if (elgg_is_admin_logged_in() || ($entity->subtype == 'blog' && pleio_template_is_subeditor($user))) {
            if (isset($input["isRecommended"])) {
                $entity->isRecommended = $input["isRecommended"];
            }
        }

        if ($container) {
            if ($input["subtype"] == "folder") {
                if ($container instanceof \ElggGroup || $container instanceof \ElggUser) {
                    $entity->container_guid = $container->guid;
                    $entity->parent_guid = 0;
                } else {
                    $entity->container_guid = $container->container_guid;
                    $entity->parent_guid = $container->guid;
                }
            } else {
                $entity->container_guid = $container->guid;
            }
        } else {
            if ($input["subtype"] === "wiki") {
                $entity->container_guid = $site->guid;
            }
        }

        if (isset($input["mentions"]) && is_array($input["mentions"])) {
            $entity->mentions = $input["mentions"];
        }

        // event
        if ($input["subtype"] === "event") {
            $entity->start_day = $startDate;
            $entity->start_time = $startDate;
            $entity->end_ts = $endDate;

            if ($input["location"]) {
                $entity->location = $input["location"];
            }

            if (isset($input["attendEventWithoutAccount"])) {
                $entity->attend_event_without_account = $input["attendEventWithoutAccount"];
            }

            if (isset($input["rsvp"])) {
                $entity->rsvp = $input["rsvp"];
            }

            if ($input["maxAttendees"]) {
                $entity->maxAttendees = $input["maxAttendees"];
            }
        }

        // news and event
        if (in_array($input["subtype"], ["news", "event"])) {
            if ($input["source"]) {
                $entity->source = $input["source"];
            }
        }

        // only subeditors or admins
        if (elgg_is_admin_logged_in() || pleio_template_is_subeditor($user)) {
            // isRecommended (blog)
            if (in_array($input["subtype"], ["blog"])) {
                if (isset($input["isRecommended"])) {
                    if ($input["isRecommended"]) {
                        $entity->isRecommended = $input["isRecommended"];
                    }
                }
            }

            // isFeatered (news, blog, discussion, question, event, wiki)
            if (in_array($input["subtype"], ["news", "blog", "discussion", "question", "event", "wiki"])) {
                if (isset($input["isFeatured"])) {
                    $entity->isFeatured = $input["isFeatured"];
                }
            }
        }

        // featured (featured image has to be added after save)
        if (in_array($input["subtype"], ["news", "blog", "event"])) {
            if ($input["featured"]) {
                if ($input["featured"]["video"]) {
                    $entity->featuredVideo = $input["featured"]["video"];
                } else {
                    unset($entity->featuredVideo);
                }

                if (isset($input["featured"]["positionY"])) {
                    $entity->featuredPositionY = (int) $input["featured"]["positionY"];
                } else {
                    unset($entity->featuredPositionY);
                }
            }
        }

        $result = $entity->save();

        if (!$result) {
            throw new Exception("could_not_save");
        }

        // featured image has to be added after save
        if (in_array($input["subtype"], ["news", "blog", "event"])) {
            if ($input["featured"]) {
                if (isset($input["featured"]["image"])) {
                    Helpers::saveToFeatured($input["featured"]["image"], $entity);
                    $entity->featuredIcontime = time();
                    $entity->save();
                }
            }
        }

        if ($input["subtype"] === "comment") {
            update_entity_last_action($container->guid, $entity->time_created);
        }

        if ($input["subtype"] !== "comment") {
            add_entity_relationship(elgg_get_logged_in_user_guid(), "content_subscription", $entity->guid);
        }

        $view = "river/object/{$input["subtype"]}/create";
        add_to_river($view, 'create', elgg_get_logged_in_user_guid(), $entity->guid);

        if ($entity->mentions) {
            Helpers::notifyMentions($entity);
        }

        return [
            "guid" => $entity->guid
        ];
    }
}
