<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class EditColumn {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "editColumnPayload",
                "fields" => [
                    "column" => [
                        "type" => $registry->get("Column"),
                        "resolve" => function($entity) {
                            return Resolver::getEntity(null, $entity, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "editColumnInput",
                        "fields" => [
                            "guid" => [ "type" => Type::nonNull(Type::string()) ],
                            "parentGuid" => [ "type" => Type::string() ],
                            "position" => [ "type" => Type::int() ],
                            "width" => [ "type" => Type::listOf(Type::int())],
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        if ($input["parentGuid"]) {
            $container = get_entity($input["parentGuid"]);
            if (!$container || !in_array($container->getSubtype(), array("row"))) {
                throw new Exception("could_not_find");
            }
        }   
        $column = get_entity((int) $input["guid"]);
        $parentGuidIsEdited = false;
        $positionIsEdited = false;

        if (!$column) {
            throw new Exception("could_not_find");
        }

        if (!$column->canEdit()) {
            throw new Exception("could_not_save");
        }

        if ($input["parentGuid"]) {
            if ($input['parentGuid'] != $column->parent_guid) {
                $previousParentGuid = $column->parent_guid;
                $column->parent_guid = $input["parentGuid"];
                $parentGuidIsEdited = true;
            }
        }

        if (isset($input["position"])) {
            if ($input['position'] != $column->position) {
                $oldPosition = $column->position;
                $column->position = $input["position"];
                $positionIsEdited = true;
            }
        }

        if ($input["width"]) {
            $column->width = $input["width"];
        }

        $result = $column->save();

        // Reorder the objects with parentGuid of column
        if ($positionIsEdited || $parentGuidIsEdited) {
            Helpers::raisePositionOfSiblingsWithSameParentGuid($column, $oldPosition);
            Helpers::orderEntitiesWithSameParentGuidByPosition($column->parent_guid, $column->getSubtype());
        }

        // Reorder position of the objects with previous parentGuid
        if ($parentGuidIsEdited) {
            Helpers::orderEntitiesWithSameParentGuidByPosition($previousParentGuid, $column->getSubtype());
        }

        if ($result) {
            return [
                "guid" => $column->guid
            ];
        }

        throw new Exception("could_not_save");
    }
}
