<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class InviteToGroup {
    public static function getMutation($registry) {
        return [
            "description" => "Create an invitation to join a group.",
            "type" => new ObjectType([
                "name" => "inviteToGroupPayload",
                "fields" => [
                    "group" => [
                        "type" => $registry->get("Group"),
                        "resolve" => function($group) {
                            return Resolver::getEntity(null, $group, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "inviteToGroupInput",
                        "fields" => [
                            "guid" => [
                                "type" => Type::nonNull(Type::string()),
                                "description" => "The guid of the group to invite to."
                            ],
                            "users" => [
                                "type" => Type::listOf($registry->get("InviteToGroupUser")),
                                "description" => "A list of users to invite."
                            ],
                            "directAdd" => [
                                "type" => Type::boolean(),
                                "description" => "Directly add the users to the group without sending an invite"
                            ],
                            "addAllUsers" => [
                                "type" => Type::boolean(),
                                "description" => "Add all site users to group. Admin only and no invites wil be send!"
                            ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $site = elgg_get_site_entity();
        $current_user = elgg_get_logged_in_user_entity();

        $group = get_entity((int) $input["guid"]);
        if (!$group || !$group instanceof \ElggGroup) {
            throw new Exception("could_not_find_group");
        }

        if (!$group->canEdit()) {
            throw new Exception("could_not_save");
        }

        if (!$current_user->isAdmin() && $input["directAdd"]) {
            throw new Exception("user_not_site_admin");
        }

        if (!$current_user->isAdmin() && $input["addAllUsers"]) {
            throw new Exception("user_not_site_admin");
        }

        if ($input["addAllUsers"]) {
            $options = [
                "type" => "user",
                "offset" => 0,
                "limit" => 0
            ];

            $users = [];
            foreach (elgg_get_entities_from_metadata($options) as $entity) {
                $users[] = [
                    "guid" => $entity->guid,
                ];
            }

            // when addAllUsers parameter is set always add users directly to group.
            $input["directAdd"] = true;

        } else {
            $users = $input["users"];
        }

        foreach ($users as $user) {
            if ($user["guid"]) {
                $user = get_entity((int) $user["guid"]);
                $email = $user->email;
            } else if ($user["email"]) {
                $email = $user["email"];
            } else {
                $email = "";
            }

            if ($input["directAdd"]) {
                if(!$user["guid"]) {
                    throw new Exception("could_not_add");
                }
                groups_join_group($group, $user);
            } else {
                if (!$email) {
                    throw new Exception("could_not_invite");
                }
                $invite_code = Helpers::group_tools_generate_email_invite_code($group->guid, $email);

                $earlier_invitation = Helpers::group_tools_check_group_email_invitation($invite_code, $group->guid);

                if (!$earlier_invitation) {
                    $group->annotate("email_invitation", $invite_code . "|" . $email, ACCESS_LOGGED_IN, $group->guid);
                }

                $site_url = elgg_get_site_url();
                $link = "{$site_url}groups/invitations/?invitecode={$invite_code}";

                $result = elgg_send_email(
                    $site->email ? $site->email : "noreply@" . get_site_domain($site->guid),
                    $email,
                    elgg_echo("pleio_template:group:membership:invitation:subject", [ $group->name ]),
                    elgg_echo("pleio_template:group:membership:invitation:body", [ $current_user->name, $group->name, $link, $link ])
                );
            }
        }

        return $group;
    }
}
