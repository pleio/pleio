<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class MoveFileFolder {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "moveFileFolderPayload",
                "fields" => [
                    "entity" => [
                        "type" => $registry->get("Entity"),
                        "resolve" => function($entity) {
                            return Resolver::getEntity(null, $entity, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "moveFileFolderInput",
                        "fields" => [
                            "guid" => [
                                "type" => Type::string()
                            ],
                            "containerGuid" => [
                                "type" => Type::string()
                            ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $entity = get_entity((int) $input["guid"]);
        if (!$entity) {
            throw new Exception("could_not_find");
        }

        if (!$entity->canEdit()) {
            throw new Exception("could_not_save");
        }

        if (!in_array($entity->getSubtype(), array("file", "folder"))) {
            throw new Exception("invalid_object_subtype");
        }

        $container = get_entity((int) $input["containerGuid"]);
        if (!$container && ($entity->container_guid !== $container->guid && $entity->container_guid !== $container->container_guid)) {
            throw new Exception("invalid_new_container");
        }

        if (!$container instanceof \ElggGroup && $container->getSubtype() !== "folder") {
            throw new Exception("invalid_new_container");
        }

        $current_container = Resolver::getParentFolder($entity);
        if ($current_container && $current_container instanceof \ElggObject) {
            Helpers::setParentFoldersTimeUpdated($current_container);
        }

        if ($container && $container instanceof \ElggObject) {
            Helpers::setParentFoldersTimeUpdated($container);
        }

        switch ($entity->getSubtype()) {
            case "file":
                remove_entity_relationships($entity->guid, "folder_of", true);

                if ($entity->container_guid != $container->guid) { // not in root directory
                    add_entity_relationship($container->guid, "folder_of", $entity->guid);
                }

                break;
            case "folder":
                if ($entity->container_guid != $container->guid) { // in root directory
                    $entity->parent_guid = $container->guid;
                } else {
                    $entity->parent_guid = 0;
                }

                $entity->save();
                break;
        }
    }
}
