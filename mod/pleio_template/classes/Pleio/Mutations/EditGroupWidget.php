<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class EditGroupWidget {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "editGroupWidgetPayload",
                "fields" => [
                    "entity" => [
                        "type" => $registry->get("Widget"),
                        "resolve" => function($entity) {
                            return Resolver::getEntity(null, $entity, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "editGroupWidgetInput",
                        "fields" => [
                            "guid" => [
                                "type" => Type::nonNull(Type::string())
                            ],
                            "settings" => [
                                "type" => Type::listOf($registry->get("WidgetSettingInput"))
                            ],
                            "position" => [ "type" => Type::int() ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $widget = get_entity((int) $input["guid"]);
        $positionIsEdited = false;

        if (!$widget) {
            throw new Exception("could_not_find");
        }

        if (!$widget->canEdit()) {
            throw new Exception("could_not_save");
        }

        if (isset($input["position"])) {
            if ($input['position'] != $widget->position) {
                $oldPosition = $widget->position;
                $widget->position = $input["position"];
                $positionIsEdited = true;
            }
        }

        if ($input["settings"]) {
            $widget->setPrivateSetting("settings", json_encode($input["settings"]));
        }

        $result = $widget->save();

        // Raise position of entities and reorder the objects with same parentGuid
        if ($positionIsEdited) {
            Helpers::raisePositionOfSiblingsWithSameContainerGuid($widget, $oldPosition);
            Helpers::orderEntitiesWithSameContainerGuidByPosition($widget->container_guid, $widget->getSubtype());
        }

        if ($result) {
            return [
                "guid" => $widget->guid
            ];
        }

        throw new Exception("could_not_save");
    }
}
