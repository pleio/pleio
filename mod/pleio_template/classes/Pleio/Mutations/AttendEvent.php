<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class AttendEvent {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "attendEventPayload",
                "fields" => [
                    "entity" => [
                        "type" => $registry->get("Event"),
                        "resolve" => function($entity) {
                            return Resolver::getEntity(null, $entity, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "attendEventInput",
                        "fields" => [
                            "guid" => [ "type" => Type::string() ],
                            "state" => [ "type" => Type::string() ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $event = get_entity((int) $input["guid"]);
        if (!$event) {
            throw new Exception("could_not_find");
        }

        if (!$event instanceof \ElggObject || $event->getSubtype() !== "event") {
            throw new Exception("could_not_save");
        }

        $user = elgg_get_logged_in_user_entity();
        if (!$user) {
            throw new Exception("not_logged_in");
        }

        $site = elgg_get_site_entity();

        if ($input["state"] === "accept") {
            $options = [
                "relationship_guid" => $event->guid,
                "relationship" => "event_attending",
                "count" => true
            ];
            if ($event->maxAttendees != "" && elgg_get_entities_from_relationship_count($options) >= (int) $event->maxAttendees) {
                throw new Exception("event_is_full");
            } else {
                add_entity_relationship($event->guid, "event_attending", $user->guid);

                $link = Helpers::getURL($event, true);
                $date = Helpers::getEventDateString($event);

                $result = elgg_send_email(
                    $site->email ? $site->email : "noreply@" . get_site_domain($site->guid),
                    $user->email,
                    elgg_echo("pleio_template:event:attend:confirmation:subject", [ $event->title ]),
                    elgg_echo("pleio_template:event:attend:confirmation:body", [ $event->title, $event->location, $date, $link, $link ])
                );
            }
        } else {
            remove_entity_relationship($event->guid, "event_attending", $user->guid);
        }

        if ($input["state"] === "maybe") {
            add_entity_relationship($event->guid, "event_maybe", $user->guid);
        } else {
            remove_entity_relationship($event->guid, "event_maybe", $user->guid);
        }

        if ($input["state"] == "reject") {
            add_entity_relationship($event->guid, "event_reject", $user->guid);
        } else {
            remove_entity_relationship($event->guid, "event_reject", $user->guid);
        }

        return [
            "guid" => $event->guid
        ];
    }
}
