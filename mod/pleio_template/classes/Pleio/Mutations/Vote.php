<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class Vote {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "votePayload",
                "fields" => [
                    "object" => [
                        "type" => Type::nonNull($registry->get("Entity")),
                        "resolve" => function($entity) {
                            return Resolver::getEntity(null, $entity, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "voteInput",
                        "fields" => [
                            "guid" => [
                                "type" => Type::nonNull((Type::string())),
                                "description" => "The guid of the entity to vote on."
                            ],
                            "score" => [
                                "type" => Type::nonNull(Type::int()),
                                "description" => "1 for upvote, -1 for downvote, 0 for deleting."
                            ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $entity = get_entity((int) $input["guid"]);
        $score = (int) $input["score"];

        if (!$entity) {
            throw new Exception("could_not_find");
        }

        $user = elgg_get_logged_in_user_entity();
        if (!$user) {
            throw new Exception("not_logged_in");
        }

        if (!in_array($score, [-1, 1])) {
            throw new Exception("invalid_value");
        }

        $past_vote = elgg_get_annotations(array(
            "guid" => $entity->guid,
            "annotation_name" => "vote",
            "annotation_owner_guid" => $user->guid
        ));

        if ($past_vote) {
            $past_vote = $past_vote[0];
            $past_value = (int) $past_vote->value;
        }

        if ($past_value === $score) {
            throw new Exception("already_voted");
        } elseif ($past_vote) {
            $result = $past_vote->delete();
        } else {
            $result = $entity->annotate("vote", $score, $entity->access_id);
        }

        if ($result) {
            return [
                "guid" => $entity->guid
            ];
        }

        throw new Exception("could_not_save");
    }
}
