<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class SendMessageToGroup {
    public static function getMutation($registry) {
        return [
            "description" => "Send a message to the group members.",
            "type" => new ObjectType([
                "name" => "sendMessageToGroupPayload",
                "fields" => [
                    "group" => [
                        "type" => $registry->get("Group"),
                        "resolve" => function($group) {
                            return Resolver::getEntity(null, $group, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "sendMessageToGroupInput",
                        "fields" => [
                            "guid" => [
                                "type" => Type::string(),
                                "description" => "The guid of the group to send the message to."
                            ],
                            "subject" => [
                                "type" => Type::string(),
                                "description" => "The subject of the message."
                            ],
                            "message" => [
                                "type" => Type::string(),
                                "description" => "The message to send."
                            ],
                            "isTest" => [
                                "type" => Type::boolean(),
                                "description" => "Is this a test message (send only to current user)"
                            ],
                            "recipients" => [
                                "type" => Type::listOf(Type::string()),
                                "description" => "An (optional) list of recipients to send the message to."
                            ],
                            "sendToAllMembers" => [
                                "type" => Type::boolean(),
                                "description" => "An (optional) parameter for sending the message to all members of the group."
                            ],
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        set_time_limit(0);

        $site = elgg_get_site_entity();

        $group = get_entity((int) $input["guid"]);
        if (!$group) {
            throw new Exception("could_not_find");
        }

        if (!$group->canEdit() || !$group instanceof \ElggGroup) {
            throw new Exception("could_not_save");
        }

        if ($input["isTest"]) {
            $current_user = elgg_get_logged_in_user_entity();
            $result =   elgg_send_email(
                $site->email ? $site->email : "noreply@" . get_site_domain($site->guid),
                $current_user->email,
                elgg_echo("pleio_template:message:subject", [ $group->name, $input['subject'] ]),
                $input['message']
            );
        } else if ($input["sendToAllMembers"]) {
            foreach ($group->getMembers(0) as $member) {

                if (!$member || $member->isBanned()) {
                    continue;
                }

                if ($member->last_action < (time() - 3600*24*30*6)) {
                    // do not send a mail to users who did not log in for the last 6 months
                    continue;
                }

                $result = elgg_send_email(
                    $site->email ? $site->email : "noreply@" . get_site_domain($site->guid),
                    $member->email,
                    elgg_echo("pleio_template:message:subject", [ $group->name, $input['subject'] ]),
                    $input['message']
                );
            }
        }
        else if ($input["recipients"]) {
            foreach ($input["recipients"] as $guid) {
                if (!$group->isMember($guid)) {
                    continue;
                }

                $member = get_entity($guid);
                if (!$member || $member->isBanned()) {
                    continue;
                }

                if ($member->last_action < (time() - 3600*24*30*6)) {
                    // do not send a mail to users who did not log in for the last 6 months
                    continue;
                }

                $result = elgg_send_email(
                    $site->email ? $site->email : "noreply@" . get_site_domain($site->guid),
                    $member->email,
                    elgg_echo("pleio_template:message:subject", [ $group->name, $input['subject'] ]),
                    $input['message']
                );
            }
        }
        else {
            throw new Exception("no_recipients");
        }

        return [
            "guid" => $group->guid
        ];
    }
}
