<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class EditPoll {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "editPollPayload",
                "fields" => [
                    "entity" => [
                        "type" => Type::nonNull($registry->get("Entity")),
                        "resolve" => function($entity, array $args, $context, ResolveInfo $info) {
                            return Resolver::getEntity(null, $entity, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "editPollInput",
                        "fields" => [
                            "guid" => [ "type" => Type::nonNull(Type::string()) ],
                            "title" => [ "type" => Type::nonNull(Type::string()) ],
                            "choices" => [ "type" => Type::nonNull(Type::listOf(Type::string())) ],
                            "accessId" => [ "type" => Type::int() ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $entity = get_entity((int) $input["guid"]);
        if (!$entity) {
            throw new Exception("could_not_find");
        }

        if (!$entity->canEdit()) {
            throw new Exception("could_not_save");
        }

        if (!$entity->getSubtype() === "poll") {
            throw new Exception("invalid_object_type");
        }

        if ($input["title"]) {
            $entity->title = $input["title"];
        }

        if (isset($input["accessId"])) {
            $entity->access_id = (int) $input["accessId"];
        }

        $result = $entity->save();

        if (!$result) {
            throw new Exception("could_not_save");
        }

        $options = [
            "type" => "object",
            "subtype" => "poll_choice",
            "relationship" => "poll_choice",
            "relationship_guid" => $entity->guid,
            "inverse_relationship" => true,
            "limit" => false
        ];

        foreach (elgg_get_entities_from_relationship($options) as $choice) {
            $choice->delete();
        }

        foreach ($input["choices"] as $choice) {
            $choice_entity = new \ElggObject();
            $choice_entity->subtype = "poll_choice";
            $choice_entity->text = $choice;
            $choice_entity->access_id = $entity->access_id;
            $result = $choice_entity->save();

            if ($result) {
                add_entity_relationship($choice_entity->guid, "poll_choice", $entity->guid);
            }
        }

        return [
            "guid" => $entity->guid
        ];
    }
}
