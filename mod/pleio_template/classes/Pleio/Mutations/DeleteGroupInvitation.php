<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class DeleteGroupInvitation {
    public static function getMutation($registry) {
        return [
            "description" => "Remove an invitation to join a group.",
            "type" => new ObjectType([
                "name" => "deleteGroupInvitationPayload",
                "fields" => [
                    "group" => [
                        "type" => $registry->get("Group"),
                        "resolve" => function($group) {
                            return Resolver::getEntity(null, $group, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "deleteGroupInvitationInput",
                        "fields" => [
                            "id" => [
                                "type" => Type::int(),
                                "description" => "The id of the invitation to delete."
                            ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $annotation = get_annotation((int) $input["id"]);

        if (!$annotation) {
            return [
                "guid" => $group->guid
            ];
        }

        if ($annotation->name !== "email_invitation") {
            throw new Exception("could_not_find");
        }

        $group = $annotation->getEntity();
        if (!$group || !$group instanceof \ElggGroup) {
            throw new Exception("could_not_find_group");
        }

        if (!$group->canEdit()) {
            throw new Exception("could_not_save");
        }

        $result = $annotation->delete();
        if (!$result) {
            throw new Exception("could_not_save");
        }

        return [
            "guid" => $group->guid
        ];
    }
}
