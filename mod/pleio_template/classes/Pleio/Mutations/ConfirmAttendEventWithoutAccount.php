<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class ConfirmAttendEventWithoutAccount {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "confirmAttendEventWithoutAccountPayload",
                "fields" => [
                    "entity" => [
                        "type" => $registry->get("Event"),
                        "resolve" => function($entity) {
                            return Resolver::getEntity(null, $entity, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "confirmAttendEventWithoutAccountInput",
                        "fields" => [
                            "guid" => [ "type" => Type::nonNull(Type::string()) ],
                            "email" => [ "type" => Type::nonNull(Type::string()) ],
                            "code" => [ "type" => Type::nonNull(Type::string()) ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $event = get_entity((int) $input["guid"]);
        if (!$event) {
            throw new Exception("could_not_find");
        }

        if (!$event instanceof \ElggObject || $event->getSubtype() !== "event" || !$event->attend_event_without_account) {
            throw new Exception("could_not_save");
        }

        $site = elgg_get_site_entity();

        $options = [
            "relationship_guid" => $event->guid,
            "relationship" => "event_attending",
            "count" => true
        ];

        if ($event->maxAttendees != "" && elgg_get_entities_from_relationship_count($options) >= (int) $event->maxAttendees) {
            throw new Exception("event_is_full");
        }

        // check for existing registration based on this email
        $options = array(
            "type" => "object",
            "subtype" => "eventregistration",
            "owner_guid" => $event->guid,
            "metadata_name_value_pairs" => array("email" => $input["email"]),
            "metadata_case_sensitive" => false,
            "limit" => 1
        );

        $event_registrations = elgg_get_entities_from_metadata($options);

        if ($event_registrations) {
            $event_registration = $event_registrations[0];
        } else {
            throw new Exception("no_registration_found");
        }

        if ($event_registration->code == $input["code"]) {
            $ia = elgg_set_ignore_access(true);
            add_entity_relationship($event->guid, "event_attending", $event_registration->guid);
            elgg_set_ignore_access($ia);

            // send confirmation mail
            $link = Helpers::getURL($event, true);
            $date = Helpers::getEventDateString($event);

            elgg_send_email(
                $site->email ? $site->email : "noreply@" . get_site_domain($site->guid),
                $input["email"],
                elgg_echo("pleio_template:event:attend:confirmation:subject", [ $event->title ]),
                elgg_echo("pleio_template:event:attend:confirmation:body", [ $event->title, $event->location, $date, $link, $link ])
            );
        } else {
            throw new Exception("code_not_correct");
        }

        return [
            "guid" => $event->guid
        ];
    }
}
