<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class EditNotifications {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "editNotificationsPayload",
                "fields" => [
                    "user" => [
                        "type" => $registry->get("User"),
                        "resolve" => function($entity) {
                            return Resolver::getEntity(null, $entity, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "editNotificationsInput",
                        "fields" => [
                            "guid" => [
                                "type" => Type::string()
                            ],
                            "emailNotifications" => [
                                "type" => Type::boolean()
                            ],
                            "newsletter" => [
                                "type" => Type::boolean()
                            ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $entity = get_entity(((int) $input["guid"]));
        if (!$entity) {
            throw new Exception("could_not_find");
        }

        if (!$entity->canEdit()) {
            throw new Exception("could_not_save");
        }

        if (!$entity instanceof \ElggUser) {
            throw new Exception("not_a_user");
        }

        $site = elgg_get_site_entity();

        if (isset($input["newsletter"])) {
            if ($input["newsletter"]) {
                $result &= newsletter_subscribe_user($entity, $site);
            } else {
                $result &= newsletter_unsubscribe_user($entity, $site);
            }
        }

        if (isset($input["emailNotifications"])) {
            if ($input["emailNotifications"]) {
                set_user_notification_setting($entity->guid, "email", true);
            } else {
                set_user_notification_setting($entity->guid, "email", false);
            }
        }

        $result = $entity->save();

        if ($result) {
            return [
                "guid" => $entity->guid
            ];
        }

        throw new Exception("could_not_save");
    }
}
