<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class JoinGroup {
    public static function getMutation($registry) {
        return [
            "description" => "Join a group. In the case of a closed group a membership request will be send, in the case of an open group the user will be joined immediately.",
            "type" => new ObjectType([
                "name" => "joinGroupPayload",
                "fields" => [
                    "group" => [
                        "type" => $registry->get("Group"),
                        "resolve" => function($group) {
                            return Resolver::getEntity(null, $group, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "joinGroupInput",
                        "fields" => [
                            "guid" => [
                                "type" => Type::string(),
                                "description" => "The guid of the group to join."
                            ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $site = elgg_get_site_entity();

        $group = get_entity((int) $input["guid"]);
        if (!$group || !$group instanceof \ElggGroup) {
            throw new Exception("could_not_find");
        }

        $user = elgg_get_logged_in_user_entity();
        if (!$user) {
            throw new Exception("not_logged_in");
        }

        if (($group->isPublicMembership() || $group->canEdit()) && !Helpers::isMembershipOnRequest($group)) {
            groups_join_group($group, $user);
        } else {
            add_entity_relationship($user->guid, "membership_request", $group->guid);

            $owner = get_entity($group->owner_guid);
            $link = Helpers::getURL($group, true);

            $hidden_email = Helpers::obfuscate_email($user->email);

            $result = elgg_send_email(
                $site->email ? $site->email : "noreply@" . get_site_domain($site->guid),
                $owner->email,
                elgg_echo("pleio_template:group:membership:membership_request:subject", [ $group->name ]),
                elgg_echo("pleio_template:group:membership:membership_request:body", [ $user->name, $hidden_email, $group->name, $link, $link ])
            );
        }

        return [
            "guid" => $group->guid
        ];
    }
}
