<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class Bookmark {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "bookmarkPayload",
                "fields" => [
                    "object" => [
                        "type" => Type::nonNull($registry->get("Entity")),
                        "resolve" => function($result) {
                            return Resolver::getEntity(null, $result["object"], null);
                        }
                    ],
                    "isFirstBookmark" => [
                        "type" => Type::nonNull(Type::boolean())
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "bookmarkInput",
                        "fields" => [
                            "guid" => [
                                "type" => Type::nonNull((Type::string())),
                                "description" => "The guid of the entity to bookmark."
                            ],
                            "isAdding" => [
                                "type" => Type::nonNull(Type::boolean()),
                                "description" => "True when adding, false when removing."
                            ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $entity = get_entity((int) $input["guid"]);
        if (!$entity) {
            throw new Exception("could_not_find");
        }

        $user = elgg_get_logged_in_user_entity();
        if (!$user) {
            throw new Exception("not_logged_in");
        }

        $options = [
            "relationship_guid" => $user->guid,
            "relationship" => "bookmarked",
            "count" => true
        ];

        $total = elgg_get_entities_from_relationship($options);
        $is_first_bookmark = ($total === 0) ? true : false;

        if ($input["isAdding"]) {
            $result = add_entity_relationship($user->guid, "bookmarked", $entity->guid);
        } else {
            $result = remove_entity_relationship($user->guid, "bookmarked", $entity->guid);
        }

        if ($result) {
            return [
                "object" => [
                    "guid" => $entity->guid
                ],
                "isFirstBookmark" => $is_first_bookmark
            ];
        }

        throw new Exception("could_not_save");
    }
}
