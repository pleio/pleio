<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class AttendEventWithoutAccount {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "attendEventWithoutAccountPayload",
                "fields" => [
                    "entity" => [
                        "type" => $registry->get("Event"),
                        "resolve" => function($entity) {
                            return Resolver::getEntity(null, $entity, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "attendEventWithoutAccountInput",
                        "fields" => [
                            "guid" => [ "type" => Type::nonNull(Type::string()) ],
                            "name" => [ "type" => Type::nonNull(Type::string()) ],
                            "email" => [ "type" => Type::nonNull(Type::string()) ],
                            "resend" => [ "type" => Type::boolean() ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $event = get_entity((int) $input["guid"]);
        if (!$event) {
            throw new Exception("could_not_find");
        }

        if (!$event instanceof \ElggObject || $event->getSubtype() !== "event" || !$event->attend_event_without_account) {
            throw new Exception("could_not_save");
        }

        $site = elgg_get_site_entity();

        $options = [
            "relationship_guid" => $event->guid,
            "relationship" => "event_attending",
            "count" => true
        ];

        if ($event->maxAttendees != "" && elgg_get_entities_from_relationship_count($options) >= (int) $event->maxAttendees) {
            throw new Exception("event_is_full");
        }

        if (!validate_email_address($input["email"])) {
            throw new Exception("email_not_valid");
        }

        // check for existing registration based on this email
        $options = array(
            "type" => "object",
            "subtype" => "eventregistration",
            "owner_guid" => $event->guid,
            "metadata_name_value_pairs" => array("email" => $input["email"]),
            "metadata_case_sensitive" => false
        );

        $is_registered = elgg_get_entities_from_metadata($options);

        if ($is_registered && !$input["resend"]) {
            throw new Exception("email_already_used");
        }

        if(!$is_registered) {
            $code = Helpers::createRandomCode();
            $ia = elgg_set_ignore_access(true);
            $entity = new \ElggObject();
            $entity->title = "EventRegistrationNotLoggedinUser";
            $entity->description = "EventRegistrationNotLoggedinUser";
            $entity->subtype = "eventregistration";
            $entity->email = $input["email"];
            $entity->name = $input["name"];
            $entity->container_guid = $event->guid;
            $entity->owner_guid = $event->guid;
            $entity->access_id = $event->access_id;
            $entity->code = $code;
            $registration_guid = $entity->save();
            elgg_set_ignore_access($ia);
        } else {
            $code = $is_registered[0]->code;
        }

        $link = Helpers::getConfirmationURL($event->guid, $code, $input["email"]);
        $date = Helpers::getEventDateString($event);

        elgg_send_email(
            $site->email ? $site->email : "noreply@" . get_site_domain($site->guid),
            $input["email"],
            elgg_echo("pleio_template:event:attend:confirmation_code:subject", [ $event->title ]),
            elgg_echo("pleio_template:event:attend:confirmation_code:body", [ $event->title, $event->location, $date, $link, $link ])
        );

        return [
            "guid" => $event->guid
        ];
    }
}
