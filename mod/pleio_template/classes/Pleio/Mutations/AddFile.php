<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;
use Psr\Http\Message\UploadedFileInterface;

class AddFile {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "addFilePayload",
                "fields" => [
                    "entity" => [
                        "type" => $registry->get("Entity"),
                        "resolve" => function($entity) {
                            return Resolver::getEntity(null, $entity, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "addFileInput",
                        "fields" => [
                            "containerGuid" => [ "type" => Type::string() ],
                            "file" => [ "type" => $registry->get("Upload") ],
                            "accessId" => [ "type" => Type::int() ],
                            "writeAccessId" => [ "type" => Type::int() ],
                            "tags" => [ "type" => Type::listOf(Type::string()) ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $site = elgg_get_site_entity();
        if (!Helpers::isUser()) {
            if (Helpers::canJoin()) {
                Helpers::addUser();
            } else {
                throw new Exception("not_member_of_site");
            }
        }

        $file = $input["file"];

        if (!$file instanceof UploadedFileInterface) {
            throw new Exception("no_file");
        }

        if ($file->getError() !== UPLOAD_ERR_OK) {
            switch($file->getError()) {
                case UPLOAD_ERR_INI_SIZE:
                    throw new Exception("invalid_filesize");
                    break;
                default:
                    throw new Exception("no_file");
            }
        }

        $entity = new \FilePluginFile();
        $entity->title = $file->getClientFilename();

        if (isset($input["accessId"])) {
            $entity->access_id = $input["accessId"];
        } else {
            $entity->access_id = get_default_access();
        }

        if (isset($input["writeAccessId"])) {
            $entity->write_access_id = $input["writeAccessId"];
        } else {
            $entity->write_access_id = ACCESS_PRIVATE;
        }

        if ($input["containerGuid"]) {
            $container = get_entity($input["containerGuid"]);
        }

        if ($container) {
            if ($container instanceof \ElggObject) {
                $entity->container_guid = $container->container_guid;
            } else {
                $entity->container_guid = $container->guid;
            }
        }

        $entity->tags = filter_tags($input["tags"]);

        $filestorename = elgg_strtolower(time() . basename($file->getClientFilename()));
        $entity->setFilename("file/" . $filestorename);
        $entity->originalfilename = $file->getClientFilename();

        $entity->open("write");
        $entity->close();

        $file->moveTo($entity->getFilenameOnFilestore());

        $mime_type = \ElggFile::detectMimeType($entity->getFilenameOnFilestore(), $file->getClientMediaType());

        $entity->setMimeType($mime_type);
        $entity->simpletype = file_get_simple_type($mime_type);

        $result = $entity->save();

        if ($entity->simpletype == "image") {
            Helpers::generateThumbs($entity);
            $entity->icontime = time();
            $entity->save();
        }

        if ($container instanceof \ElggObject) {
            add_entity_relationship($container->guid, "folder_of", $entity->guid);
            Helpers::setParentFoldersTimeUpdated($container);
        }

        if ($result) {
            return [
                "guid" => $entity->guid
            ];
        }

        throw new Exception("could_not_save");
    }
}
