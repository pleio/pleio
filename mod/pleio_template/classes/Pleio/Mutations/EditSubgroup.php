<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class EditSubgroup {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "editSubgroupPayload",
                "fields" => [
                    "success" => [ "type" => Type::boolean() ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "editSubgroupInput",
                        "fields" => [
                            "id" => [ "type" => Type::int() ],
                            "name" => [ "type" => Type::string() ],
                            "members" => [ "type" => Type::listOf(Type::string()) ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $access_collection = get_access_collection($input["id"]);
        if (!$access_collection) {
            throw new Exception("could_not_find");
        }

        $group = get_entity($access_collection->owner_guid);
        if (!$group || !$group instanceof \ElggGroup) {
            throw new Exception("could_not_find");
        }

        if (!$group->canEdit()) {
            throw new Exception("could_not_save");
        }

        if ($group->subpermissions) {
            $subpermissions = unserialize($group->subpermissions);
        }

        if (!is_array($subpermissions)) {
            $subpermissions = array();
        }

        if (!in_array($input["id"], $subpermissions)) {
            throw new Exception("could_not_find");
        }

        $dbprefix = elgg_get_config("dbprefix");

        if ($access_collection->name !== $input["name"]) {
            $id = sanitise_int($access_collection->id);
            $name = sanitize_string($input["name"]);
            update_data("UPDATE {$dbprefix}access_collections SET name = '{$name}' WHERE id = {$id}");
        }

        update_access_collection($access_collection->id, $input["members"]);

        return [
            "success" => true
        ];
    }
}
