<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;


class EditSiteSetting {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "editSiteSettingPayload",
                "fields" => [
                    "siteSettings" => [
                        "type" => $registry->get("SiteSettings"),
                        "resolve" => function() {
                            return Resolver::getSiteSettings();
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "editSiteSettingInput",
                        "fields" => [
                            #"featured" => [ "type" => $registry->get("FeaturedInput") ],
                            "name" => [ "type" => Type::string() ],
                            "description" => [ "type" => Type::string() ],
                            "language" => [ "type" => Type::string() ],
                            "isClosed" => [ "type" => Type::boolean() ],
                            "allowRegistration" => [ "type" => Type::boolean() ],
                            "defaultAccessId" => [ "type" => Type::int() ],
                            "googleAnalyticsId" => [ "type" => Type::string() ],
                            "googleSiteVerification" => [ "type" => Type::string() ],
                            "piwikUrl" => [ "type" => Type::string() ],
                            "piwikId" => [ "type" => Type::string() ],
                            "font" => [ "type" => Type::string() ],
                            "colorPrimary" => [ "type" => Type::string() ],
                            "colorSecondary" => [ "type" => Type::string() ],
                            "colorHeader" => [ "type" => Type::string() ],
                            "theme" => [ "type" => Type::string() ],
                            "logo" => [ "type" => $registry->get("Upload") ],
                            "removeLogo" => [ "type" => Type::boolean() ],
                            "logoAlt" => [ "type" => Type::string() ],
                            "likeIcon" => [ "type" => Type::string() ],

                            "startPage" => [ "type" => Type::string() ],
                            "startPageCms" => [ "type" => Type::string() ],
                            "icon" => [ "type" => $registry->get("Upload") ],
                            "showIcon" => [ "type" => Type::boolean() ],
                            "removeIcon" => [ "type" => Type::boolean() ],
                            "menu" => [ "type" => Type::listOf($registry->get("MenuItemInput")) ],
                            "numberOfFeaturedItems" => [ "type" => Type::int() ],
                            "enableFeedSorting" => [ "type" => Type::boolean() ],
                            "showExtraHomepageFilters" => [ "type" => Type::boolean() ],
                            "showLeader" => [ "type" => Type::boolean() ],
                            "showLeaderButtons" => [ "type" => Type::boolean() ],
                            "subtitle" => [ "type" => Type::string() ],
                            "leaderImage" => [ "type" => Type::string() ],
                            "showInitiative" => ["type" => Type::boolean()],
                            "initiativeTitle" => ["type" => Type::string()],
                            "initiativeImage" => ["type" => Type::string()],
                            "initiativeImageAlt" => ["type" => Type::string()],
                            "initiativeDescription" => ["type" => Type::string()],
                            "initiativeLink" => ["type" => Type::string()],
                            "directLinks" => [ "type" => Type::listOf($registry->get("DirectLinkInput")) ],
                            "footer" => [ "type" => Type::listOf($registry->get("FooterInput")) ],

                            "tagCategories" => [ "type" => Type::listOf($registry->get("TagCategoryInput")) ],
                            "showTagsInFeed" => [ "type" => Type::boolean() ],
                            "showTagsInDetail" => [ "type" => Type::boolean() ],

                            "profile" => [ "type" => Type::listOf($registry->get("ProfileItemInput")) ],

                            "defaultEmailOverviewFrequency" => ["type" => Type::string()],
                            "emailOverviewSubject" => ["type" => Type::string()],
                            "emailOverviewTitle" => ["type" => Type::string()],
                            "emailOverviewIntro" => ["type" => Type::string()],
                            "emailOverviewEnableFeatured" => ["type" => Type::boolean()],
                            "emailOverviewFeaturedTitle" => ["type" => Type::string()],
                            "emailNotificationShowExcerpt" => [ "type" => Type::boolean() ],

                            "showLoginRegister" => [ "type" => Type::boolean() ],
                            "customTagsAllowed" => [ "type" => Type::boolean() ],
                            "showUpDownVoting" => [ "type" => Type::boolean() ],
                            "enableSharing" => [ "type" => Type::boolean() ],
                            "showViewsCount" => [ "type" => Type::boolean() ],
                            "newsletter" => [ "type" => Type::boolean() ],
                            "cancelMembershipEnabled" => [ "type" => Type::boolean() ],
                            "showExcerptInNewsCard" => [ "type" => Type::boolean() ],
                            "commentsOnNews" => [ "type" => Type::boolean() ],
                            "eventExport" => [ "type" => Type::boolean() ],
                            "questionerCanChooseBestAnswer" => [ "type" => Type::boolean() ],
                            "statusUpdateGroups" => [ "type" => Type::boolean() ],
                            "subgroups" => [ "type" => Type::boolean() ],
                            "groupMemberExport" => [ "type" => Type::boolean() ],
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        if (!elgg_is_admin_logged_in()) {
            throw new Exception("could_not_save");
        }

        # Basic
        $site = elgg_get_site_entity();
        if (isset($input["name"])) {
            $site->name = $input["name"];
            $site->save();
        }
        if (isset($input["description"])) {
            $site->description = $input["description"];
            $site->save();
        }
        if (isset($input["language"])) {
            if (!in_array($input["language"], array("en", "nl"))) {
                throw new Exception("value_not_valid");
            }
            set_config('language', $input["language"], $site->getGUID());
        }

        if (isset($input["isClosed"])) {
            if ($input["isClosed"] && Resolver::getDefaultAccessId(["guid" => $site->guid]) == 2) {
                set_config('default_access', 1, $site->getGUID());
            }
            set_config('walled_garden', $input["isClosed"], $site->getGUID());
        }
        if (isset($input["allowRegistration"])) {
            set_config('allow_registration', $input["allowRegistration"], $site->getGUID());
        }
        if (isset($input["defaultAccessId"])) {
            set_config('default_access', $input["defaultAccessId"], $site->getGUID());
        }
        # Analytics
        if (isset($input["googleAnalyticsId"])) {
            elgg_set_plugin_setting("google_analytics", $input["googleAnalyticsId"], "pleio_template");
        }

        if (isset($input["googleSiteVerification"])) {
            elgg_set_plugin_setting("google_site_verification", $input["googleSiteVerification"], "pleio_template");
        }

        if (isset($input["piwikUrl"])) {
            elgg_set_plugin_setting("piwik_url", $input["piwikUrl"], "pleio_template");
        }

        if (isset($input["piwikId"])) {
            elgg_set_plugin_setting("piwik", $input["piwikId"], "pleio_template");
        }

        # Appearance
        if (isset($input["font"])) {
            elgg_set_plugin_setting('font', $input["font"], "pleio_template");
            elgg_invalidate_simplecache();
        }
        if (isset($input["colorPrimary"])) {
            elgg_set_plugin_setting('color_primary', $input["colorPrimary"], "pleio_template");
            elgg_invalidate_simplecache();
        }
        if (isset($input["colorSecondary"])) {
            elgg_set_plugin_setting('color_secondary', $input["colorSecondary"], "pleio_template");
            elgg_invalidate_simplecache();
        }
        if (isset($input["colorHeader"])) {
            elgg_set_plugin_setting('color_header', $input["colorHeader"], "pleio_template");
            elgg_invalidate_simplecache();
        }
        if (isset($input["theme"])) {
            elgg_set_plugin_setting('theme', $input["theme"], "pleio_template");
        }
        if (isset($input["logo"])) {
            Helpers::saveAdminImage($input["logo"], "logo");
        }
        if (isset($input["removeLogo"])) {
            if ($input["removeLogo"]) {
                Helpers::deleteAdminImage("logo");
            }
        }
        if (isset($input["logoAlt"])) {
            elgg_set_plugin_setting('logo_alt', $input["logoAlt"], "pleio_template");
        }
        if (isset($input["likeIcon"])) {
            elgg_set_plugin_setting('like_icon', $input["likeIcon"], "pleio_template");
        }

        # Navigation

        if (isset($input["startPage"])) {
            elgg_set_plugin_setting('startpage', $input["startPage"], "pleio_template");
        }
        if (isset($input["startPageCms"])) {
            elgg_set_plugin_setting('startpage_cms', $input["startPageCms"], "pleio_template");
        }
        if (isset($input["icon"])) {
            Helpers::saveAdminImage($input["icon"], "icon");
        }
        if (isset($input["showIcon"])) {
            elgg_set_plugin_setting('show_icon', Helpers::get_yes_no($input["showIcon"]), "pleio_template");
        }
        if (isset($input["removeIcon"])) {
            if ($input["removeIcon"]) {
                Helpers::deleteAdminImage("icon");
            }
        }

        if (isset($input["menu"])) {
            $menu_input = $input["menu"];
            $menu = [];
            foreach ($menu_input as $page) {
                if (!$page["parentId"]) {
                    array_push($menu, array(
                        "title" => $page["title"], "link" => $page["link"], "children" => Helpers::get_menu_children($menu_input, $page["id"])
                        )
                    );

                }
            }
            elgg_set_plugin_setting("menu", json_encode($menu), "pleio_template");
        }
        if (isset($input["numberOfFeaturedItems"])) {
            elgg_set_plugin_setting('number_of_featured_items', $input["numberOfFeaturedItems"], "pleio_template");
        }
        if (isset($input["enableFeedSorting"])) {
            elgg_set_plugin_setting('enable_feed_sorting', Helpers::get_yes_no($input["enableFeedSorting"]), "pleio_template");
        }
        if (isset($input["showExtraHomepageFilters"])) {
            elgg_set_plugin_setting('show_extra_homepage_filters', Helpers::get_yes_no($input["showExtraHomepageFilters"]), "pleio_template");
        }
        if (isset($input["showLeader"])) {
            elgg_set_plugin_setting('show_leader', Helpers::get_yes_no($input["showLeader"]), "pleio_template");
        }
        if (isset($input["showLeaderButtons"])) {
            elgg_set_plugin_setting('show_leader_buttons', Helpers::get_yes_no($input["showLeaderButtons"]), "pleio_template");
        }
        if (isset($input["subtitle"])) {
            elgg_set_plugin_setting('subtitle', $input["subtitle"], "pleio_template");
        }
        if (isset($input["leaderImage"])) {
            elgg_set_plugin_setting('leader_image', $input["leaderImage"], "pleio_template");
        }
        if (isset($input["showInitiative"])) {
            elgg_set_plugin_setting('show_initiative', Helpers::get_yes_no($input["showInitiative"]), "pleio_template");
        }
        if (isset($input["initiativeTitle"])) {
            elgg_set_plugin_setting('initiative_title', $input["initiativeTitle"], "pleio_template");
        }
        if (isset($input["initiativeImage"])) {
            elgg_set_plugin_setting('initiative_image', $input["initiativeImage"], "pleio_template");
        }
        if (isset($input["initiativeImageAlt"])) {
            elgg_set_plugin_setting('initiative_image_alt', $input["initiativeImageAlt"], "pleio_template");
        }
        if (isset($input["initiativeDescription"])) {
            elgg_set_plugin_setting('initiative_description', $input["initiativeDescription"], "pleio_template");
        }
        if (isset($input["initiativeLink"])) {
            elgg_set_plugin_setting('initiator_link', $input["initiativeLink"], "pleio_template");
        }
        if (isset($input["directLinks"])) {
            elgg_set_plugin_setting("directLinks", json_encode($input["directLinks"]), "pleio_template");
        }
        if (isset($input["footer"])) {
            elgg_set_plugin_setting("footer", json_encode($input["footer"]), "pleio_template");
        }

        # Mailing
        if (isset($input["defaultEmailOverviewFrequency"])) {
            elgg_set_plugin_setting('default_email_overview', $input["defaultEmailOverviewFrequency"], "pleio_template");
        }
        if (isset($input["emailOverviewSubject"])) {
            elgg_set_plugin_setting('email_overview_subject', $input["emailOverviewSubject"], "pleio_template");
        }
        if (isset($input["emailOverviewTitle"])) {
            elgg_set_plugin_setting('email_overview_title', $input["emailOverviewTitle"], "pleio_template");
        }
        if (isset($input["emailOverviewIntro"])) {
            elgg_set_plugin_setting('email_overview_intro', $input["emailOverviewIntro"], "pleio_template");
        }
        if (isset($input["emailOverviewFeaturedTitle"])) {
            elgg_set_plugin_setting('email_overview_featured_title', $input["emailOverviewFeaturedTitle"], "pleio_template");
        }
        if (isset($input["emailOverviewEnableFeatured"])) {
            elgg_set_plugin_setting('email_overview_enable_featured', Helpers::get_yes_no($input["emailOverviewEnableFeatured"]), "pleio_template");
        }
        if (isset($input["emailNotificationShowExcerpt"])) {
            elgg_set_plugin_setting('email_notification_show_excerpt', Helpers::get_yes_no($input["emailNotificationShowExcerpt"]), "pleio_template");
        }

        # Tags
        if (isset($input["tagCategories"])) {
            elgg_set_plugin_setting("tagCategories", json_encode($input["tagCategories"]), "pleio_template");
        }
        if (isset($input["showTagsInFeed"])) {
            elgg_set_plugin_setting('show_tags_in_feed', Helpers::get_yes_no($input["showTagsInFeed"]), "pleio_template");
        }
        if (isset($input["showTagsInDetail"])) {
            elgg_set_plugin_setting('show_tags_in_detail', Helpers::get_yes_no($input["showTagsInDetail"]), "pleio_template");
        }

        # Profile
        if (isset($input["profile"])) {
            elgg_set_plugin_setting("profile", json_encode($input["profile"]), "pleio_template");
        }

        # Advanced
        if (isset($input["showLoginRegister"])) {
            elgg_set_plugin_setting('show_login_register', Helpers::get_yes_no($input["showLoginRegister"]), "pleio_template");
        }
        if (isset($input["customTagsAllowed"])) {
            elgg_set_plugin_setting('custom_tags_allowed', Helpers::get_yes_no($input["customTagsAllowed"]), "pleio_template");
        }
        if (isset($input["showUpDownVoting"])) {
            elgg_set_plugin_setting('enable_up_down_voting', Helpers::get_yes_no($input["showUpDownVoting"]), "pleio_template");
        }
        if (isset($input["enableSharing"])) {
            elgg_set_plugin_setting('enable_sharing', Helpers::get_yes_no($input["enableSharing"]), "pleio_template");
        }
        if (isset($input["showViewsCount"])) {
            elgg_set_plugin_setting('enable_views_count', Helpers::get_yes_no($input["showViewsCount"]), "pleio_template");
        }
        if (isset($input["newsletter"])) {
            elgg_set_plugin_setting('newsletter', Helpers::get_yes_no($input["newsletter"]), "pleio_template");
        }
        if (isset($input["cancelMembershipEnabled"])) {
            elgg_set_plugin_setting('cancel_membership_enabled', Helpers::get_yes_no($input["cancelMembershipEnabled"]), "pleio_template");
        }
        if (isset($input["showExcerptInNewsCard"])) {
            elgg_set_plugin_setting('show_excerpt_in_news_card', Helpers::get_yes_no($input["showExcerptInNewsCard"]), "pleio_template");
        }
        if (isset($input["commentsOnNews"])) {
            elgg_set_plugin_setting('comments_on_news', Helpers::get_yes_no($input["commentsOnNews"]), "pleio_template");
        }
        if (isset($input["eventExport"])) {
            elgg_set_plugin_setting('event_export', Helpers::get_yes_no($input["eventExport"]), "pleio_template");
        }
        if (isset($input["questionerCanChooseBestAnswer"])) {
            elgg_set_plugin_setting('questioner_can_choose_best_answer', Helpers::get_yes_no($input["questionerCanChooseBestAnswer"]), "pleio_template");
        }
        if (isset($input["statusUpdateGroups"])) {
            elgg_set_plugin_setting('status_update_groups', Helpers::get_yes_no($input["statusUpdateGroups"]), "pleio_template");
        }
        if (isset($input["subgroups"])) {
            elgg_set_plugin_setting('subgroups', Helpers::get_yes_no($input["subgroups"]), "pleio_template");
        }
        if (isset($input["groupMemberExport"])) {
            elgg_set_plugin_setting('member_export', Helpers::get_yes_no($input["groupMemberExport"]), "pleio_template");
        }

        return [
            "succes" => True
        ];
    }

}