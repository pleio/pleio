<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class EditWidget {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "editWidgetPayload",
                "fields" => [
                    "widget" => [
                        "type" => $registry->get("Widget"),
                        "resolve" => function($entity) {
                            return Resolver::getEntity(null, $entity, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "editWidgetInput",
                        "fields" => [
                            "guid" => [
                                "type" => Type::nonNull(Type::string())
                            ],
                            "settings" => [
                                "type" => Type::listOf($registry->get("WidgetSettingInput"))
                            ],
                            "parentGuid" => [ "type" => Type::string() ],
                            "position" => [ "type" => Type::int() ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        if ($input["parentGuid"]) {
            $container = get_entity((int) $input["parentGuid"]);
            if (!$container || $container->getSubtype() !== "column") {
                throw new Exception("could_not_find");
            }
        }

        $widget = get_entity((int) $input["guid"]);
        $parentGuidIsEdited = false;
        $positionIsEdited = false;
        if (!$widget) {
            throw new Exception("could_not_find");
        }

        if (!$widget->canEdit()) {
            throw new Exception("could_not_save");
        }

        if ($input["parentGuid"]) {
            if ($input['parentGuid'] != $widget->parent_guid) {
                $previousParentGuid = $widget->parent_guid;
                $widget->parent_guid = $input["parentGuid"];
                $parentGuidIsEdited = true;
            }
        }

        if (isset($input["position"])) {
            if ($input['position'] != $widget->position) {
                $oldPosition = $widget->position;
                $widget->position = $input["position"];
                $positionIsEdited = true;
            }
        }

        if ($input["settings"]) {
            $widget->setPrivateSetting("settings", json_encode($input["settings"]));
        }

        $result = $widget->save();

        // Raise position of entities and reorder the objects with same parentGuid
        if ($positionIsEdited || $parentGuidIsEdited) {
            Helpers::raisePositionOfSiblingsWithSameParentGuid($widget, $oldPosition);
            Helpers::orderEntitiesWithSameParentGuidByPosition($widget->parent_guid, $widget->getSubtype());
        }

        // Reorder position of the objects with previous parentGuid
        if ($parentGuidIsEdited) {
            Helpers::orderEntitiesWithSameParentGuidByPosition($previousParentGuid, $widget->getSubtype());
        }

        if ($result) {
            return [
                "guid" => $widget->guid
            ];
        }

        throw new Exception("could_not_save");
    }
}
