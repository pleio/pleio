<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class Follow {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "followPayload",
                "fields" => [
                    "object" => [
                        "type" => Type::nonNull($registry->get("Entity")),
                        "resolve" => function($entity) {
                            return Resolver::getEntity(null, $entity, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "followInput",
                        "fields" => [
                            "guid" => [
                                "type" => Type::nonNull((Type::string())),
                                "description" => "The guid of the entity to follow."
                            ],
                            "isFollowing" => [
                                "type" => Type::nonNull(Type::boolean()),
                                "description" => "True for following, false for not following."
                            ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $entity = get_entity((int) $input["guid"]);
        if (!$entity) {
            throw new Exception("could_not_find");
        }

        $user = elgg_get_logged_in_user_entity();
        if (!$user) {
            throw new Exception("not_logged_in");
        }

        if ($input["isFollowing"]) {
            $result = add_entity_relationship($user->guid, "content_subscription", $entity->guid);
        } else {
            $result = remove_entity_relationship($user->guid, "content_subscription", $entity->guid);
        }

        if ($result) {
            return [
                "guid" => $entity->guid
            ];
        }

        throw new Exception("could_not_save");
    }
}
