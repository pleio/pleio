<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class ToggleBestAnswer {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "toggleBestAnswerPayload",
                "fields" => [
                    "entity" => [
                        "type" => Type::nonNull($registry->get("Question")),
                        "resolve" => function($entity, array $args, $context, ResolveInfo $info) {
                            return Resolver::getEntity(null, $entity, null);
                        }
                    ],
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "toggleBestAnswerInput",
                        "fields" => [
                            "guid" => [ "type" => Type::string() ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $site = elgg_get_site_entity();
        $user = elgg_get_logged_in_user_entity();
        
        $entity = get_entity($input["guid"]);
        if (!$entity || !in_array($entity->getSubtype(), ["comment", "answer"])) {
            throw new Exception("could_not_save");
        }

        $question = $entity->getContainerEntity();
        if (!$question) {
            throw new Exception("could_not_find");
        }

        if (!$user || !Resolver::canChooseBestAnswer($question)) {
            return [
                "guid" => $input["guid"]
            ];
        }

        if (check_entity_relationship($question->guid, "correctAnswer", $entity->guid)) {
            remove_entity_relationship($question->guid, "correctAnswer", $entity->guid);
        } else {
            $correctAnswers = $question->getEntitiesFromRelationship("correctAnswer", false, 0);
            if ($correctAnswers) {
                foreach ($correctAnswers as $correctAnswer) {
                    remove_entity_relationship($question->guid, "correctAnswer", $correctAnswer->guid);
                }
            }
            add_entity_relationship($question->guid, "correctAnswer", $entity->guid);
        }

        return [
            "guid" => $question->guid
        ];
    }
}
