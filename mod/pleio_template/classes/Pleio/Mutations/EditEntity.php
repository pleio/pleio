<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class EditEntity {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "editEntityPayload",
                "fields" => [
                    "entity" => [
                        "type" => $registry->get("Entity"),
                        "resolve" => function($entity) {
                            return Resolver::getEntity(null, $entity, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "editEntityInput",
                        "fields" => [
                            "guid" => [ "type" => Type::nonNull(Type::string()) ],
                            "title" => [ "type" => Type::string() ],
                            "description" => [ "type" => Type::string() ],
                            "richDescription" => [ "type" => Type::string() ],
                            "isRecommended" => [ "type" => Type::boolean() ],
                            "isFeatured" => [ "type" => Type::boolean() ],
                            "attendEventWithoutAccount" => [ "type" => Type::boolean() ],
                            "featured" => [ "type" => $registry->get("FeaturedInput") ],
                            "startDate" => [ "type" => Type::string() ],
                            "endDate" => [ "type" => Type::string() ],
                            "source" => [ "type" => Type::string() ],
                            "location" => [ "type" => Type::string() ],
                            "maxAttendees" => [ "type" => Type::string() ],
                            "rsvp" => [ "type" => Type::boolean() ],
                            "accessId" => [ "type" => Type::int() ],
                            "containerGuid" => [ "type" => Type::string() ],
                            "writeAccessId" => [ "type" => Type::int() ],
                            "tags" => [ "type" => Type::listOf(Type::string()) ],
                            "mentions" => [ "type" => Type::listOf(Type::string())],
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $user = elgg_get_logged_in_user_entity();
        $entity = get_entity((int) $input["guid"]);
        if (!$entity) {
            throw new Exception("could_not_find");
        }

        if (!$entity->canEdit()) {
            throw new Exception("could_not_save");
        }

        if (!in_array($entity->type, array("group", "object"))) {
            throw new Exception("invalid_object_type");
        }
        if ($entity->getSubtype() === "event") {
            $startDate = strtotime($input["startDate"]);
            $endDate = strtotime($input["endDate"]);
            if ($startDate > $endDate) {
                throw new Exception("startdate_after_enddate");
            }
        }

        // container checks
        if ($entity->getSubtype() === "comment" && (int) $input["containerGuid"]) {
            $content = get_entity((int) $input["containerGuid"]);
            $container = get_entity((int) $content->container_guid);
            if ($container instanceof \ElggGroup && !check_entity_relationship($user->guid,'member',$container->guid)) {
                throw new Exception("not_member_of_group");
            }
        }

        if ($entity->getSubtype() === "wiki" && (int) $input["containerGuid"]) {
            $site = elgg_get_site_entity();
            $container = get_entity((int) $input["containerGuid"]);
            if ((int) $input["containerGuid"] == (int) $site->guid || in_array($entity->getSubtype(), ["group", "wiki"])) {
                $entity->container_guid = (int) $input["containerGuid"];
            } else {
                throw new Exception("invalid_container");
            }
        }

        switch ($entity->type) {
            case "group":
                if (isset($input["name"])) {
                    $entity->name = $input["name"];
                }
            case "object":
                if (isset($input["title"])) {
                    $entity->title = $input["title"];
                }
            default:
                if (isset($input["description"])) {
                    $entity->description = $input["description"];
                }
                if ($input["richDescription"]) {
                    // throw error when mysql TEXT limit is reached
                    if (strlen($input["richDescription"]) > 65000) {
                        throw new Exception("rich_description_limit_reached");
                    }

                    $entity->richDescription = $input["richDescription"];
                }

                if (isset($input["accessId"])) {
                    $entity->access_id = (int) $input["accessId"];
                }

                if (isset($input["writeAccessId"])) {
                    $entity->write_access_id = (int) $input["writeAccessId"];
                }

                if (elgg_is_admin_logged_in()) {
                    if (isset($input["isRecommended"])) {
                        if ($input["isRecommended"]) {
                            $entity->isRecommended = $input["isRecommended"];
                        } else {
                            unset($entity->isRecommended);
                        }
                    }
                }

                $entity->tags = filter_tags($input["tags"]);

                if (isset($input["mentions"]) && is_array($input["mentions"])) {
                    $entity->mentions = $input["mentions"];
                }
        }

        // event
        if ($entity->getSubtype() === "event") {
            $entity->start_day = $startDate;
            $entity->start_time = $startDate;
            $entity->end_ts = $endDate;

            if (isset($input["location"])) {
                $entity->location = $input["location"];
            }

            if (isset($input["attendEventWithoutAccount"])) {
                $entity->attend_event_without_account = $input["attendEventWithoutAccount"];
            }

            if (isset($input["rsvp"])) {
                $entity->rsvp = $input["rsvp"];
            }

            if (isset($input["maxAttendees"])) {
                $entity->maxAttendees = $input["maxAttendees"];
            }
        }

        // news and event
        if (in_array($entity->getSubtype() , ["news", "event"])) {
            if ($input["source"]) {
                $entity->source = $input["source"];
            }
        }

        // only subeditors or admins
        if (elgg_is_admin_logged_in() || pleio_template_is_subeditor($user)) {
            // isRecommended (blog)
            if (in_array($entity->getSubtype(), ["blog"])) {
                if (isset($input["isRecommended"])) {
                    if ($input["isRecommended"]) {
                        $entity->isRecommended = $input["isRecommended"];
                    }
                }
            }

            // isFeatered (news, blog, discussion, question, event, wiki)
            if (in_array($entity->getSubtype(), ["news", "blog", "discussion", "question", "event", "wiki"])) {
                if (isset($input["isFeatured"])) {
                    $entity->isFeatured = $input["isFeatured"];
                }
            }
        }

        // featured image
        if (in_array($entity->getSubtype(), ["news", "blog", "event"])) {
            if ($input["featured"]) {
                if (isset($input["featured"]["image"])) {
                    Helpers::saveToFeatured($input["featured"]["image"], $entity);
                    $entity->featuredIcontime = time();
                }

                if ($input["featured"]["video"]) {
                    $entity->featuredVideo = $input["featured"]["video"];
                } else {
                    unset($entity->featuredVideo);
                }

                if (isset($input["featured"]["positionY"])) {
                    $entity->featuredPositionY = (int) $input["featured"]["positionY"];
                } else {
                    unset($entity->featuredPositionY);
                }
            } else {
                unset($entity->featuredIcontime);
                unset($entity->featuredVideo);
                unset($entity->featuredPositionY);
            }
        }

        if ($entity->getSubtype() === "poll") {
            // @todo
        }

        $result = $entity->save();

        if ($entity->mentions) {
            Helpers::notifyMentions($entity);
        }

        if ($result) {
            return [
                "guid" => $entity->guid
            ];
        }

        throw new Exception("could_not_save");
    }
}
