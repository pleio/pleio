<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class ResendGroupInvitation {
    public static function getMutation($registry) {
        return [
            "description" => "Resend an invitation to join a group.",
            "type" => new ObjectType([
                "name" => "resendGroupInvitationPayload",
                "fields" => [
                    "group" => [
                        "type" => $registry->get("Group"),
                        "resolve" => function($group) {
                            return Resolver::getEntity(null, $group, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "resendGroupInvitationInput",
                        "fields" => [
                            "id" => [
                                "type" => Type::int(),
                                "description" => "The id of the invitation to resend."
                            ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $site = elgg_get_site_entity();
        $current_user = elgg_get_logged_in_user_entity();

        $annotation = get_annotation((int) $input["id"]);

        if ($annotation->name !== "email_invitation") {
            throw new Exception("could_not_find");
        }

        $group = $annotation->getEntity();
        if (!$group || !$group instanceof \ElggGroup) {
            throw new Exception("could_not_find_group");
        }

        if (!$group->canEdit()) {
            throw new Exception("could_not_save");
        }

        $code = explode("|", $annotation->value);

        $site_url = elgg_get_site_url();

        $link = "{$site_url}groups/invitations/?invitecode={$code[0]}";

        $result = elgg_send_email(
            $site->email ? $site->email : "noreply@" . get_site_domain($site->guid),
            $code[1],
            elgg_echo("pleio_template:group:membership:invitation_reminder:subject", [ $group->name ]),
            elgg_echo("pleio_template:group:membership:invitation_reminder:body", [ $current_user->name, $group->name, $link, $link ])
        );

        return [
            "guid" => $group->guid
        ];
    }
}
