<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class AddGroup {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "addGroupPayload",
                "fields" => [
                    "group" => [
                        "type" => $registry->get("Group"),
                        "resolve" => function($group) {
                            return Resolver::getEntity(null, $group, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "addGroupInput",
                        "fields" => [
                            "name" => [ "type" => Type::string() ],
                            "icon" => [ "type" => $registry->get("Upload") ],
                            "featured" => [ "type" => $registry->get("FeaturedInput") ],
                            "isClosed" => [ "type" => Type::boolean() ],
                            "isMembershipOnRequest" => [ "type" => Type::boolean(), "description" => "True when membership has to be requested by the user, False when every user can join the group." ],
                            "isFeatured" => [ "type" => Type::boolean() ],
                            "autoNotification" => [ "type" => Type::boolean() ],
                            "isLeavingGroupDisabled" => [ "type" => Type::boolean() ],
                            "isAutoMembershipEnabled" => [ "type" => Type::boolean() ],
                            "description" => [ "type" => Type::string() ],
                            "richDescription" => [ "type" => Type::string() ],
                            "introduction" => [ "type" => Type::string() ],
                            "isIntroductionPublic" => [ "type" => Type::boolean() ],
                            "welcomeMessage" => [ "type" => Type::string() ],
                            "tags" => [ "type" => Type::listOf(Type::string()) ],
                            "plugins" => [ "type" => Type::listOf($registry->get("Plugins")) ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $group = new \ElggGroup();
        $user = elgg_get_logged_in_user_entity();
        $limitedGroupAdd = elgg_get_plugin_setting("limited_groups", "groups");
        if (!$user->isAdmin() && $limitedGroupAdd == 'yes') {
            throw new Exception("could_not_save");
        }

        $group->name = $input["name"];
        $group->membership = $input["isClosed"] ? ACCESS_PRIVATE : ACCESS_PUBLIC;
        $group->isMembershipOnRequest = $input["isMembershipOnRequest"];
        $group->description = $input["description"];
        $group->richDescription = $input["richDescription"];
        $group->introduction = $input["introduction"];
        $group->plugins = array_unique($input["plugins"]);
        $group->tags = filter_tags($input["tags"]);
        $group->access_id = ACCESS_PUBLIC;

        if (elgg_is_admin_logged_in() && isset($input["isFeatured"])) {
            $group->isFeatured = $input["isFeatured"];
        }

        if (isset($input["autoNotification"])) {
            $group->autoNotification = $input["autoNotification"];
        }

        if (isset($input["isIntroductionPublic"])) {
            $group->isIntroductionPublic = $input["isIntroductionPublic"];
        }

        if (elgg_is_admin_logged_in() && isset($input["isLeavingGroupDisabled"])) {
            $group->isLeavingGroupDisabled = $input["isLeavingGroupDisabled"];
        }

        if (elgg_is_admin_logged_in() && isset($input["isAutoMembershipEnabled"])) {
            $group->isAutoMembershipEnabled = $input["isAutoMembershipEnabled"];
        }

        if (!empty(trim(strip_tags($input["welcomeMessage"])))) {
            $group->setPrivateSetting("group_tools:welcome_message", $input["welcomeMessage"]);
        } else {
            $group->removePrivateSetting("group_tools:welcome_message");
        }

        $result = $group->save();

        if ($input["icon"]) {
            Helpers::saveToIcon($input["icon"], $group);
            $group->icontime = time();
        }

        if ($input["featured"]) {
            if ($input["featured"]["image"]) {
                Helpers::saveToFeatured($input["featured"]["image"], $group);
                $group->featuredIcontime = time();
            }

            if ($input["featured"]["video"]) {
                $group->featuredVideo = $input["featured"]["video"];
            }

            if (isset($input["featured"]["positionY"])) {
                $group->featuredPositionY = (int) $input["featured"]["positionY"];
            } else {
                unset($group->featuredPositionY);
            }
        }

        if ($result) {
            $group->join($user);

            return [
                "guid" => $group->guid
            ];
        }

        throw new Exception("could_not_save");
    }
}
