<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class MarkAllAsRead {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "markAllAsReadPayload",
                "fields" => [
                    "success" => [ "type" => Type::boolean() ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "markAllAsReadInput",
                        "fields" => [
                            "id" => [ "type" => Type::string() ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $dbprefix = elgg_get_config("dbprefix");

        $user = elgg_get_logged_in_user_entity();
        if (!$user) {
            return [ "success" => false ];
        }

        $result = update_data("UPDATE {$dbprefix}notifications SET unread = 'no' WHERE user_guid = {$user->guid}");
        if ($result) {
            return [ "success" => true ];
        }

        return [ "success" => false ];
    }
}
