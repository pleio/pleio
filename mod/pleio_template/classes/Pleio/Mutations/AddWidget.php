<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class AddWidget {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "addWidgetPayload",
                "fields" => [
                    "widget" => [
                        "type" => $registry->get("Widget"),
                        "resolve" => function($entity) {
                            return Resolver::getEntity(null, $entity, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "addWidgetInput",
                        "fields" => [
                            "position" => [ "type" => Type::nonNull(Type::int()) ],
                            "containerGuid" => [ "type" => Type::nonNull(Type::string()) ],
                            "parentGuid" => [ "type" => Type::nonNull(Type::string()) ],
                            "type" => [
                                "type" => Type::nonNull(Type::string())
                            ],
                            "settings" => [
                                "type" => Type::listOf($registry->get("WidgetSettingInput"))
                            ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $page = get_entity($input["containerGuid"]);
        $container = get_entity($input["parentGuid"]);
        if (!$container || !$page || $page->getSubtype() != 'page' || !in_array($container->getSubtype(), array("column"))) {
            throw new Exception("could_not_find");
        }

        if (!$container->canEdit()) {
            throw new Exception("could_not_save");
        }

        $widget = new \ElggObject();
        $widget->subtype = "page_widget";
        $widget->position = $input["position"];
        $widget->parent_guid = $input["parentGuid"];
        $widget->container_guid = $input["containerGuid"];
        $widget->access_id = $container->access_id;
        $widget->widget_type = $input["type"];
        if ($input["settings"]) {
            $widget->setPrivateSetting("settings", json_encode($input["settings"]));
        }
        $result = $widget->save();

        Helpers::raisePositionOfSiblingsWithSameParentGuid($widget, $input["position"]);
        Helpers::orderEntitiesWithSameParentGuidByPosition($widget->parent_guid, $widget->getSubtype());

        if ($result) {
            return [
                "guid" => $widget->guid
            ];
        }

        throw new Exception("could_not_save");
    }
}
