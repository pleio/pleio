<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class AddPage {
    public static function getMutation($registry) {
        return [
            "type" => new ObjectType([
                "name" => "addPagePayload",
                "fields" => [
                    "entity" => [
                        "type" => $registry->get("Entity"),
                        "resolve" => function($entity) {
                            return Resolver::getEntity(null, $entity, null);
                        }
                    ]
                ]
            ]),
            "args" => [
                "input" => [
                    "type" => Type::nonNull(new InputObjectType([
                        "name" => "addPageInput",
                        "fields" => [
                            "title" => [ "type" => Type::string() ],
                            "description" => [ "type" => Type::nonNull(Type::string()) ],
                            "richDescription" => [ "type" => Type::string() ],
                            "pageType" => [ "type" => Type::string() ],
                            "containerGuid" => [ "type" => Type::string() ],
                            "accessId" => [ "type" => Type::int() ],
                            "tags" => [ "type" => Type::listOf(Type::string()) ]
                        ]
                    ]))
                ]
            ],
            "resolve" => function ($query, $args, $context, ResolveInfo $info) {
                return self::mutate($args['input']);
            }
        ];
    }

    static function mutate($input) {
        $site = elgg_get_site_entity();

        $entity = new \ElggObject();
        $entity->subtype = "page";
        $entity->title = $input["title"];

        if (isset($input["accessId"])) {
            $entity->access_id = (int) $input["accessId"];
        } else {
            $entity->access_id = get_default_access();
        }

        $entity->description = $input["description"];

        // throw error when mysql TEXT limit is reached
        if (strlen($input["richDescription"]) > 65000) {
            throw new Exception("rich_description_limit_reached");
        }

        $entity->richDescription = $input["richDescription"];

        $entity->pageType = $input["pageType"];

        if (isset($input["containerGuid"])) {
            $entity->container_guid = $input["containerGuid"];
        } else {
            $entity->container_guid = $site->guid;
        }

        $entity->tags = filter_tags($input["tags"]);

        $result = $entity->save();

        if ($result) {
            return [
                "guid" => $entity->guid
            ];
        }

        throw new Exception("could_not_save");
    }
}
