<?php
namespace Pleio\InputTypes;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class MenuItemInput extends InputObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "MenuItemInput",
            "fields" => [
                "title" => [
                    "type" => Type::nonNull(Type::string())
                ],
                "link" => [
                    "type" => Type::string()
                ],
                "id" => [
                    "description" => "Temporary id of this menu item.",
                    "type" => Type::nonNull(Type::int())
                ],
                "parentId" => [
                    "description" => "Temporary id of the parent menu item.",
                    "type" => Type::int()
                ]
            ]
        ]);
    }
}
