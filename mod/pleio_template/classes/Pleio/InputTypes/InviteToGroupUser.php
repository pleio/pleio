<?php
namespace Pleio\InputTypes;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class InviteToGroupUser extends InputObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "InviteToGroupUser",
            "description" => "An object with either the guid or the e-mailaddress of a user.",
            "fields" => [
                "guid" => [ "type" => Type::string() ],
                "email" => [ "type" => Type::string() ]
            ]
        ]);
    }
}
