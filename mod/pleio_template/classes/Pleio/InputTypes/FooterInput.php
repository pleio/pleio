<?php
namespace Pleio\InputTypes;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class FooterInput extends InputObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "FooterInput",
            "fields" => [
                "title" => [
                    "type" => Type::nonNull(Type::string())
                ],
                "link" => [
                    "type" => Type::nonNull(Type::string())
                ]
            ]
        ]);
    }
}
