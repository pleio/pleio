<?php
namespace Pleio\InputTypes;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class FeaturedInput extends InputObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "FeaturedInput",
            "fields" => [
                "video" => [ "type" => Type::string() ],
                "image" => [ "type" => $registry->get("Upload") ],
                "positionY" => [ "type" => Type::int() ]
            ]
        ]);
    }
}
