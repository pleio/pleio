<?php
namespace Pleio\InputTypes;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class TagCategoryInput extends InputObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "TagCategoryInput",
            "fields" => [
                "name" => [
                    "type" => Type::nonNull(Type::string())
                ],
                "values" => [
                    "type" => Type::listOf(Type::string())
                ]
            ]
        ]);
    }
}
