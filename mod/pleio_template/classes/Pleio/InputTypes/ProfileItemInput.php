<?php
namespace Pleio\InputTypes;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class ProfileItemInput extends InputObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "ProfileItemInput",
            "fields" => [
                "key" => [
                    "type" => Type::nonNull(Type::string())
                ],
                "name" => [
                    "type" => Type::nonNull(Type::string())
                ],
                "isFilter" => [
                    "type" => Type::nonNull(Type::boolean())
                ],
                "isInOverview" => [
                    "type" => Type::boolean()
                ]
            ]
        ]);
    }
}