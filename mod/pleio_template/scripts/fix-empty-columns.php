<?php

function get_columns($row) {
    $options = [
        "type" => "object",
        "subtypes" => ["column", ],
        "limit" => false,
        'metadata_name' => "parent_guid",
        'metadata_value' => $row->guid,
    ];

    return elgg_get_entities_from_metadata($options);
}

function create_column($row, $position, $width) {
    if (!$position) {
        $position = 0;
    }
    $column = new \ElggObject();
    $column->subtype = "column";
    $column->parent_guid = $row->guid;
    $column->position = $position;
    $column->container_guid = $row->container_guid;
    $column->access_id = $row->access_id;
    $column->width = $width;
    $col = $column->save();
    return $col;
}

function process_rows($rows) {
    
    foreach ($rows as $row) {
        $columns = get_columns($row);
        if (count($columns) == 1) {
            foreach ($columns as $column) {
                if ($column->position > 0 && $column->width[0] != 12) {
                    print("b");
                    $column->position = 1;
                    $column->save();
                    print($column->width);
                    $width_other_row = 12 - $column->width;
                    create_column($row, 0, $width_other_row);
                }
            }
        }
    }
}

if(elgg_is_active_plugin("pleio_template")) {

    $dbprefix = elgg_get_config("dbprefix");
    $ia = elgg_set_ignore_access(true);

    # check for cms 2.0 errors
    $cms2_data = get_data("SELECT guid FROM elgg_entities ee
                            LEFT JOIN elgg_entity_subtypes es ON ee.subtype = es.id
                            LEFT JOIN elgg_metadata md ON ee.guid = md.entity_guid
                            LEFT JOIN elgg_metastrings ms ON md.value_id = ms.id
                            LEFT JOIN elgg_metastrings ms2 ON md.name_id = ms2.id
                            WHERE ms.string = 'campagne' AND ms2.string = 'pageType' AND es.subtype = 'page'");

    foreach ($cms2_data as $page) {
        $options = [
            "type" => "object",
            "subtypes" => ["row", ],
            "limit" => false,
            "order_by" => "guid ASC",
            "container_guid" => $page->guid,
        ];
        process_rows(elgg_get_entities_from_metadata($options));
    }

    elgg_set_ignore_access($ia);

    system_message("Rijen geconverteerd voor nieuwe cms");

}
