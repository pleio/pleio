<?php
// This script should be executed using a background process
// php console.php runscript migrate-tag-categories.php

// READ OLD PLUGIN SETTINGS
$predefinedTags = elgg_get_plugin_setting("predefinedTags", "pleio_template");

if ($predefinedTags) {
    $predefinedTags = json_decode(html_entity_decode($predefinedTags, ENT_COMPAT | ENT_QUOTES, 'UTF-8'));
    if (count($predefinedTags) > 0) {
        $hasPredefinedTags = true;
        $predefinedTags = $predefinedTags;
    } else {
        $hasPredefinedTags = false;
        $predefinedTags = [];
    }
} else {
    $hasPredefinedTags = false;
    $predefinedTags = [];
}

$filters = json_decode(html_entity_decode(elgg_get_plugin_setting("filters", "pleio_template"), ENT_COMPAT | ENT_QUOTES, 'UTF-8'));
if (!$filters) {
    $filters = [];
}

$tagCategories = json_decode(html_entity_decode(elgg_get_plugin_setting("tagCategories", "pleio_template"), ENT_COMPAT | ENT_QUOTES, 'UTF-8'));

// CONVERT TO NEW PLUGIN SETTINGS IF NOT ALREADY EXISTS
if (!$tagCategories) {
    $tagCategories = [];
    if ($hasPredefinedTags) {
        echo "Convert predefinedTags to tagCategories\n";

        $values = [];
        foreach($predefinedTags as $item) {
            $values[] = $item->tag;
        }
        $tagCategories = [];
        $tagCategories[] = ["name" => "Tags", "values" => $values];
    } else {
        echo "Convert filters to tagCategories\n";
        $tagCategories = $filters;
    }

    elgg_set_plugin_setting('tagCategories', json_encode($tagCategories), 'pleio_template');
} else {
    echo "Skip migration: tagCategories already exists";
}
