<?php
$users = Pleio\NotificationsAdmin::getUserStats();
?>
<table class="elgg-table mbl">
    <tbody>
        <tr>
            <td><?php echo elgg_echo("pleio_template:admin:notifications:total_users"); ?></td><td><?php echo $users["total"]; ?></td>
        </tr>
        <tr>
            <td><?php echo elgg_echo("pleio_template:admin:notifications:total_users_auto_notify"); ?></td><td><?php echo $users["email"]; ?></td>
        </tr>
        <tr>
            <td><?php echo elgg_echo("pleio_template:admin:notifications:total_users_auto_notify"); ?></td><td><?php echo $users["active"]; ?></td>
        </tr>
    </tbody>
</table>

<?php echo elgg_view("output/confirmlink", [
    "href" => "/action/admin/enable_email_notifications",
    "text" => elgg_echo("pleio_template:admin:notifications:all_users_enable_email_notifications"),
    "class" => "elgg-button elgg-button-submit",
    "is_action" => true
]); ?>

<br /><br />

<?php echo elgg_view("output/confirmlink", [
    "href" => "/action/admin/disable_email_notifications",
    "text" => elgg_echo("pleio_template:admin:notifications:all_users_disable_email_notifications"),
    "class" => "elgg-button elgg-button-submit",
    "is_action" => true
]); ?>

<br /><br />

<?php echo elgg_view("output/confirmlink", [
    "href" => "/action/admin/disable_email_overviews",
    "text" => elgg_echo("pleio_template:admin:notifications:all_users_disable_email_overview"),
    "class" => "elgg-button elgg-button-submit",
    "is_action" => true
]); ?>

<br /><br />

<?php echo elgg_view("output/confirmlink", [
    "href" => "/action/admin/enable_email_overviews",
    "text" => elgg_echo("pleio_template:admin:notifications:all_users_enable_email_overview"),
    "class" => "elgg-button elgg-button-submit",
    "is_action" => true
]); ?>

<br /><br />