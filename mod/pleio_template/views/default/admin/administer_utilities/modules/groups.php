<?php
$groups = Pleio\NotificationsAdmin::getGroupStats();
$users = Pleio\NotificationsAdmin::getUserStats();
?>
<table class="elgg-table mbl">
    <tbody>
        <tr>
            <td><?php echo elgg_echo("pleio_template:admin:notifications:total_groups"); ?></td><td><?php echo $groups["total_groups"]; ?></td>
        </tr>
        <tr>
            <td><?php echo elgg_echo("pleio_template:admin:notifications:total_groups_auto_notify"); ?></td><td><?php echo $groups["active_groups"]; ?></td>
        </tr>
        <tr>
            <td><?php echo elgg_echo("pleio_template:admin:notifications:total_groups_subscribers"); ?></td><td><?php echo $groups["total_subscribers"]; ?></td>
        </tr>
    </tbody>
</table>

<?php echo elgg_view("output/confirmlink", [
    "href" => "/action/admin/enable_auto_notifications",
    "text" => elgg_echo("pleio_template:admin:notifications:all_groups_enable_auto_notifications"),
    "class" => "elgg-button elgg-button-submit",
    "is_action" => true
]); ?>

<br /><br />

<?php echo elgg_view("output/confirmlink", [
    "href" => "/action/admin/disable_auto_notifications",
    "text" => elgg_echo("pleio_template:admin:notifications:all_groups_disable_auto_notifications"),
    "class" => "elgg-button elgg-button-submit",
    "is_action" => true
]); ?>

<br /><br />

<?php echo elgg_view("output/confirmlink", [
    "href" => "/action/admin/subscribe_users_to_auto_notification",
    "text" => elgg_echo("pleio_template:admin:notifications:subscribe_users_to_group_auto_notification"),
    "class" => "elgg-button elgg-button-submit",
    "is_action" => true
]); ?>

<br /><br />

<?php echo elgg_view("output/confirmlink", [
    "href" => "/action/admin/unsubscribe_users_to_auto_notification",
    "text" => elgg_echo("pleio_template:admin:notifications:unsubscribe_users_to_group_auto_notification"),
    "class" => "elgg-button elgg-button-submit",
    "is_action" => true
]); ?>
