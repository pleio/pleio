<?php
$plugin = $vars["entity"];
$site = $vars["site"];

echo elgg_view_module(
    "inline",
    elgg_echo("pleio_template:icon"),
    elgg_view("plugins/pleio_template/modules/icon", ["plugin" => $plugin])
);

echo elgg_view_module(
    "inline",
    elgg_echo("pleio_template:logo"),
    elgg_view("plugins/pleio_template/modules/logo", ["plugin" => $plugin])
);

echo elgg_view_module(
    "inline",
    elgg_echo("pleio_template:style"),
    elgg_view("plugins/pleio_template/modules/style", ["plugin" => $plugin])
);

echo elgg_view_module(
    "inline",
    elgg_echo("pleio_template:settings"),
    elgg_view("plugins/pleio_template/modules/settings", ["plugin" => $plugin])
);

echo elgg_view_module(
    "inline",
    elgg_echo("pleio_template:email_overview"),
    elgg_view("plugins/pleio_template/modules/email_overview", ["plugin" => $plugin, "site" => $site])
);

echo elgg_view_module(
    "inline",
    elgg_echo("pleio_template:menu"),
    elgg_view("plugins/pleio_template/modules/menu", ["plugin" => $plugin])
);

echo elgg_view_module(
    "inline",
    elgg_echo("pleio_template:profile"),
    elgg_view("plugins/pleio_template/modules/profile", ["plugin" => $plugin])
);

echo elgg_view_module(
    "inline",
    elgg_echo("pleio_template:tags"),
    elgg_view("plugins/pleio_template/modules/tags", ["plugin" => $plugin])
);

echo elgg_view_module(
    "inline",
    elgg_echo("pleio_template:direct_links"),
    elgg_view("plugins/pleio_template/modules/direct_links", ["plugin" => $plugin])
);

echo elgg_view_module(
    "inline",
    elgg_echo("pleio_template:footer"),
    elgg_view("plugins/pleio_template/modules/footer", ["plugin" => $plugin])
);

echo elgg_view_module(
    "inline",
    elgg_echo("pleio_template:analytics"),
    elgg_view("plugins/pleio_template/modules/analytics", ["plugin" => $plugin])
);
