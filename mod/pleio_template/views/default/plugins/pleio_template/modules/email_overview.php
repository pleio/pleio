<?php
$site = $vars["site"];
?>
<div>
    <label><?php echo elgg_echo("pleio_template:default_email_overview"); ?></label>
    <?php echo elgg_view("input/dropdown", [
        "name" => "params[default_email_overview]",
        "value" => $vars["plugin"]->default_email_overview ?: "weekly",
        "options_values" => [
            "daily" => elgg_echo("option:daily"),
            "weekly" => elgg_echo("option:weekly"),
            "monthly" => elgg_echo("option:monthly"),
            "never" => elgg_echo("option:never")
        ]
    ]); ?>
</div>

<div>
    <label><?php echo elgg_echo("pleio_template:admin:email_overview:subject"); ?></label>
    <?php echo elgg_view("input/text", [
        "name" => "params[email_overview_subject]",
        "value" => $vars["plugin"]->email_overview_subject,
        "placeholder" => elgg_echo("pleio_template:periodical:overview:send:subject", [ $site->name ])
    ]); ?>
</div>

<div>
    <label><?php echo elgg_echo("pleio_template:admin:email_overview:title"); ?></label>
    <?php echo elgg_view("input/text", [
        "name" => "params[email_overview_title]",
        "value" => $vars["plugin"]->email_overview_title,
        "placeholder" => $site->name
    ]); ?>
</div>

<div>
    <label><?php echo elgg_echo("pleio_template:admin:email_overview:intro"); ?></label>
    <?php echo elgg_view("input/plaintext", [
        "name" => "params[email_overview_intro]",
        "value" => $vars["plugin"]->email_overview_intro,
        "placeholder" => elgg_echo("pleio_template:email:overview", [ $site->name ])
    ]); ?>
</div>