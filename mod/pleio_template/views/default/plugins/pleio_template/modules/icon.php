<?php
$site = elgg_get_site_entity();
$icontime = $site->icontime ? $site->icontime : time();
?>
<p>
    <img src="/mod/pleio_template/icon.php?lastcache=<?php echo $icontime; ?>" class="icon">
</p>

<p>
    <input type="file" name="icon" accept="image/*">
</p>

<?php if ($site->icontime): ?>
    <p>
        <input type="checkbox" id="remove_icon" name="remove_icon" value="1">
        <label for="remove_icon"><?php echo elgg_echo("pleio_template:remove_icon"); ?></label>
    </p>
<?php endif; ?>

<p>
    <label><?php echo elgg_echo("pleio_template:icon_alt"); ?>*</label>
    <?php echo elgg_view("input/text", [
        "name" => "params[icon_alt]",
        "value" => $vars["plugin"]->icon_alt ?: elgg_echo("pleio_template:icon_alt:default"),
        "required" => true
    ]); ?>
</p>

<p>
    <label><?php echo elgg_echo("pleio_template:show_icon"); ?></label>
    <?php echo elgg_view("input/dropdown", [
        "name" => "params[show_icon]",
        "value" => $vars["plugin"]->show_icon,
        "options_values" => [
            "no" => elgg_echo("option:no"),
            "yes" => elgg_echo("option:yes")
        ]
    ]); ?><br />
    <span class="elgg-subtext"><?php echo elgg_echo("pleio_template:icon:explanation"); ?></span>
</p>
