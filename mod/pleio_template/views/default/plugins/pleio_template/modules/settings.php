<div>
    <label><?php echo elgg_echo("pleio_template:theme"); ?></label>
    <?php echo elgg_view("input/dropdown", [
        "name" => "params[theme]",
        "value" => $vars["plugin"]->theme,
        "options_values" => [
            "leraar" => elgg_echo("pleio_template:minimalistic"),
            "rijkshuisstijl" => elgg_echo("pleio_template:rijkshuisstijl")
        ]
    ]); ?>
</div>

<h4 class="pleio-template-settings-sub"><?php echo elgg_echo("pleio_template:startpage"); ?></h4>

<div>
    <label><?php echo elgg_echo("pleio_template:startpage"); ?></label>
    <?php echo elgg_view("input/dropdown", [
        "name" => "params[startpage]",
        "value" => $vars["plugin"]->startpage,
        "options_values" => [
            "activity" => elgg_echo("pleio_template:activity"),
            "cms" => elgg_echo("pleio_template:cms")
        ]
    ]); ?>
</div>

<div>
    <label><?php echo elgg_echo("pleio_template:startpage:cms"); ?></label>
    <?php echo elgg_view("input/dropdown", [
        "name" => "params[startpage_cms]",
        "value" => $vars["plugin"]->startpage_cms,
        "options_values" => pleio_template_get_cms_pages()
    ]); ?>
</div>

<h4 class="pleio-template-settings-sub"><?php echo elgg_echo("pleio_template:activity"); ?></h4>

<section class="mtl mbl">
    <div>
        <label><?php echo elgg_echo("pleio_template:show_leader"); ?></label>
        <?php echo elgg_view("input/dropdown", [
            "name" => "params[show_leader]",
            "value" => $vars["plugin"]->show_leader,
            "options_values" => [
                "no" => elgg_echo("option:no"),
                "yes" => elgg_echo("option:yes")
            ]
        ]); ?>
    </div>

    <div>
        <label><?php echo elgg_echo("pleio_template:show_leader_buttons"); ?></label>
        <?php echo elgg_view("input/dropdown", [
            "name" => "params[show_leader_buttons]",
            "value" => $vars["plugin"]->show_leader_buttons,
            "options_values" => [
                "no" => elgg_echo("option:no"),
                "yes" => elgg_echo("option:yes")
            ]
        ]); ?>
    </div>

    <div>
        <label><?php echo elgg_echo("pleio_template:subtitle"); ?></label>
        <?php echo elgg_view("input/text", [
            "name" => "params[subtitle]",
            "value" => $vars["plugin"]->subtitle,
            "options_values" => [
                "no" => elgg_echo("option:no"),
                "yes" => elgg_echo("option:yes")
            ]
        ]); ?>
    </div>

    <div>
        <label><?php echo elgg_echo("pleio_template:leader_image"); ?></label>
        <?php echo elgg_view("input/text", [
            "name" => "params[leader_image]",
            "value" => $vars["plugin"]->leader_image,
            "options_values" => [
                "no" => elgg_echo("option:no"),
                "yes" => elgg_echo("option:yes")
            ]
        ]); ?>
    </div>
</section>

<section class="mtl mbl">
    <div>
        <label><?php echo elgg_echo("pleio_template:number_of_featured_items"); ?></label>
        <?php echo elgg_view("input/dropdown", [
            "name" => "params[number_of_featured_items]",
            "value" => $vars["plugin"]->number_of_featured_items ?: 0,
            "options_values" => [
                0 => elgg_echo("0"),
                1 => elgg_echo("1"),
                2 => elgg_echo("2"),
                3 => elgg_echo("3"),
                4 => elgg_echo("4"),
                5 => elgg_echo("5"),
                6 => elgg_echo("6")
            ]
        ]); ?>
    </div>

    <div>
        <label><?php echo elgg_echo("pleio_template:enable_feed_sorting"); ?></label>
        <?php echo elgg_view("input/dropdown", [
            "name" => "params[enable_feed_sorting]",
            "value" => $vars["plugin"]->enable_feed_sorting ?: elgg_echo("option:yes"),
            "options_values" => [
                "yes" => elgg_echo("option:yes"),
                "no" => elgg_echo("option:no")
            ]
        ]); ?>
    </div>

    <div>
        <label><?php echo elgg_echo("pleio_template:show_extra_homepage_filters"); ?></label>
        <?php echo elgg_view("input/dropdown", [
            "name" => "params[show_extra_homepage_filters]",
            "value" => $vars["plugin"]->show_extra_homepage_filters,
            "options_values" => [
                "no" => elgg_echo("option:no"),
                "yes" => elgg_echo("option:yes")
            ]
        ]); ?>
    </div>

</section>

<section class="mtl mbl">
    <div class="mtl">
        <label><?php echo elgg_echo("pleio_template:show_initiative"); ?></label>
        <?php echo elgg_view("input/dropdown", [
            "name" => "params[show_initiative]",
            "value" => $vars["plugin"]->show_initiative,
            "options_values" => [
                "no" => elgg_echo("option:no"),
                "yes" => elgg_echo("option:yes")
            ]
        ]); ?>
    </div>

    <div>
        <label><?php echo elgg_echo("pleio_template:initiative_title"); ?></label>
        <?php echo elgg_view("input/text", [
            "name" => "params[initiative_title]",
            "value" => $vars["plugin"]->initiative_title
        ]); ?>
    </div>

    <div>
        <label><?php echo elgg_echo("pleio_template:initiative_image"); ?></label>
        <?php echo elgg_view("input/text", [
            "name" => "params[initiative_image]",
            "value" => $vars["plugin"]->initiative_image
            ]); ?>
    </div>

    <div>
        <label><?php echo elgg_echo("pleio_template:initiative_image_alt"); ?></label>
        <?php echo elgg_view("input/text", [
            "name" => "params[initiative_image_alt]",
            "value" => $vars["plugin"]->initiative_image_alt ?: elgg_echo("pleio_template:initiative_image_alt:default"),
            "required" => true
        ]); ?>
    </div>

    <div>
        <label><?php echo elgg_echo("pleio_template:initiative_description"); ?></label>
        <?php echo elgg_view("input/text", [
            "name" => "params[initiative_description]",
            "value" => $vars["plugin"]->initiative_description
        ]); ?>
    </div>


    <div>
        <label><?php echo elgg_echo("pleio_template:initiator_link"); ?></label>
        <?php echo elgg_view("input/text", [
            "name" => "params[initiator_link]",
            "placeholder" => elgg_echo("pleio_template:initiator_link:example"),
            "value" => $vars["plugin"]->initiator_link
        ]); ?>
    </div>
</section>

<h4 class="pleio-template-settings-sub"><?php echo elgg_echo("pleio_template:groups") ?></h4>

<div>
    <label><?php echo elgg_echo("pleio_template:subgroups"); ?></label>
    <?php echo elgg_view("input/dropdown", [
        "name" => "params[subgroups]",
        "value" => $vars["plugin"]->subgroups,
        "options_values" => [
            "no" => elgg_echo("option:no"),
            "yes" => elgg_echo("option:yes")
        ]
    ]); ?>
</div>

<div>
    <label><?php echo elgg_echo("pleio_template:status_update_groups"); ?></label>
    <?php echo elgg_view("input/dropdown", [
        "name" => "params[status_update_groups]",
        "value" => $vars["plugin"]->status_update_groups,
        "options_values" => [
            "yes" => elgg_echo("option:yes"),
            "no" => elgg_echo("option:no")
        ]
    ]); ?>
</div>

<h4 class="pleio-template-settings-sub"><?php echo elgg_echo("pleio_template:interface_elements") ?></h4>
<div>
    <label><?php echo elgg_echo("pleio_template:show_excerpt_in_news_card"); ?></label>
    <?php echo elgg_view("input/dropdown", [
        "name" => "params[show_excerpt_in_news_card]",
        "value" => $vars["plugin"]->show_excerpt_in_news_card,
        "options_values" => [
            "no" => elgg_echo("option:no"),
            "yes" => elgg_echo("option:yes")
        ]
    ]); ?>
</div>

<div>
    <label><?php echo elgg_echo("pleio_template:show_tag_in_news_card"); ?></label>
    <?php echo elgg_view("input/dropdown", [
        "name" => "params[show_tag_in_news_card]",
        "value" => $vars["plugin"]->show_tag_in_news_card,
        "options_values" => [
            "no" => elgg_echo("option:no"),
            "yes" => elgg_echo("option:yes")
        ]
    ]); ?>
</div>

<div>
    <label><?php echo elgg_echo("pleio_template:comment_on_news"); ?></label>
    <?php echo elgg_view("input/dropdown", [
        "name" => "params[comments_on_news]",
        "value" => $vars["plugin"]->comments_on_news,
        "options_values" => [
            "no" => elgg_echo("option:no"),
            "yes" => elgg_echo("option:yes")
        ]
    ]); ?>
</div>

<div>
    <label><?php echo elgg_echo("pleio_template:enable_sharing"); ?></label>
    <?php echo elgg_view("input/dropdown", [
        "name" => "params[enable_sharing]",
        "value" => $vars["plugin"]->enable_sharing,
        "options_values" => [
            "yes" => elgg_echo("option:yes"),
            "no" => elgg_echo("option:no")
        ]
    ]); ?>
</div>


<div>
    <label><?php echo elgg_echo("pleio_template:enable_up_down_voting"); ?></label>
    <?php echo elgg_view("input/dropdown", [
        "name" => "params[enable_up_down_voting]",
        "value" => $vars["plugin"]->enable_up_down_voting,
        "options_values" => [
            "yes" => elgg_echo("option:yes"),
            "no" => elgg_echo("option:no")
        ]
    ]); ?>
</div>


<div>
    <label><?php echo elgg_echo("pleio_template:questioner_can_choose_best_answer"); ?></label>
    <?php echo elgg_view("input/dropdown", [
        "name" => "params[questioner_can_choose_best_answer]",
        "value" => $vars["plugin"]->questioner_can_choose_best_answer,
        "options_values" => [
            "yes" => elgg_echo("option:yes"),
            "no" => elgg_echo("option:no")
        ]
    ]); ?>
</div>


<div>
    <label><?php echo elgg_echo("pleio_template:enable_views_count"); ?></label>
    <?php echo elgg_view("input/dropdown", [
        "name" => "params[enable_views_count]",
        "value" => $vars["plugin"]->enable_views_count,
        "options_values" => [
            "no" => elgg_echo("option:no"),
            "yes" => elgg_echo("option:yes")
        ]
    ]); ?>
</div>

<div>
    <label><?php echo elgg_echo("pleio_template:like_icon"); ?></label>
    <?php echo elgg_view("input/dropdown", [
        "name" => "params[like_icon]",
        "value" => $vars["plugin"]->like_icon,
        "options_values" => [
            "heart" => elgg_echo("pleio_template:heart"),
            "thumbs" => elgg_echo("pleio_template:thumbs")
        ]
    ]); ?>
</div>

<div>
    <label><?php echo elgg_echo("pleio_template:show_login_register"); ?></label>
    <?php echo elgg_view("input/dropdown", [
        "name" => "params[show_login_register]",
        "value" => $vars["plugin"]->show_login_register,
        "options_values" => [
            "yes" => elgg_echo("option:yes"),
            "no" => elgg_echo("option:no")
        ]
    ]); ?>
</div>

<h4 class="pleio-template-settings-sub"><?php echo elgg_echo("pleio_template:access_permissions") ?></h4>

<div>
    <label><?php echo elgg_echo("pleio_template:event_export"); ?></label>
    <?php echo elgg_view("input/dropdown", [
        "name" => "params[event_export]",
        "value" => $vars["plugin"]->event_export,
        "options_values" => [
            "no" => elgg_echo("option:no"),
            "yes" => elgg_echo("option:yes")
        ]
    ]); ?>
</div>

<h4 class="pleio-template-settings-sub"><?php echo elgg_echo("pleio_template:other_features") ?></h4>

<div>
    <label><?php echo elgg_echo("pleio_template:newsletter"); ?></label>
    <?php echo elgg_view("input/dropdown", [
        "name" => "params[newsletter]",
        "value" => $vars["plugin"]->newsletter,
        "options_values" => [
            "no" => elgg_echo("option:no"),
            "yes" => elgg_echo("option:yes")
        ]
    ]); ?>
</div>

<div>
    <label><?php echo elgg_echo("pleio_template:member_export"); ?></label>
    <?php echo elgg_view("input/dropdown", [
        "name" => "params[member_export]",
        "value" => $vars["plugin"]->member_export,
        "options_values" => [
            "no" => elgg_echo("option:no"),
            "yes" => elgg_echo("option:yes")
        ]
    ]); ?>
</div>

<div>
    <label><?php echo elgg_echo("pleio_template:cancel_membership_enabled"); ?></label>
    <?php echo elgg_view("input/dropdown", [
        "name" => "params[cancel_membership_enabled]",
        "value" => $vars["plugin"]->cancel_membership_enabled,
        "options_values" => [
            "yes" => elgg_echo("option:yes"),
            "no" => elgg_echo("option:no")
        ]
    ]); ?>
</div>


