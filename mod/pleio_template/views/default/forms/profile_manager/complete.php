<?php $fields = elgg_extract("fields", $vars, array()); ?>

<div class="form">
    <?php foreach ($fields as $field) {
        echo "<div class=\"form__item\">";
            echo "<label class=\"form__label\">" . $field->getTitle() . "*</label>";
            if($hint = $field->getHint()){
                echo  "<span class='custom_fields_more_info_text' id='text_more_info_" . $field->metadata_name . "'>" . $hint . "</span><br/>";
            }

            switch($field->metadata_type) {
                case "dropdown":
                    $input_class = 'form__input';
                    break;
                default:
                    $input_class = 'form__input';
            }

            echo elgg_view("input/{$field->metadata_type}", array(
                'name' => 'custom_profile_fields[' . $field->metadata_name . ']',
                'options' => $field->getOptions(),
                'class'=> $input_class,
                'required' => 'required' // all retrieved fields are mandatory
            ));
        echo "</div>";
    } ?>

    <div class="buttons ___space-between">
    <?php
    echo elgg_view('input/hidden', array(
        'name' => 'redirect_uri',
        'value' => get_input('redirect_uri')
    ));

    echo elgg_view('input/submit', array(
        'value' => elgg_echo('save'),
        'class' => 'button'
    ));
    ?>
    </div>
</div>
