<?php
$idp = elgg_get_plugin_setting("idp", "pleio");
$idp_name = elgg_get_plugin_setting("idp_name", "pleio");

$invitecode = get_input("invitecode");
if ($invitecode) {
    $invitecode = urldecode(get_input("invitecode"));
}

$returnto = get_input("returnto");
if ($returnto) {
    $returnto = urldecode(get_input("returnto"));
}

$query = [];
if ($invitecode) {
    $query = [ "invitecode" => $invitecode ];
} elseif ($returnto) {
    $query = [ "returnto" => $returnto ];
} else {
    $query = [];
}
?>

<div class="walled-garden_buttons">
    <?php if ($idp): ?>
        <?php echo elgg_view("output/url", array(
                "href" => "/login?" . http_build_query($query),
                "class" => "button ___medium",
                "text" => $idp && $idp_name ? elgg_echo("pleio:settings:login_through", [$idp_name]) : elgg_echo("login")
        )); ?>

        <div class="walled-garden_buttons-or">of</div>

        <?php echo elgg_view("output/url", array(
            "href" => "/login?" . http_build_query(array_merge($query, [ "login_credentials" => "true" ])),
            "class" => "button ___medium ___line",
            "text" => elgg_echo("pleio:login_with_credentials")
        )); ?>
    <?php else: ?>
        <?php echo elgg_view("output/url", array(
                "href" => "/login?" . http_build_query($query),
                "class" => "button ___medium",
                "text" => elgg_echo("pleio:login_with_credentials")
        )); ?>
    <?php endif; ?>
</div>

<div class="walled-garden_register content">
    <?php echo elgg_echo("pleio:not_yet_registered") ?>
    <?php echo elgg_view("output/url", array(
            "href" => "/login?" . http_build_query(array_merge($query, [ "login_credentials" => "true", "method" => "register" ])),
            "class" => "",
            "text" => elgg_echo("register")
    )); ?>
</div>