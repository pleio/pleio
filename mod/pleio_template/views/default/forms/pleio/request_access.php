<?php
global $CONFIG;
?>
<div class="form">
    <?php echo elgg_view("profile_manager/register/fields"); ?>

    <div class="buttons ___space-between">
        <button type="submit" class="elgg-button button" id="request_access_button">
            <?php echo elgg_echo("pleio:request_access") ?>
            <span class="button__loader"></span>
        </button>
        <?php echo elgg_view("output/url", ["class" => "button ___link", "href" => $CONFIG->pleio->url . "action/logout", "text" => elgg_echo("logout")]); ?>
    </div>
</div>
