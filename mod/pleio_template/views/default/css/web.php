<?php
include_once(dirname(__FILE__) . "/transformDefaultColors.php");

if (webpack_dev_server_is_available()) {
    $host = getenv('FRONTEND_DEV_HOST') ? getenv('FRONTEND_DEV_HOST') : "localhost";

    $path = "http://{$host}:9001/web.css";
} else {
    $path = dirname(__FILE__) . "/../../../build/web.css";
}

$contents = file_get_contents($path);

echo transformDefaultColors($contents);
