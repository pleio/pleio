<?php
/**
 * Used in pages where we serve content that is not reactified yet (eg: walled_garden.php).
 * assets/legacy.css is generated css by less code in pleio/frontend.
 */

include_once(dirname(__FILE__) . "/transformDefaultColors.php");   

$path = dirname(__FILE__) . "/../../../assets/legacy.css";
$contents = file_get_contents($path);

echo transformDefaultColors($contents);
