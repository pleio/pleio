<?php
header("Content-type: text/html; charset=utf-8");
$lang = get_current_language();

$theme = elgg_get_plugin_setting("theme", "pleio_template") ?: "leraar";
$font = elgg_get_plugin_setting("font", "pleio_template");

$icon = elgg_get_plugin_setting("icon", "pleio_template");

$custom_css = elgg_is_active_plugin("custom_css");
$custom_js = elgg_is_active_plugin("custom_js");

$footer = elgg_get_plugin_setting("walled_garden_footer", "pleio");

?>
<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta charset="utf-8">
		<title><?php echo $vars["title"] ? $vars["title"] : elgg_get_config("sitename"); ?></title>
		<?php if ($icon && $icon == "rijkshuisstijl"): ?>
			<link rel="icon" href="<?php echo pleio_template_assets("images/favicon.png"); ?>">
			<link rel="shortcut icon" href="<?php echo pleio_template_assets("images/favicon.ico"); ?>">
			<link rel="apple-touch-icon-precomposed" href="<?php echo pleio_template_assets("images/apple-touch-icon-precomposed.png"); ?>">
		<?php endif; ?>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="version" content="1.0.0">
		<meta name="relative-path" content="">

		<link href="/css/legacy.css?v=<?php echo pleio_filetime("assets/legacy.css"); ?>" rel="stylesheet" type="text/css">
		<link href="<?php echo elgg_get_simplecache_url("css", "web"); ?>" rel="stylesheet" type="text/css">

		<?php if ($font): ?>
			<?php if ($font === "Roboto"): ?>
				<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&display=swap" rel="stylesheet">
			<?php elseif ($font === "Merriweather"): ?>
				<link href="https://fonts.googleapis.com/css?family=Merriweather:400,700&display=swap" rel="stylesheet">
			<?php elseif ($font === "Open Sans"): ?>
				<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&display=swap" rel="stylesheet">
			<?php endif; ?>
		<?php endif; ?>

		<?php if ($custom_css): ?>
			<link href="<?php echo elgg_get_simplecache_url("css", "custom"); ?>" rel="stylesheet" type="text/css">
		<?php endif; ?>
	</head>
	<body class="___<?php echo $theme; ?>" style="background:white;">
		<div class="page-layout">
			<header class="page-layout__header walled-garden_header">
				<div class="walled-garden_header-top">
					<svg width="35px" height="35px" viewBox="0 0 35 35" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
						<g id="Walled-garden" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
							<g id="Nav-group-Copy-2" transform="translate(-40.000000, -32.000000)">
								<g id="logo" transform="translate(40.000000, 32.000000)">
									<g id="background" fill="#1565C0">
										<path d="M29.4,0 C32.5,0 35,2.5 35,5.6 L35,29.4 C35,32.5 32.5,35 29.4,35 L5.6,35 C2.5,35 0,32.5 0,29.4 L0,5.6 C0,2.5 2.5,0 5.6,0" id="Shape"></path>
									</g>
									<g id="lines" transform="translate(6.000000, 6.000000)" fill="#FFFFFF">
										<path d="M3,3 C4.2,1.8 6.1,1.8 7.2,3 L14,9.8 L15.3,8.5 L8.5,1.7 C6.6,-0.2 3.6,-0.2 1.7,1.7 C-0.2,3.6 -0.2,6.6 1.7,8.5 L3,9.9 L4.3,8.6 L3,7.2 C1.8,6 1.8,4.1 3,3" id="Shape"></path>
										<path d="M20,20 C18.8,21.2 16.9,21.2 15.8,20 L9,13.2 L7.7,14.5 L14.5,21.3 C16.4,23.2 19.4,23.2 21.3,21.3 C23.2,19.4 23.2,16.4 21.3,14.5 L20,13.1 L18.7,14.4 L20.1,15.8 C21.2,17 21.2,18.9 20,20" id="Shape"></path>
										<path d="M20,3 C21.2,4.2 21.2,6.1 20,7.2 L13.2,14 L14.5,15.3 L21.3,8.5 C23.2,6.6 23.2,3.6 21.3,1.7 C19.4,-0.2 16.4,-0.2 14.5,1.7 L13.1,3 L14.4,4.3 L15.8,3 C17,1.8 18.9,1.8 20,3" id="Shape"></path>
										<path d="M3,20 C1.8,18.8 1.8,16.9 3,15.8 L9.8,9 L8.5,7.7 L1.7,14.5 C-0.2,16.4 -0.2,19.4 1.7,21.3 C3.6,23.2 6.6,23.2 8.5,21.3 L9.9,19.9 L8.6,18.6 L7.2,20 C6,21.2 4.1,21.2 3,20" id="Shape"></path>
									</g>
								</g>
							</g>
						</g>
					</svg>
					<a href="https://www.pleio.nl/" target="_blank" class="walled-garden_pleio-link">
						www.pleio.nl
					</a>
				</div>
				<div class="elgg-page-messages walled-garden_messages">
					<?php echo elgg_view('page/elements/messages', array('object' => $vars['sysmessages'])); ?>
				</div>
			</header>
			<main class="page-layout__main walled-garden_main">
				<div class="walled-garden_content">
					<?php echo $vars['body']; ?>
				</div>
			</main>
			<footer class="walled-garden_footer">
				<div class="content">
					<?php echo $footer; ?>
				</div>
				<a href="https://www.pleio.nl/" target="_blank" class="walled-garden_pleio-link">
					www.pleio.nl
				</a>
			</footer>
		</div>

		<?php if ($is_sticky_register): ?>
		<script type="text/javascript">
		elgg.register_hook_handler('init', 'system', function() {
			$('.registration_link').trigger('click');
		});
		</script>
		<?php endif; ?>
		<?php echo elgg_view('page/elements/foot'); ?>
	</body>
</html>
