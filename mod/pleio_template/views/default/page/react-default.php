<?php
header("Content-type: text/html; charset=utf-8");
$lang = get_current_language();

$store = elgg_extract("store", $vars);
$metas = elgg_extract("metas", $vars, []);
$is_react = elgg_extract("is_react", $vars, false);

$settings = Pleio\Helpers::getSettings();
$apollo_state = Pleio\Helpers::getInitialApolloState();

$theme = elgg_get_plugin_setting("theme", "pleio_template") ?: "leraar";
$font = elgg_get_plugin_setting("font", "pleio_template");

$icon = elgg_get_plugin_setting("icon", "pleio_template");
$headerColor = elgg_get_plugin_setting("color_header", "pleio_template");
$primaryColor = elgg_get_plugin_setting("color_primary", "pleio_template");

$custom_css = elgg_is_active_plugin("custom_css");
$custom_js = elgg_is_active_plugin("custom_js");
$browsealoud = elgg_is_active_plugin("browsealoud");

$google_site_verification = elgg_get_plugin_setting("google_site_verification", "pleio_template");
?>
<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <title><?php echo $vars["title"] ? $vars["title"] : elgg_get_config("sitename"); ?></title>
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <meta name="msapplication-TileColor" content="#da532c">
    <?php if (${headerColor}): ?><meta name="theme-color" content="<?php echo $headerColor; ?>"><?php else: ?><meta name="theme-color" content="<?php echo $primaryColor; ?>">
    <?php endif; ?>
    <?php if ($icon && $icon == "rijkshuisstijl"): ?>
        <link rel="icon" href="<?php echo pleio_template_assets("images/favicon.png"); ?>">
        <link rel="shortcut icon" href="<?php echo pleio_template_assets("images/favicon.ico"); ?>">
        <link rel="apple-touch-icon-precomposed" href="<?php echo pleio_template_assets("images/apple-touch-icon-precomposed.png"); ?>">
    <?php endif; ?>

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="version" content="1.0.0">
    <meta name="relative-path" content="">
    <?php foreach ($metas as $name => $content): ?><meta name="<?php echo $name; ?>" content="<?php echo $content; ?>">
    <?php endforeach; ?>

    <?php if ($google_site_verification): ?><meta name="google-site-verification" content="<?php echo $google_site_verification; ?>">
    <?php endif; ?>

    <link href="<?php echo elgg_get_simplecache_url("css", "web"); ?>" rel="stylesheet" type="text/css" media="screen">

    <?php if ($font): ?>
        <?php if ($font === "Roboto"): ?>
            <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
        <?php elseif ($font === "Merriweather"): ?>
            <link href="https://fonts.googleapis.com/css?family=Merriweather:400,700&display=swap" rel="stylesheet">
        <?php elseif ($font === "Source Sans Pro"): ?>
            <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700&display=swap" rel="stylesheet">
        <?php elseif ($font === "Open Sans"): ?>
            <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&display=swap" rel="stylesheet">
        <?php elseif ($font === "PT Sans"): ?>
            <link href="https://fonts.googleapis.com/css2?family=PT+Sans:wght@400;700&display=swap" rel="stylesheet">
        <?php endif; ?>
    <?php endif; ?>

    <?php if ($custom_css): ?>
        <link href="<?php echo elgg_get_simplecache_url("css", "custom"); ?>" rel="stylesheet" type="text/css" media="screen">
    <?php endif; ?>
</head>

<body class="___<?php echo $theme; ?>">
    <?php if ($browsealoud): ?>
        <div id="__ba_launchpad"></div>
    <?php endif; ?>

    <?php echo elgg_view("page/elements/noscript"); ?>
    <div id="react-root" class="page-container">
        <?php echo elgg_extract(
            'body',
            $vars,
            '<div class="loading-pleio"><img src="/mod/pleio_template/icon.php"><p class="text-1">Pleio start op...</p><p class="text-2">In oude browsers (IE11) duurt het langer.</p></div>'
        ); ?>
    </div>

    <?php if ($store): ?>
        <script>
            window.__STORE__ = <?php echo json_encode($store); ?>;
        </script>
    <?php endif; ?>
    <?php if ($settings): ?>
        <script>
            window.__SETTINGS__ = <?php echo json_encode($settings); ?>;
        </script>
    <?php endif; ?>
    <?php if ($apollo_state): ?>
        <script>
            window.__APOLLO_STATE__ = <?php echo json_encode($apollo_state); ?>;
        </script>
    <?php endif; ?>

    <?php if ($is_react): ?>
        <?php if (webpack_dev_server_is_available()): ?>
            <script src="http://frontend-dev-server:9001/web.js"></script>
            <script src="http://frontend-dev-server:9001/vendor.js"></script>
        <?php else: ?>
            <script src="/mod/pleio_template/build/web.<?php echo pleio_filetime("build/web.js"); ?>.js"></script>
            <script src="/mod/pleio_template/build/vendor.<?php echo pleio_filetime("build/vendor.js"); ?>.js"></script>
        <?php endif; ?>
    <?php endif; ?>

    <?php if ($browsealoud): ?>
        <script type="text/javascript" src="https://www.browsealoud.com/plus/scripts/ba.js"></script>
    <?php endif; ?>

    <?php if ($custom_js): ?>
        <script src="<?php echo elgg_get_simplecache_url("js", "custom"); ?>"></script>
    <?php endif; ?>

    <?php echo elgg_view("page/elements/analytics"); ?>
    <?php echo elgg_view("page/elements/stats"); ?>
</body>
</html>
