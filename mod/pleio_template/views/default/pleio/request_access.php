<?php
global $CONFIG;

elgg_load_css('elgg.walled_garden');
elgg_load_js('elgg.walled_garden');

$title = elgg_get_site_entity()->name;
$resourceOwner = elgg_extract("resourceOwner", $vars);

$subtitle = elgg_extract("title", $vars);
$description = elgg_extract("description", $vars);

$welcome = elgg_echo('walled_garden:welcome');
$welcome .= ': <br/>' . $title;

$menu = elgg_view_menu('walled_garden', array(
    'sort_by' => 'priority',
    'class' => 'elgg-menu-general elgg-menu-hz',
));
?>
<script type="text/javascript">
function onSubmit() {
    document.getElementById('request_access_button').disabled = true;
    document.getElementById('request_access_button').classList.add('___is-loading');
}
</script>
<div class="elgg-col elgg-col-1of2">
    <div class="elgg-inner walled-garden_titles">
        <h1 class="elgg-heading-walledgarden main__title">
            <?php echo $welcome; ?>
        </h1>
    </div>
</div>
<div class="elgg-col elgg-col-1of2">
    <div class="elgg-inner">
        <h2><?php echo $subtitle; ?></h2>
        <p><?php echo $description; ?></p>
        <p><b><?php echo elgg_echo("name"); ?></b><br><?php echo $resourceOwner["name"]; ?></p>
        <p><b><?php echo elgg_echo("email"); ?></b><br><?php echo $resourceOwner["email"]; ?></p>
        <?php echo elgg_view_form("pleio/request_access", ["onsubmit" => "onSubmit()"]); ?>
    </div>
</div>

