<?php
$primary = elgg_get_plugin_setting("color_primary", "pleio_template") ?: "#01689b";
$site = elgg_get_site_entity();
$entity = elgg_extract("entity", $vars);
$owner = $entity->getOwnerEntity();
$in_group = "";

$container = $entity->getContainerEntity();
if ($container instanceof \ElggGroup) {
    $in_group = "in {$container->name}";
}
?>
<!-- Article -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="article" style="padding:16px 0 3px 0">
            <!-- Full width image -->
            <?php if ($entity->featuredIcontime): ?>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="fluid-img" style="font-size:0pt; line-height:0pt; text-align:left"><img src="<?php echo $site->url; ?>mod/pleio_template/featuredimage.php?guid=<?php echo $entity->guid; ?>&email=true" border="0" width="600" alt="<?php echo $entity->title; ?>" /></td>
                    </tr>
                </table>
            <?php endif; ?>
            <!-- END Full width image -->

            <!-- Content -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="article-body2" style="padding:26px 66px 29px 50px; background:#ffffff">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="h1" style="color:<?php echo $primary; ?>; font-family:Arial,sans-serif; font-size:24px; line-height:28px; text-align:left; padding-bottom:4px; font-weight:bold">
                                    <a href="<?php echo Pleio\Helpers::getURL($entity, $absolute = true); ?>?utm_medium=email&utm_campaign=overview" target="_blank" style="color:<?php echo $primary; ?>; text-decoration:none">
                                        <?php echo $entity->title; ?>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="h2" style="color:#999999; font-family:Arial,sans-serif; font-size:12px; line-height:18px; text-align:left; padding-bottom:12px"><?php echo elgg_echo("pleio_template:periodical:overview:created_blog"); ?> <?php echo $owner->name; ?> <?php echo elgg_echo("pleio_template:periodical:overview:on"); ?> <?php echo pleio_template_format_date($entity->time_created); ?> <?php echo $in_group; ?></td>
                            </tr>
                            <tr>
                                <td class="text" style="color:#000001; font-family:Arial,sans-serif; font-size:14px; line-height:20px; text-align:left">
                                    <?php echo elgg_get_excerpt($entity->description); ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!-- END Content -->
        </td>
    </tr>
</table>
<!-- END Article -->
