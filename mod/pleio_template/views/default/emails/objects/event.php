<?php
$primary = elgg_get_plugin_setting("color_primary", "pleio_template") ?: "#01689b";
$entity = elgg_extract("entity", $vars);
$owner = $entity->getOwnerEntity();
$in_group = "";
$start_date = mktime(
    date("H", $entity->start_time),
    date("i", $entity->start_time),
    date("s", $entity->start_time),
    date("n", $entity->start_day),
    date("j", $entity->start_day),
    date("Y", $entity->start_day)
);

$end_date = $entity->end_ts;

$container = $entity->getContainerEntity();
if ($container instanceof \ElggGroup) {
    $in_group = "in {$container->name}";
}
?>
<!-- Blue section -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="pt-22" style="padding-top:16px">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="section" style="padding:24px 50px" bgcolor="#ffffff">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="h1" style="color:<?php echo $primary; ?>; font-family:Arial,sans-serif; font-size:24px; line-height:28px; text-align:left; font-weight:bold">
                                    <a href="<?php echo Pleio\Helpers::getURL($entity, $absolute = true); ?>?utm_medium=email&utm_campaign=overview" target="_blank" style="color:<?php echo $primary; ?>; text-decoration:none">
                                        <?php echo $entity->title; ?>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="h2" style="color:#999999; font-family:Arial,sans-serif; font-size:12px; line-height:18px; text-align:left; padding-bottom:12px"><?php echo elgg_echo("pleio_template:periodical:overview:created_event"); ?> <?php echo $owner->name; ?> <?php echo elgg_echo("pleio_template:periodical:overview:takes_place_on"); ?> <?php echo pleio_template_format_date($start_date, "event"); ?> <?php echo $in_group; ?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- END Blue section -->