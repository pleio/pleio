<?php
$site = elgg_get_site_entity();

$intro = elgg_get_plugin_setting("email_overview_intro", "pleio_template") ?: elgg_echo("pleio_template:email:overview", [ $site->name ]);

?>
<!-- Intro text -->
<tr>
    <td class="intro" style="font-family:Arial,sans-serif; font-size:14px; line-height:22px; text-align:left; padding:27px 50px" bgcolor="#ffffff">
        <?php echo $intro ?>
    </td>
</tr>
<!-- END Intro text -->
