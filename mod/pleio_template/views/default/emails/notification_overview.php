<?php
$primary = elgg_get_plugin_setting("color_primary", "pleio_template") ?: "#01689b";
$notifications = elgg_extract("notifications", $vars);
$site = elgg_extract("site", $vars);
$show_excerpt = elgg_get_plugin_setting("email_notification_show_excerpt", "pleio_template") === "yes";

echo elgg_echo("pleio_template:notifications:overview:intro", [ $site->name, $site->url ]);
?>

<?php foreach ($notifications as $notification): ?><?php
        $subtype = get_subtype_from_id($notification['entity']->subtype);
        $item = "heeft een item geplaatst";
        $url = Pleio\Helpers::getURL($notification['entity'], $absolute = true);
        if ($subtype === 'blog') {
            $item = elgg_echo("pleio_template:notifications:overview:created_blog");
        } elseif ($subtype === 'discussion') {
            $item = elgg_echo("pleio_template:notifications:overview:created_discussion");
        } elseif ($subtype === 'event') {
            $item = elgg_echo("pleio_template:notifications:overview:created_event");
        } elseif ($subtype === 'thewire') {
            $item = elgg_echo("pleio_template:notifications:overview:created_update");
        } elseif ($subtype === 'question') {
            $item = elgg_echo("pleio_template:notifications:overview:created_question");
        } elseif ($subtype === 'wiki') {
            $item = elgg_echo("pleio_template:notifications:overview:created_wiki");
        }
        $in_group = "";

        if ($notification['container']->type === 'group') {
            $group_container = $notification['container'];
        }
        else {
            $group_container = get_entity($notification['entity']->container_guid);
        }
        if ($group_container->type === 'group') {
            $group_url = Pleio\Helpers::getURL($group_container, $absolute = true) . "?utm_medium=email&utm_campaign=notification_overview";
            $in_group = ' in <a href="' . $group_url . '" target="_blank" style="color:"' . $primary . ';" "text-decoration:none"><b>' . $group_container->name . '</b></a>';
        }

        $notification_title = '<a href="' . $url . '" target="_blank" style="color:"' . $primary . ';" "text-decoration:none"><b>' . $notification['entity']->title . '</b></a>';

        $notification_excerpt = '<div style="margin:0;padding:8px 0 0 0;">'.elgg_get_excerpt(html_entity_decode($notification['entity']->description, ENT_COMPAT | ENT_QUOTES, 'UTF-8'), 120).'</div>';

    echo "<hr style=\"border:none;border-bottom:1px solid #ececec;margin:1.5rem 0;width:100%\">";
    if ($notification['action'] === "created") {
        echo "<b>{$notification['performer']->name}</b> {$item}: {$notification_title}{$in_group}.";
    } elseif ($notification['action'] === "commented") {
        echo "<b>{$notification['performer']->name}</b> " . elgg_echo("pleio_template:notifications:overview:responded_on") . " {$notification_title}{$in_group}.";
    }

    if ($show_excerpt) {
        echo $notification_excerpt;
    }

?><?php endforeach; ?>