<?php
$entities = elgg_extract("featured", $vars, []);
$featured_title = elgg_get_plugin_setting("email_overview_featured_title", "pleio_template");
$primary = elgg_get_plugin_setting("color_primary", "pleio_template") ?: "#01689b";
?>

<?php if (count($entities) > 0): ?>

    <?php if ($featured_title): ?>
    <!-- Head -->
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding:16px 0 3px 0;">
        <tr>
            <td class="featured-title" style="font-family:Arial,sans-serif; font-size:24px; line-height:28px; text-align:left; font-weight:bold; padding:0 50px;">
                <?php echo $featured_title; ?>
            </td>
        </tr>
    </table>
    <!-- END Head -->
    <?php endif; ?>

<?php

    foreach ($entities as $entity) {
        $subtype = $entity->getSubtype();
        if (elgg_view_exists("emails/objects/${subtype}")) {
            echo elgg_view("emails/objects/${subtype}", [ "entity" => $entity ]);
        }
    }

?>
    <!-- Footer -->
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td style="font-family:Arial,sans-serif; font-size:26px; line-height:30px; text-align:left; padding:16px 50px 0 50px;">
                <hr>
            </td>
        </tr>
    </table>
    <!-- END Footer-->
<?php endif; ?>