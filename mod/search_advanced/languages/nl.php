<?php
$language = array (
  'search_advanced:widgets:search:edit:submit_behaviour' => 'Gedrag van zoeken knop',
  'search_advanced:widgets:search:edit:submit_behaviour:show_in_widget' => 'Toon in widget',
  'search_advanced:widgets:search:edit:submit_behaviour:go_to_search' => 'Ga naar zoeken',
  'search_advanced:settings:profile_fields' => 'Configureer profielveld specifieke instellingen',
  'search_advanced:settings:profile_fields:field' => 'Profielveld',
  'search_advanced:settings:user_profile_fields:show_on_form' => 'Toon op zoek formulier',
  'search_advanced:settings:user_profile_fields:info' => 'Sta gebruikers toe om de zoekopdracht te verfijnen op basis van profiel velden. Op dit moment worden alleen de tekst gebaseerde velden ondersteund (text, location, url. etc).',
  'search_advanced:forms:search:user:autocomplete_info' => 'Start met typen en selecteer uit de lijst',
  'search_advanced:settings:combine_search_results' => 'Combineer zoekresultaten',
  'search_advanced:settings:combine_search_results:info' => 'Dit zal een gecombineerde lijst van de meest recente content tonen op basis van een zoekopdracht. Er zal geen groepering meer zijn op de resultaten pagina. Filteren middels het menu is nog steeds mogelijk.',
  'search_advanced:settings:enable_multi_tag' => 'Multi tag search activeren',
  'search_advanced:settings:enable_multi_tag:info' => 'Indien er een komma gebruikt wordt in een zoek opdracht zal deze behandeld worden als een aparte query. De resultaten zullen worden gecombineerd. De komma werkt dan als een OF selectie. Dit is niet van toepassing op de autocomplete zoekfunctionaliteit.',
  'search_advanced:content:title' => 'Content',
  'search_advanced:content:last_updated' => 'Laatst geüpdate',
  'search_advanced:content:created' => 'Aangemaakt',
  'search_advanced:multisite:label' => 'Zoeken in',
  'search_advanced:multisite:current' => 'huidige site',
  'search_advanced:multisite:mine' => 'al mijn sites',
  'search_advanced:settings:user_profile_fields:use_autocomplete' => 'Zoek veld is een autocomplete',
);
add_translation("nl", $language);
