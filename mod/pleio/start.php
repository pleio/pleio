<?php
global $PLEIO_TASKS;
$PLEIO_TASKS = [];

require_once(dirname(__FILE__) . "/lib/background_tasks.php");
require_once(dirname(__FILE__) . "/lib/cron.php");

require_once(dirname(__FILE__) . "/../../vendor/autoload.php");
spl_autoload_register("pleio_autoloader");
function pleio_autoloader($class) {
    $filename = "classes/" . str_replace("\\", "/", $class) . ".php";
    if (file_exists(dirname(__FILE__) . "/" . $filename)) {
        include($filename);
    }
}

elgg_register_event_handler("init", "system", "pleio_init");

function pleio_init() {
    elgg_unregister_page_handler("login");
    elgg_register_page_handler("login", "pleio_page_handler");

    elgg_unregister_action("register");
    elgg_unregister_page_handler("register");
    elgg_register_page_handler("register", "pleio_register_page_handler");

    elgg_register_page_handler("invitation", "pleio_invitation_page_handler");

    elgg_unregister_action("logout");
    elgg_register_action("logout", dirname(__FILE__) . "/actions/logout.php", "public");

    elgg_unregister_action("avatar/crop");
    elgg_unregister_action("avatar/remove");
    elgg_register_action("avatar/remove", dirname(__FILE__) . "/actions/avatar/remove.php");

    elgg_unregister_action("avatar/upload");
    elgg_register_action("avatar/upload", dirname(__FILE__) . "/actions/avatar/upload.php");

    elgg_unregister_action("user/passwordreset");
    elgg_unregister_action("user/requestnewpassword");

    elgg_unregister_action("rijkshuisstijl/profile/changepassword");
    elgg_register_action("rijkshuisstijl/profile/changepassword", dirname(__FILE__) . "/actions/profile/changepassword.php");

    elgg_unregister_action("admin/user/resetpassword");
    elgg_unregister_action("admin/user/delete");
    elgg_register_action("admin/user/delete", dirname(__FILE__) . "/actions/admin/user/delete.php", "admin");

    elgg_unregister_action("admin/user/ban");
    elgg_register_action("admin/user/ban", dirname(__FILE__) . "/actions/admin/user/ban.php", "admin");

    elgg_unregister_action("admin/user/unban");
    elgg_register_action("admin/user/unban", dirname(__FILE__) . "/actions/admin/user/unban.php", "admin");

    elgg_register_action("admin/repair_hidden_users", dirname(__FILE__) . "/actions/admin/repair_hidden_users.php", "admin");
    elgg_register_action("admin/repair_hidden_groups", dirname(__FILE__) . "/actions/admin/repair_hidden_groups.php", "admin");
    elgg_register_action("admin/repair_broken_plugins", dirname(__FILE__) . "/actions/admin/repair_broken_plugins.php", "admin");
    elgg_register_action("admin/repair_hidden_users", dirname(__FILE__) . "/actions/admin/repair_hidden_users.php", "admin");
    elgg_register_action("admin/repair_incomplete_entities", dirname(__FILE__) . "/actions/admin/repair_incomplete_entities.php", "admin");

    elgg_register_action("admin/upgrade_database_static_pages", dirname(__FILE__) . "/actions/admin/upgrade_database_static_pages.php", "admin");
    elgg_register_action("admin/upgrade_database_groups", dirname(__FILE__) . "/actions/admin/upgrade_database_groups.php", "admin");
    elgg_register_action("admin/upgrade_database_wiki", dirname(__FILE__) . "/actions/admin/upgrade_database_wiki.php", "admin");
    elgg_register_action("admin/upgrade_database_cms_pages", dirname(__FILE__) . "/actions/admin/upgrade_database_cms_pages.php", "admin");
    elgg_register_action("admin/upgrade_database_discussion", dirname(__FILE__) . "/actions/admin/upgrade_database_discussion.php", "admin");
    elgg_register_action("admin/upgrade_database_videolists", dirname(__FILE__) . "/actions/admin/upgrade_database_videolists.php", "admin");
    elgg_register_action("admin/upgrade_database_photoalbums", dirname(__FILE__) . "/actions/admin/upgrade_database_photoalbums.php", "admin");

    elgg_register_action("admin/replace_links/replace", dirname(__FILE__) . "/actions/admin/replace_links/replace.php", "admin");

    elgg_register_action("admin/haarlem_accordion", dirname(__FILE__) . "/actions/admin/haarlem_accordion.php", "admin");
    elgg_register_action("admin/haarlem_wiki_div", dirname(__FILE__) . "/actions/admin/haarlem_wiki_div.php", "admin");

    elgg_unregister_menu_item("page", "users:unvalidated");
    elgg_unregister_menu_item("page", "users:add");
    elgg_unregister_action("useradd");

    elgg_register_plugin_hook_handler("register", "menu:user_hover", "pleio_user_hover_menu");

    elgg_unregister_plugin_hook_handler("usersettings:save", "user", "users_settings_save");
    elgg_register_plugin_hook_handler("usersettings:save", "user", "pleio_users_settings_save");

    elgg_unregister_action("admin/site/update_advanced");
    elgg_register_action("admin/site/update_advanced", dirname(__FILE__) . "/actions/admin/site/update_advanced.php", "admin");

    elgg_unregister_action("profile_manager/export");
    elgg_register_action("profile_manager/export", dirname(__FILE__) . "/actions/profile_manager/export.php", "admin");

    elgg_register_page_handler("access_requested", "pleio_access_requested_page_handler");
    elgg_register_page_handler("validate_access", "pleio_access_validate_access_page_handler");
    elgg_register_page_handler("invite_autocomplete", "pleio_invite_autocomplete_page_handler");

    elgg_register_page_handler("robots.txt", "pleio_robots_page_handler");
    elgg_register_page_handler("sitemap.xml", "pleio_sitemap_page_handler");

    elgg_register_action("pleio/request_access", dirname(__FILE__) . "/actions/request_access.php", "public");
    elgg_register_action("admin/pleio/process_access", dirname(__FILE__) . "/actions/admin/process_access.php", "admin");

    elgg_register_plugin_hook_handler("public_pages", "walled_garden", "pleio_public_pages_handler");
    elgg_register_plugin_hook_handler("action", "admin/site/update_basic", "pleio_admin_update_basic_handler");
    elgg_register_plugin_hook_handler("entity:icon:url", "user", "pleio_user_icon_url_handler");

    elgg_register_plugin_hook_handler("route", "notifications", "pleio_route_notifications_hook");

    elgg_register_plugin_hook_handler("cron", "daily", "pleio_ban_users_that_bounce");
    elgg_register_plugin_hook_handler("cron", "daily", "pleio_ban_deleted_accounts");
    elgg_register_plugin_hook_handler("cron", "weekly", "pleio_maintain_system_log");

    elgg_register_admin_menu_item("administer", "all", "users");
    if (elgg_in_context("admin")) {
        elgg_register_plugin_hook_handler("register", "menu:entity", "pleio_template_user_setup_menu", 502);
    }

    elgg_register_admin_menu_item("administer", "repair", "administer_utilities");
    elgg_register_admin_menu_item("administer", "replace_links", "administer_utilities");
    elgg_register_admin_menu_item("administer", "cache", "administer_utilities");
    elgg_register_admin_menu_item("administer", "access_requests", "users");
    elgg_register_admin_menu_item("administer", "delete_requests", "users");
    elgg_register_admin_menu_item("administer", "membership", "users", 120);
    elgg_register_admin_menu_item("administer", "invite", "users", 130);
    elgg_register_admin_menu_item("administer", "invitations", "users", 140);
    elgg_register_admin_menu_item("administer", "import", "users");
    elgg_register_admin_menu_item("administer", "roles", "users");

    elgg_register_action("admin/user/import_step1", dirname(__FILE__) . "/actions/admin/user/import_step1.php", "admin");
    elgg_register_action("admin/user/import_step2", dirname(__FILE__) . "/actions/admin/user/import_step2.php", "admin");
    elgg_register_action("admin/users/invite", dirname(__FILE__) . "/actions/admin/users/invite.php", "admin");
    elgg_register_action("admin/users/invite_csv", dirname(__FILE__) . "/actions/admin/users/invite_csv.php", "admin");
    elgg_register_action("admin/users/revoke_invite", dirname(__FILE__) . "/actions/admin/users/revoke_invite.php", "admin");

	// admin membership request widget
	elgg_register_widget_type("membership_requests", elgg_echo("admin:users:membership"), elgg_echo("pleio:title:membership"), "admin");

    elgg_extend_view("css/elgg", "pleio/css/site");
    elgg_extend_view("css/admin", "pleio/css/admin");

    elgg_extend_view("page/elements/head", "page/elements/topbar/fix");
    elgg_extend_view("page/elements/foot", "page/elements/stats");

    if (function_exists('pleio_register_console_handler')) {
        pleio_register_console_handler('runscript', 'Run pleio console script', 'pleio_runscript');

        pleio_register_console_handler('schedule_task', 'TASK_NAME JSON_ARGUMENTS: Schedule a specific task', 'pleio_cli_schedule_task');
        pleio_register_console_handler('execute_task', 'TASK_NAME JSON_ARGUMENTS: Execute a specific task', 'pleio_cli_execute_task');
    }

    if (elgg_is_active_plugin("newsletter")) {
        elgg_register_action("newsletter/admin/newsletter_blacklist_exporter", dirname(__FILE__) . "/actions/admin/newsletter/blacklist_exporter.php", "admin");
        elgg_register_action("newsletter/admin/newsletter_exporter", dirname(__FILE__) . "/actions/admin/newsletter/exporter.php", "admin");
    }
}

function pleio_page_handler($page) {
    include(dirname(__FILE__) . "/pages/login.php");
    return true;
}

function pleio_access_requested_page_handler($page) {
    $body = elgg_view_layout("walled_garden", [
        "content" => elgg_view("pleio/access_requested", [
            "resourceOwner" => $_SESSION["pleio_resource_owner"]
        ]),
        "class" => "elgg-walledgarden-double",
        "id" => "elgg-walledgarden-login"
    ]);

    echo elgg_view_page(elgg_echo("pleio:access_requested"), $body, "walled_garden");
    return true;
}

function pleio_access_validate_access_page_handler($page) {
    include(dirname(__FILE__) . "/pages/validate_access.php");
    return true;
}

function pleio_invite_autocomplete_page_handler($page) {
    include(dirname(__FILE__) . "/procedures/invite_autocomplete.php");
    return true;
}

function pleio_register_page_handler($page) {
    forward("/login?method=register");

    return true;
}

function pleio_invitation_page_handler($page) {
    include(dirname(__FILE__) . "/pages/invitation.php");
    return true;
}

function pleio_robots_page_handler($page) {
    include(dirname(__FILE__) . "/pages/robots.php");
    return true;
}

function pleio_sitemap_page_handler($page) {
    include(dirname(__FILE__) . "/pages/sitemap.php");
    return true;
}

function pleio_admin_update_basic_handler($hook, $type, $value, $params) {
    $site = elgg_get_site_entity();

    $site_permission = get_input("site_permission");
    if ($site_permission) {
        set_config("site_permission", $site_permission, $site->guid);
    }
}

function pleio_users_settings_save($hook, $type, $value, $params) {
    $user = elgg_get_logged_in_user_entity();
    if (!$user) {
        return;
    }

    $name = get_input("name");
    if ($user->name !== $name) {
        pleio_user_settings_change_name($name);
    }

    $email = get_input("email");
    if ($user->email !== $email) {
        pleio_user_settings_change_email($email);
    }
}

function pleio_user_settings_change_name($name) {
    $user = elgg_get_logged_in_user_entity();

    $profile_handler = new ModPleio\ProfileHandler($user);

    try {
        $body = $profile_handler->changeName($name)->getBody();
        $response = json_decode($body);
    } catch (Exception $e) {
        throw new Exception("could_not_save");
    }

    if ($response->success) {
        $user->name = $name;
        $user->save();
        system_message(elgg_echo("pleio:name:save:success"));
    } else {
        register_error(elgg_echo("pleio:name:save:{$response->message}"));
    }

}

function pleio_user_settings_change_email($email) {
    $user = elgg_get_logged_in_user_entity();

	if (!is_email_address($email)) {
		register_error(elgg_echo('email:save:fail'));
		return false;
    }

    $profile_handler = new ModPleio\ProfileHandler($user);

    try {
        $body = $profile_handler->changeEmail($email)->getBody();
        $response = json_decode($body);
    } catch (Exception $e) {
        throw new Exception("could_not_save");
    }

    if ($response->success) {
        system_message(elgg_echo("pleio:email:save:success"));
    } else {
        register_error(elgg_echo("pleio:email:save:{$response->message}"));
    }
}

function pleio_public_pages_handler($hook, $type, $value, $params) {
    $value[] = "logout";
    $value[] = "action/logout";
    $value[] = "action/pleio/request_access";
    $value[] = "validate_access";
    $value[] = "access_requested";
    $value[] = "invitation";
    return $value;
}

function pleio_route_notifications_hook($hook, $entity_type, $returnvalue, $params) {
    if (empty($returnvalue) || !is_array($returnvalue)) {
        return $returnvalue;
    }

    gatekeeper();

    $page = elgg_extract("segments", $returnvalue);
    $username = "";
    $segment = "";
    switch ($page[0]) {
        case "group":
            $segment = $page[0];
        case "personal":
            if (isset($page[1])) {
                $username = $page[1];
            } else {
                $username = elgg_get_logged_in_user_entity()->username;
            }

            forward("notifications/" . $username . "#" . $segment);
            break;
        default:
            set_input("username", $page[0]);
            include(dirname(__FILE__) . "/pages/notifications/owner.php");
            return false;
            break;
    }
}

function pleio_user_icon_url_handler($hook, $type, $value, $params) {
    global $CONFIG;

    $entity = $params["entity"];
    $size = $params["size"];

    if (!$entity) {
        return $value;
    }

    if (!in_array($size, ["large", "medium", "small", "tiny", "master", "topbar"])) {
        $size = "medium";
    }

    $dbprefix = elgg_get_config("dbprefix");
    $guid = (int) $entity->guid;

    $result = get_data_row("SELECT pleio_guid FROM {$dbprefix}users_entity WHERE guid = $guid");
    if ($result) {
        $pleio_guid = $result->pleio_guid;
    } else {
        $pleio_guid = 0;
    }

    $url = $CONFIG->pleio->url . "mod/profile/icondirect.php?guid={$pleio_guid}&size={$size}";

    if ($entity->icontime) {
        $url .= "&lastcache={$entity->icontime}";
    } elseif ($entity->last_login) {
        $url .= "&lastcache={$entity->last_login}";
    }

    return $url;
}

function pleio_user_hover_menu($hook, $type, $items, $params) {
    foreach ($items as $key => $item) {
        if (in_array($item->getName(), ["resetpassword"])) {
            unset($items[$key]);
        }
    }

    return $items;
}

function pleio_is_valid_returnto($url) {
    $site_url = parse_url(elgg_get_site_url());
    $returnto_url = parse_url($url);

    if (!$site_url || !$returnto_url) {
        return false;
    }

    // check returnto is relative or absolute
    if (!$returnto_url["host"] && $returnto_url["path"]) {
        return true;
    } else {
        if ($site_url["scheme"] !== $returnto_url["scheme"]) {
            return false;
        }

        if ($site_url["host"] !== $returnto_url["host"]) {
            return false;
        }
    }

    return true;
}

function get_user_by_pleio_guid_or_email($guid, $email) {
    $guid = (int) $guid;
    if (!$guid) {
        return false;
    }

    $email = sanitize_string($email);
    if (!$email) {
        return false;
    }

    $dbprefix = elgg_get_config("dbprefix");
    $result = get_data_row("SELECT guid FROM {$dbprefix}users_entity WHERE pleio_guid = {$guid}");
    if ($result) {
        return get_entity($result->guid);
    }

    $result = get_data_row("SELECT guid FROM {$dbprefix}users_entity WHERE email = '{$email}'");
    if ($result) {
        update_data("UPDATE {$dbprefix}users_entity SET pleio_guid = {$guid} WHERE guid={$result->guid}");
        return get_entity($result->guid);
    }

    return false;
}

function get_user_by_pleio_guid($guid) {
    $guid = (int) $guid;
    if (!$guid) {
        return false;
    }

    $dbprefix = elgg_get_config("dbprefix");
    $result = get_data_row("SELECT guid FROM {$dbprefix}users_entity WHERE pleio_guid = {$guid}");
    if ($result) {
        return get_entity($result->guid);
    }

    return false;
}


function pleio_get_required_profile_fields() {
    if (!elgg_is_active_plugin("profile_manager")) {
        return [];
    }

    $result = profile_manager_get_categorized_fields(null, true, true, true, $profile_type_guid);

    if (empty($result["categories"])) {
        return [];
    }

    $return = [];
    foreach ($result["categories"] as $category_guid => $category) {
        foreach ($result["fields"][$category_guid] as $field) {
            if ($field->show_on_register == "yes" && $field->mandatory == "yes") {
                $return[] = $field;
            }
        }
    }

    return $return;
}

function pleio_get_domain_from_email($email) {
    return substr(strrchr($email, "@"), 1);
}

function pleio_domain_in_whitelist($domain) {
    $plugin_setting = elgg_get_plugin_setting("domain_whitelist", "pleio");
    $domains = $plugin_setting ? explode(",", $plugin_setting) : [];

    $domains = array_map(function($domain) { return trim($domain); }, $domains);

    if (in_array($domain, $domains)) {
        return true;
    }

    return false;
}

function pleio_schedule_in_background($function, $param) {
    $input = base64_encode(json_encode([
        "http_host" => $_SERVER["HTTP_HOST"],
        "https" => $_SERVER["HTTPS"],
        "env" => [
            "DB_USER" => getenv("DB_USER"),
            "DB_PASS" => getenv("DB_PASS"),
            "DB_NAME" => getenv("DB_NAME"),
            "DB_HOST" => getenv("DB_HOST"),
            "DB_PREFIX" => getenv("DB_PREFIX"),
            "DATAROOT" => getenv("DATAROOT"),
            "PLEIO_ENV" => getenv("PLEIO_ENV"),
            "SMTP_DOMAIN" => getenv("SMTP_DOMAIN"),
            "BLOCK_EMAIL" => getenv("BLOCK_EMAIL"),
            "MEMCACHE_ENABLED" => getenv("MEMCACHE_ENABLED"),
            "MEMCACHE_PREFIX" => getenv("MEMCACHE_PREFIX"),
            "MEMCACHE_SERVER_1" => getenv("MEMCACHE_SERVER_1"),
            "ELASTIC_INDEX" => getenv("ELASTIC_INDEX"),
            "ELASTIC_SERVER_1" => getenv("ELASTIC_SERVER_1")
        ],
        "function" => $function,
        "param" => $param
    ]));

    $script_location = dirname(__FILE__) . "/procedures/run_function.php";

    if (file_exists("/usr/local/bin/php")) {
        $binary = "/usr/local/bin/php";
    } else {
        $binary = "php";
    }

    if (PHP_OS === "WINNT") {
        pclose(popen("start /B {$binary} {$script_location} {$input}", "r"));
    } else {
        exec("{$binary} {$script_location} {$input} > /tmp/pleio-background.log &");
    }
}

function pleio_runscript($argv) {
    $script = $argv[0];
    $fullPath = dirname(__FILE__) ."/scripts/${script}";

    if ($script && file_exists($fullPath)) {
        echo "Run script: ${script}\n";
        require_once($fullPath);
    } else {
        echo "Script: ${script} not found.\n";
    }
}

function pleio_register_task($task_name) {
    global $PLEIO_TASKS;

    if (in_array($task_name, $PLEIO_TASKS)) {
        return;
    }

    if (!function_exists($task_name)) {
        throw new Exception("Cannot register a task that does not resolve to a callable function.");
    }

    $PLEIO_TASKS[] = $task_name;
}

function pleio_cli_schedule_task($argv) {
    $task_name = isset($argv[0]) ? $argv[0] : null;
    if (!$task_name) {
        echo "Task name is required.";
        return;
    }

    $arguments = isset($argv[1]) ? json_decode($argv[1]) : [];
    if (json_last_error()) {
        echo "Cannot parse JSON arguments.";
        return;
    }

    return pleio_schedule_task($task_name, $arguments);
}

function pleio_cli_execute_task($argv) {
    $task_name = isset($argv[0]) ? $argv[0] : null;
    if (!$task_name) {
        echo "Task name is required.";
        return;
    }

    $arguments = isset($argv[1]) ? json_decode($argv[1]) : [];
    if (json_last_error()) {
        echo "Cannot parse JSON arguments.";
        return;
    }

    return pleio_execute_task($task_name, $arguments);
}

function pleio_schedule_task($task_name, $arguments=[]) {
    global $CONFIG;

    if (isset($CONFIG->tasks_always_eager) && $CONFIG->tasks_always_eager) {
        $result = pleio_execute_task($task_name, $arguments);
        return;
    }

    if (!isset($CONFIG->celery)) {
        $CONFIG->celery = new ModPleio\Celery($CONFIG->amqp_host, $CONFIG->amqp_port, $CONFIG->amqp_user, $CONFIG->amqp_pass, $CONFIG->amqp_vhost);
    }

    $tenant_name = $CONFIG->dbname;
    $task_id = $CONFIG->celery->publishTask("elgg.execute_task", [$tenant_name, $task_name, $arguments]);

    if (PHP_SAPI === "cli") {
        echo "Scheduled task {$task_id} in queue.\n";
    }
}

function pleio_execute_task($task_name, $arguments=[]) {
    global $CONFIG;
    global $PLEIO_TASKS;

    if (!in_array($task_name, $PLEIO_TASKS)) {
        if (PHP_SAPI === "cli") {
            echo ("Ignoring task ${taskname}. This task is not registered.");
        }
        return;
    }

    $result = call_user_func_array($task_name, $arguments);
    if ($result) {
        if (PHP_SAPI === "cli") {
            echo "Executed {$task_name} succesfully with result {$result}.";
        }
        return;
    }

    if (PHP_SAPI === "cli") {
        echo "Executed {$task_name} succesfully.";
    }
}
