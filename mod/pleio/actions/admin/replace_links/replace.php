<?php
$replace = get_input("replace", "");
$with = get_input("with", "");
$test = get_input("test_run", false);

set_time_limit(0);

if (substr($replace, 0, 7) === "http://" || substr($replace, 0, 8) === "https://" ) {
    $ia = elgg_set_ignore_access(true);
    $count = 0;

    $options = [
        "type" => "object",
        "subtypes" => ["static", "static_top", "page", "page_top", "wiki"],
        "limit" => false
    ];

    $total = 0;

    foreach (elgg_get_entities($options) as $entity) {

        $search = "href=\"${replace}";
        $new_string = "href=\"${with}";

        $new_description = str_replace($search, $new_string, $entity->description, $count);

        if ($count > 0 && !$test) {
            $entity->description = $new_description;
            $entity->save();
        }

        $total += $count;
    }

    if ($test) {
        $message = elgg_echo("admin:administer_utilities:replace_links:message_test", [$total, $replace, $with]);
    } else {
        $message = elgg_echo("admin:administer_utilities:replace_links:message", [$total, $replace, $with]);
    }

    elgg_set_ignore_access($ia);
    system_message($message);
} else {
    system_message(elgg_echo("admin:administer_utilities:replace_links:input_error"));
}

forward(REFERER);
