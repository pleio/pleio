<?php
if(elgg_is_active_plugin("pleio_template")) {

    $dbprefix = elgg_get_config("dbprefix");

    $ia = elgg_set_ignore_access(true);

    $total = 0;

    $rows = get_data("SELECT * FROM elgg_objects_entity WHERE description LIKE '%theme-haarlem-intranet-accordion-header%'");

    foreach ($rows as $row) {
        $count = 0;

        // check description for accordion
        $description = preg_replace('/<h3 .*?class="(.*?theme-haarlem-intranet-accordion-header.*?)">(.*?)<\/h3>/','<h2 class="$1">$2</h2>', $row->description, -1, $count);
        $description = preg_replace('/<\/div>(?<!<br \/>)([\s]*?)<h2/','</div><div><br /></div><h2', $description, -1, $count);

        if ($description != $row->description) {
            $entity = get_entity($row->guid);
            $entity->description = $description;
            $entity->save();
        }

        $total+=$count;
    }

    elgg_set_ignore_access($ia);

    system_message("${total} accordion headings geconverteerd");

    forward(REFERER);
}
