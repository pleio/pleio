<?php
$dbprefix = elgg_get_config("dbprefix");

$i = 0;

$result = get_data("SELECT e.guid FROM {$dbprefix}entities e WHERE e.type = 'user' AND e.enabled = 'no'");

access_show_hidden_entities(true);

if ($result) {
    foreach ($result as $user) {
        $user = get_entity($user->guid);

        if (!$user) {
            continue;
        }

        $user->enable();

        $i++;
    }
}

system_message("Probleem opgelost voor {$i} gebruiker(s).");

forward(REFERER);