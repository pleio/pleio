<?php
if(elgg_is_active_plugin("pleio_template")) {

    $dbprefix = elgg_get_config("dbprefix");
    $ia = elgg_set_ignore_access(true);

    # repair featured groups
    $group_result = get_data("SELECT guid FROM elgg_groups_entity WHERE guid NOT IN (SELECT guid FROM elgg_metadata md INNER
                              JOIN elgg_metastrings ms ON md.name_id = ms.id INNER JOIN elgg_groups_entity ge ON md.entity_guid = ge.guid
                              WHERE string = 'isFeatured');");
    if ($group_result) {
        foreach ($group_result as $row) {
            $group = get_entity($row->guid);

            if (isset($group->featured_group)) {
                if ($group->featured_group == 'yes') {
                    $group->isFeatured = '1';
                } else {
                    $group->isFeatured = '0';
                }
            } else {
                $group->isFeatured = '0';
            }
            $group->save();
        }
    }

    # repair auto_join groups
    $group_auto_join_result = get_data("SELECT * FROM elgg_private_settings WHERE name = 'auto_join'");

    $groups_auto_join_count = 0;
    if ($group_auto_join_result && !empty($group_auto_join_result) ) {
        $groups_auto_join = explode(",", $group_auto_join_result[0]->value);
        foreach ($groups_auto_join as $group_guid) {
            $group = get_entity($group_guid);
            if ($group instanceof \ElggGroup) {
                $group->isAutoMembershipEnabled = true;
                $group->save();
            }
        }
        remove_private_setting($group_auto_join_result[0]->entity_guid, 'auto_join');
    } else {
        $groups_auto_join_count = 0;
    }

    # repair group plugin configuration
    function migrate_plugins_config($group) {
        if ($group->blog_enable == 'yes') {
            create_metadata($group->guid, 'plugins', 'blog', 'text', $group->owner_guid, 2, TRUE);
            $group->deleteMetadata('blog_enable');
        }
        if ($group->event_manager_enable == 'yes') {
            create_metadata($group->guid, 'plugins', 'events', 'text', $group->owner_guid, 2, TRUE);
            $group->deleteMetadata('event_manager_enable');
        }
        if ($group->forum_enable == 'yes') {
            create_metadata($group->guid, 'plugins', 'discussion', 'text', $group->owner_guid, 2, TRUE);
            $group->deleteMetadata('forum_enable');
        }
        if ($group->questions_enable == 'yes') {
            create_metadata($group->guid, 'plugins', 'questions', 'text', $group->owner_guid, 2, TRUE);
            $group->deleteMetadata('questions_enable');
        }
        if ($group->file_enable == 'yes') {
            create_metadata($group->guid, 'plugins', 'files', 'text', $group->owner_guid, 2, TRUE);
            $group->deleteMetadata('file_enable');
        }
        if (($group->pages_enable == 'yes' || $group->static_enable == 'yes')) {
            create_metadata($group->guid, 'plugins', 'wiki', 'text', $group->owner_guid, 2, TRUE);
            $group->deleteMetadata('pages_enable');
            $group->deleteMetadata('static_enable');
        }
        if ($group->tasks_enable == 'yes') {
            create_metadata($group->guid, 'plugins', 'tasks', 'text', $group->owner_guid, 2, TRUE);
            error_log($group->deleteMetadata('tasks_enable'));
        }
    }

    function make_groups_plugins_unique($group) {
        if (is_array($group->plugins)) {
            if (count($group->plugins) != count(array_unique($group->plugins))) {
                $group->plugins = array_unique($group->plugins);
                $group->save();
            }
        }
    }

    function migrate_interests_to_tags($group) {
        $tags = $group->tags ? (is_array($group->tags) ? $group->tags : [$group->tags]) :[];
        $interests = $group->interests ? (is_array($group->interests) ? $group->interests : [$group->interests]) :[];

        if (empty($interests)) {
            return;
        }

        $group->tags = array_unique(array_merge($interests, $tags));
        $group->deleteMetadata('interests');
        $group->save();
    }


    $options = [
        "type" => "group",
        "limit" => false
    ];
    $groups = elgg_get_entities($options);

    foreach ($groups as $group) {
        migrate_plugins_config($group);
        make_groups_plugins_unique($group);
        migrate_interests_to_tags($group);
    }

    elgg_set_ignore_access($ia);

    system_message("Groepen geconverteerd voor nieuwe template");

    forward(REFERER);
}
