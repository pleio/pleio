<?php
$dbprefix = elgg_get_config("dbprefix");

$i = 0;

$result = get_data("SELECT e.guid FROM elgg_entities e LEFT JOIN elgg_objects_entity o USING(guid) WHERE e.type = 'object' AND o.guid IS NULL");

access_show_hidden_entities(true);

if ($result) {
    foreach ($result as $entity) {
        delete_data("DELETE from elgg_entities where guid={$entity->guid}");

        $i++;
    }
}

system_message("Probleem opgelost voor {$i} entities.");

forward(REFERER);

