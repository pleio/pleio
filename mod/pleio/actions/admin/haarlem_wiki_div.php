<?php
if(elgg_is_active_plugin("pleio_template")) {

    $dbprefix = elgg_get_config("dbprefix");
    $ia = elgg_set_ignore_access(true);

    $options = [
        "type" => "object",
        "subtypes" => ["wiki", ],
        "limit" => false
    ];

    $total = 0;

    foreach (elgg_get_entities($options) as $wiki) {
        $count = 0;

        $description = preg_replace('/<p>(.*?)<\/p>/','<div>$1</div>', $wiki->description, -1, $count);
        $description = preg_replace('/(?<!<br \/>)<\/div>([\s]*?)<h2/','</div><div><br /></div><h2', $description, -1, $count);

        if ($count > 0) {
            $wiki->description = $description;
            $wiki->save();
        }
        $total += $count;
    }

    elgg_set_ignore_access($ia);

    system_message("${total} paragraphs in wiki geconverteerd naar div");

    forward(REFERER);
}
