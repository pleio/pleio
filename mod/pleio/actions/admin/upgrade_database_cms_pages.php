<?php

function create_column($row, $position, $width) {
    if (!$position) {
        $position = 0;
    }
    $column = new \ElggObject();
    $column->subtype = "column";
    $column->parent_guid = $row->guid;
    $column->position = $position;
    $column->container_guid = $row->container_guid;
    $column->access_id = $row->access_id;
    $column->width = $width;
    $col = $column->save();
    return $col;
}

function migrate_widget($widget, $row, $column) {
    $widget->container_guid = $row->container_guid;
    $widget->parent_guid = $column;
    $widget->position = 0;

    if ($row->layout == "text" && $widget->widget_type == "text") {
        $settings = $widget->getPrivateSetting("settings") ? json_decode($widget->getPrivateSetting("settings")) : [];
        array_push($settings, array("key" => "centered", "value" => "1"));
        $widget->setPrivateSetting("settings", json_encode($settings));
    }

    $widget->save();
}

function get_width($layout, $position) {
    if (!$position) {
        $position = 0;
    }
    if (in_array($layout, array("full", "text", "12"))){
        return array(12);
    } else if ($layout == "4/4/4") {
        return array(4);
    } else if ($layout == "6/6") {
        return array(6);
    } else if ($layout == "4/8") {
        if ($position == 0) {
            return array(4);
        } else if ($position == 1) {
            return array(8);
        }
    } else if ($layout == "8/4") {
        if ($position == 0) {
            return array(8);
        } else if ($position == 1) {
            return array(4);
        }
    }
}

function migrate_widgets($widgets, $row) {
    $claimed_positions = array();
    foreach ($widgets as $widget) {
        $widget_position = $widget->position;
        if (!in_array($widget_position, $claimed_positions)) {
            $width = get_width($row->layout, $widget->position);
            $column = create_column($row, $widget->position, $width);
            migrate_widget($widget, $row, $column);
        } else {
            $widget->delete();
        }
        $claimed_positions[] = $widget_position;
    }
}

function migrate_rows($rows) {
    $position = 0;
    foreach ($rows as $row) {
        if (!in_array($row->layout, array("full", "text", "12", "6/6", "8/4", "4/8", "4/4/4"))){
            register_error('Row ' . $row->guid . ' has unknown layout: ' . $row->layout);
            continue;
        }
        if ($row->order) {
            $row->position = $row->order;
        } else {
            $row->position = $position;
        }
        $position += 1;

        if ($row->layout == 'full') {
            $row->is_full_width = true;
        }

        $row->parent_guid = $row->container_guid;
        
        $options = array(
            "container_guid" => (int) $row->guid,
            "type" => "object",
            "subtype" => "page_widget",
            "order_by" => "guid DESC",
            "limit" => false
        );
        migrate_widgets(elgg_get_entities($options), $row);
        $row->save();
        $row->deleteMetadata("layout");
        _elgg_invalidate_memcache_for_entity($row->guid);
    }
}

if(elgg_is_active_plugin("pleio_template")) {

    $dbprefix = elgg_get_config("dbprefix");
    $ia = elgg_set_ignore_access(true);

    # check for cms 2.0 errors
    $cms2_data = get_data("SELECT guid FROM elgg_entities ee
                            LEFT JOIN elgg_entity_subtypes es ON ee.subtype = es.id
                            LEFT JOIN elgg_metadata md ON ee.guid = md.entity_guid
                            LEFT JOIN elgg_metastrings ms ON md.value_id = ms.id
                            LEFT JOIN elgg_metastrings ms2 ON md.name_id = ms2.id
                            WHERE ms.string = 'campagne' AND ms2.string = 'pageType' AND es.subtype = 'page'");

    foreach ($cms2_data as $page) {
        $options = [
            "type" => "object",
            "subtypes" => ["row", ],
            "metadata_name" => "layout",
            "limit" => false,
            "order_by" => "guid ASC",
            "container_guid" => $page->guid,
        ];
        migrate_rows(elgg_get_entities_from_metadata($options));
    }


    elgg_set_ignore_access($ia);

    system_message("Rijen geconverteerd voor nieuwe cms");

    forward(REFERER);
}
