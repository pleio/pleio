<?php
if(elgg_is_active_plugin("pleio_template")) {

    function inGroup($entity, $cnt=0) {
        $cnt++;
        if ($cnt > 10) {
            return NULL;
        }

        $parent = $entity->getContainerEntity();
        if (!$parent) {
            return NULL;
        } elseif ($parent instanceof ElggGroup) {
            return $parent;
        } elseif($parent instanceof ElggSite) {
            return false;
        } else {
            return inGroup($parent, $cnt);
        }
    }

    $dbprefix = elgg_get_config("dbprefix");
    $ia = elgg_set_ignore_access(true);

    $site = elgg_get_site_entity();
    $site_guid = (int) $site->guid;

    $subtype_wiki_id = add_subtype("object", "wiki");
    $subtype_page_id = add_subtype("object", "page");

    $options = [
        "type" => "object",
        "subtypes" => ["static", "static_top"],
        "limit" => false
    ];

    foreach (elgg_get_entities($options) as $page) {
        $group = inGroup($page);

        if ($group === false) { // on site level: convert to page
            update_data("UPDATE {$dbprefix}entities SET subtype = {$subtype_page_id} WHERE guid = {$page->guid}");
            _elgg_invalidate_memcache_for_entity($page->guid);

        } else if ($group === NULL) { // lost: put on site level and convert to page

            update_data("UPDATE {$dbprefix}entities SET subtype = {$subtype_page_id}, container_guid = {$site_guid} WHERE guid = {$page->guid}");
            _elgg_invalidate_memcache_for_entity($page->guid);

        } else { // in group: convert to wiki
            $container_guid = $page->container_guid;

            update_data("UPDATE {$dbprefix}entities SET subtype = {$subtype_wiki_id}, container_guid = {$container_guid} WHERE guid = {$page->guid}");
            _elgg_invalidate_memcache_for_entity($page->guid);
        }
    }

    elgg_set_ignore_access($ia);

    system_message("Statische pagina's geconverteerd voor nieuwe template");

    forward(REFERER);
}
