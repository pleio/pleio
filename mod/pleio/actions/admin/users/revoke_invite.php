<?php
$email = get_input("email");

ModPleio\InviteCodeHandler::revokeInvite($email);

system_message(elgg_echo("pleio:revoked_invite"));