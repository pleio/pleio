<?php

	/**
	 * Invite users by CSV upload (this is step 2)
	 */

	elgg_make_sticky_form("pleio_invite_csv");

	$forward_url = REFERER;

	if(!empty($_SESSION["pleio_csv"])){
		$name_column = (int) get_input("name");
		$email_column = (int) get_input("email");
		$message = get_input("message");

		if($email_column >= 0){
			$site = elgg_get_site_entity();
			$loggedin_user = elgg_get_logged_in_user_entity();
			$secret = get_site_secret();

			$invited_count = 0;
			$unbanned_count = 0;
			$on_site_count = 0;

			$sample_data = elgg_extract("column", $_SESSION["pleio_csv"]);
			$tmp_location = elgg_extract("location", $_SESSION["pleio_csv"]);

			if($email_column < count($sample_data)){
				if($fh = fopen($tmp_location, "r")){
					$invited = 0;

					// show hidden (unvalidated) users
					$hidden = access_get_show_hidden_status();
					access_show_hidden_entities(true);

					while(($data = fgetcsv($fh, 0, ";")) !== false){
						$email = trim($data[$email_column]);

						if(!empty($email) && is_email_address($email)){
							if (!is_email_address($email)) {
								continue;
							}

							$users = get_user_by_email($email);
							if ($users) {
								$user = $users[0];
							}

							if ($user) {
								if ($user->isBanned()) {
									$user->unban();
									add_entity_relationship($user->guid, "member_of_site", $site->guid);
									$unbanned_count++;
									continue;
								} else {
									$on_site_count++;
									continue;
								}
							}

							$code = ModPleio\InviteCodeHandler::generateCode($email);
							$join_link = $site->url . "invitation?invitecode=" . $code;

							elgg_send_email(
								ModPleio\Helpers::getSiteEmail(),
								$email,
								html_entity_decode(elgg_echo("pleio:invitation:subject", [$site->name])),
								elgg_echo("pleio:invitation:message", [
									$loggedin_user->name,
									html_entity_decode($site->name),
									$join_link,
									$message
								])
							);
							$invited_count++;
						}
					}

					if($invited_count > 0){
						unlink($tmp_location);
						elgg_clear_sticky_form("pleio_invite_csv");
						unset($_SESSION["pleio_csv"]);

						$forward_url = "admin/users/invite";

						system_message(elgg_echo("pleio:action:invite:csv:success", array($invited_count)));
					} else {
						register_error(elgg_echo("pleio:action:invite:csv:error:users"));
					}
				} else {
					register_error(elgg_echo("pleio:action:invite:csv:error:csv"));
				}
			} else {
				register_error(elgg_echo("pleio:action:invite:csv:error:email_column:invalid"));
			}
		} else {
			register_error(elgg_echo("pleio:action:invite:csv:error:email_column"));
		}
	} else {
		register_error(elgg_echo("pleio:action:invite:csv:error:content"));
	}


	forward($forward_url);


