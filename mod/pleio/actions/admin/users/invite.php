<?php
elgg_make_sticky_form("pleio_invite");

$user_guids = get_input("user_guids");
$emails = get_input("user_guids_email", []);
$csv = get_uploaded_file("csv");
$message = get_input("message");

$site = elgg_get_site_entity();
$loggedin_user = elgg_get_logged_in_user_entity();
$secret = get_site_secret();

$invited_count = 0;
$unbanned_count = 0;
$on_site_count = 0;


// handle csv
if(!empty($csv)){

    $directory = elgg_get_config("dataroot") . 'tmp/';
    if(!file_exists($directory)) {
        mkdir($directory, 0755, true);
    }

    $tmp_location = $_FILES["csv"]["tmp_name"];
    if($fh = fopen($tmp_location, "r")){
        if(($data = fgetcsv($fh, 0, ";")) !== false){
            $cleanup_stick_form = false;

            $location =  $directory . get_config("site_guid") . '.csv';
            move_uploaded_file($tmp_location, $location);

            $_SESSION["pleio_csv"] = array(
                "column" => $data,
                "location" => $location
            );

            forward("admin/users/invite_csv");
        }
    }
}

foreach ($emails as $email) {
    if (!is_email_address($email)) {
        continue;
    }

    $users = get_user_by_email($email);
    if ($users) {
        $user = $users[0];
    }

    if ($user) {
        if ($user->isBanned()) {
            $user->unban();
            add_entity_relationship($user->guid, "member_of_site", $site->guid);
            $unbanned_count++;
            continue;
        } else {
            $on_site_count++;
            continue;
        }
    }

    $code = ModPleio\InviteCodeHandler::generateCode($email);
    $join_link = $site->url . "invitation?invitecode=" . $code;

    elgg_send_email(
        ModPleio\Helpers::getSiteEmail(),
        $email,
        html_entity_decode(elgg_echo("pleio:invitation:subject", [$site->name])),
        elgg_echo("pleio:invitation:message", [
            $loggedin_user->name,
            html_entity_decode($site->name),
            $join_link,
            $message
        ])
    );

    $invited_count++;
}

elgg_clear_sticky_form("pleio_invite");

system_message(elgg_echo("pleio:invited", [$invited_count, $unbanned_count, $on_site_count]));
forward(REFERER);