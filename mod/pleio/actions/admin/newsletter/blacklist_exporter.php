<?php

global $DB_QUERY_CACHE;
$DB_QUERY_CACHE = false; // no need for cache. Will only cause OOM issues

set_time_limit(0);

function newsletter_get_blacklisted(ElggEntity $container) {
	$result = false;

	if (!empty($container) && (elgg_instanceof($container, "site") || elgg_instanceof($container, "group"))) {
		// get the subscribers
        $result = array(
            "users" => array(),
            "emails" => array()
        );

        // get all subscribed community members
        $options = array(
            "type" => "user",
            "selects" => array("ue.email"),
            "site_guids" => false,
            "limit" => false,
            "relationship" => NewsletterSubscription::BLACKLIST,
            "relationship_guid" => $container->getGUID(),
            "inverse_relationship" => true,
            "joins" => array("JOIN " . elgg_get_config("dbprefix") . "users_entity ue ON e.guid = ue.guid"),
            "callback" => "newsletter_user_row_to_subscriber_info"
        );

        // @todo make this easier????
        $tmp_users = elgg_get_entities_from_relationship($options);
        if (!empty($tmp_users)) {
            foreach ($tmp_users as $tmp_user) {
                $result["users"][$tmp_user["guid"]] = $tmp_user["email"];
            }
        }

        // check the email subscriptions
        $options = array(
            "type" => "object",
            "subtype" => NewsletterSubscription::SUBTYPE,
            "selects" => array("oe.title"),
            "limit" => false,
            "relationship" => NewsletterSubscription::BLACKLIST,
            "relationship_guid" => $container->getGUID(),
            "inverse_relationship" => true,
            "joins" => array("JOIN " . elgg_get_config("dbprefix") . "objects_entity oe ON e.guid = oe.guid"),
            "callback" => "newsletter_subscription_row_to_subscriber_info"
        );

        $result["emails"] = elgg_get_entities_from_relationship($options);
	}

	return $result;
}


$filename = "blacklisted.csv";

$fieldtype = get_input("fieldtype");
$fields = get_input("export");


header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Disposition: attachment;filename={$filename}");
header("Content-Transfer-Encoding: binary");

ob_start();

$df = fopen("php://output", "w");

fputcsv($df, array("email", "site", "name", "is_user"), ";");

$site = elgg_get_site_entity();
$subscribers = newsletter_get_blacklisted($site);
foreach ($subscribers["users"] as $email)  {
    fputcsv($df, array($email, "site", $site->name, 1), ";");
}
foreach ($subscribers["emails"] as $email)  {
    fputcsv($df, array($email, "site", $site->name, 0), ";");
}

fclose($df);

echo ob_get_clean();
exit;
exit();
