<?php
if(elgg_is_active_plugin("pleio_template")) {

    $dbprefix = elgg_get_config("dbprefix");
    $ia = elgg_set_ignore_access(true);

    $site = elgg_get_site_entity();

    $subtype_id = add_subtype("object", "discussion");

    $options = [
        "type" => "object",
        "subtypes" => ["videolist_item", ],
        "limit" => false
    ];


    $vl_result = get_data(
        "SELECT container_guid, GROUP_CONCAT(DISTINCT guid) as guids
         FROM elgg_entities ee
         LEFT JOIN elgg_entity_subtypes es ON ee.subtype = es.id
         WHERE es.subtype = 'videolist_item'
         GROUP BY container_guid
        "
    );

    foreach ($vl_result as $row) {

        $group = get_entity($row->container_guid);
        if ($group->type != 'group') {
            continue;
        }

        $options = array(
            "container_guid" => (int) $group->guid,
            "type" => "object",
            "subtype" => "page_widget",
            "limit" => false,
            "count" => true
        );

        // get lowest free widget position
        $nr_of_group_widgets = elgg_get_entities($options);
        if ($nr_of_group_widgets) {
            $widget_position = $nr_of_group_widgets;
        } else {
            $widget_position = 0;
        }

        $video_link_guids = explode(',', $row->guids);
        $widget_settings = array(array(
            "key" => "title",
            "value" => "video links",
        ));
        $links = array();
        $videolist_items = array();
        foreach ($video_link_guids as $guid) {

            $videolist_item = get_entity($guid);
            array_push($videolist_items, $videolist_item);
            array_push($links, array(
                "url" => $videolist_item->video_url,
                "label" => $videolist_item->title)
            );
        }
        array_push($widget_settings, array(
            "key" => "links",
            "value" => json_encode($links)
        ));

        $widget = new \ElggObject();
        $widget->subtype = "page_widget";
        $widget->container_guid = $group->guid;
        $widget->position = $widget_position;
        $widget->access_id = $group->access_id;
        $widget->widget_type = "linklist";

        $created_widget_guid = $widget->save();
        $created_widget = get_entity($created_widget_guid);
        $created_widget->setPrivateSetting("settings", json_encode($widget_settings));

        foreach ($videolist_items as $item) {
            $item->delete();
        }
    }

    $site->setPrivateSetting("videolist_items_migrated", true);

    elgg_set_ignore_access($ia);

    system_message("Links uit videolists geconverteerd voor nieuwe template");

    forward(REFERER);
}


