<?php
if(elgg_is_active_plugin("pleio_template")) {

    $dbprefix = elgg_get_config("dbprefix");
    $ia = elgg_set_ignore_access(true);

    $subtype_id = add_subtype("object", "discussion");

    $options = [
        "type" => "object",
        "subtypes" => ["groupforumtopic", ],
        "limit" => false
    ];

    foreach (elgg_get_entities($options) as $discussion) {
        update_data("UPDATE {$dbprefix}entities SET subtype = {$subtype_id} WHERE guid = {$discussion->guid}");
        $annotations = $discussion->getAnnotations(["name" => "group_topic_post"]);
        foreach ($annotations as $annotation) {
            $entity = new \ElggObject();
            $entity->subtype = "comment";
            $entity->owner_guid = $annotation->owner_guid;
            $entity->container_guid = $discussion->guid;
            $entity->write_access_id = 0;
            $entity->tags = "";
            $entity->access_id = $annotation->access_id;
            $entity->description = $annotation->value;
            $comment = $entity->save();
            update_data("UPDATE {$dbprefix}entities SET time_created = {$annotation->time_created} WHERE guid = {$comment}");
            update_data("UPDATE {$dbprefix}entities SET time_created = {$annotation->time_created}, time_updated = {$annotation->time_created}, last_action = {$annotation->time_created} WHERE guid = {$comment}");
            elgg_delete_annotation_by_id($annotation->id);
        }
        _elgg_invalidate_memcache_for_entity($discussion->guid);

    }

    elgg_set_ignore_access($ia);

    system_message("Discussies geconverteerd voor nieuwe template");

    forward(REFERER);
}


