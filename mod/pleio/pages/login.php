<?php
global $CONFIG;

if (elgg_is_logged_in()) {
    if(elgg_is_active_plugin("pleio_main_template")) {
        forward("/dashboard");
    } else {
        forward("/");
    }
}

$site = elgg_get_site_entity();

$method = get_input("method");
$code = get_input("code");
$state = get_input("state");

$invitecode = urldecode(get_input("invitecode"));
$returnto = urldecode(get_input("returnto"));
$login_credentials = get_input("login_credentials");

$logintype = get_input("logintype", false);

$idp = elgg_get_plugin_setting("idp", "pleio");

if (!$CONFIG->pleio->client || !$CONFIG->pleio->secret || !$CONFIG->pleio->url) {
    register_error(elgg_echo("pleio:not_configured"));
    forward(REFERER);
}

$query = [];
if ($invitecode) {
    $query = [ "invitecode" => $invitecode ];
} elseif ($returnto) {
    $query = [ "returnto" => $returnto ];
} else {
    $query = [];
}

$provider = new ModPleio\Provider([
    "clientId" => $CONFIG->pleio->client,
    "clientSecret" => $CONFIG->pleio->secret,
    "url" => $CONFIG->pleio->url,
    "redirectUri" => "{$site->url}login?" . http_build_query($query)
]);

if (!isset($code)) {
    $authorizationUrl = $provider->getAuthorizationUrl();
    $_SESSION["oauth2state"] = $provider->getState();

    if ($idp && $login_credentials !== "true") {
        $authorizationUrl .= "&idp={$idp}";
    }

    if ($logintype === "crawler") {
        $authorizationUrl .= "&logintype=crawler";
    }

    if ($method === "register") {
        $authorizationUrl .= "&method=register";
    }

    header("Location: {$authorizationUrl}");

} elseif (empty($state) || $state !== $_SESSION["oauth2state"]) {
    // mitigate CSRF attack
    unset($_SESSION["oauth2state"]);
    forward("/");
} else {
    try {
        $accessToken = $provider->getAccessToken("authorization_code", [
            "code" => $code
        ]);

        unset($_SESSION["oauth2state"]);
        unset($_SESSION['pleio_user_is_banned']);

        $resourceOwner = $provider->getResourceOwner($accessToken);
        $loginHandler = new ModPleio\LoginHandler($resourceOwner, $invitecode);


        try {
            $user = $loginHandler->handleLogin();
            $user->setPrivateSetting("pleio_token", $accessToken->getToken());

            system_message(elgg_echo("loginok"));

            if ($returnto && pleio_is_valid_returnto($returnto)) {
                forward($returnto);
            } else {
                if(elgg_is_active_plugin("pleio_main_template")) {
                    forward("/dashboard");
                } else {
                    forward("/");
                }
            }
        } catch (ModPleio\Exceptions\CouldNotLoginException $e) {
            register_error(elgg_echo("pleio:is_banned"));
            $_SESSION['pleio_user_is_banned'] = true;
            forward("/");
        } catch (ModPleio\Exceptions\InvalidInviteCodeException $e) {
            register_error(elgg_echo("pleio:invalid_invitecode"));
            forward("/");
        } catch (ModPleio\Exceptions\ShouldRegisterException $e) {
            if (ModPleio\Helpers::emailInWhitelist($resourceOwner->getEmail())) {
                $title = elgg_echo("pleio:validate_access");
                $description = elgg_echo("pleio:validate_access:description");
            } else {
                $title = elgg_echo("pleio:request_access");
                $description = elgg_echo("pleio:request_access:description");
            }

            $_SESSION["pleio_resource_owner"] = $resourceOwner->toArray();

            $body = elgg_view_layout("walled_garden", [
                "content" => elgg_view("pleio/request_access", [
                    "title" => $title,
                    "description" => $description,
                    "resourceOwner" => $resourceOwner->toArray()
                ]),
                "class" => "elgg-walledgarden-double",
                "id" => "elgg-walledgarden-login"
            ]);

            echo elgg_view_page(elgg_echo("pleio:request_access"), $body, "walled_garden");
            return true;
        } catch (\RegistrationException $e) {
            register_error($e->getMessage());
            forward("/");
        }
    } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
        register_error($e->getMessage());
        forward("/");
    }
}
