<?php

if (elgg_is_active_plugin("pleio_template")) {

    set_time_limit(0);

    $dbprefix = elgg_get_config("dbprefix");
    $ia = elgg_set_ignore_access(true);

    $rich_description_id = get_metastring_id("richDescription");
    $count = 0;

    if ($rich_description_id) {

        // Get all entities having "richDescription" metadata

        $query = "SELECT ms.id, ms.string FROM {$dbprefix}metastrings AS ms INNER JOIN {$dbprefix}metadata md ON md.value_id = ms.id AND md.name_id = {$rich_description_id}";

        $result = get_data($query);

        // replace all "richDescription" metadata

        foreach($result as $row) {
            $rich_description  = json_decode(html_entity_decode($row->string, ENT_COMPAT | ENT_QUOTES, 'UTF-8'));

            if(!$rich_description)
                continue;

            // check for images with alt and convert to 
            if (isset($rich_description->entityMap)) {

                $update = false;

                foreach($rich_description->entityMap as $key=>$object) {
                    
                    if ($object->type === "IMAGE") {
                        if (isset($object->data->alt) && !isset($object->data->caption)) {

                            $caption = sanitise_string($object->data->alt);

                            // used example: '{"blocks":[{"key":"fn0na","text":"'.$caption.'","type":"unstyled","depth":0,"inlineStyleRanges":[],"entityRanges":[],"data":{}}],"entityMap":{}}';

                            $draft_object = (object) [
                                "blocks" => [[
                                    "key" => "fn0na",
                                    "text" => $object->data->alt,
                                    "type" => "unstyled",
                                    "depth" => 0,
                                    "inlineStyleRanges" => [],
                                    "entityRanges" => [],
                                    "data" => (object)[]

                                ]],
                                "entityMap" => (object)[]
                            ];

                            $object->data->caption = json_encode($draft_object);
                            $rich_description->entityMap->{$key}->data = $object->data;
                            $update = true;
                        }
                    }
                }

                if ($update) {
                    $new_string = sanitise_string(json_encode($rich_description));
                    update_data("UPDATE {$dbprefix}metastrings SET string = '{$new_string}' WHERE id = {$row->id}");
                    $count++;
                }
            }
        }
    }

    // select widgets
    $options = array(
        "type" => "object",
        "subtype" => "page_widget",
        "limit" => false
    );

    // search for widgets with richDescription settings
    foreach(elgg_get_entities($options) as $idx=>$widget) {
        if($settings = json_decode($widget->getPrivateSetting("settings"))) {
            
            $update = false;
            $rich_description_idx = -1;

            foreach($settings as $idx=>$item) {
                if($item->key === "richDescription") {
                    $rich_description_idx = $idx;
                }
            }

            if ($rich_description_idx > -1) {

                $rich_description  = json_decode($settings[$rich_description_idx]->value);

                if(!$rich_description)
                    continue;
    
                // check for images with alt and convert to 
                if (isset($rich_description->entityMap)) {

                    foreach($rich_description->entityMap as $key=>$object) {
                        
                        if ($object->type === "IMAGE") {
                            if (isset($object->data->alt) && !isset($object->data->caption)) {
        
                                $caption = sanitise_string($object->data->alt);
        
                                // used example: '{"blocks":[{"key":"fn0na","text":"'.$caption.'","type":"unstyled","depth":0,"inlineStyleRanges":[],"entityRanges":[],"data":{}}],"entityMap":{}}';
        
                                $draft_object = (object) [
                                    "blocks" => [[
                                        "key" => "fn0na",
                                        "text" => $object->data->alt,
                                        "type" => "unstyled",
                                        "depth" => 0,
                                        "inlineStyleRanges" => [],
                                        "entityRanges" => [],
                                        "data" => (object)[]
        
                                    ]],
                                    "entityMap" => (object)[]
                                ];
        
                                $object->data->caption = json_encode($draft_object);
                                $rich_description->entityMap->{$key}->data = $object->data;

                                $settings[$rich_description_idx]->value = json_encode($rich_description);
                                $update = true;
                            }
                        }
                    }
        
                    if ($update) {
                        $widget->setPrivateSetting("settings", json_encode($settings));
                        $count++;
                    }
                }

            }
        }
    }

    error_log("Updated caption for {$count} richDescriptions");

    elgg_set_ignore_access($ia);

}
