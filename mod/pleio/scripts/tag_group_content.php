<?php
    /*
        Give all content in a group an tag if it does exist yet, this will be the tag of the group
    */
    set_time_limit(0);
    $dbprefix = elgg_get_config("dbprefix");
    $ia = elgg_set_ignore_access(true);

    $groups = elgg_get_entities(["type" => "group", "limit" => 0]);

    foreach ($groups as $group) {
        $entities = elgg_get_entities([
            "type" => "object",
            "subtypes" => ["blog", "wiki", "news", "question", "discussion", "event", "file"],
            "container_guid" => (int) $group->guid,
            "limit" => 0
        ]);
        foreach ($entities as $entity) {
            try {
                if (!$entity->tags) {
                    $entity->tags = filter_tags($group->name);
                    $entity->save();
                } else {
                    $tags = $entity->tags;
                    if (!is_array($tags)) {
                        $tags = array($tags);
                    }

                    if (!in_array($group->name, $tags)) {
                        $tags[] = $group->name;
                        $entity->tags = $tags;
                        $entity->save();
                    }
                }
            } catch (\Exception $e) {
                // silently fail
                error_log("Failed to add tag: ".$e->getMessage());
            }
        }
    }

?>