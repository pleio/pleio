<?php
// This script should be executed using a background process
// php console.php runscript fix-group-acl.php

// Check if site has issues

$acl = get_data("SELECT * FROM elgg_access_collections WHERE id = 1 OR id = 2");

$max = get_data_row("SELECT max(id) as id FROM elgg_access_collections");

if ($max->id < 3) {
    $next_id = 3;
} else {
    $next_id = $max->id;
}

foreach($acl as $item) {
    $next_id+=1;

    $dbprefix = elgg_get_config("dbprefix");
    $ia = elgg_set_ignore_access(true);

    echo "Fixing group: {$item->name} ";

    $group = get_entity($item->owner_guid);

    if (!$group || !$group instanceof \ElggGroup) {
        echo " no group found skipping...\n";
        continue;
    }

    $old_id = $item->id;
    if(update_data("UPDATE elgg_access_collections SET id = {$next_id} WHERE id = {$item->id}")) {
        echo " ACL updated to {$next_id} ";

        update_data("UPDATE elgg_access_collection_membership SET access_collection_id = {$next_id} WHERE access_collection_id = {$item->id}");

        echo " members converted ";

        $group->group_acl = $next_id;
        $group->save();

        $group->setPrivateSetting("elgg_default_access", $next_id);

        // update all items connected to group?
        $options = [
            "type" => "object",
            "container_guid" => $group->guid,
            "access_id" => $old_id,
            "limit" => 0
        ];
        
        $entities = elgg_get_entities($options);

        foreach ($entities as $entity) {
            echo "Fix {$entity->getSubtype()} {$entity->guid}\n";

            try {
                $entity->access_id = $next_id;
                $entity->save();
            } catch(Exception $e) {
                // catches error with tidypics thumbnail which cannot be loaded on my local filesystem
                echo "catched error: {$e}\n";
            }
        }
    }

    elgg_set_ignore_access($ia);
}
