<?php

set_time_limit(0);
ini_set("memory_limit", "500M");

if(elgg_is_active_plugin("pleio_template")) {
    global $CONFIG;

    $dbprefix = elgg_get_config("dbprefix");

    $ia = elgg_set_ignore_access(true);

    function _is_group($guid) {
        try {
            $exists = get_data_row("SELECT guid from elgg_groups_entity WHERE guid={$guid}");
            if ($exists) {
                return true;
            }
        }
        catch(Exception $e) {
            return false;
        }
        return false;
    }

    function _validate_access($file, $group_access_collection_id) {
        if (in_array($file->access_id, array(1,2))) {
            if (!_is_group($file->container_guid)) {
                print($file->access_id);
                $file->access_id = $group_access_collection_id;
                $file->save();
                log_to_file($file->guid);
            }
        }
    }

    function _set_file_access($text, $group_access_collection_id) {
        preg_match_all('/^\/file\/download\/([\w]+)/', $text, $matches);

        foreach ($matches[1] as $guid) {
            $e = get_entity($guid);

            if (get_subtype_from_id($e->subtype) == 'file') {
                _validate_access($e, $group_access_collection_id);
            }
        }
    }


    function _set_file_access_rich_description_json($entity, $group_access_collection_id) {
        if (isset($entity->richDescription)) {
            $data = json_decode($entity->richDescription);

            foreach ($data->entityMap as $idx) {
                if (in_array($idx->type, array("DOCUMENT"))) {
                    if (isset($idx->data->url)) {
                        _set_file_access($idx->data->url, $group_access_collection_id);
                    }
                    if (isset($idx->data->href)) {
                        _set_file_access($idx->data->href, $group_access_collection_id);
                    }
                }
            }
        }
    }

    function log_to_file($text) {
        global $CONFIG;
        $directory = "/tmp/_log-group-file-fix";
        if(!file_exists($directory)) {
            mkdir($directory, 0755, true);
        }

        $logfile = $directory . "/" . $CONFIG->dbname .".txt";

        file_put_contents($logfile, "$text\n", FILE_APPEND | LOCK_EX);

        error_log($text);
    }

    log_to_file("------------------------------------");
    log_to_file("Site: $CONFIG->dbname");

    $options = [
        "type" => "group",
        "limit" => 0,
        "offset" => 0,
        "joins" => [],
        "wheres" => []
    ];

    $count_entities = 0;

    foreach (elgg_get_entities($options) as $group) {

        if ($group->membership === ACCESS_PRIVATE) {
            $query = "SELECT * FROM {$dbprefix}access_collections WHERE owner_guid = {$group->guid}";
            $group_access_collection_id = get_data_row($query)->id;

            $options = [
                "type" => "object",
                "limit" => 0,
                "offset" => 0,
                "container" => $group,
                "metadata_name" => "richDescription"
            ];
            foreach (elgg_get_entities_from_metadata($options) as $entity) {
                $count_entities += 1;
                _set_file_access_rich_description_json($entity, $group_access_collection_id);
            }
        }
    }
    log_to_file("{$count_entities} entities checked");
}

?>