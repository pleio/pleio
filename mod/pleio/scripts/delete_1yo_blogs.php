<?php

set_time_limit(0);

$dbprefix = elgg_get_config("dbprefix");
$ia = elgg_set_ignore_access(true);

$ts_upper = time() - (365 * 24 * 60 * 60);

echo "> get blogs before: ". date("c", $ts_upper)."\n";

$entities = elgg_get_entities(['type' => 'object', "subtype" => "blog", "limit" => 0, "created_time_upper" => $ts_upper]);

echo "> found: ".count($entities)." old blogs\n";

foreach ($entities as $entity) {
    $entity->delete();
}

echo "done...\n";