<?php
use Pleio\Helpers;

set_time_limit(0);
ini_set("memory_limit", "500M");

if(elgg_is_active_plugin("pleio_template")) {

    $dbprefix = elgg_get_config("dbprefix");
    $ia = elgg_set_ignore_access(true);

    $subtyps = [];
    $subtypes[] = add_subtype("object", "wiki");
    $subtypes[] = add_subtype("object", "blog");
    $subtypes[] = add_subtype("object", "event");
    $subtypes[] = add_subtype("object", "discussion");
    $subtypes[] = add_subtype("object", "news");
    $subtypes[] = add_subtype("object", "page");
    $subtypes[] = add_subtype("object", "question");

    $ids = implode(",", $subtypes);

    $query = "SELECT e.guid, o.description FROM {$dbprefix}entities AS e LEFT JOIN {$dbprefix}objects_entity o ON e.guid = o.guid WHERE e.subtype IN ({$ids})";

    $result = get_data($query);
    echo "> get entities\n";

    echo "> cleanup: ".count($result)." entities\n";

    foreach ($result as $row) {
        $clean_description = sanitise_string(Helpers::filterHtml(elgg_autop($row->description)));
        update_data("UPDATE {$dbprefix}objects_entity SET description = '{$clean_description}' WHERE guid = {$row->guid}");
        echo ".";
    }

    echo "\n> done.";

    elgg_set_ignore_access($ia);
    
}