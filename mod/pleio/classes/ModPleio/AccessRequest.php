<?php
namespace ModPleio;

class AccessRequest {
    public function __construct($data) {
        $this->id = (int) $data->id;
        $this->user = unserialize($data->user);
        $this->time_created = $data->time_created;
    }

    public function getURL() {
        return $this->user["url"];
    }

    public function getIconURL() {
        global $CONFIG;
        return "{$CONFIG->pleio->url}mod/profile/icondirect.php?guid={$this->user["guid"]}&size=medium";
    }

    public function getType() {
        return "accessRequest";
    }

    public function approve() {
        $resourceOwner = new ResourceOwner($this->user);
        $loginHandler = new LoginHandler($resourceOwner);
        $site = elgg_get_site_entity();
        $dbprefix = elgg_get_config("dbprefix");

        try {
            $userExists = get_data_row("SELECT guid FROM {$dbprefix}users_entity WHERE email = '{$this->user["email"]}'");

            // If user already exists silently remove AccessRequest and InviteCode (This can happen when the user is already imported with user import)
            if ($userExists) {
                $this->remove();
                InviteCodeHandler::revokeInvite($this->user["email"]);
                return true;
            }

            $user = $loginHandler->createUser();
            if ($user) {
                $this->remove();
                $this->sendEmail(
                    html_entity_decode(elgg_echo("pleio:approved:subject", [$site->name])),
                    elgg_echo("pleio:approved:body", [
                        $user->name,
                        html_entity_decode($site->name),
                        $site->url
                    ])
                );

                InviteCodeHandler::revokeInvite($user->email);
                return true;
            }
        } catch (\RegistrationException $e) {
            register_error($e->getMessage());
        }

        return false;
    }

    public function decline() {
        $site = elgg_get_site_entity();
        $resourceOwner = new ResourceOwner($this->user);

        if ($this->remove()) {
            $this->sendEmail(
                html_entity_decode(elgg_echo("pleio:declined:subject", [$site->name])),
                elgg_echo("pleio:declined:body", [
                    $resourceOwner->getName(),
                    html_entity_decode($site->name)
                ])
            );

            return true;
        }

        return false;
    }

    public function remove() {
        return delete_data("DELETE FROM pleio_request_access WHERE id = {$this->id}");
    }

    private function sendEmail($subject, $body, $params = null) {
        $site = elgg_get_site_entity();

        if ($site->email) {
            $from = $site->email;
        } else {
            $from = "noreply@" . get_site_domain($site->guid);
        }

        return elgg_send_email($from, $this->user["email"], $subject, $body, $params);
    }
}