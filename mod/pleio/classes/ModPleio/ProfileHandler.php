<?php
namespace ModPleio;

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use Psr\Http\Message\UploadedFileInterface;

class ProfileHandler {
    public function __construct(\ElggUser $user) {
        global $CONFIG;

        if (!$user || !$user instanceof \ElggUser) {
            throw new \Exception("no_user_available");
        }

        $token = $user->getPrivateSetting("pleio_token");
        if (!$token) {
            throw new \Exception("no_token_available");
        }

        $this->user = $user;
        $this->client = new \GuzzleHttp\Client([
            'base_uri' => $CONFIG->pleio->url,
            'timeout' => 2,
            'headers' => [
                'Authorization' => "Bearer {$token}"
            ]
        ]);
    }

    private function getToken() {
        if (!$this->user) {
            throw new \Exception("could_not_find_user");
        }

        $token = $this->user->getPrivateSetting("pleio_token");

        if (!$token) {
            throw new \Exception("could_not_find_token");
        }

        return $token;
    }

    public function changeAvatar(UploadedFileInterface $file) {

        try {
            $response = $this->client->request("POST", "api/users/me/change_avatar", [
                "multipart" => [
                    [
                        "name" => "avatar",
                        "contents" => $file->getStream()->getContents(),
                        "filename" => $file->getClientFilename()
                    ]
                ]
            ]);

        } catch(ClientException $e) {
            throw new \Exception('profile_edit_client_error');
        } catch (RequestException $e) {
            throw new \Exception('profile_edit_server_error');
        }

        return $response;
    }

    public function removeAvatar() {
        try {
            return $this->client->request("POST", "api/users/me/remove_avatar");
        } catch(ClientException $e) {
            throw new \Exception('profile_edit_client_error');
        } catch (RequestException $e) {
            throw new \Exception('profile_edit_server_error');
        }
    }

    public function changeName($new_name) {
        try {
            return $this->client->request("POST", "api/users/me/change_name", [
                "form_params" => [
                    "name" => $new_name
                ]
            ]);
        } catch(ClientException $e) {
            throw new \Exception('profile_edit_client_error');
        } catch (RequestException $e) {
            throw new \Exception('profile_edit_server_error');
        }
    }

    public function changeEmail($email) {
        try {
            return $this->client->request("POST", "api/users/me/change_email", [
                "form_params" => [
                    "email" => $email
                ]
            ]);
        } catch(ClientException $e) {
            throw new \Exception('profile_edit_client_error');
        } catch (RequestException $e) {
            throw new \Exception('profile_edit_server_error');
        }
    }

    public function changePassword($old_password, $new_password) {
        try {
            return $this->client->request("POST", "api/users/me/change_password", [
                "form_params" => [
                    "old_password" => $old_password,
                    "new_password" => $new_password
                ]
            ]);
        } catch(ClientException $e) {
            throw new \Exception('profile_edit_client_error');
        } catch (RequestException $e) {
            throw new \Exception('profile_edit_server_error');
        }
    }
}
