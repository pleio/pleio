<?php
namespace ModPleio;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Wire\AMQPTable;

class Celery {
    public function __construct($host, $port, $username, $password, $vhost) {
        $this->connection = new AMQPStreamConnection($host, $port, $username, $password, $vhost);
        $this->channel = $this->connection->channel();
        $this->channel->queue_declare("celery", false, true, false, false);
    }

    public function __destruct() {
        if ($this->channel) {
            $this->channel->close();
        }

        if ($this->connection) {
            $this->connection->close();
        }
    }

    public function publishTask($task, $arguments=[]) {
        if (!is_array($arguments)) {
            throw new \Exception("Args should be an array");
        }

        $id = uniqid("php-", true);

        $headers = [
            "lang" => "py",
            "task" => $task,
            "id" => $id
        ];

        $properties = [
            "correlation_id" => $id,
            "content_type" => "application/json",
            "content_encoding" => "utf-8",
            "application_headers" => new AMQPTable($headers)
        ];

        $body = [
            $arguments,
            (object)[],
            null,
        ];

        $msg = new AMQPMessage(json_encode($body), $properties);
        $this->channel->basic_publish($msg, "celery", "celery");

        return $id;
    }
}
