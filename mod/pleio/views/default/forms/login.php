<?php
$idp = elgg_get_plugin_setting("idp", "pleio");
$idp_name = elgg_get_plugin_setting("idp_name", "pleio");

$invitecode = get_input("invitecode");
if ($invitecode) {
    $invitecode = urldecode(get_input("invitecode"));
}

$returnto = get_input("returnto");
if ($returnto) {
    $returnto = urldecode(get_input("returnto"));
}

$query = [];
if ($invitecode) {
    $query = [ "invitecode" => $invitecode ];
} elseif ($returnto) {
    $query = [ "returnto" => $returnto ];
} else {
    $query = [];
}
?>
<?php if ($idp): ?>
    <div style="display: flex; align-items: center">
        <?php echo elgg_view("output/url", array(
            "href" => "/login?" . http_build_query($query),
            "class" => "elgg-button-submit elgg-button",
            "text" => $idp && $idp_name ? elgg_echo("pleio:settings:login_through", [$idp_name]) : elgg_echo("login")
        )); ?>

        <?php echo elgg_view("output/url", array(
            "href" => "/login?" . http_build_query(array_merge($query, [ "login_credentials" => "true" ])),
            "class" => "elgg-button-submit elgg-button",
            "text" => elgg_echo("pleio:login_with_credentials")
        )); ?>
    </div>
<?php else: ?>
    <p>
        <?php echo elgg_view("output/url", array(
                "href" => "/login?" . http_build_query($query),
                "class" => "elgg-button-submit elgg-button",
                "text" => elgg_echo("login")
        )); ?>
        <?php echo elgg_view("output/url", array(
                "href" => "/login?" . http_build_query(array_merge($query, [ "method" => "register" ])),
                "class" => "elgg-button-submit elgg-button",
                "text" => elgg_echo("register")
        )); ?>
    </p>
<?php endif; ?>
