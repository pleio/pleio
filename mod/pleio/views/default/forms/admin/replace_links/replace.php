<div class="elgg-module elgg-module-inline">
    <?php echo elgg_echo("admin:administer_utilities:replace_links:label_replace") ?><br>
    <?php echo elgg_view("input/text", array("name" => "replace", "placeholder" => "https://..." )) ?>
</div>

<div class="elgg-module elgg-module-inline">
    <?php echo elgg_echo("admin:administer_utilities:replace_links:label_with") ?><br>
    <?php echo elgg_view("input/text", array("name" => "with", "value" => "")) ?>
</div>

<div class="elgg-module elgg-module-inline">
    <?php echo elgg_echo("admin:administer_utilities:replace_links:label_test") ?><br>
    <?php echo elgg_view("input/checkbox", array("name" => "test_run", "checked" => true)) ?>
</div>

<div class="elgg-foot">
    <?php echo elgg_view("input/submit", array("value" => elgg_echo("admin:administer_utilities:replace_links:label_submit"))) ?>
</div>
