<p>
    <?php echo elgg_echo("admin:administer_utilities:cache:description"); ?>
</p>

<?php echo elgg_view("output/confirmlink", [
        "href" => "/action/admin/site/flush_cache",
        "text" => "Flush",
        "class" => "elgg-button elgg-button-submit",
        "is_action" => true
    ]); ?>