<?php


echo "<form action='" . $vars['url'] . "action/newsletter/admin/newsletter_exporter' method='POST'>";
echo elgg_view("input/securitytoken");
echo elgg_view("input/submit", array("value" => elgg_echo("admin:administer_utilities:newsletter_exporter:export_subscribers")));
echo "</form>";

echo "<div><i>Export all email addresses that will be used to send a newsletter. (Following to plugin newsletter plugin settings).</i></div>";

echo "<br><br>";

echo "<form action='" . $vars['url'] . "action/newsletter/admin/newsletter_blacklist_exporter' method='POST'>";
echo elgg_view("input/securitytoken");
echo elgg_view("input/submit", array("value" => elgg_echo("admin:administer_utilities:newsletter_exporter:export_blacklisted")));
echo "</form>";

echo "<div><i>Export all email addresses that opted-out for the newsletter</i></div>";
