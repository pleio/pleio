<?php
$dbprefix = elgg_get_config("dbprefix");
$site = elgg_get_site_entity();

# check for group featured errors
$group_result = get_data("SELECT guid FROM elgg_groups_entity WHERE guid NOT IN (SELECT guid FROM elgg_metadata md INNER
                          JOIN elgg_metastrings ms ON md.name_id = ms.id INNER JOIN elgg_groups_entity ge ON md.entity_guid = ge.guid
                          WHERE string = 'isFeatured');");

$errors = FALSE;

if ($group_result) {
    $group_count = count($group_result);
    $errors = TRUE;
} else {
    $group_count = 0;
}


# check for group auto_join settings
$group_auto_join_result = get_data("SELECT value FROM elgg_private_settings WHERE name = 'auto_join'");

$groups_auto_join_count = 0;
if ($group_auto_join_result && !empty($group_auto_join_result) ) {
    $groups_auto_join = explode(",", $group_auto_join_result[0]->value);
    foreach ($groups_auto_join as $group) {
        $groups_auto_join_count++;
    }
    $errors = TRUE;
} else {
    $groups_auto_join_count = 0;
}




# check for group plugin configuration errors
function is_plugins_on_old_template($group) {
    if ($group->blog_enable == 'yes') {
        return true;
    }
    if ($group->event_manager_enable == 'yes') {
        return true;
    }
    if ($group->forum_enable == 'yes') {
        return true;
    }
    if ($group->questions_enable == 'yes') {
        return true;
    }
    if ($group->file_enable == 'yes') {
        return true;
    }
    if (($group->pages_enable == 'yes' || $group->static_enable == 'yes')) {
        return true;
    }
    if ($group->tasks_enable == 'yes') {
        return true;
    }
    return false;
}


$options = [
    "type" => "group",
    "limit" => false
];
$groups = elgg_get_entities($options);
$groups_plugins_count = 0;
$groups_duplicate_plugins_count = 0;
$groups_interests_count = 0;

foreach ($groups as $group) {
    if (is_array($group->plugins)) {
        if (count($group->plugins) != count(array_unique($group->plugins))) {
            $groups_duplicate_plugins_count++;
            $errors = TRUE;
        }
    }

    if (is_plugins_on_old_template($group)) {
        $groups_plugins_count++;
        $errors = TRUE;
    }

    if ($group->interests) {
        $groups_interests_count++;
        $errors = TRUE;
    }
}

error_log($groups_duplicate_plugins_count);

# check for wiki errors
$wiki_result = get_data("SELECT guid FROM elgg_entities ee
                         LEFT JOIN elgg_entity_subtypes es ON ee.subtype = es.id
                         LEFT JOIN elgg_metadata md ON ee.guid = md.entity_guid
                         LEFT JOIN elgg_metastrings ms ON md.name_id = ms.id
                         WHERE (ms.string = 'parent_guid' AND es.subtype = 'page') OR es.subtype = 'page_top'");


if ($wiki_result) {
    $wiki_count = count($wiki_result);
    $errors = TRUE;
} else {
    $wiki_count = 0;
}

# check for groupforumtopic errors
$gft_result = get_data("SELECT guid FROM elgg_entities ee
                         LEFT JOIN elgg_entity_subtypes es ON ee.subtype = es.id
                         WHERE es.subtype = 'groupforumtopic'");


if ($gft_result) {
    $gft_count = count($gft_result);
    $errors = TRUE;
} else {
    $gft_count = 0;
}

# check for site static error

$static_result = get_data("SELECT guid FROM elgg_entities ee
                         LEFT JOIN elgg_entity_subtypes es ON ee.subtype = es.id
                         WHERE es.subtype in ('static', 'static_top')");


if ($static_result) {
    $static_count = count($static_result);
    $errors = TRUE;
} else {
    $static_count = 0;
}

# check for photo album errors
$album_result = get_data("SELECT guid FROM elgg_entities ee
                         LEFT JOIN elgg_entity_subtypes es ON ee.subtype = es.id
                         WHERE es.subtype = 'album'");


if ($album_result) {
    $album_count = count($album_result);
    $errors = TRUE;
} else {
    $album_count = 0;
}

# check for photo album image errors
$album_image_result = get_data("SELECT guid FROM elgg_entities ee
                         LEFT JOIN elgg_entity_subtypes es ON ee.subtype = es.id
                         WHERE es.subtype = 'image'");


if ($album_image_result) {
    $album_image_count = count($album_image_result);
    $errors = TRUE;
} else {
    $album_image_count = 0;
}

# check for cms 2.0 errors
$cms2_data = get_data("SELECT guid FROM elgg_entities ee
                         LEFT JOIN elgg_entity_subtypes es ON ee.subtype = es.id
                         LEFT JOIN elgg_metadata md ON ee.guid = md.entity_guid
                         LEFT JOIN elgg_metastrings ms ON md.value_id = ms.id
                         LEFT JOIN elgg_metastrings ms2 ON md.name_id = ms2.id
                         WHERE ms.string = 'campagne' AND ms2.string = 'pageType' AND es.subtype = 'page'");

$cms2_count = 0;

foreach ($cms2_data as $page) {
    $options = [
        "type" => "object",
        "subtypes" => ["row", ],
        "metadata_name" => "layout",
        "limit" => false,
        "container_guid" => $page->guid,
    ];
    $cms2_count += count(elgg_get_entities_from_metadata($options));
}

if ($cms2_count) {
    $errors = TRUE;
} else {
    $cms2_count = 0;
}


# check for videolist errors

$vl_result = get_data("SELECT guid FROM elgg_entities ee
                         LEFT JOIN elgg_entity_subtypes es ON ee.subtype = es.id
                         WHERE es.subtype = 'videolist_item'");


if ($vl_result && !$site->getPrivateSetting("videolist_items_migrated")) {
    $vl_count = count($vl_result);
    $errors = TRUE;
} else {
    $vl_count = 0;
}




?>


<?php if ($errors): ?>

    <p>Er zijn problemen met de database:</p>

    <?php if ($group_count > 0 || $groups_plugins_count > 0 || $groups_auto_join_count > 0 || $groups_duplicate_plugins_count > 0 || $groups_interests_count > 0): ?>
        <p>Er zijn <?php echo $group_count; ?> groepen waarvan de aangeraden vlag nog niet bekend is in nieuwe template.</p>
        <p>Er zijn <?php echo $groups_plugins_count; ?> groepen waarvan de plugin configuratie niet is overgenomen.</p>
        <p>Er zijn <?php echo $groups_duplicate_plugins_count; ?> groepen waarvan de plugin configuratie dubbel is.</p>
        <p>Er zijn <?php echo $groups_interests_count; ?> groepen waarvan de tags niet zijn overgenomen.</p>
        <p>Er zijn <?php echo $groups_auto_join_count; ?> groepen waarvan de automatisch lid setting niet werkt.</p>
        <?php echo elgg_view("output/confirmlink", [
            "href" => "/action/admin/upgrade_database_groups",
            "text" => "Update groepen",
            "class" => "elgg-button elgg-button-submit",
            "is_action" => true
        ]); ?> <br><br>
    <?php endif; ?>
    <?php if ($wiki_count > 0): ?>
        <p>Er zijn <?php echo $wiki_count; ?> wiki pagina's welke niet beschikbaar zijn in het nieuwe template.</p>

        <?php echo elgg_view("output/confirmlink", [
            "href" => "/action/admin/upgrade_database_wiki",
            "text" => "Update wiki's",
            "class" => "elgg-button elgg-button-submit",
            "is_action" => true
        ]); ?> <br><br>
    <?php endif; ?>
    <?php if ($gft_count > 0): ?>
        <p>Er zijn <?php echo $gft_count; ?> discussie pagina's welke niet beschikbaar zijn in de filters van het nieuwe template.</p>

        <?php echo elgg_view("output/confirmlink", [
            "href" => "/action/admin/upgrade_database_discussion",
            "text" => "Update discussies",
            "class" => "elgg-button elgg-button-submit",
            "is_action" => true
        ]); ?> <br><br>
    <?php endif; ?>
    <?php if ($album_count > 0): ?>
        <p>Er zijn <?php echo $album_count; ?> fotoalbums met <?php echo $album_image_count; ?> foto's welke niet beschikbaar zijn in het nieuwe template.</p>

        <?php echo elgg_view("output/confirmlink", [
            "href" => "/action/admin/upgrade_database_photoalbums",
            "text" => "Update fotoalbums",
            "class" => "elgg-button elgg-button-submit",
            "is_action" => true
        ]); ?> <br><br>
    <?php endif; ?>

    <?php if ($static_count > 0): ?>
        <p>Er zijn <?php echo $static_count; ?> statische pagina's welke niet beschikbaar zijn in het nieuwe template.</p>

        <?php echo elgg_view("output/confirmlink", [
            "href" => "/action/admin/upgrade_database_static_pages",
            "text" => "Update statische pagina's",
            "class" => "elgg-button elgg-button-submit",
            "is_action" => true
        ]); ?> <br><br>
    <?php endif; ?>

    <?php if ($cms2_count > 0): ?>
    <p>Er zijn <?php echo $cms2_count; ?> rijen in campagne pagina's welke niet beschikbaar zijn in het nieuwe template</p>

        <?php echo elgg_view("output/confirmlink", [
            "href" => "/action/admin/upgrade_database_cms_pages",
            "text" => "Update naar cms2",
            "class" => "elgg-button elgg-button-submit",
            "is_action" => true
        ]); ?> <br><br>
    <?php endif; ?>

    <?php if ($vl_count > 0): ?>
        <p>Er zijn <?php echo $vl_count; ?> video links uit videolists welke niet omgezet naar een link in een lijklijstwidget in een groep. (Let op alleen video list items in groepen worden gemigreerd.)</p>

        <?php echo elgg_view("output/confirmlink", [
            "href" => "/action/admin/upgrade_database_videolists",
            "text" => "Update videolists",
            "class" => "elgg-button elgg-button-submit",
            "is_action" => true
        ]); ?> <br><br>
    <?php endif; ?>

<?php else: ?>
    <p>Database klaar voor nieuwe template</p>
<?php endif; ?>
