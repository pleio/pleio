<?php
$dbprefix = elgg_get_config("dbprefix");

$result = get_data_row("SELECT COUNT(*) AS count FROM {$dbprefix}entities e WHERE e.type = 'user' AND e.enabled = 'no'");
if ($result) {
    $count = $result->count;
} else {
    $count = 0;
}
?>

<?php if ($count > 0): ?>
    Er zijn problemen met <?php echo $count; ?> gebruiker(s).

    <?php echo elgg_view("output/confirmlink", [
        "href" => "/action/admin/repair_hidden_users",
        "text" => "Repareer verborgen gebruikers",
        "class" => "elgg-button elgg-button-submit",
        "is_action" => true
    ]); ?>

<?php else: ?>
    Er zijn geen problemen gevonden.
<?php endif; ?>
