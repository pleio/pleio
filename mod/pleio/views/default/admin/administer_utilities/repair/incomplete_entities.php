<?php
$dbprefix = elgg_get_config("dbprefix");


$result = get_data("SELECT e.guid FROM elgg_entities e LEFT JOIN elgg_objects_entity o USING(guid) WHERE e.type = 'object' AND o.guid IS NULL");

if ($result) {
    $count = count($result);
} else {
    $count = 0;
}
?>

<?php if ($count > 0): ?>
    Er zijn problemen met <?php echo $count; ?> entities.

    <?php echo elgg_view("output/confirmlink", [
        "href" => "/action/admin/repair_incomplete_entities",
        "text" => "Verwijder incomplete entities",
        "class" => "elgg-button elgg-button-submit",
        "is_action" => true
    ]); ?>

<?php else: ?>
    Er zijn geen problemen gevonden.
<?php endif; ?>
