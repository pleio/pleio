<p><?php echo elgg_echo("admin:administer_utilities:replace_links:description"); ?></p>

<?php

$form_vars = array(
    "enctype" => "multipart/form-data"
);

echo elgg_view_form("admin/replace_links/replace", $form_vars);
?>
