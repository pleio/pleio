<?php
$options = array(
    'relationship' => 'is_subeditor',
    "inverse_relationship" => true,
);
$subeditors = elgg_list_entities_from_relationship($options);

?>
<div class="elgg-module elgg-module-inline">
	<div class="elgg-head">
		<h3><?php echo elgg_echo('admin:users:subeditors'); ?></h3>
	</div>
	<div class="elgg-body">
		<?php echo $subeditors; ?>
	</div>
</div>

<?php
if (elgg_is_active_plugin("questions")) {

    $options = array(
        'relationship' => 'questions_expert',
        "inverse_relationship" => true,
    );
    $questions_experts = elgg_list_entities_from_relationship($options);
    
?>

<div class="elgg-module elgg-module-inline">
	<div class="elgg-head">
		<h3><?php echo elgg_echo('admin:users:questions_expert'); ?></h3>
	</div>
	<div class="elgg-body">
		<?php echo $questions_experts; ?>
	</div>
</div>

<?php
}
?>




