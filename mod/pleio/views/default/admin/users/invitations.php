<?php

$site = elgg_get_site_entity();
$result = array();

$invites = ModPleio\InviteCodeHandler::getInvites();

echo "<div>" . elgg_echo("pleio:invitations:description") . "</div>";

$email_body = "<table class='elgg-table'>";
$email_body .= "<tr>";
$email_body .= "<th>" . elgg_echo("pleio:date_invited") . "</th>";
$email_body .= "<th>" . elgg_echo("email") . "</th>";
$email_body .= "<th>&nbsp;</th>";
$email_body .= "</tr>";

foreach ($invites as $invite) {
    $email = explode("|", $invite->value)[1];
    $encoded_email = urlencode($email);

    $email_body .= "<tr>";
    $email_body .= "<td>";
    $email_body .= elgg_view("output/date", [ "value" => $invite->time_created ]);
    $email_body .= "</td>";
    $email_body .= "<td>{$email}</td>";
    $email_body .= "<td class='center'>";
    $email_body .= elgg_view("output/confirmlink", array(
        "text" => elgg_echo("pleio:revoke"),
        "href" => "action/admin/users/revoke_invite?email={$encoded_email}",
        "confirm" => elgg_echo("pleio:invitations:revoke:confirm"),
        "class" => "elgg-button elgg-button-action"));
    $email_body .= "</td>";
    $email_body .= "</tr>";
}

$email_body .= "</table>";

echo elgg_view_module("inline", elgg_echo("admin:users:invitations"), $email_body);