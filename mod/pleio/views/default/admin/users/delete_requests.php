<?php
// newest users
$users = elgg_list_entities_from_metadata(array(
	'type' => 'user',
	'subtype'=> null,
    "full_view" => false,
	'metadata_name' => 'requestDelete',
	'metadata_value' => '1',
));

?>

<div class="elgg-module elgg-module-inline">
	<div class="elgg-head">
		<h3><?php echo elgg_echo('users'); ?></h3>
	</div>
	<div class="elgg-body">
		<?php echo $users; ?>
	</div>
</div>
