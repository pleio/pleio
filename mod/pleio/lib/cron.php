<?php
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;

// This function bans all users that have e-mailaddresses that bounce
function pleio_ban_users_that_bounce($hook, $period, $return, $params) {
    global $CONFIG;

    $bouncer_url = $CONFIG->bouncer_url;
    $bouncer_token = $CONFIG->bouncer_token;

    if (!$bouncer_url || !$bouncer_token) {
        error_log("Could not process bouncing emails as bouncer_url or bouncer_token is not set");
        return;
    }

    error_log("Started processing bouncing emails");

    $site = elgg_get_site_entity();


    $client = new GuzzleHttp\Client([
        "timeout" => 2,
        "headers" => [
            "Authorization" => "Token {$bouncer_token}"
        ]
    ]);

    $last_received = $site->getPrivateSetting("last_received_bouncing_email");

    try {
        $response = $client->request(
            "GET",
            "{$bouncer_url}/api/orphans?last_received__gt={$last_received}"
        );
        
        $orphans = json_decode($response->getBody());

        $ia = elgg_set_ignore_access(true);

        foreach ($orphans as $orphan) {
            $last_received = $orphan->last_received;

            $users = get_user_by_email($orphan->email);

            if (count($users) === 0) {
                continue;
            }

            $user = $users[0];

            if ($user->isBanned()) {
                continue;
            }

            if ($user->ban("bouncing_email")) {
                remove_entity_relationship($user->guid, "member_of_site", $site->guid);
            }
        }

        elgg_set_ignore_access($ia);
    } catch (Exception $e) {
        error_log("Could not process bouncing emails ({$e->getMessage()}).");
        return;
    }

    if ($last_received) {
        $site->setPrivateSetting("last_received_bouncing_email", $last_received);
    }

    error_log("Finished processing bouncing emails");
}

// This function bans all users that have been deleted on account
function pleio_ban_deleted_accounts($hook, $period, $return, $params) {
    global $CONFIG;

    $account_system_api_url = $CONFIG->pleio->url;
    $account_system_api_token = $CONFIG->account_system_api_token;

    if (!$account_system_api_url || !$account_system_api_token) {
        error_log("Could not process deleted accounts as account_system_api_url or account_system_api_token is not set");
        return;
    }

    error_log("Started processing deleted accounts");

    $site = elgg_get_site_entity();

    $client = new GuzzleHttp\Client([
        "timeout" => 2,
        "headers" => [
            "Authorization" => "Token {$account_system_api_token}"
        ]
    ]);

    $last_received = $site->getPrivateSetting("last_received_deleted_accounts");

    try {
        $response = $client->request(
            "GET",
            "{$account_system_api_url}api/users/deleted?event_time__gt={$last_received}"
        );

        $orphans = json_decode($response->getBody());

        $ia = elgg_set_ignore_access(true);

        $count = 0;

        foreach ($orphans as $orphan) {
            $last_received = $orphan->event_time;

            $user = get_user_by_pleio_guid($orphan->userid);

            if (!$user) {
                continue;
            }

            if ($user->isBanned()) {
                continue;
            }

            if ($user->ban("pleio_account_deleted")) {
                remove_entity_relationship($user->guid, "member_of_site", $site->guid);
                $count+=1;
            }
        }

        error_log("Banned {$count} user accounts"); 

        elgg_set_ignore_access($ia);
    } catch (Exception $e) {
        error_log("Could not process delete ({$e->getMessage()}).");
        return;
    }

    if ($last_received) {
        $site->setPrivateSetting("last_received_deleted_accounts", $last_received);
    }

    error_log("Finished processing deleted accounts");
}

// Clean elgg_system_log table: keep 175 days of logs
function pleio_maintain_system_log($hook, $period, $return, $params) {
    global $CONFIG;
    $dblink = get_db_link('write');

	$offset = 175 * 86400;
	$now = time();

    $ts = $now - $offset;

    error_log("Start elgg_system_log maintenance");

    $first = get_data_row("SELECT MIN(id) as id FROM {$CONFIG->dbprefix}system_log");
    $offset = $first->id;
    $deleted = 0;

    while($last = get_data_row("SELECT id from {$CONFIG->dbprefix}system_log WHERE id > {$offset} ORDER BY id LIMIT 1000,1")) {
        if (!$last)
            continue;

        $query = "DELETE from {$CONFIG->dbprefix}system_log WHERE id >= {$offset} AND id < {$last->id} AND time_created < $ts";
        if($result = execute_query("DELETE from {$CONFIG->dbprefix}system_log WHERE id >= {$offset} AND id < {$last->id} AND time_created < $ts", $dblink)) {
            $deleted += mysqli_affected_rows($dblink);
        }
        $offset = $last->id;
    }

    // delete last chunk
    if($result = execute_query("DELETE from {$CONFIG->dbprefix}system_log WHERE id >= {$offset} AND time_created<$ts", $dblink)) {
        $deleted += mysqli_affected_rows($dblink);
    }

    error_log("Deleted {$deleted} records from {$CONFIG->dbprefix}system_log");

    error_log("Optimize tabel {$CONFIG->dbprefix}system_log");

    optimize_table("{$CONFIG->dbprefix}system_log");

    error_log("Done elgg_system_log maintenance");

    return;
}
