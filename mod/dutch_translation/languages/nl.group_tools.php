<?php
$language = array (
  'widgets:discussion:status' => 'Welke discussies moeten worden getoond',
  'group_tools:delete_selected' => 'Verwijder selectie',
  'admin:administer_utilities:group_bulk_delete' => 'Bulk groep verwijderen',
  'group_tools:action:bulk_delete:success' => 'De geselecteerde groepen zijn verwijderd',
  'group_tools:action:bulk_delete:error' => 'Er is een fout opgetreden tijdens het verwijderen van de groepen, probeer het nogmaals',
  'groups:search:title' => 'Zoek naar groepen met de tag \'%s\'',
  'groups:search:tags' => 'tag',
  'groups:searchtag' => 'Naar groepen zoeken op tag',
  'group_tools:settings:member_export' => 'Groep beheerder mogen leden informatie exporteren',
  'group_tools:settings:member_export:description' => 'Dit bevat onder andere de naam, gebruikersnaam en email adres van de gebruiker',
  'group_tools:member_export:title_button' => 'Exporteer leden',
  'group_tools:settings:domain_based' => 'Activeer domein gebaseerde groepen',
  'group_tools:settings:domain_based:description' => 'Gebruikers kunnen lid worden van een groep gebaseerd op hun e-mail domein. Gedurende de registratie zullen ze automatisch lid worden van groepen met een overeenkomend e-mail domein.',
  'group_tools:join:domain_based:tooltip' => 'Je mag lid worden van deze groep vanwege je e-mail domein.',
  'group_tools:domain_based:title' => 'Configureer e-mail domeinen',
  'group_tools:domain_based:description' => 'Als je één (of meerdere) e-mail domeinen configureert, zullen gebruikers tijdens de registratie automatisch lid worden van je groep als hun e-mail domein overeenkomt. Ook kunnen gebruikers lid worden als je groep besloten is, mits hun e-mail domein overeenkomt. Je kunt meerdere domein configureren door een komma te gebruiken. Neem het @ teken niet op.',
  'group_tools:action:domain_based:success' => 'De e-mail domeinen zijn opgeslagen',
  'groups_tools:related_groups:tool_option' => 'Toon gerelateerde groepen',
  'groups_tools:related_groups:widget:title' => 'Gerelateerde groepen',
  'groups_tools:related_groups:widget:description' => 'Toon een lijst met groepen die je hebt toegevoegd als gerelateerd.',
  'groups_tools:related_groups:none' => 'Geen gerelateerde groepen gevonden.',
  'group_tools:related_groups:title' => 'Gerelateerde groepen',
  'group_tools:related_groups:form:placeholder' => 'Zoek een nieuwe gerelateerde groep',
  'group_tools:related_groups:form:description' => 'Hier kun je zoeken naar een nieuwe gerelateerde groep, selecteer die uit de lijst en klik op Voeg toe.',
  'group_tools:action:related_groups:error:same' => 'Je kunt deze groep niet aan zichzelf relateren',
  'group_tools:action:related_groups:error:already' => 'De geselecteerde groep is al gerelateerd',
  'group_tools:action:related_groups:error:add' => 'Er is een onbekende fout opgetreden tijdens het toevoegen van de relatie, probeer het nogmaals',
  'group_tools:action:related_groups:success' => 'De groep is nu gerelateerd',
  'group_tools:related_groups:notify:owner:subject' => 'Er is een nieuwe gerelateerde groep toegevoegd',
  'group_tools:related_groups:notify:owner:message' => 'Beste %s,

%s heeft je groep %s als een gerelateerde groep toegevoegd aan %s',
  'group_tools:related_groups:entity:remove' => 'Verwijder gerelateerde groep',
  'group_tools:action:remove_related_groups:error:not_related' => 'De groep is niet gerelateerd',
  'group_tools:action:remove_related_groups:error:remove' => 'Er is een onbekende fout opgetreden tijdens het verwijderen van de relatie, probeer het nogmaals',
  'group_tools:action:remove_related_groups:success' => 'De groep is niet langer gerelateerd',
  'group_tools:join:already:tooltip' => 'Je bent uitgenodigd voor deze groep, dus je kunt direct lid worden.',
  'group_tools:groups:membershipreq:email_invitations' => 'Uitgenodigde e-mail adressen',
  'group_tools:groups:membershipreq:email_invitations:none' => 'Geen uitstaande e-mail uitnodigingen',
  'group_tools:welcome_message:title' => 'Groep welkom bericht',
  'group_tools:welcome_message:description' => 'Je kunt een welkomstbericht configureren welke wordt verzonden aan nieuwe leden van de groep zodra ze lid worden. Als je geen bericht wilt versturen moet je dit veld leeg laten.',
  'group_tools:welcome_message:explain' => 'Om het bericht te personaliseren kun je gebruik maken van de volgende plaatsaanduidingen:
[name]: de naam van de nieuwe gebruiker (bijv. %s)
[group_name]: de naam van deze groep (bijv. %s)
[group_url]: de URL naar deze groep (bijv. %s)',
  'group_tools:action:welcome_message:success' => 'Het welkomstbericht is opgeslagen',
  'group_tools:welcome_message:subject' => 'Welkom bij %s',
  'group_tools:action:revoke_email_invitation:error' => 'Er is een fout opgetreden tijdens het intrekken van de uitnodiging, probeer het nogmaals',
  'group_tools:action:revoke_email_invitation:success' => 'De uitnodiging is ingetrokken',
  'group_tools:settings:show_hidden_group_indicator:group_acl' => 'Ja, als de groep alleen voor leden is',
  'group_tools:settings:show_hidden_group_indicator:logged_in' => 'Ja, als de groep niet publiek is',
  'group_tools:settings:show_hidden_group_indicator' => 'Toon een icoon als een groep verborgen is',
  'widgets:index_groups:sorting' => 'Hoe moeten de groepen worden gesorteerd',
  'group_tools:suggested_groups:info' => 'De volgende groepen zijn mogelijk interessant voor je. Klik op "Wordt lid" om direct lid te worden, of klik op de naam van de groep om meer informatie te zien.',
  'group_tools:settings:auto_suggest_groups' => 'Automatisch groepen voorstellen op de "Voorgesteld" pagina gebaseerd op profiel informatie. De lijst zal worden aangevuld met de voorgedefinieerde lijst van voorgestelde groepen. Als dit op "Nee" staat zullen alleen de voorgedefinieerde groepen worden getoond (als die er zijn)',
  'group_tools:settings:special_states' => 'Groepen met een speciale status',
  'group_tools:settings:special_states:featured' => 'Aangeraden',
  'group_tools:settings:special_states:featured:description' => 'De beheerder hebben de volgende groepen aangeraden.',
  'group_tools:settings:special_states:auto_join' => 'Automatisch lidmaatschap',
  'group_tools:settings:special_states:auto_join:description' => 'Nieuwe gebruikers van de site worden automatisch lid van de onderstaande groepen.',
  'group_tools:settings:special_states:suggested' => 'Voorgesteld',
  'group_tools:settings:special_states:suggested:description' => 'De volgende groepen worden voorgesteld aan (nieuwe) gebruikers. Het is mogelijk om automatisch voorgestelde groepen te krijgen, als er geen groepen worden gedetecteerd of te weinig, wordt de lijst aangevuld met de onderstaande groepen.',
  'group_tools:special_states:title' => 'Speciale groep status',
  'group_tools:special_states:description' => 'Een groep kan verschillende speciale statussen hebben, hier is een overzicht van de statussen en de huidige waarde.',
  'group_tools:special_states:featured' => 'Is deze groep aangeraden',
  'group_tools:special_states:auto_join' => 'Worden gebruikers automatisch lid van deze groep',
  'group_tools:special_states:auto_join:fix' => 'Om alle gebruikers lid te maken van deze groep, %sklik hier%s.',
  'group_tools:special_states:suggested' => 'Wordt deze groep voorgesteld aan (nieuwe) gebruikers',
  'group_tools:groups:sorting:suggested' => 'Voorgesteld',
  'group_tools:suggested_groups:none' => 'We kunnen geen groep aan je voorstellen. Dit kan gebeuren als we te weinig informatie van jou hebben of als je al lid bent van alle groepen die we aan je voor wilden stellen. Gebruik de zoek functionaliteit om meer groepen te vinden.',
  'group_tools:action:toggle_special_state:error:auto_join' => 'Er is een fout opgetreden tijdens het opslaan van de nieuwe instelling van automatisch lid worden',
  'group_tools:action:toggle_special_state:error:suggested' => 'Er is een fout opgetreden tijdens het opslaan van de nieuwe instelling voor het voorstellen',
  'group_tools:action:toggle_special_state:error:state' => 'Ongeldige status opgegeven',
  'group_tools:action:toggle_special_state:auto_join' => 'De nieuwe instelling voor het automatisch lid worden is opgeslagen',
  'group_tools:action:toggle_special_state:suggested' => 'De nieuwe instelling voor het voorstellen is opgeslagen',
  'group_tools:settings:invite_members' => 'Mogen groepsleden andere gebruikers uitnodigen',
  'group_tools:settings:invite_members:default_off' => 'Ja, standaard uit',
  'group_tools:settings:invite_members:default_on' => 'Ja, standaard aan',
  'group_tools:settings:invite_members:description' => 'Groep eigenaren/beheerders kunnen dit aan- / uitzetten foor hun groep',
  'group_tools:settings:show_membership_mode' => 'Toon open/gesloten lidmaatschap status op het profiel van een groep en in de zijbalk',
  'group_tools:invite_members:title' => 'Groepsleden mogen uitnodigen',
  'group_tools:invite_members:description' => 'Mogen de leden van deze groep nieuwe leden uitnodigen',
  'group_tools:widgets:start_discussion:title' => 'Begin een discussie',
  'group_tools:widgets:start_discussion:description' => 'Begin snel een discussie in de geselecteerde groep',
  'group_tools:widgets:start_discussion:login_required' => 'Om gebruik te kunnen maken van deze widget moet je zijn aangemeld',
  'group_tools:widgets:start_discussion:membership_required' => 'Je moet van ten minste één groep lid zijn om gebruik te kunnen maken. %sHier%s kun je interessante groepen vinden.',
  'group_tools:forms:discussion:quick_start:group' => 'Selecteer een groep voor deze discussie',
  'group_tools:forms:discussion:quick_start:group:required' => 'Selecteer een groep',
  'group_tools:discussion:confirm:open' => 'Weet je zeker dat je deze discussie wilt heropenen?',
  'group_tools:discussion:confirm:close' => 'Weet je zeker dat je deze discussie wilt sluiten?',
  'group_tools:action:discussion:toggle_status:success:open' => 'De discussie is succesvol heropent',
  'group_tools:action:discussion:toggle_status:success:close' => 'De discussie is succesvol gesloten',
  'group_tools:cleanup:my_status' => 'Verberg de Mijn Status zijbalk',
  'group_tools:cleanup:my_status:explain' => 'In de zijbalk op de groepsprofiel pagina is er een item welke je informatie toont over je groepslidmaatschap. Tevens wordt hier andere status informatie getoond. Je kunt deze zijbalk verbergen.',
  'group_tools:restrict_discussions:title' => 'Beperk het aanmaken van discussies',
  'group_tools:restrict_discussions:description' => 'Met deze instelling kan het aanmaken van discussies beperkt worden zodat alleen groepsbeheerders discussies kunnen aanmaken.',
  'group_tools:settings:fix:title' => 'Los problemen met groep toegang op',
  'group_tools:settings:fix:missing' => 'Er zijn %d gebruikers die lid zijn van een groep maar geen toegang hebben tot de content die gedeeld wordt met de groep.',
  'group_tools:settings:fix:excess' => 'Er zijn %d gebruikers die toegang hebben tot content van een groep waar ze geen lid meer van zijn.',
  'group_tools:settings:fix:without' => 'Er zijn %d groepen die niet de mogelijkheid hebben om content te delen met de leden.',
  'group_tools:settings:fix:all:description' => 'Los alle bovenstaande problemen op.',
  'group_tools:settings:fix_it' => 'Los dit op',
  'group_tools:settings:fix:all' => 'Los alles op',
  'group_tools:settings:fix:nothing' => 'Er zijn geen problemen gevonden met de groepen op je site!',
  'group_tools:action:fix_acl:error:input' => 'Ongeldige optie, je kunt %s niet oplossen',
  'group_tools:action:fix_acl:error:missing:nothing' => 'Er zijn geen ontbrekende gebruikers gevonden in de groep ACLs',
  'group_tools:action:fix_acl:error:excess:nothing' => 'Er zijn geen ongeldige gebruikers gevonden in de groep ACLs',
  'group_tools:action:fix_acl:error:without:nothing' => 'Er zijn geen groepen gevonden zonder ACL',
  'group_tools:action:fix_acl:success:missing' => 'Er zijn %d gebruikers toegevoegd aan de groep ACLs',
  'group_tools:action:fix_acl:success:excess' => 'Er zijn %d gebruikers verwijderd uit de groep ACLs',
  'group_tools:action:fix_acl:success:without' => 'Er zijn %d groep ACLs aangemaakt',
  'group_tools:action:groups:decline_email_invitation:error:delete' => 'Er is een fout opgetreden tijdens het verwijderen van de uitnodiging',
  'widgets:index_groups:filter:field' => 'Filter de groepen obv een profielveld',
  'widgets:index_groups:filter:value' => 'met de waarde',
  'widgets:index_groups:filter:no_filter' => 'Geen filter',
  'group_tools:notifications:title' => 'Groepsmeldingen',
  'group_tools:notifications:description' => 'Deze groep heeft %s leden, hiervan hebben %s meldingen op activiteit in deze groep ingeschakeld. Hieronder kun je dit aanpassen voor alle leden van de groep.',
  'group_tools:notifications:disclaimer' => 'Met grote groepen kan dit enige tijd duren.',
  'group_tools:notifications:enable' => 'Schakel meldingen voor iedereen in',
  'group_tools:notifications:disable' => 'Schakel meldingen voor iedereen uit',
  'group_tools:action:notifications:error:toggle' => 'Ongeldige schakel optie',
  'group_tools:action:notifications:success:disable' => 'De meldingen zijn voor iedereen uitgeschakeld',
  'group_tools:action:notifications:success:enable' => 'De meldingen zijn voor iedereen ingeschakeld',
  'group_tools:explain' => 'Uitleg',
  'group_tools:default:access:group' => 'Alleen groepsleden',
  'group_tools:settings:invite:title' => 'Groepsuitnodiging instellingen',
  'group_tools:settings:management:title' => 'Algemene groepsinstellingen',
  'group_tools:settings:default_access:title' => 'Standaard groep toegang',
  'group_tools:settings:default_access' => 'Wat moet het standaard toegangsniveau zijn van nieuwe content in groepen op deze site',
  'group_tools:settings:default_access:disclaimer' => '<b>LET OP:</b> dit werkt alleen indien je <a href="https://github.com/Elgg/Elgg/pull/253" target="_blank">https://github.com/Elgg/Elgg/pull/253</a> hebt toegepast op je Elgg installatie.',
  'group_tools:cleanup:search' => 'Verberg het zoeken in de groep',
  'group_tools:cleanup:search:explain' => 'Op de groepspagina staat een zoekvenster. Je kunt ervoor kiezen om die onzichtbaar te maken.',
  'group_tools:cleanup:featured_sorting' => 'Hoe groepen gesorteerd kunnen worden',
  'group_tools:cleanup:featured_sorting:time_created' => 'Laatste eerst',
  'group_tools:cleanup:featured_sorting:alphabetical' => 'Alfabetisch',
  'group_tools:default_access:title' => 'Standaard groepstoegang',
  'group_tools:default_access:description' => 'Hier kun je aangeven wat het standaard toegangsniveau moet zijn van nieuwe content in je groep.',
  'group_tools:actions:cleanup:success' => 'De instellingen voor het opschonen zijn opgeslagen',
  'group_tools:actions:restrict_discussions:success' => 'De instelling voor het beperken van discussies is opgeslagen.',
  'group_tools:actions:default_access:success' => 'Het standaard toegangsniveau voor de groep is succesvol opgeslagen',
  'widgets:discussion:settings:group_only' => 'Toon alleen discussies uit je eigen groepen',
  'widgets:discussion:more' => 'Bekijk meer discussies',
  'widgets:discussion:description' => 'Toont de meest recente discussies',
  'widgets:index_groups:description' => 'Toont groepen uit de community',
  'widgets:index_groups:show_members' => 'Toon aantal groepsleden',
  'widgets:index_groups:featured' => 'Toon enkel aangeraden groepen',
  'widgets:group_news:settings:group_icon_size' => 'Grootte van het logo van de groep',
  'widgets:group_news:settings:group_icon_size:small' => 'Klein',
  'widgets:group_news:settings:group_icon_size:medium' => 'Middel',
  'widgets:group_news:title' => 'Groepsnieuws',
  'widgets:group_forum_topics:description' => 'Toon de meest recente discussies',
  'widgets:group_news:description' => 'Toon de nieuwste 5 blogs uit verschillende groepen',
  'widgets:group_news:no_projects' => 'Er zijn geen groepen geconfigureerd',
  'widgets:group_news:no_news' => 'Geen blogs voor deze groep',
  'widgets:group_news:settings:project' => 'Groep',
  'widgets:group_news:settings:no_project' => 'Selecteer een groep',
  'widgets:group_news:settings:blog_count' => 'Maximum aantal blogs',
  'group_tools:group:invite:friends:select_all' => 'Selecteer alle contacten',
  'group_tools:group:invite:friends:deselect_all' => 'Annuleer alle contacten',
  'group_tools:group:invite:users:all' => 'Nodig alle site gebruikers uit voor deze groep',
  'widgets:featured_groups:description' => 'Toont een lijst met willekeurig geselecteerde aangeraden groepen',
  'widgets:featured_groups:edit:show_random_group' => 'Toon een willekeurige niet aangeraden groep',
  'group_tools:joinrequest:already' => 'Lidmaatschap verzoek intrekken',
  'group_tools:joinrequest:already:tooltip' => 'Je hebt reeds verzocht lid te worden van deze groep, klik hier om dit verzoek in te trekken',
  'group_tools:menu:invitations' => 'Beheer uitnodigingen',
  'group_tools:admin_transfer:title' => 'Draag het beheer van deze groep over',
  'group_tools:groups:invite:title' => 'Nodig gebruikers uit voor deze groep',
  'group_tools:action:invite:error:invite' => 'Er zijn geen gebruikers uitgenodigd (%s waren al uitgenodigd, %s waren al lid)',
  'group_tools:action:invite:error:add' => 'Er zijn geen gebruikers uitgenodigd (%s waren al uitgenodigd, %s waren al lid)',
  'group_tools:action:invite:success:invite' => 'Er zijn %s gebruikers uitgenodigd (%s waren al uitgenodigd, %s waren al lid)',
  'group_tools:action:invite:success:add' => 'Er zijn %s gebruikers toegevoegd (%s waren al uitgenodigd, %s waren al lid)',
  'group_tools:profile_widgets:title' => 'Toon de widgets in deze groep aan niet-gebruikers',
  'group_tools:profile_widgets:description' => 'Dit is een besloten groep. De widgets op de voorpagina worden dus niet getoond aan gebruikers die geen lid zijn van de groep. Als je wel widgets wil tonen op de voorpagina, dan kun je dat hieronder aanpassen. Vervolgens kun je per widget bepalen door wie deze gezien kan worden door de instellingen van die widget te wijzigen.',
  'group_tools:profile_widgets:option' => 'Toon de widgets ook aan gebruikers die geen lid zijn van de groep:',
  'group_tools:action:error:save' => 'Er is een fout opgetreden tijdens het opslaan van de instellingen',
  'group_tools:action:success' => 'De instellingen zijn opgeslagen',
  'group_tools:groups:sorting:open' => 'Open',
  'group_tools:groups:sorting:closed' => 'Besloten',
  'widgets:group_river_widget:view:noactivity' => 'Er zijn geen activiteiten beschikbaar.',
  'widgets:group_river_widget:show_more' => 'Meer activiteiten',
  'group_tools:group:edit:profile' => 'Groep profiel / tools',
  'group_tools:group:edit:other' => 'Andere opties',
  'widgets:group_river_widget:edit:no_groups' => 'Je moet lid zijn van ten minste een groep om van deze widget gebruik te kunnen maken.',
  'group_tools:mail:form:js:members' => 'Selecteer minstens één lid om het bericht aan te versturen',
  'group_tools:action:fix_auto_join:success' => 'Groep lidmaatschap gerepareerd: %s nieuwe leden, %s waren reeds lid en %s fouten',
  'group_tools:group:invite:resend' => 'Uitnodiging opnieuw versturen naar gebruikers die al zijn uitgenodigd',
  'group_tools:clear_selection' => 'Selectie wissen',
  'group_tools:all_members' => 'Alle leden',
  'group_tools:mail:form:recipients' => 'Aantal geselecteerde ontvangers',
  'group_tools:mail:form:members:selection' => 'Selecteer individuele gebruikers',
  'group_tools:mail:form:js:description' => 'Geef een bericht op',
  'group_tools:remove' => 'Verwijder',
  'group_tools:notify:transfer:subject' => 'Beheer van de groep %s is aan je overgedragen',
  'group_tools:notify:transfer:message' => 'Beste %s,

%s heeft het beheer van de groep %s aan je overgedragen.

Om de groep te bezoeken klik op onderstaande link:
%s',
  'group_tools:notify:newadmin:subject' => 'Een nieuwe beheerder is toegevoegd aan %s',
  'group_tools:notify:newadmin:message' => 'Hallo,

%s is toegevoegd als groepsbeheerder aan %s.

Om de groep te bezoeken klik op de onderstaande link:
%s',
  'widgets:group_invitations:title' => 'Groepsuitnodigingen',
  'widgets:group_invitations:description' => 'Toont de nog niet beantwoorde groepsuitnodigingen voor deze gebruiker',
  'widgets:group_members:title' => 'Groepsleden',
  'widgets:group_members:description' => 'Bekijk de leden van deze groep',
  'widgets:group_members:edit:num_display' => 'Hoeveel geberuikers moeten er getoond worden',
  'widgets:group_members:view:no_members' => 'Er zijn geen groepsleden gevonden',
  'widgets:group_river_widget:title' => 'Activiteiten in de groep',
  'widgets:group_river_widget:description' => 'Toon de activiteiten in deze groep in een widget',
  'widgets:group_river_widget:edit:num_display' => 'Aantal activiteiten',
  'widgets:group_river_widget:edit:group' => 'Selecteer een groep',
  'widgets:group_river_widget:view:not_configured' => 'Deze widget is nog niet geconfigureerd',
  'widgets:group_river_widget:view:more' => 'Activiteiten in de groep %s',
  'group_tools:decline' => 'Afkeuren',
  'group_tools:revoke' => 'Intrekken',
  'group_tools:add_users' => 'Gebruikers direct toevoegen (alleen voor beheerders)',
  'group_tools:in' => 'in',
  'group_tools:menu:mail' => 'Stuur een e-mail aan de leden',
  'group_tools:settings:admin_transfer' => 'Sta het overdragen van groepsbeheerder toe',
  'group_tools:settings:admin_transfer:admin' => 'Alleen de Pleio-beheerder heeft deze rechten',
  'group_tools:settings:admin_transfer:owner' => 'Groepsbeheerder en deelsitebeheerders',
  'group_tools:settings:multiple_admin' => 'Sta meerdere groepsbeheerders toe',
  'group_tools:settings:invite' => 'Sta toe dat alle gebruikers worden uitgenodigd (niet alleen contacten)',
  'group_tools:settings:invite_email' => 'Nodig gebruikers uit op basis van e-mailadres',
  'group_tools:settings:invite_csv' => 'Nodig gebruikers uit op basis van een CSV-bestand',
  'group_tools:settings:mail' => 'Sta groepsmail toe (alleen beheerders hebben deze rechten)',
  'group_tools:settings:listing:default' => 'Standaard groep overzicht tab',
  'group_tools:settings:listing:available' => 'Beschikbare groepsoverzicht tabs',
  'group_tools:settings:search_index' => 'Toestaan dat besloten groepen worden geïndexeerd door zoekmachines',
  'group_tools:settings:auto_notification' => 'Schakel groepsmeldingen automatisch in bij het lid worden van een groep',
  'group_tools:groups:invite:body' => 'Beste %s,

%s heeft je uitgenodigd om lid te worden van de groep \'%s\'.
%s

Klik hieronder om uw uitnodigingen te bekijken en desgewenst te bevestigen: 
%s',
  'group_tools:groups:invite:add:subject' => 'Je bent toegevoegd aan de groep %s',
  'group_tools:groups:invite:add:body' => 'Beste  %s,

%s heeft je toegevoegd aan de groep  %s.
%s

Om de groep te bekijken klik op deze link
%s',
  'group_tools:groups:invite:email:subject' => 'Je bent uitgenodigd voor de groep %s.',
  'group_tools:groups:invite:email:body' => 'Beste,

%s heeft je uitgenodigd om deel te nemen aan de groep %s op %s.
%s

Klik op de volgende link om de uitnodiging te accepteren:

%s

Je kunt ook naar Profiel -> Groep uitnodigingen gaan en daar de volgende code invoeren: %s',
  'group_tools:admin_transfer:transfer' => 'Draag het beheer over aan:',
  'group_tools:admin_transfer:myself' => 'Mijzelf',
  'group_tools:admin_transfer:submit' => 'Overdragen',
  'group_tools:admin_transfer:no_users' => 'Geen gebruikers of contacten om het beheer aan over te dragen',
  'group_tools:admin_transfer:confirm' => 'Weet je zeker dat je het beheer wilt overdragen?',
  'group_tools:multiple_admin:group_admins' => 'Groepsbeheerders',
  'group_tools:multiple_admin:profile_actions:remove' => 'Verwijder groepsbeheerder',
  'group_tools:multiple_admin:profile_actions:add' => 'Voeg groepsbeheerder toe',
  'group_tools:multiple_admin:group_tool_option' => 'Wil je gebruik maken van de mogelijkheid om andere groepsbeheerders toe te voegen?',
  'group_tools:mail:message:from' => 'Uit de groep',
  'group_tools:mail:title' => 'Stuur een e-mail naar alle groepsleden',
  'group_tools:mail:form:title' => 'Onderwerp',
  'group_tools:mail:form:description' => 'Bericht',
  'group_tools:groups:invite' => 'Gebruikers uitnodigen',
  'group_tools:group:invite:users' => 'Alle leden van deze site',
  'group_tools:group:invite:users:description' => 'Vul een naam of gebruikersnaam van een site gebruiker in en selecteer hem/haar uit de lijst',
  'group_tools:group:invite:email' => 'Op basis van een e-mailadres',
  'group_tools:group:invite:email:description' => 'Vul een e-mailadres in en selecteer deze uit de lijst!',
  'group_tools:group:invite:csv' => 'Gebruik een CSV-bestand',
  'group_tools:group:invite:csv:description' => 'Je kunt een CSV-bestand uploaden om gebruikers uit te nodigen.<br>
De structuur van het bestand moet zijn: naam;e-mailadres<br>
Er mag geen regel boven staan met de namen van de kolommen.<br>',
  'group_tools:group:invite:text' => 'Persoonlijke boodschap (optioneel)',
  'group_tools:group:invite:add:confirm' => 'Bent je er zeker dat je de gebruiker direct toe wilt voegen?',
  'group_tools:groups:invitation:code:title' => 'Groepsuitnodiging per e-mail',
  'group_tools:groups:invitation:code:description' => 'Als je een uitnodiging ontvangen hebt voor een groep via e-mail, kun je hier de uitnodiging code invoeren  om de uitnodiging te accepteren. Als je op de link klikt in de uitnodiging per e-mail de code wordt voor u ingevuld.',
  'group_tools:groups:membershipreq:requests' => 'Aanvragen voor lidmaatschap',
  'group_tools:groups:membershipreq:invitations' => 'Openstaande uitnodigingen',
  'group_tools:groups:membershipreq:invitations:none' => 'Geen openstaande uitnodigingen',
  'group_tools:groups:membershipreq:invitations:revoke:confirm' => 'Weet je zeker dat je deze uitnodiging wilt intrekken?',
  'group_tools:group:invitations:request' => 'Openstaande aanvragen voor lidmaatschap',
  'group_tools:group:invitations:request:revoke:confirm' => 'Weet je zeker dat je de lidmaatschap aanvragen wilt intrekken?',
  'group_tools:group:invitations:request:non_found' => 'Er zijn geen openstaande aanvragen voor lidmaatschap op dit moment',
  'group_tools:groups:sorting:alphabetical' => 'Alfabetisch',
  'group_tools:groups:sorting:ordered' => 'Gesorteerd',
  'group_tools:action:error:input' => 'Ongeldige invoer om deze actie uit te voeren',
  'group_tools:action:error:entities' => 'De gegeven GUID resulteerde niet in de juiste entiteiten',
  'group_tools:action:error:entity' => 'De gegeven GUID resulteerde niet in een juiste entiteit',
  'group_tools:action:error:edit' => 'Je hebt geen toegang tot de bepaalde entiteit',
  'group_tools:action:admin_transfer:error:access' => 'Je bent niet geautoriseerd om het eigenaarschap van deze groep over te dragen.',
  'group_tools:action:admin_transfer:error:self' => 'Je kunt eigenaarschap niet aan jezelf overdragen. Je bent al eigenaar.',
  'group_tools:action:admin_transfer:error:save' => 'Er is een onbekende fout opgetreden tijdens het opslaan van de groep. Excuus voor het ongemak. Probeer het opnieuw of raadpleeg anders de beheerder van Pleio (support@pleio.org)',
  'group_tools:action:admin_transfer:success' => 'Het eigenaarschap van de groep is overgedragen aan %s',
  'group_tools:action:toggle_admin:error:group' => 'De gegeven input resulteert niet in een groep of je kunt niets bewerken in deze groep of de gebruiker is geen lid',
  'group_tools:action:toggle_admin:error:remove' => 'Er is een onbekende fout opgetreden tijdens het verwijderen van de gebruiker als een groepsbeheerder',
  'group_tools:action:toggle_admin:error:add' => 'Er is een onbekende fout opgetreden tijdens het toevoegen van de gebruiker als een groepsbeheerder. Excuus voor het ongemak. Probeer het opnieuw of raadpleeg anders de beheerder van Pleio (support@pleio.org)',
  'group_tools:action:toggle_admin:success:remove' => 'De gebruiker is verwijderd als groepsbeheerder',
  'group_tools:action:toggle_admin:success:add' => 'De gebruiker is beheerder geworden van deze groep',
  'group_tools:action:mail:success' => 'Boodschap is verzonden',
  'group_tools:action:groups:email_invitation:error:input' => 'Geef een uitnodigingscode op',
  'group_tools:action:groups:email_invitation:error:code' => 'De opgegeven uitnodigingscode in niet meer geldig',
  'group_tools:action:groups:email_invitation:error:join' => 'Er is een onbekende fout opgetreden tijdens het lid worden van de groep %s, misschien ben je al lid',
  'group_tools:action:groups:email_invitation:success' => 'Je bent lid geworden van de groep',
  'group_tools:cleanup:title' => 'Groepsmenu opschonen',
  'group_tools:cleanup:description' => 'Hier kun je bepalen wat in het groepsmenu aan de rechterkant van het scherm wordt getoond. Als beheerder blijf je wel alle opties zien.',
  'group_tools:cleanup:owner_block' => 'Verberg de iconen bovenaan het menu',
  'group_tools:cleanup:owner_block:explain' => 'Bovenaan het menu staan enkele iconen. Het is mogelijk om daar extra links aan toe te voegen, bijvoorbeeld RSS-links',
  'group_tools:cleanup:actions' => 'Verberg de optie om lid te worden van deze groep',
  'group_tools:cleanup:actions:explain' => 'Afhankelijk van de groep instelling, kunnen gebruikers direct lid worden van de groep of lidmaatschap aanvragen.',
  'group_tools:cleanup:menu' => 'Verberg de overige menu-items',
  'group_tools:cleanup:menu:explain' => 'Verberg de overige menu links naar de verschillende groep functionaliteiten. De gebruikers kunnen alleen naar de verschillende functionaliteiten door gebruik te maken van de links in widgets.',
  'group_tools:cleanup:members' => 'Verberg het overzicht van groepsleden',
  'group_tools:cleanup:members:explain' => 'Op de profielpagina van de groep staat op de aangegeven plaats een lijst met leden van de groep. Je kunt deze verbergen.',
  'group_tools:cleanup:featured' => 'Toon de aangeraden groepen in het menu',
  'group_tools:cleanup:featured:explain' => 'Je kunt instellen dat op de voorpagina van je groep een lijst wordt getoond met aangeraden groepen',
  'group_tools:subpermissions' => 'Subgroepen',
  'group_tools:subpermissions:add' => 'Subgroep toevoegen',
  'group_tools:subpermissions:manage_members' => 'Leden beheren',
  'group_tools:subpermissions:add:created' => 'Subgroep is aangemaakt.',
  'group_tools:subpermissions:activate' => 'Subgroepen activeren',
  'group_tools:subpermissions:add:noname' => 'Subgroep naam is verplicht.',
  'group_tools:subpermissions:nosubpermission' => 'Deze subgroep bestaat niet.',
  'group_tools:subpermissions:nouser' => 'Gebruiker is geen lid van de groep.',
  'group_tools:subpermissions:delete:cantdelete' => 'Kan deze subgroep niet verwijderen.',
  'group_tools:subpermissions:member' => 'Naam',
  'group_tools:subpermissions:name' => 'Naam',
  'group_tools:subpermissions:notenabled' => 'Subgroepen zijn niet ingeschakeld.',
  'group_tools:subpermission' => 'Subgroep',
  'group_tools:subpermissions:delete:confirm' => 'Weet je zeker dat je deze subgroep wil verwijderen?',
  'group_tools:subpermissions:delete_member' => 'Verwijder uit subgroep',
  'group_tools:subpermissions:nosubpermissions' => 'Er zijn geen subgroepen beschikbaar.',
  'group_tools:subpermissions:select' => 'Selecteer',
  'group_tools:subpermissions:export' => 'Export',
);
add_translation("nl", $language);
