<?php
$language = array (
  'videolist' => 'Video\'s',
  'videolist:owner' => '%s\'s video\'s',
  'videolist:friends' => 'Video\'s van mijn contacten',
  'videolist:all' => 'Alle video\'s',
  'videolist:add' => 'Voeg een video toe',
  'videolist:group' => 'Video\'s',
  'groups:enablevideolist' => 'Groep video\'s inschakelen',
  'videolist:edit' => 'Video bewerken',
  'videolist:delete' => 'Verwijder deze video',
  'videolist:new' => 'Voeg een video toe',
  'videolist:notification' => '%s plaatste een nieuwe video:

%s
%s

Bekijk de video en reageer:
%s',
  'videolist:delete:confirm' => 'Weet je zeker dat je deze video wil verwijderen?',
  'item:object:videolist_item' => 'Video\'s',
  'videolist:nogroup' => 'Deze groep heeft nog geen video\'s',
  'videolist:more' => 'Meer video\'s',
  'videolist:none' => 'Deze groep heeft nog geen video\'s',
  'river:create:object:videolist_item' => '%s voegde de video %s toe',
  'river:update:object:videolist_item' => '%s heeft de video %s bijgewerkt',
  'river:comment:object:videolist_item' => '%s heeft een reactie toegevoegd aan %s',
  'videolist:title' => 'Titel',
  'videolist:description' => 'Beschrijving',
  'videolist:video_url' => 'Vul het internetadres van de video in',
  'videolist:access_id' => 'Wie kan zien dat jij deze video hebt toegevoegd?',
  'videolist:tags' => 'Voeg tags toe',
  'videolist:error:no_save' => 'Het lukte niet om de video op te slaan. Excuus voor het ongemak. Kun je het iets later nog een keer proberen?',
  'videolist:saved' => 'Je video is opgeslagen',
  'videolist:deleted' => 'Je video is verwijderd',
  'videolist:deletefailed' => 'Het lukte niet om de video te verwijderen. Excuus voor het ongemak. Kun je het later nog een keer proberen?',
  'videolist:num_videos' => 'Het aantal video\'s dat je wil tonen',
  'videolist:widget:description' => 'Toon je persoonlijke video-overzicht van YouTube',
  'videolist:continue' => 'Verder',
  'videolist:notification:subject' => 'Een nieuwe video is toegevoegd',
);
add_translation("nl", $language);
