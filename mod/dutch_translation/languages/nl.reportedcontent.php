<?php
$language = array (
  'reportedcontent:report' => 'Dit vind ik niet OK!',
  'reportedcontent:title' => 'Titel',
  'reportedcontent:success' => 'Je melding is verstuurd naar de beheerder',
  'reportedcontent:objecttitle' => 'Paginatitel',
  'reportedcontent:objecturl' => 'Pagina-url',
  'reportedcontent:this' => 'Misbruik melden',
  'reportedcontent:this:tooltip' => 'Meld misbruik en spammers van en op Pleio aan de beheerders',
	'reportedcontent:usersettings:notify:description' => "Hou mij op de hoogte wanneer er ongewenste content gemeld wordt.",
	'reportedcontent:usersettings:notify:subject' => "Een nieuw item is gerapporteerd",
	'reportedcontent:usersettings:notify:message' => "Beste,

%s heeft een nieuw item gerapporteerd. Bekijk via deze link het rapport:
%s",
);
add_translation("nl", $language);
