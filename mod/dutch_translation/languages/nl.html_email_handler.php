<?php
$language = array (
  'html_email_handler' => 'Het versturen van e-mails in HTML',
  'html_email_handler:theme_preview:menu' => 'HTML-berichten',
  'html_email_handler:settings:notifications:description' => 'Als je deze optie aanzet, zullen alle berichten naar gebruikers van je site in een HTML-opmaak zijn.',
  'html_email_handler:settings:notifications' => 'Gebruik dit als standaard bericht.',
  'html_email_handler:settings:notifications:subtext' => 'Verstuur berichten in HTML-opmaak',
  'html_email_handler:settings:sendmail_options' => 'Aanvullende instellingen voor het verzenden van mails (optioneel)',
  'html_email_handler:settings:sendmail_options:description' => 'Je kunt meer mogelijkheden instellen bij het verzenden van mail, bijvoorbeeld -f%s (om ervoor te zorgen dat mails niet als spam worden aangemerkt)',
  'html_email_handler:notification:footer:settings' => '%sHier%s kun je instellen waarover je mail wilt ontvangen.',
);
add_translation("nl", $language);
