<?php
$language = array (
  'account_removal' => 'Account verwijderen',
  'account_removal:disable:default' => 'Deze gebruiker heeft zijn account op Pleio verwijderd',
  'account_removal:admin:settings:user_options:disable' => 'Uitschakelen',
  'account_removal:admin:settings:user_options:remove' => 'Verwijderen',
  'account_removal:admin:settings:user_options:disable_and_remove' => 'Uitschakelen en verwijderen',
  'account_removal:admin:settings:groupadmins_allowed' => 'Groepsbeheerders kunnen hun account uitschakelen / verwijderen',
  'account_removal:user:error:admin' => 'Beheerders niet toestaan om hun account uit te schakelen / te verwijderen',
  'account_removal:user:error:user' => 'Je kunt alleen je eigen account uitschakelen / verwijderen',
  'account_removal:user:error:no_user' => 'De gebruiker is niet gevonden',
  'account_removal:forms:user:user_options:description:disable' => 'Hier kun je je account op Pleio <b>uitschakelen</b>. Uitschakelen betekent dat je profiel niet meer zichtbaar is en niet meer gevonden kan worden. Je content (blogs, bestanden, wiki\'s) is nog wel zichtbaar voor andere gebruikers!',
  'account_removal:forms:user:user_options:description:remove' => 'Hier kun je je account op Pleio <b>verwijderen</b>. Verwijderen betekent dat je profiel niet meer zichtbaar is en niet meer gevonden kan worden. Maar het betekent ook dat al je content (blogs, bestanden, wiki\'s) verwijderd wordt. Let op: deze handeling kan niet meer ongedaan gemaakt worden!',
  'account_removal:forms:user:user_options:description:disable_and_remove' => 'Hier kun je je account op Pleio uitschakelen of verwijderen.<br><br><b>Uitschakelen</b> betekent dat je profiel niet meer zichtbaar is en niet meer gevonden kan worden. Je content (blogs, bestanden, wiki\'s) blijft wel zichtbaar voor andere gebruikers.<br><br><b>Verwijderen</b> betekent dat je profiel niet meer zichtbaar is en niet meer gevonden kan worden. Maar het betekent ook dat al je content (blogs, bestanden, wiki\'s) verwijderd wordt. Let op: deze handeling kan niet meer ongedaan gemaakt worden!',
  'account_removal:forms:user:user_options:disable' => 'Account uitschakelen',
  'account_removal:forms:user:user_options:remove' => 'Account verwijderen',
  'account_removal:forms:user:required' => 'Verplicht in te vullen',
  'account_removal:forms:user:js:error:reason' => 'Geef een reden',
  'account_removal:forms:user:js:confirm:disable' => 'Weet je zeker dat je je account wil uitschakelen?',
  'account_removal:forms:user:js:confirm:remove' => 'Weet je zeker dat je je account wil verwijderen?',
  'account_removal:forms:user:error:no_user' => 'De gebruiker is niet gevonden',
  'account_removal:forms:user:error:group_owner' => 'Het is niet mogelijk om je account uit te schakelen of te verwijderen zolang je beheerder bent van een groep. Momenteel ben je nog beheerder van de volgende groep(en):',
  'account_removal:message:disable:subject' => 'Je wil je account op %s uitschakelen',
  'account_removal:message:disable:body' => 'Beste %s,

Je hebt aangegeven dat je je account wil uitschakelen. Dat betekent dat al je content zichtbaar blijft op de site, maar je kunt niet meer inloggen en je profiel wordt onzichtbaar gemaakt. Om te bevestigen dat je je account wil uitschakelen, klik op de volgende link: %s

Vriendelijke groet,

Pleio Community Master

Marcel Ziemerink',
  'account_removal:message:remove:subject' => 'Je wil je account op %s verwijderen',
  'account_removal:message:remove:body' => 'Beste %s,

Je hebt aangegeven dat je je account wil verwijderen. Dat betekent dat je niet meer kunt inloggen en je profiel en al je content wordt verwijderd. Om te bevestigen dat je je account wil verwijderen, klik op de volgende link: %s

Vriendelijke groet,

Pleio Community Master

Marcel Ziemerink',
  'account_removal:message:thank_you:remove:subject' => 'Dank dat je gebruik hebt gemaakt van %s',
  'account_removal:message:thank_you:remove:body' => 'Beste %s,

Dank dat je gebruik hebt gemaakt van %s. Als je toch weer een bijdrage wil leveren, dan kun je natuurlijk altijd weer een nieuwe account aanmaken.

Vriendelijke groet,

Pleio Community Master

Marcel Ziemerink',
  'account_removal:message:thank_you:disable:subject' => 'Dank dat je gebruik hebt gemaakt van %s',
  'account_removal:message:thank_you:disable:body' => 'Beste %s,

Dank dat je gebruik hebt gemaakt van %s. Als je toch weer een bijdrage wil leveren, dan kun je natuurlijk altijd een nieuwe account aanmaken of de beheerder vragen om je account opnieuw in te schakelen.

Vriendelijke groet,

Pleio Community Master

Marcel Ziemerink',
  'account_removal:actions:remove:error:user_guid:admin' => 'Beheerders mogen hun account niet uitschakelen / verwijderen',
  'account_removal:actions:remove:error:user_guid:user' => 'Je kunt alleen je eigen account uitschakelen / verwijderen',
  'account_removal:actions:remove:error:user_guid:unknown' => 'De gebruiker is niet gevonden',
  'account_removal:actions:remove:error:group_owner' => 'Het is nu niet mogelijk om je account uit te schakelen of te verwijderen omdat je nog beheerder bent van een groep.',
  'account_removal:actions:remove:error:reason' => 'Geef een reden',
  'account_removal:actions:remove:error:token_mismatch' => 'Helaas, de bevestigingscode klopt niet. Vraag een nieuwe bevestigingsmail aan',
  'account_removal:actions:remove:success:remove' => 'Je account is nu verwijderd',
  'account_removal:actions:remove:success:disable' => 'Je account is nu uitgeschakeld',
  'account_removal:actions:remove:success:request' => 'Je hebt een verzoek gedaan om je account uit te schakelen / te verwijderen. Er is een e-mail gestuurd om dit verzoek te bevestigen. Klik op de link in de e-mail om je verzoek te voltooien.',
  'account_removal:menu:title' => 'Account verwijderen',
  'account_removal:admin:settings:user_options' => 'Selecteer de beschikbare opties voor het verwijderen van het account',
  'account_removal:admin:settings:reason_required' => 'Account verwijderen reden nodig voor de gebruiker',
  'account_removal:user:title' => 'Account verwijderen',
  'account_removal:forms:user:reason' => 'Wat is de reden voor het verwijderen van dit account?',
  'account_removal:forms:user:user_options:description:general' => 'Als je dit account verwijderd ontvang je een e-mail met verdere instructies, of neem contact op met <a href="mailto:support@pleio.org">support@pleio.org</a>.',
  'account_removal:actions:remove:error:type_match' => 'Er ging iets niet goed, neem contact op met <a href="mailto:support@pleio.org">support@pleio.org</a>.',
);
add_translation("nl", $language);
