<?php
$language = array (
  'global_tags:settings:tags' => 'Configureren van standaard tags voor gebruik op deze site (komma gescheiden)',
  'global_tags:group:title' => 'Tags die zijn gerelateerd aan een groep',
  'global_tags:group:tags' => 'Configureren van standaard tags voor gebruik met inhoud in deze groep (komma gescheiden)',
  'global_tags:group:new_content_tags' => 'Stel tags in voor nieuwe content in deze groep (komma gescheiden)',
  'global_tags:action:group:save' => 'Opgeslagen tags van een groep',
);
add_translation("nl", $language);
