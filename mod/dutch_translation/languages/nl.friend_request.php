<?php
$language = array (
  'friend_request' => 'Contactverzoek',
  'friend_request:menu' => 'Contactverzoeken',
  'friend_request:title' => 'Contactverzoeken voor: %s',
  'friend_request:new' => 'Nieuwe contactverzoeken',
  'friend_request:friend:add:pending' => 'Uitstaand contactverzoek',
  'friend_request:newfriend:subject' => '%s wil een contact van je worden!',
  'friend_request:newfriend:body' => '%s wil een contact van jou worden! Er wordt echter gewacht op je goedkeuring, dus meld je nu aan om het verzoek goed te keuren.

Je kunt je openstaande verzoeken bekijken op:
%s

(Controleer of je bent aangemeld voor je op de link klikt, anders word je doorverwezen naar de aanmeldpagina)


Je kunt niet antwoorden op deze e-mail.',
  'friend_request:add:failure' => 'Excuus, er is iets verkeerd gegaan. Zou je het nog een keer kunnen proberen?',
  'friend_request:add:successful' => 'Je wil een contact worden van %s. Dit verzoek moet echter nog door hem/haar worden goedgekeurd.',
  'friend_request:add:exists' => 'Je hebt al eens verzocht om een contact te worden van %s.',
  'friend_request:approve' => 'Toestaan',
  'friend_request:approve:successful' => '%s is nu een contact',
  'friend_request:approve:fail' => 'Er is iets misgegaan bij het contact leggen met %s',
  'friend_request:decline' => 'Afwijzen',
  'friend_request:decline:subject' => '%s heeft je verzoek om een contact te worden afgewezen',
  'friend_request:decline:message' => 'Beste %s,

%s heeft je verzoek om een contact te worden afgewezen.',
  'friend_request:decline:success' => 'Je hebt het verzoek om een contact te worden afgewezen.',
  'friend_request:decline:fail' => 'Er ging iets mis bij het afwijzen van het verzoek. Zou je het nog een keer kunnen proberen?',
  'friend_request:revoke' => 'Intrekken',
  'friend_request:revoke:success' => 'Het verzoek om een contact te worden is ingetrokken.',
  'friend_request:revoke:fail' => 'Er ging iets mis bij het intrekken van het verzoek. Zou je het nog een keer kunnen proberen?',
  'friend_request:received:title' => 'Ontvangen contactverzoeken',
  'friend_request:received:none' => 'Geen contactverzoeken gevonden die wachten op goedkeuring',
  'friend_request:sent:title' => 'Verzonden contactverzoeken',
  'friend_request:sent:none' => 'Geen openstaande verzonden contactverzoeken die nog moeten worden goedgekeurd',
  'friend_request:approve:subject' => '%s heeft je contactverzoek geaccepteerd',
  'friend_request:approve:message' => 'Beste %s,

%s heeft je contactverzoek geaccepteerd.',
);
add_translation("nl", $language);
