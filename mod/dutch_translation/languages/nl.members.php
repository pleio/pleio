<?php
$language = array (
  'members:label:newest' => 'Nieuwste',
  'members:label:popular' => 'Populair',
  'members:label:online' => 'Online',
  'members:labels:alpha' => 'Alfabetisch',
  'members:search' => 'Zoek gebruikers',
  'members:title:search' => 'Gebruikers gevonden voor %s',
);
add_translation("nl", $language);
