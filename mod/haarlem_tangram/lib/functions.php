<?php
/**
 * All helper functions are bundled here
 */

/**
 * Get a plugin setting
 *
 * @param string $setting the name of the setting
 *
 * @return false|mixed
 */
function haarlem_tangram_get_setting($setting) {
	static $settings_cache;

	if (empty($setting)) {
		return false;
	}

	if (!isset($settings_cache)) {
		$settings_cache = [
			// optional defaults
		];

		$plugin = elgg_get_plugin_from_id('haarlem_tangram');
		$settings = $plugin->getAllSettings();
		if (!empty($settings)) {
			foreach ($settings as $key => $value) {
				$settings_cache[$key] = $value;
			}
		}
	}

	return elgg_extract($setting, $settings_cache, false);
}

function haarlem_tangram_get_xml() {
	$site_guid = elgg_get_config('site_guid');

	if (is_memcache_available()) {
		$memcache = new ElggMemcache("tangram_{$site_guid}");
	}

	if ($memcache) {
		$data = $memcache->load("cached_xml");
		if ($data) {
			return simplexml_load_string($data);
		}
	}

	$tangram_url = haarlem_tangram_get_setting('tangram_url');
	if (empty($tangram_url)) {
		return;
	}

	elgg_set_plugin_setting('tangram_last_update', time(), 'haarlem_tangram');

	$ch = curl_init($tangram_url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_TIMEOUT, 4);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

	$content = curl_exec($ch);
	$curl_info = curl_getinfo($ch);

	curl_close($ch);

	if (elgg_extract('http_code', $curl_info) !== 200 || stristr(elgg_extract('content_type', $curl_info), 'text/xml') === false) {
		return;
	}

	if ($memcache) {
		$memcache->save("cached_xml", $content, 60*60);
	}

	elgg_set_plugin_setting('tangram_last_update_success', time(), 'haarlem_tangram');

	return simplexml_load_string($content);
}

/**
 * Get all interal vacancies
 *
 * @return false|TangramVacancy[]
 */
function haarlem_tangram_get_internal_vacancies() {
	$result = false;

	$xml = haarlem_tangram_get_xml();
	if (empty($xml)) {
		return false;
	}

	foreach ($xml->Vacature as $vacancy) {
		// is it interal
		if (empty($vacancy->Administratie->Datum_publ_internstart)) {
			continue;
		}

		$start_date = strtotime($vacancy->Administratie->Datum_publ_internstart);
		if ($start_date > time()) {
			// no yet open
			continue;
		}

		// check end date
		if (!empty($vacancy->Administratie->Datum_publ_internstop)) {
			$end_date = strtotime($vacancy->Administratie->Datum_publ_internstop);
			if ($end_date < time()) {
				// already ended
				continue;
			}
		}

		$result[] = haarlem_tangram_xml_vacancy_to_entity($vacancy);
	}

	return $result;
}

/**
 * Get all external vacancies
 *
 * @return false|TangramVacancy[]
 */
function haarlem_tangram_get_external_vacancies() {
	$result = false;

	$xml = haarlem_tangram_get_xml();
	if (empty($xml)) {
		return false;
	}

	if (!is_array($xml->Vacature)) {
		return false;
	}

	foreach ($xml->Vacature as $vacancy) {
		// is it interal
		if (empty($vacancy->Administratie->Datum_publ_bpstart)) {
			continue;
		}

		$start_date = strtotime($vacancy->Administratie->Datum_publ_bpstart);
		if ($start_date > time()) {
			// no yet open
			continue;
		}

		// check end date
		if (!empty($vacancy->Administratie->Datum_publ_stop)) {
			$end_date = strtotime($vacancy->Administratie->Datum_publ_stop);
// 			if ($end_date < time()) {
// 				// already ended
// 				continue;
// 			}
		}

		$result[] = haarlem_tangram_xml_vacancy_to_entity($vacancy);
	}

	return $result;
}

/**
 * Convert the xml vacancy to an ElggEntity
 *
 * @param SimpleXMLElement $vacancy the XML vacancy
 *
 * @return false|TangramVacancy
 */
function haarlem_tangram_xml_vacancy_to_entity($vacancy) {

	if (empty($vacancy) || !($vacancy instanceof SimpleXMLElement)) {
		return false;
	}

	$entity = new TangramVacancy();
	$entity->setXmlSource($vacancy);

	if (!empty($vacancy->Administratie->Datum_gewijzigd)) {
		$entity->last_updated = strtotime($vacancy->Administratie->Datum_gewijzigd);
	}

	$entity->title = $vacancy->Vacaturetitel;
	$entity->description = $vacancy->Functie->Omschrijving;



	return $entity;
}

/**
 * Find a vacancy in the xml
 *
 * @param stirng $vacaturenummer the vacancy number
 *
 * @return false|TangramVacancy
 */
function haarlem_tangram_find_vacancy($vacaturenummer) {

	$xml = haarlem_tangram_get_xml();
	if (empty($xml)) {
		return false;
	}

	foreach ($xml->Vacature as $vacancy) {

		if ((string) $vacancy->Vacaturenummer !== $vacaturenummer) {
			continue;
		}

		return haarlem_tangram_xml_vacancy_to_entity($vacancy);
	}

	return false;
}

/**
 * Get the configured page_owner_guid from the plugin settings.
 *
 * The returned int is a valid user/group
 *
 * @return false|int
 */
function haarlem_tangram_get_page_owner_guid() {

	$page_owner_guid = (int) haarlem_tangram_get_setting('page_owner_guid');
	if ($page_owner_guid <= 0) {
		return false;
	}

	$page_owner = get_entity($page_owner_guid);
	if (!($page_owner instanceof ElggUser) && !($page_owner instanceof ElggGroup)) {
		return false;
	}

	return $page_owner_guid;
}
