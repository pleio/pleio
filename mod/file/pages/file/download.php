<?php
/**
 * Elgg file download.
 *
 * @package ElggFile
 */

use Pleio\Helpers;

// Get the guid
$file_guid = get_input("guid");

// Get the file
$file = get_entity($file_guid);


if (elgg_is_active_plugin("pleio_template")) {
	if ($file->container_guid) {
		$container = get_entity($file->container_guid);
		if ($container instanceof \ElggGroup && $container->membership === ACCESS_PRIVATE && Helpers::getGroupMembership($container) != 'joined') {
			throw new Exception("not_member_of_closed_group");
		}
	} 
}

if (!$file) {
	register_error(elgg_echo("file:downloadfailed"));
	forward();
}

$filename = $file->originalfilename;
if (!$filename) {
	$pathinfo = pathinfo($file->getFilenameOnFilestore());
	if ($pathinfo) {
		$filename = $pathinfo['basename'];
	}
}

$mime = $file->getMimeType();
if (!$mime && $pathinfo) {
	switch ($pathinfo["extension"]) {
		case "jpg":
			$mime = "image/jpeg";
			break;
	}
}

$inlines = [
	"image/jpeg",
	"image/png",
	"application/pdf"
];

// fix for IE https issue
header("Pragma: public");

header("Content-type: $mime");
if (in_array($mime, $inlines)) {
	header("Content-Disposition: inline; filename=\"$filename\"");
} else {
	header("Content-Disposition: attachment; filename=\"$filename\"");
}

ob_clean();
flush();
readfile($file->getFilenameOnFilestore());
exit;
