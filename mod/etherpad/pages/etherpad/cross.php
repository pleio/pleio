<?php

$session_id = get_input("s");
$hash = get_input("h");

$sessionID = etherpad_decode_cross_domain_session($session_id, $hash);

if (!empty($sessionID)) {
	$validUntil = mktime(date("H"), date("i") + 5, 0, date("n"), date("j"), date("Y")); // 5 minutes in the future
	
	setcookie("sessionID", $sessionID, $validUntil, "/", ".pleio.nl");
}

header("Content-type: image/png");
header("Content-length: 0");
