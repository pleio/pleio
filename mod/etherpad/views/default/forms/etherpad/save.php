<?php
/**
 * Pad save form body
 *
 * @package ElggPad
 */

$variables = elgg_get_config('etherpad');
$entity = elgg_extract("entity", $vars);
foreach ($variables as $name => $type) {
?>
<div>
	<label><?php echo elgg_echo("etherpad:$name") ?></label>
	<?php
		if ($type != 'longtext') {
			echo '<br />';
		}
	?>
	<?php 
	
		$field_options = array("name" => $name);
		if($entity){
			$field_options["value"] = $entity->$name;
		}
		
		echo elgg_view("input/$type", $field_options);
	?>
</div>
<?php
}

$cats = elgg_view('input/categories', $vars);
if (!empty($cats)) {
	echo $cats;
}


echo '<div class="elgg-foot">';
echo elgg_view('input/hidden', array(
	'name' => 'container_guid',
	'value' => elgg_get_page_owner_guid(),
));
if($entity){
	echo elgg_view('input/hidden', array(
		'name' => 'page_guid',
		'value' => $entity->getGUID(),
	));
}

echo elgg_view('input/submit', array('value' => elgg_echo('save')));

echo '</div>';
