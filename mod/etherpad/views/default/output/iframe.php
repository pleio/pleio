<?php
/**
 * Display a page in an embedded window
 *
 * @package Elgg
 * @subpackage Core
 *
 * @uses $vars['value'] Source of the page.
 * @uses $vars['type'] type of iframe.
 */
  
//This is where theme makers can override iframe dimensions.
$fullsrc = "\"" . $vars['value'] . "\"";
$type = $vars['type'];
if ($type) {
	switch ($type) {
		case "etherpad" :
			$fullsrc .= " width='100%' height=500";
			
			if (!empty($_SESSION["etherpad_session"])) {
				$url = etherpad_get_cross_domain_url($_SESSION["etherpad_session"]);
				echo elgg_view("output/img", array("src" => $url, "class" => "hidden"));
			}
			break;
	}
}
?>
<iframe src=<?php echo $fullsrc; ?>>
</iframe>
