<?php
/**
 * Custom class for Newsletters
 *
 * @var SUBTYPE		The subtype of the newsletters
 * @var SEND_TO		A relationship to link users to this newsletter
 *
 * @package Newsletter
 */
class Newsletter extends ElggObject {
	const SUBTYPE = "newsletter";
	const SEND_TO = "send_to";
	
	/**
	 * Clones the newsletter
	 *
	 * @return void
	 *
	 * @see ElggEntity::__clone()
	 */
	public function __clone() {
		parent::__clone();
		
		$this->title = elgg_echo("newsletter:duplicate_of") . " " . $this->title;
		$this->status = "concept";
		unset($this->temp_metadata["scheduled"]);
		unset($this->temp_metadata["start_time"]);
	}
	
	/**
	 * Initializes attributes for this class
	 *
	 * @return void
	 *
	 * @see ElggObject::initializeAttributes()
	 */
	protected function initializeAttributes() {
		parent::initializeAttributes();
		
		$this->attributes["subtype"] = self::SUBTYPE;
	}
	
	/**
	 * Returns the url to the newsletter
	 *
	 * @return string url to the newsletter
	 * @see ElggEntity::getURL()
	 */
	public function getURL() {
		return elgg_normalize_url("newsletter/view/" . $this->getGUID() . "/" . newsletter_generate_commandline_secret($this->getGUID()));
	}

	/**
	 * Write data to log
	 *
	 * @param mixed $data the data to be appended to the log
	 *
	 * @return int
	 */
	public function writeToLog($data, $append = true) {
		$file = new ElggFile();
		$file->owner_guid = $this->getGUID();
		$file->setFilename("logging.v2.json");

		if (!$append) {
			// Open the file to guarantee the directory exists
			$file->open("write");
			$file->close();
		}

		$result = file_put_contents(
			$file->getFilenameOnFilestore(),
			json_encode($data) . PHP_EOL,
			($append ? FILE_APPEND : 0)
		);

		return $result;
	}

	/**
	 * Returns logging from a file
	 *
	 * @return mixed
	 */
	public function getLogging() {
		$file = new ElggFile();
		$file->owner_guid = $this->getGUID();
		$file->setFilename("logging.v2.json");

		if ($file->exists()) {
			$start_time = null;
			$end_time = null;
			$recipients = [];

			$handle = fopen($file->getFilenameOnFilestore(), "r");

			while (($line = fgets($handle)) !== false) {
				$data = json_decode($line, true);

				if (isset($data["start_time"])) {
					$start_time = $data["start_time"];
					continue;
				}

				if (isset($data["end_time"])) {
					$end_time = $data["end_time"];
					continue;
				}

				$recipients[] = $data;
			}

			fclose($handle);

			return [
				"start_time" => $start_time,
				"end_time" => $end_time,
				"recipients" => $recipients
			];
		}

		$file = new ElggFile();
		$file->owner_guid = $this->getGUID();
		$file->setFilename("logging.json");

		if ($file->exists()) {
			$content = $file->grabFile();
			return json_decode($content, true);
		}

		return false;
	}

	/**
	 * Save the recipients on disk
	 * 
	 * @param array $recipients the recipients config
	 * 
	 * @return boolean
	 */
	public function setRecipients($recipients) {
		$result = false;

		$fh = new ElggFile();
		$fh->owner_guid = $this->getGUID();
		$fh->setFilename("recipients.json");

		$fh->open("write");
		$result = $fh->write(json_encode($recipients) . "\n");
		$fh->close();

		return $result;
	}

	/**
	 * Get the recipients
	 * 
	 * @return bool|array
	 */
	public function getRecipients() {
		$result = false;

		$fh = new ElggFile();
		$fh->owner_guid = $this->getGUID();
		$fh->setFilename("recipients.json");

		if ($fh->exists()) {
			$raw = $fh->grabFile();
			$result = json_decode($raw, true);
		}

		return $result;
	}
}
