<?php
/**
 * Elgg poll individual widget view
 *
 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html GNU Public License version 2
 *
 * @uses $vars['entity'] Optionally, the poll post to view
*/

elgg_load_js('elgg.polls');

$poll = elgg_extract('entity', $vars);
$msg = elgg_extract('msg', $vars);
$context = elgg_extract('context', $vars);

$can_vote = !polls_check_for_previous_vote($poll, elgg_get_logged_in_user_guid());
if (isset($poll->close_date) && $poll->close_date < time()) {
    $poll_closed = true;
} else {
    $poll_closed = false;
}

if ($poll_closed) {
    $can_vote = false;
}

if ($poll_closed) {
    $display_results = true;
} else {
    if ($can_vote) {
        $display_results = false;
    } else {
        $display_results = true;
    }
}

?>

<h3><?php echo $poll->question; ?></h3>

<?php
if ($msg) {
    echo '<div>' . $msg . '</div>';
}
?>

<div id="poll-container-<?php echo $poll->guid; ?>" class="poll">
    <div id="poll-results-<?php echo $poll->guid; ?>" class="poll_results" style="display:<?php echo (!$display_results) ? "none" : "block"; ?>">
        <?php echo elgg_view('polls/results_for_widget', array('entity' => $poll)); ?>
    </div>
    <?php
    if ($can_vote && !$poll_closed) {
        echo elgg_view_form('polls/vote', array('id' => 'poll-vote-form-' . $poll->guid), array('entity' => $poll, 'callback' => 1));
    }
    ?>
</div>

<div>
    <?php echo elgg_view("output/url", array(
        "id" => "poll-toggle-results-" . $poll->guid,
        "href" => "#",
        "text" => elgg_echo("polls:show_results"),
        "class" => "poll-toggle-results",
        "style" => ($can_vote) ? "display:inline" : "display:none",
        "data-guid" => $poll->guid
    )); ?>

    <?php echo elgg_view("output/url", array(
        "id" => "poll-show-responses-" . $poll->guid,
        "href" => $poll->getURL(),
        "text" => elgg_echo("polls:show_responses"),
        "style" => (!$can_vote) ? "display:inline" : "display:none"
    )); ?>
</div>

<?php if ($context == "index"): ?>
    <div>
        <?php echo elgg_view("output/url", array("href" => "/polls/all", "text" => elgg_echo("polls:all"))); ?>
    </div>
<?php endif; ?>