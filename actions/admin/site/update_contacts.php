<?php
/**
 * Updates the basic settings for the primary site object.
 *
 * Basic site settings are saved as metadata on the site object,
 * with the exception of the default language, which is saved in
 * the config table.
 *
 * @package Elgg.Core
 * @subpackage Administration.Site
 */

if ($site = elgg_get_site_entity()) {
	if (!($site instanceof ElggSite)) {
		throw new InstallationException(elgg_echo('InvalidParameterException:NonElggSite'));
	}

	$site->communication_contact_name = get_input('communication_contact_name');
	$site->communication_contact_tel = get_input('communication_contact_tel');
	$site->communication_contact_email = get_input('communication_contact_email');
	$site->technical_contact_name = get_input('technical_contact_name');
	$site->technical_contact_tel = get_input('technical_contact_tel');
	$site->technical_contact_email = get_input('technical_contact_email');
	$site->responsible_contact_name = get_input('responsible_contact_name');
	$site->responsible_contact_tel = get_input('responsible_contact_tel');
	$site->responsible_contact_email = get_input('responsible_contact_email');
	$site->save();
}

system_message(elgg_echo('admin:configuration:success'));
forward(REFERER);