# Pleio

This codebase is the main Pleio repository. Installation of the dependencies is done through [Composer](https://getcomposer.org/).

## Installation

### Using Docker

Preferred workflow is using Docker. To get started follow the steps below.

Copy `.env-example` to `.env` and update the pleio-account credentials.

Install composer dependencies (check [Composer](https://getcomposer.org/) for installation):

```
composer install
```

Start your enviroment by running:

```
docker-compose up
```

Then point your browser to http://localhost:8090/ . Changes in the code on the development environment are automatically synced with the app container.

Mail can be tested by browsing to localhost:1080, this works with mailcatcher

### Manually

The old-skool development method is installing everything locally. The documentation below may be outdated and is not supported.

- Make sure [brew](https://brew.sh/) and [Sequel Pro](https://sequelpro.com/) is installed.
- Install composer, MariaDB, Apache2.4, PHP and Memcached using brew:

```
brew install composer mariadb httpd24 libmemcached
brew install php@7.1
brew link --force php@7.1
brew services start mariadb
brew services restart httpd24
```

- Clone the repository on your local machine.

```
cd ~/dev
git clone git@github.com:Pleio/pleio.git
cd pleio
```

- Install all the PHP dependencies using Composer:

```
composer install
```

- Install memcached from the pleio directory

```
brew install pkg-config zlib
echo 'no' | pecl install memcached
```

- Use Sequal Pro to create a new username and password for pleio and create a new database Pleio and restore the database dump in `docker/database.sql` to your database.
- Change the variable path and dataroot in elgg_datalists to a path on your system, for example `/Users/{username}/dev/pleio/` and `/Users/{username}/dev/pleio-data/` and also the url in `elgg_sites_entity`, for example to `http://www.pleio.test:8080/`.

- Create a `settings.php` file in engine/ (using the following template) and configure the correct SQL user, password, host and database.

```
<?php
global $CONFIG;
if (!isset($CONFIG)) {
  $CONFIG = new stdClass;
}

$CONFIG->dbuser = "pleio";
$CONFIG->dbpass = "yourpassword";
$CONFIG->dbname = "pleio";
$CONFIG->dbhost = "localhost";
$CONFIG->dbprefix = "elgg_";

$CONFIG->dataroot = "/Users/{username}/dev/pleio-data/";

$CONFIG->broken_mta = false;
$CONFIG->db_disable_query_cache = false;
$CONFIG->minusername = 1;
$CONFIG->min_password_length = 8;
$CONFIG->exception_include = "";

$CONFIG->memcache = getenv("MEMCACHE_ENABLED");
$CONFIG->memcache_prefix = getenv("MEMCACHE_PREFIX");
$CONFIG->memcache_servers = ["localhost"];

$CONFIG->elasticsearch = ["hosts" => ["localhost"]];
$CONFIG->elasticsearch_index = "pleio";
```

- Edit your hosts file **/etc/hosts** (with sudo) so `frontend-dev-server` points to `127.0.0.1`
- Also make an entry for `www.pleio.tes`t to point to your local machine or use [dnsmasq](http://www.thekelleys.org.uk/dnsmasq/doc.html) to forward `*.test` to `127.0.0.1`. For dnsmasq use these steps:

```
brew install dnsmasq
echo 'address=/test/127.0.0.1' > $(brew --prefix)/etc/dnsmasq.conf
brew services start dnsmasq
sudo mkdir -v /etc/resolver
sudo bash -c 'echo "nameserver 127.0.0.1" > /etc/resolver/test'
```

- Configure httpd24 with the following parameters:

In the file `/usr/local/etc/httpd/extra/httpd-vhosts.conf` add the following:

```
<VirtualHost *:8080>
    ServerName www.pleio.test
    DocumentRoot "/Users/{username}/dev/pleio"
</VirtualHost>
```

In the file `/usr/local/etc/httpd/httpd.conf` uncomment this line:

```
LoadModule rewrite_module lib/httpd/modules/mod_rewrite.so
```

The following line might be added by brew. But if it isn't, add it:

```
LoadModule php7_module /usr/local/opt/php@7.1/lib/httpd/modules/libphp7.so
```

Also make sure a `FileMatch .php` exists in the config. If not there, add it below the `LoadModule php7_module` directive.

```
<FilesMatch \.php$>
    SetHandler application/x-httpd-php
</FilesMatch>
```

To allow the webserver to touch the upload folder, change user and group to your local user:

```
User {username}
Group staff
```

Change the default documentroot to your dev folder, and make sure `AllowOverride` is set to All:

```
DocumentRoot "/Users/{username}/dev"
<Directory "/Users/{username}/dev">
    Options Indexes FollowSymLinks
    AllowOverride All
    Require all granted
</Directory>
```

Add `index.php` to the DirectoryIndex

```
<IfModule dir_module>
    DirectoryIndex index.html index.php
</IfModule>
```

Uncomment the virtual hosts include:

```
Include /usr/local/etc/httpd/extra/httpd-vhosts.conf
```

- Setup PHP

Copy the the `php.ini` file from the `pleio/docker` folder to `/usr/local/etc/php/7.1/php.ini`

Additionally, add the following line to the php.ini file:

```
extension="/usr/local/Cellar/php@7.1/7.1.30/pecl/20160303/memcached.so"
```

- Enable all changes by restarting the services

```
brew services restart mariadb
brew services restart httpd24
```

- Now browse to http://www.pleio.test:8080 and login with `admin/adminadmin`.

To use the new Pleio theme, enable the `pleio_template` plugin in `/admin/plugins`. You probably need to enable and disable some other plugins first due to the plugin dependencies.

There is also a `user/useruser` available.

## Tests

All unit tests can be run with the following command

```
php console.php test
```

or within a Docker container

```
docker run -it \
--env DB_USER="" \
--env DB_PASS="" \
--env DB_NAME="" \
--env DB_HOST="" \
--env PLEIO_CLIENT="" \
--env PLEIO_SECRET="" \
--env PLEIO_URL="" \
--env PLEIO_ENV="test" \
--env SMTP_HOST="mail.example.com" \
--env SMTP_DOMAIN="example.com" \
--env MEMCACHE_ENABLED="true" \
--env MEMCACHE_SERVER_1="127.0.0.1" \
--env BLOCK_MAIL="true" \
pleio/pleio php /app/console.php test
```

## Testing email locally

Install and run [Mailcatcher](https://mailcatcher.me/):

```
gem install mailcatcher
mailcatcher
```

Add mailcatcher to your virtual host configuration in `/usr/local/etc/httpd/extra/httpd-vhosts.conf` :

```
php_admin_value sendmail_path "/usr/bin/env catchmail -f mailcatcher@pleio.test"
```

Go to http://localhost:1080

## Background tasks

For information about running background tasks see [background/README.md](background/README.md).

## Acknowledgements

[![image](https://cloud.githubusercontent.com/assets/5213690/24361644/b78b7866-130a-11e7-91a8-6ecdf7045bb3.png)](https://www.browserstack.com)

Special thanks to [Browserstack](https://www.browserstack.com) for providing the infrastructure to test Pleio in real browsers.
