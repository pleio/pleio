#!/bin/sh

echo "[i] Initializing environment..."
/scripts/initialize.sh

echo "[i] Starting celery..."
celery -A background.app "$@"
