<?php
list($host,) = explode(":", $_SERVER["HTTP_HOST"]);

$dbhost = getenv("PLEIO_DB_HOST");
$dbuser = getenv("PLEIO_DB_USER");
$dbpass = getenv("PLEIO_DB_PASS");
$dbname = getenv("PLEIO_DB_NAME");
$dataroot = getenv("PLEIO_DATAROOT");

if (!$dbhost || !$dbuser || !$dbpass || !$dbname) {
    return;
}

$link = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
if (!$link) {
    echo "Kon geen verbinding maken met de server.";
    exit();
}

$result = mysqli_select_db($link, $dbname);
if (!$result) {
    echo "Kon geen verbinding maken met de database.";
    exit();
}

$result = mysqli_query($link, sprintf("SELECT name, username, password, enabled, env FROM instances WHERE host = '%s'", mysqli_real_escape_string($link, $host)));
if (!$result) {
    echo "Er is een probleem opgetreden tijdens het ophalen van de instanties.";
    exit();
}

$instance = mysqli_fetch_object($result);
if (!$instance) {
    include("errors/400.html");
    exit();
}

if ($instance->enabled === "0") {
    include("errors/503.html");
    exit();
}

if ($dataroot) {
    $dataroot .= $instance->name . "/";
}

mysqli_close($link);

putenv("DB_HOST={$dbhost}");
putenv("DB_USER={$instance->username}");
putenv("DB_PASS={$instance->password}");
putenv("DB_NAME={$instance->name}");
putenv("MEMCACHE_PREFIX={$instance->name}");
putenv("ELASTIC_INDEX={$instance->name}");
putenv("DATAROOT={$dataroot}");
