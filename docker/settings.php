<?php
global $CONFIG;
if (!isset($CONFIG)) {
    $CONFIG = new stdClass;
}

$CONFIG->dbuser = getenv("DB_USER");
$CONFIG->dbpass = getenv("DB_PASS");
$CONFIG->dbname = getenv("DB_NAME");
$CONFIG->dbhost = getenv("DB_HOST");
$CONFIG->dbprefix = getenv("DB_PREFIX") ? getenv("DB_PREFIX") : "elgg_";

if (getenv("DB_READ")) {
    $db_write = new stdClass;
    $db_write->dbuser = $CONFIG->dbuser;
    $db_write->dbpass = $CONFIG->dbpass;
    $db_write->dbname = $CONFIG->dbname;
    $db_write->dbhost = $CONFIG->dbhost;

    $db_read = new stdClass;
    $db_read->dbuser = $CONFIG->dbuser;
    $db_read->dbpass = $CONFIG->dbpass;
    $db_read->dbname = $CONFIG->dbname;
    $db_read->dbhost = getenv("DB_READ_HOST");

    $CONFIG->db_split = true;
    $CONFIG->db["read"] = [$db_write, $db_read];
    $CONFIG->db["write"] = [$db_write];
}

$CONFIG->dataroot = getenv("DATAROOT");

$CONFIG->broken_mta = false;
$CONFIG->db_disable_query_cache = false;
$CONFIG->minusername = 1;
$CONFIG->min_password_length = 8;
$CONFIG->exception_include = "";

$CONFIG->pleio = new \stdClass;
$CONFIG->pleio->client = getenv("PLEIO_CLIENT");
$CONFIG->pleio->secret = getenv("PLEIO_SECRET");
$CONFIG->pleio->url = getenv("PLEIO_URL");

# supported languages
$CONFIG->translations['nl']['Nederlands'] = 'nl';

$CONFIG->env = getenv("PLEIO_ENV");

$host = getenv("SMTP_DOMAIN");
if ($host) {
    $CONFIG->email_from = "noreply@" . $host;
}

if (getenv("BLOCK_MAIL")) {
    $CONFIG->block_mail = true;
}

if (getenv("MEMCACHE_ENABLED")) {
    $CONFIG->memcache = getenv("MEMCACHE_ENABLED");
    $CONFIG->memcache_prefix = getenv("MEMCACHE_PREFIX");
    $CONFIG->memcache_servers = [];

    if (getenv("MEMCACHE_SERVER_1")) {
        $CONFIG->memcache_servers[] = [getenv("MEMCACHE_SERVER_1"), 11211];
    }

    if (getenv("MEMCACHE_SERVER_2")) {
        $CONFIG->memcache_servers[] = [getenv("MEMCACHE_SERVER_2"), 11211];
    }

    if (getenv("MEMCACHE_SERVER_3")) {
        $CONFIG->memcache_servers[] = [getenv("MEMCACHE_SERVER_3"), 11211];
    }
}

$CONFIG->elasticsearch = ["hosts" => []];
$CONFIG->elasticsearch_index = getenv("ELASTIC_INDEX");

if (getenv("ELASTIC_SERVER_1")) {
    $CONFIG->elasticsearch["hosts"][] = getenv("ELASTIC_SERVER_1");
}

if (getenv("ELASTIC_SERVER_2")) {
    $CONFIG->elasticsearch["hosts"][] = getenv("ELASTIC_SERVER_2");
}

if (getenv("ELASTIC_SERVER_3")) {
    $CONFIG->elasticsearch["hosts"][] = getenv("ELASTIC_SERVER_3");
}

$CONFIG->amqp_host = getenv("AMQP_HOST");
$CONFIG->amqp_user = getenv("AMQP_USER");
$CONFIG->amqp_pass = getenv("AMQP_PASS");
$CONFIG->amqp_port = getenv("AMQP_PORT") ? getenv("AMQP_PORT") : "5672";
$CONFIG->amqp_vhost = getenv("AMQP_VHOST") ? getenv("AMQP_VHOST") : "/";
$CONFIG->tasks_always_eager = getenv("TASKS_ALWAYS_EAGER") == "true" ? true : false;

$CONFIG->bouncer_url = getenv("BOUNCER_URL");
$CONFIG->bouncer_token = getenv("BOUNCER_TOKEN");

$CONFIG->account_system_api_token = getenv("ACCOUNT_SYSTEM_API_TOKEN");
