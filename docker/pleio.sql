-- MySQL dump 10.17  Distrib 10.3.15-MariaDB, for osx10.14 (x86_64)
--
-- Host: localhost    Database: sjabloon
-- ------------------------------------------------------
-- Server version	10.3.15-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `elgg_access_collection_membership`
--

DROP TABLE IF EXISTS `elgg_access_collection_membership`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elgg_access_collection_membership` (
  `user_guid` bigint(20) NOT NULL,
  `access_collection_id` bigint(20) NOT NULL,
  PRIMARY KEY (`user_guid`,`access_collection_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elgg_access_collection_membership`
--

LOCK TABLES `elgg_access_collection_membership` WRITE;
/*!40000 ALTER TABLE `elgg_access_collection_membership` DISABLE KEYS */;
/*!40000 ALTER TABLE `elgg_access_collection_membership` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elgg_access_collections`
--

DROP TABLE IF EXISTS `elgg_access_collections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elgg_access_collections` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner_guid` bigint(20) unsigned NOT NULL,
  `site_guid` bigint(20) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `owner_guid` (`owner_guid`),
  KEY `site_guid` (`site_guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elgg_access_collections`
--

LOCK TABLES `elgg_access_collections` WRITE;
/*!40000 ALTER TABLE `elgg_access_collections` DISABLE KEYS */;
/*!40000 ALTER TABLE `elgg_access_collections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elgg_annotations`
--

DROP TABLE IF EXISTS `elgg_annotations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elgg_annotations` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `entity_guid` bigint(20) unsigned NOT NULL,
  `name_id` bigint(20) NOT NULL,
  `value_id` bigint(20) NOT NULL,
  `value_type` enum('integer','text') COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner_guid` bigint(20) unsigned NOT NULL,
  `access_id` bigint(20) NOT NULL,
  `time_created` bigint(20) NOT NULL,
  `enabled` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`id`),
  KEY `entity_guid` (`entity_guid`),
  KEY `name_id` (`name_id`),
  KEY `value_id` (`value_id`),
  KEY `owner_guid` (`owner_guid`),
  KEY `access_id` (`access_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elgg_annotations`
--

LOCK TABLES `elgg_annotations` WRITE;
/*!40000 ALTER TABLE `elgg_annotations` DISABLE KEYS */;
INSERT INTO `elgg_annotations` VALUES (5,57977371,76524069,76524068,'text',57977371,2,1533283574,'yes'),(6,57977371,76524069,76524070,'text',57977371,2,1533283575,'yes'),(7,57977371,76524069,76524071,'text',57977371,2,1533283576,'yes'),(8,57977371,76524069,76524072,'text',57977371,2,1533283577,'yes'),(9,57977371,76524069,76524073,'text',57977371,2,1533283578,'yes'),(10,57977371,76524069,76524074,'text',57977371,2,1533283580,'yes'),(11,57977371,76524069,76524075,'text',57977371,2,1533283581,'yes'),(12,57977371,76524069,76524076,'text',57977371,2,1533283582,'yes'),(13,57977371,76524069,76524077,'text',57977371,2,1533283583,'yes'),(14,57977371,76524069,76524078,'text',57977371,2,1533283584,'yes'),(15,57977371,76524069,76524079,'text',57977371,2,1533283585,'yes'),(16,57977371,76524069,76524080,'text',57977371,2,1533283586,'yes');
/*!40000 ALTER TABLE `elgg_annotations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elgg_api_users`
--

DROP TABLE IF EXISTS `elgg_api_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elgg_api_users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `site_guid` bigint(20) unsigned DEFAULT NULL,
  `api_key` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secret` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` int(1) DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE KEY `api_key` (`api_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elgg_api_users`
--

LOCK TABLES `elgg_api_users` WRITE;
/*!40000 ALTER TABLE `elgg_api_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `elgg_api_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elgg_backup`
--

DROP TABLE IF EXISTS `elgg_backup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elgg_backup` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `transaction_id` varchar(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_guid` bigint(20) NOT NULL,
  `time_created` bigint(20) NOT NULL,
  `performed_by` bigint(20) NOT NULL,
  `type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `time_created` (`time_created`),
  KEY `transaction_id` (`transaction_id`),
  KEY `performed_by` (`performed_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elgg_backup`
--

LOCK TABLES `elgg_backup` WRITE;
/*!40000 ALTER TABLE `elgg_backup` DISABLE KEYS */;
/*!40000 ALTER TABLE `elgg_backup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elgg_config`
--

DROP TABLE IF EXISTS `elgg_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elgg_config` (
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_guid` bigint(20) NOT NULL,
  PRIMARY KEY (`name`,`site_guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elgg_config`
--

LOCK TABLES `elgg_config` WRITE;
/*!40000 ALTER TABLE `elgg_config` DISABLE KEYS */;
INSERT INTO `elgg_config` VALUES ('allow_registration','b:1;',57977371),('allow_user_default_access','i:0;',57977371),('default_access','s:1:\"1\";',57977371),('disable_api','s:8:\"disabled\";',57977371),('language','s:2:\"nl\";',57977371),('view','s:7:\"default\";',57977371),('walled_garden','b:0;',57977371);
/*!40000 ALTER TABLE `elgg_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elgg_datalists`
--

DROP TABLE IF EXISTS `elgg_datalists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elgg_datalists` (
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elgg_datalists`
--

LOCK TABLES `elgg_datalists` WRITE;
/*!40000 ALTER TABLE `elgg_datalists` DISABLE KEYS */;
INSERT INTO `elgg_datalists` VALUES ('__site_secret__','zbop5RowTu7ZKmn3VBXU97QhakDjZxuz'),('admin_registered','1'),('default_site','57977371'),('elgg_widget_run_once','1481013903'),('filestore_run_once','1481013903'),('installed','1481013923'),('plugin_run_once','1481013903'),('pm_fix_access_default','1481014825'),('processed_upgrades','a:47:{i:0;s:14:\"2008100701.php\";i:1;s:14:\"2008101303.php\";i:2;s:14:\"2009022701.php\";i:3;s:14:\"2009041701.php\";i:4;s:14:\"2009070101.php\";i:5;s:14:\"2009102801.php\";i:6;s:14:\"2010010501.php\";i:7;s:14:\"2010033101.php\";i:8;s:14:\"2010040201.php\";i:9;s:14:\"2010052601.php\";i:10;s:14:\"2010060101.php\";i:11;s:14:\"2010060401.php\";i:12;s:14:\"2010061501.php\";i:13;s:14:\"2010062301.php\";i:14;s:14:\"2010062302.php\";i:15;s:14:\"2010070301.php\";i:16;s:14:\"2010071001.php\";i:17;s:14:\"2010071002.php\";i:18;s:14:\"2010111501.php\";i:19;s:14:\"2010121601.php\";i:20;s:14:\"2010121602.php\";i:21;s:14:\"2010121701.php\";i:22;s:14:\"2010123101.php\";i:23;s:14:\"2011010101.php\";i:24;s:61:\"2011021800-1.8_svn-goodbye_walled_garden-083121a656d06894.php\";i:25;s:61:\"2011022000-1.8_svn-custom_profile_fields-390ac967b0bb5665.php\";i:26;s:60:\"2011030700-1.8_svn-blog_status_metadata-4645225d7b440876.php\";i:27;s:51:\"2011031300-1.8_svn-twitter_api-12b832a5a7a3e1bd.php\";i:28;s:57:\"2011031600-1.8_svn-datalist_grows_up-0b8aec5a55cc1e1c.php\";i:29;s:61:\"2011032000-1.8_svn-widgets_arent_plugins-61836261fa280a5c.php\";i:30;s:59:\"2011032200-1.8_svn-admins_like_widgets-7f19d2783c1680d3.php\";i:31;s:14:\"2011052801.php\";i:32;s:60:\"2011061200-1.8b1-sites_need_a_site_guid-6d9dcbf46c0826cc.php\";i:33;s:62:\"2011092500-1.8.0.1-forum_reply_river_view-5758ce8d86ac56ce.php\";i:34;s:54:\"2011123100-1.8.2-fix_friend_river-b17e7ff8345c2269.php\";i:35;s:53:\"2011123101-1.8.2-fix_blog_status-b14c2a0e7b9e7d55.php\";i:36;s:50:\"2012012000-1.8.3-ip_in_syslog-87fe0f068cf62428.php\";i:37;s:50:\"2012012100-1.8.3-system_cache-93100e7d55a24a11.php\";i:38;s:59:\"2012041800-1.8.3-dont_filter_passwords-c0ca4a18b38ae2bc.php\";i:39;s:58:\"2012041801-1.8.3-multiple_user_tokens-852225f7fd89f6c5.php\";i:40;s:59:\"2013030600-1.8.13-update_user_location-8999eb8bf1bdd9a3.php\";i:41;s:62:\"2013051700-1.8.15-add_missing_group_index-52a63a3a3ffaced2.php\";i:42;s:53:\"2013052900-1.8.15-ipv6_in_syslog-f5c2cc0196e9e731.php\";i:43;s:50:\"2013060900-1.8.15-site_secret-404fc165cf9e0ac9.php\";i:44;s:50:\"2014012000-1.8.18-remember_me-9a8a433685cf7be9.php\";i:45;s:48:\"2012101001-file_tools-folder-parent_guid-fix.php\";i:46;s:43:\"20140210_newsletter_fix_template_access.php\";}'),('profile_manager_run_once','1481014825'),('simplecache_enabled','0'),('simplecache_lastcached_default','1572526441'),('simplecache_lastcached_failsafe','0'),('simplecache_lastcached_foaf','0'),('simplecache_lastcached_ical','0'),('simplecache_lastcached_installation','0'),('simplecache_lastcached_json','0'),('simplecache_lastcached_opendd','0'),('simplecache_lastcached_php','0'),('simplecache_lastcached_rss','0'),('simplecache_lastcached_xml','0'),('simplecache_lastupdate_default','1572526441'),('simplecache_lastupdate_failsafe','0'),('simplecache_lastupdate_foaf','0'),('simplecache_lastupdate_ical','0'),('simplecache_lastupdate_installation','0'),('simplecache_lastupdate_json','0'),('simplecache_lastupdate_opendd','0'),('simplecache_lastupdate_php','0'),('simplecache_lastupdate_rss','0'),('simplecache_lastupdate_xml','0'),('system_cache_enabled','0'),('te_last_update_en','1527676291'),('te_last_update_nl','1527676290'),('thewire_tools_runonce','1481014825'),('translation_editor_version_053','1527676290'),('version','2014090700');
/*!40000 ALTER TABLE `elgg_datalists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elgg_entities`
--

DROP TABLE IF EXISTS `elgg_entities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elgg_entities` (
  `guid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('object','user','group','site') COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtype` int(11) DEFAULT NULL,
  `owner_guid` bigint(20) unsigned NOT NULL,
  `site_guid` bigint(20) unsigned NOT NULL,
  `container_guid` bigint(20) unsigned NOT NULL,
  `access_id` bigint(20) NOT NULL,
  `time_created` bigint(20) NOT NULL,
  `time_updated` bigint(20) NOT NULL,
  `last_action` bigint(20) NOT NULL DEFAULT 0,
  `enabled` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`guid`),
  KEY `type` (`type`),
  KEY `subtype` (`subtype`),
  KEY `owner_guid` (`owner_guid`),
  KEY `site_guid` (`site_guid`),
  KEY `container_guid` (`container_guid`),
  KEY `access_id` (`access_id`),
  KEY `time_created` (`time_created`),
  KEY `time_updated` (`time_updated`)
) ENGINE=InnoDB AUTO_INCREMENT=57979212 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elgg_entities`
--

LOCK TABLES `elgg_entities` WRITE;
/*!40000 ALTER TABLE `elgg_entities` DISABLE KEYS */;
INSERT INTO `elgg_entities` VALUES (57977371,'site',0,0,57977371,0,2,1526488078,1572526543,1572526428,'yes'),(57977381,'object',35,57977371,57977371,57977371,2,1526488078,1526488078,1526488078,'yes'),(57977391,'object',35,57977371,57977371,57977371,1,1526488078,1526488078,1526488078,'yes'),(57977401,'object',35,57977371,57977371,57977371,1,1526488078,1526488078,1526488078,'yes'),(57977411,'object',35,57977371,57977371,57977371,1,1526488078,1526488078,1526488078,'yes'),(57977421,'object',35,57977371,57977371,57977371,1,1526488078,1526488078,1526488078,'yes'),(57977431,'object',35,57977371,57977371,57977371,1,1526488079,1526488079,1526488079,'yes'),(57977441,'object',35,57977371,57977371,57977371,1,1526488079,1526488079,1526488079,'yes'),(57977451,'object',35,57977371,57977371,57977371,1,1526488079,1526488079,1526488079,'yes'),(57977461,'object',35,57977371,57977371,57977371,1,1526488079,1526488079,1526488079,'yes'),(57977471,'object',35,57977371,57977371,57977371,1,1526488079,1526488079,1526488079,'yes'),(57977481,'object',35,57977371,57977371,57977371,1,1526488079,1526488079,1526488079,'yes'),(57977491,'object',35,57977371,57977371,57977371,1,1526488079,1526488079,1526488079,'yes'),(57977501,'object',35,57977371,57977371,57977371,1,1526488079,1526488079,1526488079,'yes'),(57977511,'object',35,57977371,57977371,57977371,1,1526488079,1526488079,1526488079,'yes'),(57977521,'object',35,57977371,57977371,57977371,1,1526488080,1526488080,1526488080,'yes'),(57977531,'object',35,57977371,57977371,57977371,1,1526488080,1526488080,1526488080,'yes'),(57977541,'object',35,57977371,57977371,57977371,1,1526488080,1526488080,1526488080,'yes'),(57977551,'object',35,57977371,57977371,57977371,0,1526488080,1526488080,1526488080,'yes'),(57977561,'object',35,57977371,57977371,57977371,0,1526488080,1526488080,1526488080,'yes'),(57977571,'object',35,57977371,57977371,57977371,0,1526488080,1526488080,1526488080,'yes'),(57977581,'object',35,57977371,57977371,57977371,0,1526488080,1526488080,1526488080,'yes'),(57977591,'object',35,57977371,57977371,57977371,0,1526488080,1526488080,1526488080,'yes'),(57977601,'object',2,57977371,57977371,57977371,2,1526488099,1526488099,1526488099,'yes'),(57977611,'object',2,57977371,57977371,57977371,2,1526488099,1526488099,1526488099,'yes'),(57977621,'object',2,57977371,57977371,57977371,2,1526488100,1526488100,1526488100,'yes'),(57977631,'object',2,57977371,57977371,57977371,2,1526488100,1526488100,1526488100,'yes'),(57977651,'object',2,57977371,57977371,57977371,2,1526488100,1526488100,1526488100,'yes'),(57977661,'object',2,57977371,57977371,57977371,2,1526488100,1526488100,1526488100,'yes'),(57977671,'object',2,57977371,57977371,57977371,2,1526488101,1526488101,1526488101,'yes'),(57977701,'object',2,57977371,57977371,57977371,2,1526488101,1526488101,1526488101,'yes'),(57977711,'object',2,57977371,57977371,57977371,2,1526488101,1526488101,1526488101,'yes'),(57977721,'object',2,57977371,57977371,57977371,2,1526488102,1526488102,1526488102,'yes'),(57977731,'object',2,57977371,57977371,57977371,2,1526488102,1526488102,1526488102,'yes'),(57977741,'object',2,57977371,57977371,57977371,2,1526488102,1526488102,1526488102,'yes'),(57977751,'object',2,57977371,57977371,57977371,2,1526488102,1526488102,1526488102,'yes'),(57977781,'object',2,57977371,57977371,57977371,2,1526488103,1526488103,1526488103,'yes'),(57977791,'object',2,57977371,57977371,57977371,2,1526488103,1526488103,1526488103,'yes'),(57977811,'object',2,57977371,57977371,57977371,2,1526488103,1526488103,1526488103,'yes'),(57977821,'object',2,57977371,57977371,57977371,2,1526488103,1526488103,1526488103,'yes'),(57977841,'object',2,57977371,57977371,57977371,2,1526488104,1526488104,1526488104,'yes'),(57977871,'object',2,57977371,57977371,57977371,2,1526488104,1526488104,1526488104,'yes'),(57977881,'object',2,57977371,57977371,57977371,2,1526488104,1526488104,1526488104,'yes'),(57977891,'object',2,57977371,57977371,57977371,2,1526488104,1526488104,1526488104,'yes'),(57977921,'object',2,57977371,57977371,57977371,2,1526488105,1526488105,1526488105,'yes'),(57977931,'object',2,57977371,57977371,57977371,2,1526488105,1526488105,1526488105,'yes'),(57977941,'object',2,57977371,57977371,57977371,2,1526488105,1526488105,1526488105,'yes'),(57977951,'object',2,57977371,57977371,57977371,2,1526488105,1526488105,1526488105,'yes'),(57977961,'object',2,57977371,57977371,57977371,2,1526488106,1526488106,1526488106,'yes'),(57977971,'object',2,57977371,57977371,57977371,2,1526488106,1526488106,1526488106,'yes'),(57977981,'object',2,57977371,57977371,57977371,2,1526488106,1526488106,1526488106,'yes'),(57977991,'object',2,57977371,57977371,57977371,2,1526488106,1526488106,1526488106,'yes'),(57978001,'object',2,57977371,57977371,57977371,2,1526488106,1526488106,1526488106,'yes'),(57978011,'object',2,57977371,57977371,57977371,2,1526488107,1526488107,1526488107,'yes'),(57978021,'object',2,57977371,57977371,57977371,2,1526488107,1526488107,1526488107,'no'),(57978071,'object',2,57977371,57977371,57977371,2,1526488108,1526488108,1526488108,'yes'),(57978081,'object',2,57977371,57977371,57977371,2,1526488108,1526488108,1526488108,'yes'),(57978101,'object',2,57977371,57977371,57977371,2,1526488108,1526488108,1526488108,'yes'),(57978121,'object',2,57977371,57977371,57977371,2,1526488108,1526488108,1526488108,'yes'),(57978151,'object',2,57977371,57977371,57977371,2,1526488109,1526488109,1526488109,'yes'),(57978161,'object',2,57977371,57977371,57977371,2,1526488109,1526488109,1526488109,'yes'),(57978171,'object',2,57977371,57977371,57977371,2,1526488109,1526488109,1526488109,'yes'),(57978211,'object',2,57977371,57977371,57977371,2,1526488110,1526488110,1526488110,'yes'),(57978221,'object',2,57977371,57977371,57977371,2,1526488110,1526488110,1526488110,'yes'),(57978231,'object',2,57977371,57977371,57977371,2,1526488110,1526488110,1526488110,'yes'),(57978241,'object',2,57977371,57977371,57977371,2,1526488111,1526488111,1526488111,'yes'),(57978271,'object',2,57977371,57977371,57977371,2,1526488111,1526488111,1526488111,'yes'),(57978281,'object',2,57977371,57977371,57977371,2,1526488111,1526488111,1526488111,'yes'),(57978291,'object',2,57977371,57977371,57977371,2,1526488111,1526488111,1526488111,'yes'),(57978311,'object',2,57977371,57977371,57977371,2,1526488112,1526488112,1526488112,'yes'),(57978321,'object',2,57977371,57977371,57977371,2,1526488112,1526488112,1526488112,'yes'),(57978331,'object',2,57977371,57977371,57977371,2,1526488112,1526488112,1526488112,'yes'),(57978361,'object',2,57977371,57977371,57977371,2,1526488113,1526488113,1526488113,'yes'),(57978381,'object',2,57977371,57977371,57977371,2,1526488113,1526488113,1526488113,'yes'),(57978421,'object',2,57977371,57977371,57977371,2,1526488114,1526488114,1526488114,'yes'),(57978431,'object',2,57977371,57977371,57977371,2,1526488114,1526488114,1526488114,'yes'),(57978441,'object',2,57977371,57977371,57977371,2,1526488114,1526488114,1526488114,'yes'),(57978451,'object',2,57977371,57977371,57977371,2,1526488114,1526488114,1526488114,'yes'),(57978471,'object',2,57977371,57977371,57977371,2,1526488115,1526488115,1526488115,'yes'),(57978481,'object',2,57977371,57977371,57977371,2,1526488115,1526488115,1526488115,'yes'),(57978491,'object',2,57977371,57977371,57977371,2,1526488115,1526488115,1526488115,'yes'),(57978501,'object',2,57977371,57977371,57977371,2,1526488115,1526488115,1526488115,'yes'),(57978511,'object',2,57977371,57977371,57977371,2,1526488115,1526488115,1526488115,'yes'),(57978521,'object',2,57977371,57977371,57977371,2,1526488116,1526488116,1526488116,'yes'),(57978551,'object',2,57977371,57977371,57977371,2,1526488116,1526488116,1526488116,'yes'),(57978561,'object',2,57977371,57977371,57977371,2,1526488116,1526488116,1526488116,'yes'),(57978581,'object',2,57977371,57977371,57977371,2,1526488117,1526488117,1526488117,'yes'),(57978591,'object',2,57977371,57977371,57977371,2,1526488117,1526488117,1526488117,'yes'),(57978601,'object',2,57977371,57977371,57977371,2,1526488117,1526488117,1526488117,'yes'),(57978611,'object',2,57977371,57977371,57977371,2,1526488117,1526488117,1526488117,'yes'),(57978631,'object',2,57977371,57977371,57977371,2,1526488117,1526488117,1526488117,'yes'),(57978671,'object',2,57977371,57977371,57977371,2,1526488118,1526488118,1526488118,'yes'),(57978691,'object',2,57977371,57977371,57977371,2,1526488118,1526488118,1526488118,'yes'),(57978881,'object',2,57977371,57977371,57977371,2,1526488122,1526488122,1526488122,'yes'),(57978891,'object',2,57977371,57977371,57977371,2,1526488122,1526488122,1526488122,'yes'),(57978901,'object',2,57977371,57977371,57977371,2,1526488122,1526488122,1526488122,'yes'),(57978911,'object',2,57977371,57977371,57977371,2,1526488122,1526488122,1526488122,'yes'),(57978921,'object',2,57977371,57977371,57977371,2,1526488123,1526488123,1526488123,'yes'),(57978931,'object',2,57977371,57977371,57977371,2,1526488123,1526488123,1526488123,'yes'),(57978951,'object',2,57977371,57977371,57977371,2,1526488123,1526488123,1526488123,'yes'),(57978971,'object',2,57977371,57977371,57977371,2,1526488123,1526488123,1526488123,'yes'),(57978981,'object',2,57977371,57977371,57977371,2,1526488124,1526488124,1526488124,'yes'),(57978991,'object',2,57977371,57977371,57977371,2,1526488124,1526488124,1526488124,'yes'),(57979021,'object',2,57977371,57977371,57977371,2,1526488124,1526488124,1526488124,'yes'),(57979031,'object',2,57977371,57977371,57977371,2,1526488124,1526488124,1526488124,'yes'),(57979041,'object',2,57977371,57977371,57977371,2,1526488125,1526488125,1526488125,'yes'),(57979052,'object',2,57977371,57977371,57977371,2,1527050774,1527050774,1527050774,'yes'),(57979054,'object',3,57979053,57977371,57979053,1,1527676320,1527676320,1527676320,'yes'),(57979055,'object',3,57979053,57977371,57979053,1,1527676342,1527676342,1527676342,'yes'),(57979056,'object',3,57979053,57977371,57979053,1,1527676343,1527676343,1527676343,'yes'),(57979057,'object',3,57979053,57977371,57979053,1,1527676343,1527676343,1527676343,'yes'),(57979058,'object',3,57979053,57977371,57979053,1,1527676344,1527676344,1527676344,'yes'),(57979059,'object',3,57979053,57977371,57979053,1,1527676345,1527676345,1527676345,'yes'),(57979060,'object',3,57979053,57977371,57979053,1,1527676345,1527676345,1527676345,'yes'),(57979061,'object',2,57977371,57977371,57977371,2,1527684222,1527684222,1527684222,'yes'),(57979062,'object',2,57977371,57977371,57977371,2,1527684222,1527684222,1527684222,'yes'),(57979063,'object',2,57977371,57977371,57977371,2,1527684222,1527684222,1527684222,'yes'),(57979064,'object',2,57977371,57977371,57977371,2,1527684222,1527684222,1527684222,'yes'),(57979065,'object',2,57977371,57977371,57977371,2,1527684222,1527684222,1527684222,'yes'),(57979066,'object',2,57977371,57977371,57977371,2,1527684222,1527684222,1527684222,'no'),(57979067,'object',2,57977371,57977371,57977371,2,1527684222,1527684222,1527684222,'yes'),(57979068,'object',2,57977371,57977371,57977371,2,1527684222,1527684222,1527684222,'yes'),(57979069,'object',2,57977371,57977371,57977371,2,1527684222,1527684222,1527684222,'yes'),(57979070,'object',2,57977371,57977371,57977371,2,1527684223,1527684223,1527684223,'no'),(57979071,'object',2,57977371,57977371,57977371,2,1527684223,1527684223,1527684223,'yes'),(57979072,'object',2,57977371,57977371,57977371,2,1527684223,1527684223,1527684223,'yes'),(57979073,'object',3,57979053,57977371,57979053,1,1527684284,1527684284,1527684284,'yes'),(57979074,'object',3,57979053,57977371,57979053,1,1527684285,1527684285,1527684285,'yes'),(57979075,'object',3,57979053,57977371,57979053,1,1527684286,1527684286,1527684286,'yes'),(57979076,'object',3,57979053,57977371,57979053,1,1527684287,1527684287,1527684287,'yes'),(57979077,'object',3,57979053,57977371,57979053,1,1527684289,1527684289,1527684289,'yes'),(57979078,'object',3,57979053,57977371,57979053,1,1527684289,1527684289,1527684289,'yes'),(57979079,'object',3,57979053,57977371,57979053,1,1527684339,1527684339,1527684339,'yes'),(57979080,'object',3,57979053,57977371,57979053,1,1527684340,1527684340,1527684340,'yes'),(57979081,'object',3,57979053,57977371,57979053,1,1527684341,1527684341,1527684341,'yes'),(57979082,'object',3,57979053,57977371,57979053,1,1527684341,1527684341,1527684341,'yes'),(57979083,'object',3,57979053,57977371,57979053,1,1527684342,1527684342,1527684342,'yes'),(57979084,'object',3,57979053,57977371,57979053,1,1527684343,1527684343,1527684343,'yes'),(57979085,'object',3,57979053,57977371,57979053,1,1527684384,1527684384,1527684384,'yes'),(57979086,'object',3,57979053,57977371,57979053,1,1527684385,1527684385,1527684385,'yes'),(57979087,'object',3,57979053,57977371,57979053,1,1527684386,1527684386,1527684386,'yes'),(57979088,'object',3,57979053,57977371,57979053,1,1527684387,1527684387,1527684387,'yes'),(57979089,'object',3,57979053,57977371,57979053,1,1527684388,1527684388,1527684388,'yes'),(57979090,'object',3,57979053,57977371,57979053,1,1527684388,1527684388,1527684388,'yes'),(57979091,'object',3,57979053,57977371,57979053,1,1527684389,1527684389,1527684389,'yes'),(57979095,'object',2,57977371,57977371,57977371,2,1528002656,1528002656,1528002656,'yes'),(57979096,'object',2,57977371,57977371,57977371,2,1528002656,1528002656,1528002656,'yes'),(57979097,'object',2,57977371,57977371,57977371,2,1528002656,1528002656,1528002656,'yes'),(57979098,'object',2,57977371,57977371,57977371,2,1528002656,1528002656,1528002656,'yes'),(57979099,'object',2,57977371,57977371,57977371,2,1528002656,1528002656,1528002656,'yes'),(57979100,'object',2,57977371,57977371,57977371,2,1528002656,1528002656,1528002656,'yes'),(57979101,'object',2,57977371,57977371,57977371,2,1528002656,1528002656,1528002656,'yes'),(57979102,'object',2,57977371,57977371,57977371,2,1528002656,1528002656,1528002656,'yes'),(57979103,'object',2,57977371,57977371,57977371,2,1528002656,1528002656,1528002656,'yes'),(57979104,'object',2,57977371,57977371,57977371,2,1528002656,1528002656,1528002656,'yes'),(57979105,'object',2,57977371,57977371,57977371,2,1528002657,1528002657,1528002657,'yes'),(57979106,'object',2,57977371,57977371,57977371,2,1528002657,1528002657,1528002657,'yes'),(57979107,'object',2,57977371,57977371,57977371,2,1528002657,1528002657,1528002657,'yes'),(57979108,'object',2,57977371,57977371,57977371,2,1528002657,1528002657,1528002657,'yes'),(57979109,'object',2,57977371,57977371,57977371,2,1528002657,1528002657,1528002657,'yes'),(57979110,'object',2,57977371,57977371,57977371,2,1528002657,1528002657,1528002657,'yes'),(57979111,'object',2,57977371,57977371,57977371,2,1528002657,1528002657,1528002657,'yes'),(57979112,'object',2,57977371,57977371,57977371,2,1528002657,1528002657,1528002657,'yes'),(57979113,'object',2,57977371,57977371,57977371,2,1528002657,1528002657,1528002657,'yes'),(57979114,'object',2,57977371,57977371,57977371,2,1528002657,1528002657,1528002657,'yes'),(57979115,'object',2,57977371,57977371,57977371,2,1528002657,1528002657,1528002657,'yes'),(57979116,'object',2,57977371,57977371,57977371,2,1528002657,1528002657,1528002657,'yes'),(57979117,'object',2,57977371,57977371,57977371,2,1528002657,1528002657,1528002657,'yes'),(57979118,'object',2,57977371,57977371,57977371,2,1528002657,1528002657,1528002657,'yes'),(57979122,'object',1,57979053,57977371,57979053,1,1528002792,1528002792,1528002792,'yes'),(57979129,'object',14,57979053,57977371,57977371,1,1528003072,1560258890,1528003072,'yes'),(57979173,'object',2,57977371,57977371,57977371,2,1535445365,1535445365,1535445365,'yes'),(57979201,'object',2,57977371,57977371,57977371,2,1565168526,1565168526,1565168526,'yes'),(57979202,'object',2,57977371,57977371,57977371,2,1565168526,1565168526,1565168526,'yes'),(57979204,'object',14,57979198,57977371,57977371,1,1568896313,1568899480,1568896313,'yes'),(57979205,'object',433,57979198,57977371,57979204,1,1568896322,1571250840,1568896322,'yes'),(57979206,'object',434,57979198,57977371,57979204,1,1568896324,1571250840,1568896324,'yes'),(57979208,'object',435,0,57977371,57979204,1,1571250839,1571250839,1571250839,'yes'),(57979211,'object',2,57977371,57977371,57977371,2,1572526435,1572526435,1572526435,'yes');
/*!40000 ALTER TABLE `elgg_entities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elgg_entity_relationships`
--

DROP TABLE IF EXISTS `elgg_entity_relationships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elgg_entity_relationships` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `guid_one` bigint(20) unsigned NOT NULL,
  `relationship` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guid_two` bigint(20) unsigned NOT NULL,
  `time_created` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `guid_one` (`guid_one`,`relationship`,`guid_two`),
  KEY `relationship` (`relationship`),
  KEY `guid_two` (`guid_two`)
) ENGINE=InnoDB AUTO_INCREMENT=169468389 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elgg_entity_relationships`
--

LOCK TABLES `elgg_entity_relationships` WRITE;
/*!40000 ALTER TABLE `elgg_entity_relationships` DISABLE KEYS */;
INSERT INTO `elgg_entity_relationships` VALUES (169467721,57978081,'active_plugin',57977371,1526488127),(169467731,57978901,'active_plugin',57977371,1526488127),(169467741,57978951,'active_plugin',57977371,1526488127),(169467751,57978511,'active_plugin',57977371,1526488127),(169467761,57978521,'active_plugin',57977371,1526488127),(169467771,57977651,'active_plugin',57977371,1526488127),(169467811,57978071,'active_plugin',57977371,1526488127),(169467831,57977621,'active_plugin',57977371,1526488127),(169467841,57977711,'active_plugin',57977371,1526488127),(169467861,57977731,'active_plugin',57977371,1526488128),(169467871,57977781,'active_plugin',57977371,1526488128),(169467881,57977841,'active_plugin',57977371,1526488128),(169467891,57977881,'active_plugin',57977371,1526488128),(169467901,57977991,'active_plugin',57977371,1526488128),(169467931,57978121,'active_plugin',57977371,1526488128),(169467941,57978231,'active_plugin',57977371,1526488128),(169467961,57978291,'active_plugin',57977371,1526488128),(169467971,57978321,'active_plugin',57977371,1526488128),(169467981,57978501,'active_plugin',57977371,1526488128),(169467991,57978581,'active_plugin',57977371,1526488128),(169468001,57978611,'active_plugin',57977371,1526488128),(169468021,57978671,'active_plugin',57977371,1526488128),(169468041,57978881,'active_plugin',57977371,1526488128),(169468051,57978891,'active_plugin',57977371,1526488128),(169468061,57978911,'active_plugin',57977371,1526488128),(169468081,57978971,'active_plugin',57977371,1526488128),(169468091,57978921,'active_plugin',57977371,1526488128),(169468111,57978331,'active_plugin',57977371,1526488128),(169468121,57977631,'active_plugin',57977371,1526488128),(169468141,57977961,'active_plugin',57977371,1526488129),(169468151,57978161,'active_plugin',57977371,1526488129),(169468161,57978491,'active_plugin',57977371,1526488129),(169468171,57977811,'active_plugin',57977371,1526488129),(169468201,57977891,'active_plugin',57977371,1526488129),(169468241,57978101,'active_plugin',57977371,1526488259),(169468251,57978281,'active_plugin',57977371,1526488267),(169468291,57978631,'active_plugin',57977371,1526488297),(169468301,57978271,'active_plugin',57977371,1526488306),(169468342,57979052,'active_plugin',57977371,1527050775),(169468357,57978591,'active_plugin',57977371,1529325479),(169468381,57979173,'active_plugin',57977371,1559653002),(169468388,57978431,'active_plugin',57977371,1572526440);
/*!40000 ALTER TABLE `elgg_entity_relationships` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elgg_entity_subtypes`
--

DROP TABLE IF EXISTS `elgg_entity_subtypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elgg_entity_subtypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('object','user','group','site') COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtype` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `class` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`,`subtype`)
) ENGINE=InnoDB AUTO_INCREMENT=436 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elgg_entity_subtypes`
--

LOCK TABLES `elgg_entity_subtypes` WRITE;
/*!40000 ALTER TABLE `elgg_entity_subtypes` DISABLE KEYS */;
INSERT INTO `elgg_entity_subtypes` VALUES (1,'object','file','ElggFile'),(2,'object','plugin','ElggPlugin'),(3,'object','widget','ElggWidget'),(4,'object','bookmarks',''),(5,'object','image','TidypicsImage'),(6,'object','album','TidypicsAlbum'),(7,'object','forum',''),(8,'object','custom_profile_field','ProfileManagerCustomProfileField'),(9,'object','custom_profile_field_category','ProfileManagerCustomFieldCategory'),(10,'object','thewire','ElggWire'),(11,'object','messages',''),(12,'object','blog','ElggBlog'),(13,'object','page_top',''),(14,'object','page',''),(15,'object','sitemessage',''),(16,'object','customstylebackground',''),(17,'object','customstylecolors',''),(18,'object','groupforumtopic',''),(19,'object','reported_content',''),(20,'object','event_calendar',''),(21,'object','moddefaultwidgets',''),(22,'object','pages_welcome',''),(24,'object','custom_group_field','ProfileManagerCustomGroupField'),(25,'object','custom_profile_type','ProfileManagerCustomProfileType'),(26,'object','about',''),(27,'object','folder',''),(28,'object','front',''),(29,'object','terms',''),(30,'object','privacy',''),(31,'object','videolist',''),(32,'object','custom_layout',''),(33,'object','group_widget',''),(34,'object','videochat_room',''),(35,'object','menu_builder_menu_item',''),(36,'object','mnet_support_keypair',''),(37,'object','mnet_support_peer',''),(38,'object','mnet_support_sso_session',''),(39,'object','faq','UserSupportFAQ'),(40,'object','help','UserSupportHelp'),(41,'object','support_ticket','UserSupportTicket'),(42,'object','poll',''),(43,'object','poll_choice',''),(44,'object','static',''),(45,'object','event','Event'),(46,'object','eventday','EventDay'),(47,'object','eventslot','EventSlot'),(48,'object','eventquestions','EventQuestions'),(49,'object','eventregistration','EventRegistration'),(50,'object','eventregistrationform','EventRegistrationForm'),(51,'object','eventregistrationquestion','EventRegistrationQuestion'),(52,'object','uploadedzip','UploadedZip'),(62,'object','multi_dashboard','MultiDashboard'),(72,'object','videolist_item',''),(82,'object','personal_menu_item',''),(92,'object','tidypics_batch','TidypicsBatch'),(102,'object','admin_notice',''),(112,'object','widget_favorite',''),(122,'object','etherpad',''),(132,'object','ws_pack_application','APIApplication'),(142,'object','ws_pack_application_user_setting','APIApplicationUserSetting'),(152,'object','best_practice','BestPractice'),(162,'object','workorder','Workorder'),(172,'object','push_message_queue',''),(182,'object','spam_login_filter_ip',''),(192,'object','task_top',''),(202,'object','task',''),(212,'object','newsletter','Newsletter'),(222,'object','newsletter_subscription','NewsletterSubscription'),(232,'object','newsletter_content',''),(242,'object','newsletter_template',''),(252,'object','question','ElggQuestion'),(262,'object','answer','ElggAnswer'),(272,'object','paidsubscription',''),(282,'object','site_announcement',''),(292,'object','intanswer','ElggIntAnswer'),(302,'object','questions_workflow_phase','QuestionsWorkflowPhase'),(312,'object','api_key',''),(322,'object','quicklink',''),(332,'object','todolist','TodoList'),(342,'object','todoitem','TodoItem'),(352,'object','profile_sync_datasource',''),(362,'object','profile_sync_config',''),(372,'object','wizard','Wizard'),(382,'object','cafe','ElggCafe'),(392,'object','comment','ElggComment'),(402,'object','tangram_vacancy','TangramVacancy'),(412,'object','videos','ElggVideos'),(422,'object','news','ElggNews'),(432,'object','wiki',''),(433,'object','row',''),(434,'object','page_widget',''),(435,'object','column','');
/*!40000 ALTER TABLE `elgg_entity_subtypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elgg_entity_views`
--

DROP TABLE IF EXISTS `elgg_entity_views`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elgg_entity_views` (
  `guid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('object','user','group','site') COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtype` int(11) DEFAULT NULL,
  `container_guid` bigint(20) unsigned NOT NULL,
  `site_guid` bigint(20) unsigned NOT NULL,
  `views` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`guid`),
  KEY `type` (`type`),
  KEY `subtype` (`subtype`),
  KEY `container_guid` (`container_guid`),
  KEY `views` (`views`)
) ENGINE=InnoDB AUTO_INCREMENT=57979207 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `elgg_entity_views_log`
--

DROP TABLE IF EXISTS `elgg_entity_views_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elgg_entity_views_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `entity_guid` bigint(20) NOT NULL,
  `type` enum('object','user','group','site') COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtype` int(11) DEFAULT NULL,
  `container_guid` bigint(20) unsigned NOT NULL,
  `site_guid` bigint(20) unsigned NOT NULL,
  `performed_by_guid` bigint(20) unsigned NOT NULL,
  `time_created` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type_subtype` (`type`,`subtype`),
  KEY `performed_by_guid` (`performed_by_guid`),
  KEY `time_created` (`time_created`),
  KEY `site_guid` (`site_guid`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `elgg_geocode_cache`
--

DROP TABLE IF EXISTS `elgg_geocode_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elgg_geocode_cache` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `location` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lat` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `long` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `location` (`location`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elgg_geocode_cache`
--

LOCK TABLES `elgg_geocode_cache` WRITE;
/*!40000 ALTER TABLE `elgg_geocode_cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `elgg_geocode_cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elgg_groups_entity`
--

DROP TABLE IF EXISTS `elgg_groups_entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elgg_groups_entity` (
  `guid` bigint(20) unsigned NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`guid`),
  KEY `name` (`name`(50)),
  KEY `description` (`description`(50)),
  FULLTEXT KEY `name_2` (`name`,`description`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elgg_groups_entity`
--

LOCK TABLES `elgg_groups_entity` WRITE;
/*!40000 ALTER TABLE `elgg_groups_entity` DISABLE KEYS */;
/*!40000 ALTER TABLE `elgg_groups_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elgg_hmac_cache`
--

DROP TABLE IF EXISTS `elgg_hmac_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elgg_hmac_cache` (
  `hmac` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ts` bigint(20) NOT NULL,
  PRIMARY KEY (`hmac`),
  KEY `ts` (`ts`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elgg_hmac_cache`
--

LOCK TABLES `elgg_hmac_cache` WRITE;
/*!40000 ALTER TABLE `elgg_hmac_cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `elgg_hmac_cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elgg_metadata`
--

DROP TABLE IF EXISTS `elgg_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elgg_metadata` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `entity_guid` bigint(20) unsigned NOT NULL,
  `name_id` bigint(20) NOT NULL,
  `value_id` bigint(20) NOT NULL,
  `value_type` enum('integer','text') COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner_guid` bigint(20) unsigned NOT NULL,
  `site_guid` bigint(20) unsigned DEFAULT NULL,
  `access_id` bigint(20) NOT NULL,
  `time_created` bigint(20) NOT NULL,
  `enabled` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`id`),
  KEY `entity_guid` (`entity_guid`),
  KEY `name_id` (`name_id`),
  KEY `value_id` (`value_id`),
  KEY `owner_guid` (`owner_guid`),
  KEY `access_id` (`access_id`)
) ENGINE=InnoDB AUTO_INCREMENT=381636912 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elgg_metadata`
--

LOCK TABLES `elgg_metadata` WRITE;
/*!40000 ALTER TABLE `elgg_metadata` DISABLE KEYS */;
INSERT INTO `elgg_metadata` VALUES (381635831,57977381,297,125,'integer',57977371,57977371,2,1526488078,'yes'),(381635841,57977381,208,18074,'text',57977371,57977371,2,1526488078,'yes'),(381635851,57977381,200,125,'integer',57977371,57977371,2,1526488078,'yes'),(381635861,57977391,297,76523921,'integer',57977371,57977371,1,1526488078,'yes'),(381635871,57977391,208,1577552,'text',57977371,57977371,1,1526488078,'yes'),(381635881,57977391,200,11,'integer',57977371,57977371,1,1526488078,'yes'),(381635891,57977401,297,76523921,'integer',57977371,57977371,1,1526488078,'yes'),(381635901,57977401,208,1577562,'text',57977371,57977371,1,1526488078,'yes'),(381635911,57977401,200,51,'integer',57977371,57977371,1,1526488078,'yes'),(381635921,57977411,297,125,'integer',57977371,57977371,1,1526488078,'yes'),(381635931,57977411,208,18036,'text',57977371,57977371,1,1526488078,'yes'),(381635941,57977411,200,202,'integer',57977371,57977371,1,1526488078,'yes'),(381635951,57977421,297,76523931,'integer',57977371,57977371,1,1526488078,'yes'),(381635961,57977421,208,1577522,'text',57977371,57977371,1,1526488078,'yes'),(381635971,57977421,200,203,'integer',57977371,57977371,1,1526488078,'yes'),(381635981,57977431,297,76523931,'integer',57977371,57977371,1,1526488079,'yes'),(381635991,57977431,208,1577532,'text',57977371,57977371,1,1526488079,'yes'),(381636001,57977431,200,204,'integer',57977371,57977371,1,1526488079,'yes'),(381636011,57977441,297,76523931,'integer',57977371,57977371,1,1526488079,'yes'),(381636021,57977441,208,1577572,'text',57977371,57977371,1,1526488079,'yes'),(381636031,57977441,200,205,'integer',57977371,57977371,1,1526488079,'yes'),(381636041,57977451,297,125,'integer',57977371,57977371,1,1526488079,'yes'),(381636051,57977451,208,18036,'text',57977371,57977371,1,1526488079,'yes'),(381636061,57977451,200,206,'integer',57977371,57977371,1,1526488079,'yes'),(381636071,57977461,297,76523941,'integer',57977371,57977371,1,1526488079,'yes'),(381636081,57977461,208,1601942,'text',57977371,57977371,1,1526488079,'yes'),(381636091,57977461,200,207,'integer',57977371,57977371,1,1526488079,'yes'),(381636101,57977471,297,76523941,'integer',57977371,57977371,1,1526488079,'yes'),(381636111,57977471,208,1584102,'text',57977371,57977371,1,1526488079,'yes'),(381636121,57977471,200,209,'integer',57977371,57977371,1,1526488079,'yes'),(381636131,57977481,297,125,'integer',57977371,57977371,1,1526488079,'yes'),(381636141,57977481,208,18036,'text',57977371,57977371,1,1526488079,'yes'),(381636151,57977481,200,260,'integer',57977371,57977371,1,1526488079,'yes'),(381636161,57977491,297,76523951,'integer',57977371,57977371,1,1526488079,'yes'),(381636171,57977491,208,1577492,'text',57977371,57977371,1,1526488079,'yes'),(381636181,57977491,200,734,'integer',57977371,57977371,1,1526488079,'yes'),(381636191,57977501,297,76523951,'integer',57977371,57977371,1,1526488079,'yes'),(381636201,57977501,208,1577482,'text',57977371,57977371,1,1526488079,'yes'),(381636211,57977501,200,754,'integer',57977371,57977371,1,1526488079,'yes'),(381636221,57977511,297,76523951,'integer',57977371,57977371,1,1526488079,'yes'),(381636231,57977511,208,1577502,'text',57977371,57977371,1,1526488079,'yes'),(381636241,57977511,200,755,'integer',57977371,57977371,1,1526488080,'yes'),(381636251,57977521,297,125,'integer',57977371,57977371,1,1526488080,'yes'),(381636261,57977521,208,18036,'text',57977371,57977371,1,1526488080,'yes'),(381636271,57977521,200,756,'integer',57977371,57977371,1,1526488080,'yes'),(381636281,57977531,297,76523961,'integer',57977371,57977371,1,1526488080,'yes'),(381636291,57977531,208,1574742,'text',57977371,57977371,1,1526488080,'yes'),(381636301,57977531,200,757,'integer',57977371,57977371,1,1526488080,'yes'),(381636311,57977541,297,76523961,'integer',57977371,57977371,1,1526488080,'yes'),(381636321,57977541,208,1576932,'text',57977371,57977371,1,1526488080,'yes'),(381636331,57977541,200,758,'integer',57977371,57977371,1,1526488080,'yes'),(381636341,57977551,297,125,'integer',57977371,57977371,0,1526488080,'yes'),(381636351,57977551,208,1426092,'text',57977371,57977371,0,1526488080,'yes'),(381636361,57977551,200,759,'integer',57977371,57977371,0,1526488080,'yes'),(381636371,57977561,297,76523971,'integer',57977371,57977371,0,1526488080,'yes'),(381636381,57977561,208,1601982,'text',57977371,57977371,0,1526488080,'yes'),(381636391,57977561,200,760,'integer',57977371,57977371,0,1526488080,'yes'),(381636401,57977571,297,76523971,'integer',57977371,57977371,0,1526488080,'yes'),(381636411,57977571,208,1601992,'text',57977371,57977371,0,1526488080,'yes'),(381636421,57977571,200,316,'integer',57977371,57977371,0,1526488080,'yes'),(381636431,57977581,297,76523971,'integer',57977371,57977371,0,1526488080,'yes'),(381636441,57977581,208,1602002,'text',57977371,57977371,0,1526488080,'yes'),(381636451,57977581,200,765,'integer',57977371,57977371,0,1526488080,'yes'),(381636461,57977591,297,76523971,'integer',57977371,57977371,0,1526488080,'yes'),(381636471,57977591,208,1574752,'text',57977371,57977371,0,1526488080,'yes'),(381636481,57977591,200,791,'integer',57977371,57977371,0,1526488081,'yes'),(381636483,57979053,76523974,11,'text',0,NULL,2,1527676328,'yes'),(381636484,57979053,76523975,76523976,'text',0,NULL,2,1527676328,'yes'),(381636497,57979094,76523973,11,'text',57979094,NULL,2,1527965797,'yes'),(381636504,57979122,76524002,76524003,'text',57979053,NULL,1,1528002792,'yes'),(381636505,57979122,76524004,76524005,'text',57979053,NULL,1,1528002792,'yes'),(381636506,57979122,76524006,76524007,'text',57979053,NULL,1,1528002792,'yes'),(381636517,57979129,76523995,76524001,'text',57979053,NULL,1,1528003072,'yes'),(381636626,57979053,76523973,11,'text',57979053,NULL,2,1529322852,'yes'),(381636634,57979053,76524055,76524056,'text',57979053,NULL,1,1529923394,'yes'),(381636802,57979182,76523973,11,'text',57979182,NULL,2,1546436154,'yes'),(381636806,57979184,76523973,11,'text',57979184,NULL,2,1553521500,'yes'),(381636807,57979185,76523973,11,'text',57979185,NULL,2,1553611054,'yes'),(381636808,57979185,76523974,11,'text',0,NULL,2,1553611054,'yes'),(381636809,57979185,76523975,76523976,'text',0,NULL,2,1553611054,'yes'),(381636810,57979186,76523973,11,'text',57979186,NULL,2,1553611512,'yes'),(381636811,57979186,76523974,11,'text',0,NULL,2,1553611512,'yes'),(381636812,57979186,76523975,76523976,'text',0,NULL,2,1553611512,'yes'),(381636863,57979194,76523973,11,'text',57979194,NULL,2,1556176939,'yes'),(381636864,57979194,76523974,11,'text',0,NULL,2,1556176939,'yes'),(381636865,57979194,76523975,76523976,'text',0,NULL,2,1556176939,'yes'),(381636866,57979195,76523973,11,'text',57979195,NULL,2,1556260944,'yes'),(381636867,57979195,76523974,11,'text',0,NULL,2,1556260944,'yes'),(381636868,57979195,76523975,76523976,'text',0,NULL,2,1556260944,'yes'),(381636869,57979196,76523973,11,'text',57979196,NULL,2,1556610989,'yes'),(381636870,57979196,76523974,11,'text',0,NULL,2,1556610989,'yes'),(381636871,57979196,76523975,76523976,'text',0,NULL,2,1556610989,'yes'),(381636873,57979197,76523974,11,'text',0,NULL,2,1559654435,'yes'),(381636874,57979197,76523975,76523976,'text',0,NULL,2,1559654435,'yes'),(381636878,57979129,76523982,76524096,'text',57979053,NULL,1,1560258890,'yes'),(381636879,57979198,76523973,11,'text',57979198,NULL,2,1560931159,'yes'),(381636880,57979198,76523974,11,'text',0,NULL,2,1560931159,'yes'),(381636881,57979198,76523975,76523976,'text',0,NULL,2,1560931159,'yes'),(381636883,57979197,76523973,125,'text',57979197,NULL,2,1561976805,'yes'),(381636884,57979199,76523973,11,'text',57979199,NULL,2,1562077834,'yes'),(381636885,57979199,76523974,11,'text',0,NULL,2,1562077834,'yes'),(381636886,57979199,76523975,76523976,'text',0,NULL,2,1562077834,'yes'),(381636887,57979200,76523973,11,'text',57979200,NULL,2,1564562523,'yes'),(381636888,57979200,76523974,11,'text',0,NULL,2,1564562523,'yes'),(381636889,57979200,76523975,76523976,'text',0,NULL,2,1564562523,'yes'),(381636893,57979204,76523995,76523996,'text',57979198,NULL,1,1568896313,'yes'),(381636896,57979206,76524000,76524001,'text',57979198,NULL,1,1568896324,'yes'),(381636898,57979204,76523982,76524102,'text',57979198,NULL,1,1568899480,'yes'),(381636900,57979205,76523999,125,'integer',57979198,NULL,1,1571250839,'yes'),(381636901,57979205,297,76524104,'text',57979198,NULL,1,1571250839,'yes'),(381636902,57979208,297,76524105,'integer',0,NULL,1,1571250839,'yes'),(381636903,57979208,76523999,125,'integer',0,NULL,1,1571250839,'yes'),(381636904,57979208,76524106,754,'integer',0,NULL,1,1571250839,'yes'),(381636905,57979206,297,76524107,'integer',57979198,NULL,1,1571250839,'yes'),(381636906,57979206,76523999,125,'integer',57979198,NULL,1,1571250840,'yes'),(381636908,57979210,76523973,11,'text',57979210,NULL,2,1572526428,'yes'),(381636909,57979210,76523974,11,'text',0,NULL,2,1572526428,'yes'),(381636910,57979210,76523975,76523976,'text',0,NULL,2,1572526428,'yes'),(381636911,57977371,76524097,76524099,'text',57979210,NULL,2,1572526509,'yes');
/*!40000 ALTER TABLE `elgg_metadata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elgg_metastrings`
--

DROP TABLE IF EXISTS `elgg_metastrings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elgg_metastrings` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `string` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `string` (`string`(50))
) ENGINE=InnoDB AUTO_INCREMENT=76524109 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elgg_metastrings`
--

LOCK TABLES `elgg_metastrings` WRITE;
/*!40000 ALTER TABLE `elgg_metastrings` DISABLE KEYS */;
INSERT INTO `elgg_metastrings` VALUES (11,'1'),(51,'2'),(125,'0'),(200,'order'),(202,'3'),(203,'4'),(204,'5'),(205,'6'),(206,'7'),(207,'8'),(208,'url'),(209,'9'),(260,'10'),(297,'parent_guid'),(316,'19'),(734,'11'),(754,'12'),(755,'13'),(756,'14'),(757,'15'),(758,'16'),(759,'17'),(760,'18'),(765,'20'),(791,'21'),(18036,'#'),(18074,'[wwwroot]'),(1426092,'[wwwroot]admin'),(1574742,'[wwwroot]add'),(1574752,'[wwwroot]admin/appearance/template'),(1576932,'[wwwroot]groups/add'),(1577482,'[wwwroot]friends/[username]'),(1577492,'[wwwroot]members'),(1577502,'[wwwroot]friend_request/'),(1577522,'[wwwroot]dashboard'),(1577532,'[wwwroot]profile/[username]'),(1577552,'[wwwroot]blog/all'),(1577562,'[wwwroot]activity'),(1577572,'[wwwroot]settings'),(1584102,'[wwwroot]groups/all/?filter=pop'),(1601942,'[wwwroot]groups/member/[username]'),(1601982,'[wwwroot]admin/users/newest'),(1601992,'[wwwroot]admin/users/invite'),(1602002,'[wwwroot]admin/plugins'),(76523921,'57977381'),(76523931,'57977411'),(76523941,'57977451'),(76523951,'57977481'),(76523961,'57977521'),(76523971,'57977551'),(76523972,'email_invitation'),(76523973,'notification:method:email'),(76523974,'validated'),(76523975,'validated_method'),(76523976,'admin_user'),(76523977,'translation_editor'),(76523978,'ban_reason'),(76523979,'banned'),(76523980,'logotime'),(76523981,'1527759412'),(76523982,'richDescription'),(76523983,'{\"entityMap\":{},\"blocks\":[{\"key\":\"d1tmi\",\"text\":\"test\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[],\"data\":{}}]}'),(76523984,'write_access_id'),(76523985,'isFeatured'),(76523986,'featuredIcontime'),(76523987,'1527965728'),(76523988,'featuredPositionY'),(76523989,'50'),(76523990,'{\"entityMap\":{},\"blocks\":[{\"key\":\"4sra2\",\"text\":\"test 2\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[],\"data\":{}}]}'),(76523991,'isRecommended'),(76523992,'1527965754'),(76523993,'1528002641'),(76523994,'{\"entityMap\":{},\"blocks\":[{\"key\":\"7jk3p\",\"text\":\"Voorpagina\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[],\"data\":{}}]}'),(76523995,'pageType'),(76523996,'campagne'),(76523997,'layout'),(76523998,'full'),(76523999,'position'),(76524000,'widget_type'),(76524001,'text'),(76524002,'filename'),(76524003,'image/1528002792.jpg'),(76524004,'filestore::dir_root'),(76524005,'/app-data/sjabloon/'),(76524006,'filestore::filestore'),(76524007,'ElggDiskFilestore'),(76524008,'4/4/4'),(76524009,'8/4'),(76524010,'html'),(76524011,'activity'),(76524012,'{\"entityMap\":{},\"blocks\":[{\"key\":\"8pp6i\",\"text\":\"Contact\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[],\"data\":{}}]}'),(76524013,'{\"entityMap\":{},\"blocks\":[{\"key\":\"8jd6e\",\"text\":\"rvcrfv\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[],\"data\":{}}]}'),(76524014,'membership'),(76524015,'{\"entityMap\":{},\"blocks\":[{\"key\":\"4vg41\",\"text\":\"test\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[],\"data\":{}}]}'),(76524016,'introduction'),(76524017,'{\"entityMap\":{},\"blocks\":[{\"key\":\"4h5nr\",\"text\":\"\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[],\"data\":{}}]}'),(76524018,'plugins'),(76524019,'events'),(76524020,'blog'),(76524021,'files'),(76524022,'discussion'),(76524023,'wiki'),(76524024,'autoNotification'),(76524025,'group_acl'),(76524026,'1528003240'),(76524027,'icontime'),(76524028,'{\"entityMap\":{},\"blocks\":[{\"key\":\"4f892\",\"text\":\"presentatie van de site en groepen\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[],\"data\":{}}]}'),(76524029,'tags'),(76524030,''),(76524031,'{\"entityMap\":{},\"blocks\":[{\"key\":\"445ko\",\"text\":\"test\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[],\"data\":{}}]}'),(76524032,'1528003315'),(76524033,'start_day'),(76524034,'1528004546'),(76524035,'start_time'),(76524036,'end_ts'),(76524037,'rsvp'),(76524038,'questions'),(76524039,'tasks'),(76524040,'{\"entityMap\":{},\"blocks\":[{\"key\":\"93hoi\",\"text\":\"test\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[],\"data\":{}}]}'),(76524041,'state'),(76524042,'BUSY'),(76524043,'DONE'),(76524044,'NEW'),(76524045,'1e6901b93cfa78ebfbb7a68aa9e1a7e1'),(76524046,'1e6901b93cfa78ebfbb7a68aa9e1a7e1|marcel@pleio.nl'),(76524047,'leader'),(76524048,'lead'),(76524049,'8d98a14228e90f6b958866da69381c07'),(76524050,'8d98a14228e90f6b958866da69381c07|marcelsjabloon@pleio.nl'),(76524051,'410c04ea22a52bdcc79ae1e52e381ab4'),(76524052,'410c04ea22a52bdcc79ae1e52e381ab4|info@pleio.nl'),(76524053,'6c34d09e1beb540c3bb094fad0a45b6a'),(76524054,'6c34d09e1beb540c3bb094fad0a45b6a|mh.ziemerink@gmail.com'),(76524055,'site'),(76524056,'www.pleio.nl'),(76524057,'{\"entityMap\":{},\"blocks\":[{\"key\":\"bcntn\",\"text\":\"crcrc\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[],\"data\":{}}]}'),(76524058,'{\"entityMap\":{},\"blocks\":[{\"key\":\"at5an\",\"text\":\"crcrc\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[],\"data\":{}}]}'),(76524059,'{\"entityMap\":{},\"blocks\":[{\"key\":\"bpdlu\",\"text\":\"vtvgvtvvtv\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[],\"data\":{}}]}'),(76524060,'{\"entityMap\":{},\"blocks\":[{\"key\":\"fu0hq\",\"text\":\"vtv\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[],\"data\":{}}]}'),(76524061,'{\"entityMap\":{},\"blocks\":[{\"key\":\"277dm\",\"text\":\"vcrtv\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[],\"data\":{}}]}'),(76524062,'{\"entityMap\":{},\"blocks\":[{\"key\":\"emgk8\",\"text\":\"cvrvc\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[],\"data\":{}}]}'),(76524063,'6/6'),(76524064,'{\"entityMap\":{},\"blocks\":[{\"key\":\"1f77r\",\"text\":\"test\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[],\"data\":{}}]}'),(76524065,'1531737979'),(76524066,'1531692000'),(76524067,'1531738939'),(76524068,'7t8xh4c4xlrsl3hnk8y4kzm4hgzcywqp|test@pleio.nl'),(76524069,'site_invitation'),(76524070,'rvzmfr9b6yp8dlxrvwffh8hkwr48rvv7|test3@pleio.nl'),(76524071,'krd4xzkrj4dmwvtzd7qg4w7ljw2ztj86|test4@pleio.nl'),(76524072,'dsng4ffsb7lkc62dywb8x4dwq622yz3q|test5@pleio.nl'),(76524073,'v22l478h9yfp89qt3drpmnspfp8x8mcg|test6@pleio.nl'),(76524074,'tndbkv8lj9jt4hb9rryxw9qcdzm2x7m9|test2@pleio.nl'),(76524075,'7pfhjsjrvwq8pcq7j9nc47v92h6xdrb7|test7@pleio.nl'),(76524076,'bsyqxfm9p7mbg7k2r7vbvbp4wxmkppy3|test8@pleio.nl'),(76524077,'8pfkdgpdhgnlkmchdzlv82wvtsxsdn4n|test9@pleio.nl'),(76524078,'bpq3bgcffg8wqnlz8j6vqths3cbkf3kt|test10@pleio.nl'),(76524079,'84q46nv7ygl4m3kzx3bllqcldm2pmmy9|test11@pleio.nl'),(76524080,'wqhmcpbhpp3xjhpszhwwhpvd6nv2sv44|test12@pleio.nl'),(76524081,'{\"blocks\":[{\"key\":\"7jh8\",\"text\":\"crfvcfr\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[],\"data\":{}}],\"entityMap\":{}}'),(76524082,'1542009600'),(76524083,'1542027600'),(76524084,'objects'),(76524085,'1544465401'),(76524086,'logo_extension'),(76524087,'png'),(76524088,'1553612876'),(76524089,'1553613489'),(76524090,'{\"blocks\":[{\"key\":\"e74fs\",\"text\":\"Test\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[],\"data\":{}}],\"entityMap\":{}}'),(76524091,'{\"blocks\":[{\"key\":\"2qia6\",\"text\":\"\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[],\"data\":{}}],\"entityMap\":{}}'),(76524092,'{\"blocks\":[{\"key\":\"66vug\",\"text\":\"test\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[],\"data\":{}}],\"entityMap\":{}}'),(76524093,'{\"blocks\":[{\"key\":\"8pp6i\",\"text\":\"Sitebeheerder: \",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[{\"offset\":0,\"length\":14,\"style\":\"BOLD\"},{\"offset\":0,\"length\":14,\"style\":\"ITALIC\"}],\"entityRanges\":[],\"data\":{}},{\"key\":\"6mqh4\",\"text\":\"E-mailadres:\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[{\"offset\":0,\"length\":12,\"style\":\"BOLD\"},{\"offset\":0,\"length\":12,\"style\":\"ITALIC\"}],\"entityRanges\":[],\"data\":{}}],\"entityMap\":{}}'),(76524094,'{\"blocks\":[{\"key\":\"8pp6i\",\"text\":\"Sitebeheerder: \",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[{\"offset\":0,\"length\":14,\"style\":\"ITALIC\"}],\"entityRanges\":[],\"data\":{}},{\"key\":\"6mqh4\",\"text\":\"E-mailadres:\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[{\"offset\":0,\"length\":12,\"style\":\"ITALIC\"}],\"entityRanges\":[],\"data\":{}}],\"entityMap\":{}}'),(76524095,'{\"blocks\":[{\"key\":\"8pp6i\",\"text\":\"Voor vragen kunt u terecht bij\",\"type\":\"intro\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[],\"data\":{}},{\"key\":\"6eggd\",\"text\":\"Sitebeheerder: \",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[{\"offset\":0,\"length\":14,\"style\":\"ITALIC\"}],\"entityRanges\":[],\"data\":{}},{\"key\":\"6mqh4\",\"text\":\"E-mailadres:\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[{\"offset\":0,\"length\":12,\"style\":\"ITALIC\"}],\"entityRanges\":[],\"data\":{}}],\"entityMap\":{}}'),(76524096,'{\"blocks\":[{\"key\":\"8pp6i\",\"text\":\"Voor vragen kunt u terecht bij:\",\"type\":\"intro\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[],\"data\":{}},{\"key\":\"6eggd\",\"text\":\"\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[],\"data\":{}}],\"entityMap\":{}}'),(76524097,'email'),(76524098,'nn@gmail.com'),(76524099,'noreply@pleio.nl'),(76524100,'admin_notice_id'),(76524101,'cannot_startfrench_translation'),(76524102,'{\"blocks\":[{\"key\":\"8a0hm\",\"text\":\"Toegankelijkheidsverklaring\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[],\"data\":{}}],\"entityMap\":{}}'),(76524103,'cannot_startevent_manager'),(76524104,'57979204'),(76524105,'57979205'),(76524106,'width'),(76524107,'57979208'),(76524108,'cannot_startpleio_template');
/*!40000 ALTER TABLE `elgg_metastrings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elgg_notifications`
--

DROP TABLE IF EXISTS `elgg_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elgg_notifications` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_guid` bigint(20) unsigned NOT NULL,
  `action` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `performer_guid` bigint(20) unsigned NOT NULL,
  `entity_guid` bigint(20) unsigned NOT NULL,
  `container_guid` bigint(20) unsigned NOT NULL,
  `unread` enum('yes','no') COLLATE utf8mb4_unicode_ci DEFAULT 'yes',
  `site_guid` bigint(20) unsigned NOT NULL,
  `time_created` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_guid` (`user_guid`,`site_guid`),
  KEY `unread` (`unread`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `elgg_objects_entity`
--

DROP TABLE IF EXISTS `elgg_objects_entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elgg_objects_entity` (
  `guid` bigint(20) unsigned NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`guid`),
  FULLTEXT KEY `title` (`title`,`description`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elgg_objects_entity`
--

LOCK TABLES `elgg_objects_entity` WRITE;
/*!40000 ALTER TABLE `elgg_objects_entity` DISABLE KEYS */;
INSERT INTO `elgg_objects_entity` VALUES (57977381,'Voorpagina',''),(57977391,'Alle blogs',''),(57977401,'Alle activiteiten',''),(57977411,'Mijn pagina',''),(57977421,'Mijn dashboard',''),(57977431,'Mijn profielpagina',''),(57977441,'Mijn instellingen',''),(57977451,'Groepen',''),(57977461,'Mijn groepen',''),(57977471,'Alle groepen',''),(57977481,'Leden',''),(57977491,'Zoeken',''),(57977501,'Mijn contacten',''),(57977511,'Contactverzoeken',''),(57977521,'Toevoegen',''),(57977531,'Content toevoegen',''),(57977541,'Nieuwe groep maken',''),(57977551,'Beheer',''),(57977561,'Gebruikersbeheer',''),(57977571,'Nodig leden uit',''),(57977581,'Pluginbeheer',''),(57977591,'Beheer template',''),(57977601,'account_removal',''),(57977611,'addthis',''),(57977621,'advanced_comments',''),(57977631,'advanced_notifications',''),(57977651,'analytics',''),(57977661,'apiadmin',''),(57977671,'backup',''),(57977701,'birthdays',''),(57977711,'blog',''),(57977721,'blog_tools',''),(57977731,'bookmarks',''),(57977741,'bookmarks_tools',''),(57977751,'categories',''),(57977781,'content_redirector',''),(57977791,'content_subscriptions',''),(57977811,'csv_exporter',''),(57977821,'custom_css',''),(57977841,'dashboard',''),(57977871,'digest',''),(57977881,'dutch_translation',''),(57977891,'elasticsearch',''),(57977921,'embed',''),(57977931,'embedded_login',''),(57977941,'entity_tools',''),(57977951,'entity_view_counter',''),(57977961,'etherpad',''),(57977971,'event_manager',''),(57977981,'externalpages',''),(57977991,'file',''),(57978001,'file_tools',''),(57978011,'fontawesome',''),(57978021,'french_translation',''),(57978071,'group_tools',''),(57978081,'groups',''),(57978101,'html_email_handler',''),(57978121,'htmlawed',''),(57978151,'language_selector',''),(57978161,'lazy_hover',''),(57978171,'likes',''),(57978211,'members',''),(57978221,'mentions',''),(57978231,'menu_builder',''),(57978241,'messageboard',''),(57978271,'news',''),(57978281,'newsletter',''),(57978291,'notifications',''),(57978311,'odt_editor',''),(57978321,'pages',''),(57978331,'pages_tools',''),(57978361,'pinboard',''),(57978381,'pleio_beconnummer',''),(57978421,'pleio_rest',''),(57978431,'pleio_template',''),(57978441,'pleio_template_selector',''),(57978451,'pleio_test',''),(57978471,'pleiobox',''),(57978481,'pleiofile',''),(57978491,'plugin_manager',''),(57978501,'polls',''),(57978511,'profile',''),(57978521,'profile_manager',''),(57978551,'questions',''),(57978561,'quicklinks',''),(57978581,'reportedcontent',''),(57978591,'rewrite',''),(57978601,'rijkshuisstijl',''),(57978611,'search',''),(57978631,'security_tools',''),(57978671,'static',''),(57978691,'tasks',''),(57978881,'thewire',''),(57978891,'thewire_tools',''),(57978901,'tidypics',''),(57978911,'tinymce',''),(57978921,'tinymce_extended',''),(57978931,'todos',''),(57978951,'translation_editor',''),(57978971,'uservalidationbyemail',''),(57978981,'videolist',''),(57978991,'videos',''),(57979021,'widget_manager',''),(57979031,'wizard',''),(57979041,'ws_pack',''),(57979052,'pleio',''),(57979054,'',''),(57979055,'',''),(57979056,'',''),(57979057,'',''),(57979058,'',''),(57979059,'',''),(57979060,'',''),(57979061,'croncheck',''),(57979062,'custom_js',''),(57979063,'flow',''),(57979064,'friend_request',''),(57979065,'garbagecollector',''),(57979066,'graphql',''),(57979067,'html_pages',''),(57979068,'messages',''),(57979069,'tagcloud',''),(57979070,'template',''),(57979071,'transfer',''),(57979072,'user_support',''),(57979073,'',''),(57979074,'',''),(57979075,'',''),(57979076,'',''),(57979077,'',''),(57979078,'',''),(57979079,'',''),(57979080,'',''),(57979081,'',''),(57979082,'',''),(57979083,'',''),(57979084,'',''),(57979085,'',''),(57979086,'',''),(57979087,'',''),(57979088,'',''),(57979089,'',''),(57979090,'',''),(57979091,'',''),(57979095,'best_practices',''),(57979096,'elggx_fivestar',''),(57979097,'global_tags',''),(57979098,'group_custom_layout',''),(57979099,'haarlem_tangram',''),(57979100,'ictu_laa',''),(57979101,'od_rivierenland_modifications',''),(57979102,'pleio_main_template',''),(57979103,'profile_sync',''),(57979104,'promo_widget',''),(57979105,'recaptcha',''),(57979106,'search_advanced',''),(57979107,'simplesaml',''),(57979108,'site_announcements',''),(57979109,'theme_eersel',''),(57979110,'theme_ffd',''),(57979111,'theme_giessenlanden',''),(57979112,'theme_haarlem',''),(57979113,'theme_haarlem_intranet',''),(57979114,'theme_heerhugowaard',''),(57979115,'theme_oirschot',''),(57979116,'theme_rdm',''),(57979117,'theme_stadsdeelcommunity',''),(57979118,'walled_garden_by_ip',''),(57979122,'',''),(57979129,'Contact','Voor vragen kunt u terecht bij:'),(57979173,'cookie_consent',''),(57979201,'browsealoud',''),(57979202,'profile_sync_api',''),(57979204,'Toegankelijkheidsverklaring','Toegankelijkheidsverklaring'),(57979205,'',''),(57979206,'',''),(57979208,'',''),(57979211,'_theme_haarlem_intranet','');
/*!40000 ALTER TABLE `elgg_objects_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elgg_private_settings`
--

DROP TABLE IF EXISTS `elgg_private_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elgg_private_settings` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `entity_guid` bigint(20) NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `entity_guid` (`entity_guid`,`name`),
  KEY `name` (`name`),
  KEY `value` (`value`(50))
) ENGINE=InnoDB AUTO_INCREMENT=3095451201 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elgg_private_settings`
--

LOCK TABLES `elgg_private_settings` WRITE;
/*!40000 ALTER TABLE `elgg_private_settings` DISABLE KEYS */;
INSERT INTO `elgg_private_settings` VALUES (3039324101,57977601,'elgg:internal:priority','14'),(3039324291,57977611,'elgg:internal:priority','55'),(3039324471,57977621,'elgg:internal:priority','18'),(3039324491,57977631,'elgg:internal:priority','61'),(3039324531,57977651,'elgg:internal:priority','9'),(3039324551,57977661,'elgg:internal:priority','80'),(3039324571,57977671,'elgg:internal:priority','88'),(3039324631,57977701,'elgg:internal:priority','91'),(3039324651,57977711,'elgg:internal:priority','19'),(3039324671,57977721,'elgg:internal:priority','20'),(3039324691,57977731,'elgg:internal:priority','21'),(3039324711,57977741,'elgg:internal:priority','70'),(3039324731,57977751,'elgg:internal:priority','22'),(3039324791,57977781,'elgg:internal:priority','23'),(3039324811,57977791,'elgg:internal:priority','75'),(3039324851,57977811,'elgg:internal:priority','78'),(3039324871,57977821,'elgg:internal:priority','120'),(3039324911,57977841,'elgg:internal:priority','25'),(3039324971,57977871,'elgg:internal:priority','15'),(3039324991,57977881,'elgg:internal:priority','26'),(3039325011,57977891,'elgg:internal:priority','89'),(3039325071,57977921,'elgg:internal:priority','27'),(3039325091,57977931,'elgg:internal:priority','68'),(3039325111,57977941,'elgg:internal:priority','57'),(3039325131,57977951,'elgg:internal:priority','66'),(3039325151,57977961,'elgg:internal:priority','62'),(3039325171,57977971,'elgg:internal:priority','17'),(3039325191,57977981,'elgg:internal:priority','28'),(3039325211,57977991,'elgg:internal:priority','29'),(3039325231,57978001,'elgg:internal:priority','54'),(3039325251,57978011,'elgg:internal:priority','79'),(3039325371,57978071,'elgg:internal:priority','16'),(3039325391,57978081,'elgg:internal:priority','2'),(3039325431,57978101,'elgg:internal:priority','13'),(3039325471,57978121,'elgg:internal:priority','32'),(3039325531,57978151,'elgg:internal:priority','58'),(3039325551,57978161,'elgg:internal:priority','67'),(3039325571,57978171,'elgg:internal:priority','5'),(3039325651,57978211,'elgg:internal:priority','33'),(3039325671,57978221,'elgg:internal:priority','83'),(3039325691,57978231,'elgg:internal:priority','34'),(3039325711,57978241,'elgg:internal:priority','35'),(3039325771,57978271,'elgg:internal:priority','99'),(3039325791,57978281,'elgg:internal:priority','72'),(3039325811,57978291,'elgg:internal:priority','37'),(3039325851,57978311,'elgg:internal:priority','53'),(3039325871,57978321,'elgg:internal:priority','38'),(3039325891,57978331,'elgg:internal:priority','56'),(3039326011,57978361,'elgg:internal:priority','97'),(3039326511,57978381,'elgg:internal:priority','59'),(3039327491,57978421,'elgg:internal:priority','95'),(3039327511,57978431,'elgg:internal:priority','115'),(3039327531,57978441,'elgg:internal:priority','101'),(3039327551,57978451,'elgg:internal:priority','103'),(3039327591,57978471,'elgg:internal:priority','93'),(3039327611,57978481,'elgg:internal:priority','94'),(3039327631,57978491,'elgg:internal:priority','71'),(3039327651,57978501,'elgg:internal:priority','39'),(3039327671,57978511,'elgg:internal:priority','7'),(3039327691,57978521,'elgg:internal:priority','8'),(3039327751,57978551,'elgg:internal:priority','73'),(3039327771,57978561,'elgg:internal:priority','81'),(3039327811,57978581,'elgg:internal:priority','40'),(3039327831,57978591,'elgg:internal:priority','90'),(3039327851,57978601,'elgg:internal:priority','113'),(3039327871,57978611,'elgg:internal:priority','41'),(3039327911,57978631,'elgg:internal:priority','74'),(3039327991,57978671,'elgg:internal:priority','43'),(3039328031,57978691,'elgg:internal:priority','69'),(3039328761,57978881,'elgg:internal:priority','45'),(3039329001,57978891,'elgg:internal:priority','46'),(3039329051,57978901,'elgg:internal:priority','3'),(3039329071,57978911,'elgg:internal:priority','47'),(3039329091,57978921,'elgg:internal:priority','50'),(3039329111,57978931,'elgg:internal:priority','82'),(3039329151,57978951,'elgg:internal:priority','4'),(3039329191,57978971,'elgg:internal:priority','49'),(3039329211,57978981,'elgg:internal:priority','51'),(3039329231,57978991,'elgg:internal:priority','100'),(3039329291,57979021,'elgg:internal:priority','48'),(3039329311,57979031,'elgg:internal:priority','87'),(3039329331,57979041,'elgg:internal:priority','63'),(3039331521,57978901,'restrict_tagging',''),(3039331531,57978901,'slideshow',''),(3039331541,57978901,'river_comments_thumbnails','none'),(3039331551,57978901,'river_thumbnails_size','tiny'),(3039331561,57977891,'is_enabled','no'),(3039357671,57978431,'initiator_link',''),(3039357681,57978431,'font','Rijksoverheid Sans'),(3039357691,57978431,'color_primary','#01689b'),(3039357701,57978431,'color_secondary','#009ee3'),(3039357711,57978431,'color_tertiary','#00c6ff'),(3039357721,57978431,'color_quaternary','#0272a9'),(3039357731,57978431,'theme','leraar'),(3039357741,57978431,'startpage','activity'),(3039357751,57978431,'startpage_cms','0'),(3039357761,57978431,'show_logo','no'),(3039357771,57978431,'newsletter','yes'),(3039357781,57978431,'show_leader','no'),(3039357791,57978431,'show_leader_buttons','yes'),(3039357801,57978431,'subtitle',''),(3039357811,57978431,'leader_image',''),(3039357821,57978431,'show_initiative','no'),(3039357831,57978431,'advanced_permissions','no'),(3039357841,57978431,'google_analytics',''),(3039357851,57978431,'piwik',''),(3039357861,57978431,'sentry',''),(3039357871,57978431,'menu','[{\"title\":\"Blog\",\"link\":\"\\/blog\"},{\"title\":\"Nieuws\",\"link\":\"\\/news\"},{\"title\":\"Vragen\",\"link\":\"\\/questions\"},{\"title\":\"Discussies\",\"link\":\"\\/discussion\"},{\"title\":\"Groepen\",\"link\":\"\\/groups\"},{\"title\":\"Agenda\",\"link\":\"\\/events\"},{\"title\":\"Contact\",\"link\":\"\\/cms\\/view\\/57979129\\/contact\"}]'),(3039357881,57978431,'profile','[]'),(3039357891,57978431,'filters','[]'),(3039357901,57978431,'footer','[{\"title\":\"Toegankelijkheidsverklaring\",\"link\":\"\\/cms\\/view\\/57979204\\/Toegankelijkheidsverklaring\"}]'),(3095447082,57979052,'elgg:internal:priority','114'),(3095447085,57979053,'email_overview_57977371','weekly'),(3095447086,57979054,'handler','control_panel'),(3095447087,57979054,'context','admin'),(3095447088,57979053,'pleio_token','EaRk19UPSAcGnxybXWwZxdxgRkJRed'),(3095447089,57979055,'handler','new_users'),(3095447090,57979055,'context','admin'),(3095447091,57979056,'handler','online_users'),(3095447092,57979056,'context','admin'),(3095447093,57979057,'handler','reportedcontent'),(3095447094,57979057,'context','admin'),(3095447095,57979058,'handler','content_stats'),(3095447096,57979058,'context','admin'),(3095447097,57979059,'handler','control_panel'),(3095447098,57979059,'context','admin'),(3095447099,57979060,'handler','admin_welcome'),(3095447100,57979060,'context','admin'),(3095447101,57979061,'elgg:internal:priority','10'),(3095447102,57979062,'elgg:internal:priority','24'),(3095447103,57979063,'elgg:internal:priority','119'),(3095447104,57979064,'elgg:internal:priority','30'),(3095447105,57979065,'elgg:internal:priority','6'),(3095447107,57979067,'elgg:internal:priority','118'),(3095447108,57979068,'elgg:internal:priority','36'),(3095447109,57979069,'elgg:internal:priority','44'),(3095447111,57979071,'elgg:internal:priority','117'),(3095447112,57979072,'elgg:internal:priority','12'),(3095447307,57979073,'handler','online_users'),(3095447308,57979073,'context','admin'),(3095447309,57979074,'handler','new_users'),(3095447310,57979074,'context','admin'),(3095447311,57979075,'handler','content_stats'),(3095447312,57979075,'context','admin'),(3095447313,57979076,'handler','admin_welcome'),(3095447314,57979076,'context','admin'),(3095447315,57979077,'handler','reportedcontent'),(3095447316,57979077,'context','admin'),(3095447317,57979078,'handler','control_panel'),(3095447318,57979078,'context','admin'),(3095447513,57979079,'handler','new_users'),(3095447514,57979079,'context','admin'),(3095447515,57979080,'handler','content_stats'),(3095447516,57979080,'context','admin'),(3095447517,57979081,'handler','admin_welcome'),(3095447518,57979081,'context','admin'),(3095447519,57979082,'handler','reportedcontent'),(3095447520,57979082,'context','admin'),(3095447521,57979083,'handler','control_panel'),(3095447522,57979083,'context','admin'),(3095447523,57979084,'handler','online_users'),(3095447524,57979084,'context','admin'),(3095447719,57979085,'handler','control_panel'),(3095447720,57979085,'context','admin'),(3095447721,57979085,'column','1'),(3095447722,57979085,'order','60'),(3095447723,57979086,'handler','user_search'),(3095447724,57979086,'context','admin'),(3095447725,57979086,'order','0'),(3095447726,57979086,'column','1'),(3095447727,57979087,'handler','content_stats'),(3095447728,57979087,'context','admin'),(3095447729,57979087,'order','40'),(3095447730,57979087,'column','1'),(3095447731,57979087,'num_display','8'),(3095447732,57979088,'handler','new_users'),(3095447733,57979088,'context','admin'),(3095447734,57979088,'order','30'),(3095447735,57979088,'column','1'),(3095447736,57979088,'num_display','5'),(3095447737,57979089,'handler','online_users'),(3095447738,57979089,'context','admin'),(3095447739,57979089,'order','10'),(3095447740,57979089,'column','1'),(3095447741,57979089,'num_display','8'),(3095447742,57979090,'handler','reportedcontent'),(3095447743,57979090,'context','admin'),(3095447744,57979090,'order','20'),(3095447745,57979090,'column','1'),(3095447746,57979090,'num_display','4'),(3095447747,57979091,'handler','admin_welcome'),(3095447748,57979091,'context','admin'),(3095447749,57979091,'order','0'),(3095447750,57979091,'column','2'),(3095448171,57978431,'icon','none'),(3095448182,57978431,'subgroups','no'),(3095448183,57978431,'event_export','no'),(3095448184,57978431,'comments_on_news','no'),(3095448185,57978431,'member_export','no'),(3095448186,57978431,'default_email_overview','weekly'),(3095448188,57978431,'piwik_url','https://stats.pleio.nl/'),(3095448190,57979094,'email_overview_57977371','weekly'),(3095448253,57979095,'elgg:internal:priority','64'),(3095448254,57979096,'elgg:internal:priority','65'),(3095448255,57979097,'elgg:internal:priority','31'),(3095448256,57979098,'elgg:internal:priority','11'),(3095448257,57979099,'elgg:internal:priority','98'),(3095448258,57979100,'elgg:internal:priority','92'),(3095448259,57979101,'elgg:internal:priority','76'),(3095448260,57979102,'elgg:internal:priority','102'),(3095448261,57979103,'elgg:internal:priority','85'),(3095448262,57979104,'elgg:internal:priority','84'),(3095448263,57979105,'elgg:internal:priority','96'),(3095448264,57979106,'elgg:internal:priority','42'),(3095448265,57979107,'elgg:internal:priority','60'),(3095448266,57979108,'elgg:internal:priority','77'),(3095448267,57979109,'elgg:internal:priority','104'),(3095448268,57979110,'elgg:internal:priority','111'),(3095448269,57979111,'elgg:internal:priority','110'),(3095448270,57979112,'elgg:internal:priority','105'),(3095448271,57979113,'elgg:internal:priority','106'),(3095448272,57979114,'elgg:internal:priority','107'),(3095448273,57979115,'elgg:internal:priority','109'),(3095448274,57979116,'elgg:internal:priority','112'),(3095448275,57979117,'elgg:internal:priority','108'),(3095448276,57979118,'elgg:internal:priority','52'),(3095448769,57979094,'pleio_token','6ayhTMajKurPmjK8zDEmwwursU7JtT'),(3095448770,57979094,'latest_email_overview_57977371','1541970210'),(3095448894,57979094,'latest_notifications_57977371','1530176484'),(3095448909,57979173,'elgg:internal:priority','121'),(3095449049,57978431,'initiative_image',''),(3095449054,57978431,'enable_sharing','yes'),(3095449057,57978431,'like_icon','heart'),(3095449063,57978431,'directLinks','[{\"title\":\"Nieuw\",\"link\":\"\\/nieuw\"},{\"title\":\"Nieuw\",\"link\":\"\\/nieuw\"},{\"title\":\"Nieuw\",\"link\":\"\\/nieuw\"}]'),(3095449070,57978431,'show_icon','no'),(3095449089,57978431,'status_update_groups','yes'),(3095449091,57978431,'show_excerpt_in_news_card','no'),(3095449094,57978431,'enable_up_down_voting','yes'),(3095449106,57978431,'predefinedTags','[]'),(3095449261,57979182,'email_overview_57977371','weekly'),(3095449265,57979184,'email_overview_57977371','weekly'),(3095449266,57979185,'email_overview_57977371','weekly'),(3095449267,57979185,'pleio_token','E21YPsbOJGbdlydjL18u5aV6U5lMnQ'),(3095449268,57979186,'email_overview_57977371','weekly'),(3095449269,57979186,'pleio_token','pNesdAmGbqeSLvfqmbwog34VtHhUVS'),(3095449274,57978431,'color_header',''),(3095449294,57978431,'enable_views_count','no'),(3095449298,57978431,'number_of_featured_items','0'),(3095449299,57978431,'show_extra_homepage_filters','no'),(3095449914,57979194,'email_overview_57977371','weekly'),(3095449915,57979194,'pleio_token','9AFIhBB4Dqe9oPMpkmVxURmicPkD8A'),(3095449916,57979195,'email_overview_57977371','weekly'),(3095449917,57979195,'pleio_token','7rGxixCeghTdSVWFe78RCwvTuHo7Pg'),(3095449918,57979196,'email_overview_57977371','weekly'),(3095449919,57979196,'pleio_token','c2MmwBI1QeicmzX6serU03sUsYuvAY'),(3095450171,57978431,'enable_feed_sorting','yes'),(3095450172,57978431,'tagCategories','[]'),(3095450293,57978081,'hidden_groups','no'),(3095450294,57978081,'limited_groups','no'),(3095450655,57979197,'email_overview_57977371','never'),(3095450656,57979197,'pleio_token','zj9pYQBWaZ6V3o4KpDd33q4vUPKNoo'),(3095450657,57979198,'email_overview_57977371','weekly'),(3095450658,57979198,'pleio_token','tx6kk06j1arFDfmdZryREZ5i04s0sV'),(3095450660,57979199,'email_overview_57977371','weekly'),(3095450661,57979199,'pleio_token','hGst85iwTNaZiyq12eU9WJGRBJhVlR'),(3095450662,57979200,'email_overview_57977371','weekly'),(3095450663,57979200,'pleio_token','nX0jLUCouTVTQ2TQFhBKeodL7qQ62H'),(3095450665,57979201,'elgg:internal:priority','116'),(3095450666,57979202,'elgg:internal:priority','86'),(3095450912,57979206,'settings','[{\"key\":\"description\",\"value\":\"Toegankelijkheidsverklaring\\nPleio streeft naar het toegankelijk maken van de eigen online informatie en dienstverlening, in overeenstemming met Tijdelijk besluit digitale toegankelijkheid overheid.\\nDeze toegankelijkheidsverklaring is van toepassing op de inhoud van de website pleio die valt binnen de werkingssfeer van Tijdelijk besluit digitale toegankelijkheid overheid.\\nDe links waarop de inhoud van de website pleio te vinden is:\\nhet hoofddomein: https:\\/\\/support.pleio.nl      \\n\\n\\nsubdomeinen en andere domeinen die tot de website behoren: geen\\n\\nEen actueel en volledig overzicht van de toegankelijkheidsverklaringen die vallen onder de verantwoordelijkheid van pleio is beschikbaar via de volgende link:\\nhttps:\\/\\/support.pleio.nl     \\n\\n\\nNalevingsstatus\\n\\nPleio verklaart dat de website pleio gedeeltelijk voldoet aan Tijdelijk besluit digitale toegankelijkheid overheid.\\nUit toegankelijkheidsonderzoek is gebleken dat nog niet aan alle eisen wordt voldaan. Voor elke afzonderlijke afwijking van de eisen is de oorzaak bekend en is het gevolg beschreven, zijn maatregelen genomen om de afwijking te kunnen opheffen en is een concrete planning gemaakt waarop de maatregelen zullen zijn uitgevoerd.\\nZie onder het kopje Toelichting op de nalevingsstatus voor meer gedetailleerde informatie.\\nOpstelling van deze verklaring\\n\\nDeze verklaring is opgesteld op XX-XX-XX met instemming van de verantwoordelijke bestuurder van XXX, XX XXX .\\nDe actualiteit, volledigheid en juistheid van deze verklaring zijn voor het laatst herzien op 19-09-2019.\\nFeedback en contactgegevens\\n\\nLoopt u tegen een toegankelijkheidsprobleem aan? Of heeft u een vraag of opmerking over toegankelijkheid? Neem dan contact op via [support@pleio.nl]\\nWat kunt u van ons verwachten?\\n\\nBinnen 5 werkdagen krijgt u een ontvangstbevestiging.\\n\\nWe informeren u over de voortgang en de uitkomst.\\n\\nBinnen 3 weken is uw verzoek afgehandeld.\\n\\nHandhavingsprocedure\\n\\n].Bent u niet tevreden met de manier waarop uw klacht is behandeld? Of hebben we niet op tijd gereageerd? Dan kunt u [contact opnemen met de Nationale Ombudsman\\nToelichting op de nalevingsstatus\\n\\nIn deze paragraaf wordt de claim dat gedeeltelijkaan de in Tijdelijk besluit digitale toegankelijkheid overheid gestelde eisen is voldaan nader onderbouwd.\\nDe website pleio is onderzocht op toegankelijkheid en uit de rapportage blijkt volgens pleio dat aan alleonderstaande kenmerken is voldaan:\\nhet onderzoek omvat alle inhoud die volgens Tijdelijk besluit digitale toegankelijkheid overheid toegankelijk moet worden gemaakt;\\n\\nhet onderzoek is gebaseerd op meet- en onderzoeksgegevens die niet ouder zijn dan 12 maanden;\\n\\nof gelijkwaardig; de handmatige evaluatie is uitgevoerd overeenkomstig een adequaat gedocumenteerde evaluatiemethode; WCAG-EM     \\n\\n\\nCommunity Group; voor de (semi-)automatische tests is een toetsinstrument ingezet dat is gebaseerd op de algoritmen die zijn gedocumenteerd door de Auto-WCAG     \\n\\n\\n; alle onderzoekresultaten zijn nauwkeurig, eenduidig en op reproduceerbare wijze vastgelegd in een voor mensen leesbaar formaat OFin het machineleesbare formaat EARL     \\n\\n\\n(zie onder het kopje Afwijkingen)voor elke afzonderlijke afwijking op de eisen die tijdens het onderzoek werd gevonden en die niet kon worden hersteld wordt aangegeven: [referentienummer] [beknopte beschrijving van de afwijking] oorzaak:[reden waarom (nog) niet aan de eis kon worden voldaan] gevolg:[impact van de afwijking voor personen met een functiebeperking] alternatief:[of een toegankelijk alternatief beschikbaar is. En zo ja, welk] maatregel:[te nemen maatregel(en) om de afwijking op te heffen] mogelijkheid om aan te geven of de uitvoering van de maatregel een onevenredige lastmet zich meebrengt: [ja\\/nee] [toelichting als het antwoord op de vraag \'ja\' is] planning:[uiterste datum waarop de afwijking zal zijn hersteld]\\n\\nalle evaluatie- en onderzoekresultaten waarop de claim is gebaseerd zijn online beschikbaar (zie onder het kopje Evaluatie- en onderzoekresultaten\\n\\nAfwijkingen\\n\\n9 (= SC uit Anders (in onderstaand tekstvak benoemen en toelichten)) We hebben geen controle over al onze content oorzaak:Pleio bied mensen de mogelijkheid om deelsites aan te maken gevolg:Daar kunnen mensen content plaatsen, hier hebben we geen controle op. Of attachments toegankelijk zijn is buiten onze controle. Ook kan er custom CSS worden gezet waardoor bijvoorbeeld het contrast wordt verslechterd alternatief:Gebruikers moeten kennis op gaan doen van toegankelijkheid. maatregel:- brengt de uitvoering van de maatregel een onevenredige lastmet zich mee? Nee planning:04-08-2020\\n\\n1.3.1 (= SC uit 1.3.1 - Info en relaties [niveau A]) Certain ARIA roles must be contained by particular parents oorzaak:button with role tab is not in tablist gevolg:Assistive technologies, like screen readers, can\'t interpret ARIA attributes with invalid values alternatief:nvt maatregel:We gaan de aria rollen kritisch bekijken en waar nodig verbeteren brengt de uitvoering van de maatregel een onevenredige lastmet zich mee? Nee planning:02-12-2019\\n\\n1.3.1 (= SC uit 1.3.1 - Info en relaties [niveau A]) Niet elk formulier veld heeft een label oorzaak:niet alle velden in een formulier hebben een label gevolg:Screenreaders kunnen moeite hebben om de betekenis te geven aan formuliervelden. alternatief:nvt maatregel:We gaan ieder formulier veld van een label voorzien. brengt de uitvoering van de maatregel een onevenredige lastmet zich mee? Nee planning:02-12-2019\\n\\n1.4.3 (= SC uit 1.4.3 - Contrast (minimum) [niveau AA]) Niet alle tekst op de site heeft voldoende contrast oorzaak:tekst is te licht gevolg:niet alle tekst is goed leesbaar voor alle bezoekers alternatief:nvt maatregel:We gaan het ontwerp aanpassen zodat de tekst, buttons en iconen beter contrast hebben brengt de uitvoering van de maatregel een onevenredige lastmet zich mee? Nee planning:02-12-2019\\n\\n4.1.2 (= SC uit 4.1.2 - Naam, rol, waarde [niveau A]) Niet alle buttons of links hebben een voorleesbare tekst oorzaak:niet alle links of button hebben een tekst, title of value ingevuld gevolg:voor een screenreader is het niet mogelijk om te vertellen waar de link heen gaat of wat de button gaat doen. alternatief:nvt maatregel:We gaan ervoor zorgen dat iedere link en button conformeerd aan deze richtlijn. brengt de uitvoering van de maatregel een onevenredige lastmet zich mee? Nee planning:02-12-2019\\n\\nEvaluatie- en onderzoekresultaten\\n\\nof gelijkwaardig: welhttps:\\/\\/support.pleio.nl\\/files\\/view\\/57980637\\/support.pleio.nl-20190917T151248.json  \\n\\nonderzoeksdatum: 17-09-2019 onderzoeksmethode: automatisch onderzoek de beschikbare informatie is gebaseerd op meet- en onderzoeksgegevens die nietouder zijn dan 12 maanden de handmatige evaluatie is uitgevoerd overeenkomstig een adequaat gedocumenteerde evaluatiemethode; WCAG-EM      \\n\\n\\nInhoud die buiten de werkingssfeer valt van Tijdelijk besluit digitale toegankelijkheid overheid\\n\\n.Zie Artikel 2, tweede lid van het Tijdelijk besluit digitale toegankelijkheid overheid\\n\\n\"},{\"key\":\"richDescription\",\"value\":\"{\\\"blocks\\\":[{\\\"key\\\":\\\"225i5\\\",\\\"text\\\":\\\"Toegankelijkheidsverklaring\\\",\\\"type\\\":\\\"header-two\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[],\\\"entityRanges\\\":[],\\\"data\\\":{}},{\\\"key\\\":\\\"10l0f\\\",\\\"text\\\":\\\"Pleio streeft naar het toegankelijk maken van de eigen online informatie en dienstverlening, in overeenstemming met Tijdelijk besluit digitale toegankelijkheid overheid.\\\",\\\"type\\\":\\\"unstyled\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[],\\\"entityRanges\\\":[],\\\"data\\\":{}},{\\\"key\\\":\\\"afo3f\\\",\\\"text\\\":\\\"Deze toegankelijkheidsverklaring is van toepassing op de inhoud van de website pleio die valt binnen de werkingssfeer van Tijdelijk besluit digitale toegankelijkheid overheid.\\\",\\\"type\\\":\\\"unstyled\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[],\\\"entityRanges\\\":[],\\\"data\\\":{}},{\\\"key\\\":\\\"2kokd\\\",\\\"text\\\":\\\"De links waarop de inhoud van de website pleio te vinden is:\\\",\\\"type\\\":\\\"unstyled\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[],\\\"entityRanges\\\":[],\\\"data\\\":{}},{\\\"key\\\":\\\"cbd05\\\",\\\"text\\\":\\\"het hoofddomein: https:\\/\\/support.pleio.nl      \\\\n\\\\n\\\",\\\"type\\\":\\\"unordered-list-item\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[],\\\"entityRanges\\\":[{\\\"offset\\\":17,\\\"length\\\":32,\\\"key\\\":0}],\\\"data\\\":{}},{\\\"key\\\":\\\"ntuh\\\",\\\"text\\\":\\\"subdomeinen en andere domeinen die tot de website behoren: geen\\\\n\\\",\\\"type\\\":\\\"unordered-list-item\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[{\\\"offset\\\":59,\\\"length\\\":5,\\\"style\\\":\\\"ITALIC\\\"}],\\\"entityRanges\\\":[],\\\"data\\\":{}},{\\\"key\\\":\\\"bn4h0\\\",\\\"text\\\":\\\"Een actueel en volledig overzicht van de toegankelijkheidsverklaringen die vallen onder de verantwoordelijkheid van pleio is beschikbaar via de volgende link:\\\",\\\"type\\\":\\\"unstyled\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[],\\\"entityRanges\\\":[],\\\"data\\\":{}},{\\\"key\\\":\\\"7b63g\\\",\\\"text\\\":\\\"https:\\/\\/support.pleio.nl     \\\\n\\\\n\\\",\\\"type\\\":\\\"unordered-list-item\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[],\\\"entityRanges\\\":[{\\\"offset\\\":0,\\\"length\\\":31,\\\"key\\\":1}],\\\"data\\\":{}},{\\\"key\\\":\\\"3erle\\\",\\\"text\\\":\\\"Nalevingsstatus\\\\n\\\",\\\"type\\\":\\\"header-three\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[],\\\"entityRanges\\\":[],\\\"data\\\":{}},{\\\"key\\\":\\\"67q4e\\\",\\\"text\\\":\\\"Pleio verklaart dat de website pleio gedeeltelijk voldoet aan Tijdelijk besluit digitale toegankelijkheid overheid.\\\",\\\"type\\\":\\\"unstyled\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[],\\\"entityRanges\\\":[],\\\"data\\\":{}},{\\\"key\\\":\\\"84k4l\\\",\\\"text\\\":\\\"Uit toegankelijkheidsonderzoek is gebleken dat nog niet aan alle eisen wordt voldaan. Voor elke afzonderlijke afwijking van de eisen is de oorzaak bekend en is het gevolg beschreven, zijn maatregelen genomen om de afwijking te kunnen opheffen en is een concrete planning gemaakt waarop de maatregelen zullen zijn uitgevoerd.\\\",\\\"type\\\":\\\"unstyled\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[],\\\"entityRanges\\\":[],\\\"data\\\":{}},{\\\"key\\\":\\\"fu7js\\\",\\\"text\\\":\\\"Zie onder het kopje Toelichting op de nalevingsstatus voor meer gedetailleerde informatie.\\\",\\\"type\\\":\\\"unstyled\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[],\\\"entityRanges\\\":[],\\\"data\\\":{}},{\\\"key\\\":\\\"8itpv\\\",\\\"text\\\":\\\"Opstelling van deze verklaring\\\\n\\\",\\\"type\\\":\\\"header-four\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[],\\\"entityRanges\\\":[],\\\"data\\\":{}},{\\\"key\\\":\\\"ihmu\\\",\\\"text\\\":\\\"Deze verklaring is opgesteld op XX-XX-XX met instemming van de verantwoordelijke bestuurder van XXX, XX XXX .\\\",\\\"type\\\":\\\"unstyled\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[],\\\"entityRanges\\\":[],\\\"data\\\":{}},{\\\"key\\\":\\\"ch6ns\\\",\\\"text\\\":\\\"De actualiteit, volledigheid en juistheid van deze verklaring zijn voor het laatst herzien op 19-09-2019.\\\",\\\"type\\\":\\\"unstyled\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[],\\\"entityRanges\\\":[],\\\"data\\\":{}},{\\\"key\\\":\\\"nubu\\\",\\\"text\\\":\\\"Feedback en contactgegevens\\\\n\\\",\\\"type\\\":\\\"header-four\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[],\\\"entityRanges\\\":[],\\\"data\\\":{}},{\\\"key\\\":\\\"frj4k\\\",\\\"text\\\":\\\"Loopt u tegen een toegankelijkheidsprobleem aan? Of heeft u een vraag of opmerking over toegankelijkheid? Neem dan contact op via [support@pleio.nl]\\\",\\\"type\\\":\\\"unstyled\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[],\\\"entityRanges\\\":[],\\\"data\\\":{}},{\\\"key\\\":\\\"9e1th\\\",\\\"text\\\":\\\"Wat kunt u van ons verwachten?\\\\n\\\",\\\"type\\\":\\\"header-four\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[],\\\"entityRanges\\\":[],\\\"data\\\":{}},{\\\"key\\\":\\\"9g80b\\\",\\\"text\\\":\\\"Binnen 5 werkdagen krijgt u een ontvangstbevestiging.\\\\n\\\",\\\"type\\\":\\\"unordered-list-item\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[],\\\"entityRanges\\\":[],\\\"data\\\":{}},{\\\"key\\\":\\\"1t7ca\\\",\\\"text\\\":\\\"We informeren u over de voortgang en de uitkomst.\\\\n\\\",\\\"type\\\":\\\"unordered-list-item\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[],\\\"entityRanges\\\":[],\\\"data\\\":{}},{\\\"key\\\":\\\"eb2ov\\\",\\\"text\\\":\\\"Binnen 3 weken is uw verzoek afgehandeld.\\\\n\\\",\\\"type\\\":\\\"unordered-list-item\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[],\\\"entityRanges\\\":[],\\\"data\\\":{}},{\\\"key\\\":\\\"fiv4c\\\",\\\"text\\\":\\\"Handhavingsprocedure\\\\n\\\",\\\"type\\\":\\\"header-four\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[],\\\"entityRanges\\\":[],\\\"data\\\":{}},{\\\"key\\\":\\\"2s64p\\\",\\\"text\\\":\\\"].Bent u niet tevreden met de manier waarop uw klacht is behandeld? Of hebben we niet op tijd gereageerd? Dan kunt u [contact opnemen met de Nationale Ombudsman\\\",\\\"type\\\":\\\"unstyled\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[],\\\"entityRanges\\\":[{\\\"offset\\\":118,\\\"length\\\":42,\\\"key\\\":2}],\\\"data\\\":{}},{\\\"key\\\":\\\"9hbgl\\\",\\\"text\\\":\\\"Toelichting op de nalevingsstatus\\\\n\\\",\\\"type\\\":\\\"header-three\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[],\\\"entityRanges\\\":[],\\\"data\\\":{}},{\\\"key\\\":\\\"chqsh\\\",\\\"text\\\":\\\"In deze paragraaf wordt de claim dat gedeeltelijkaan de in Tijdelijk besluit digitale toegankelijkheid overheid gestelde eisen is voldaan nader onderbouwd.\\\",\\\"type\\\":\\\"unstyled\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[{\\\"offset\\\":37,\\\"length\\\":12,\\\"style\\\":\\\"BOLD\\\"},{\\\"offset\\\":37,\\\"length\\\":12,\\\"style\\\":\\\"ITALIC\\\"}],\\\"entityRanges\\\":[],\\\"data\\\":{}},{\\\"key\\\":\\\"2192f\\\",\\\"text\\\":\\\"De website pleio is onderzocht op toegankelijkheid en uit de rapportage blijkt volgens pleio dat aan alleonderstaande kenmerken is voldaan:\\\",\\\"type\\\":\\\"unstyled\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[{\\\"offset\\\":101,\\\"length\\\":4,\\\"style\\\":\\\"ITALIC\\\"}],\\\"entityRanges\\\":[],\\\"data\\\":{}},{\\\"key\\\":\\\"9ttun\\\",\\\"text\\\":\\\"het onderzoek omvat alle inhoud die volgens Tijdelijk besluit digitale toegankelijkheid overheid toegankelijk moet worden gemaakt;\\\\n\\\",\\\"type\\\":\\\"ordered-list-item\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[],\\\"entityRanges\\\":[],\\\"data\\\":{}},{\\\"key\\\":\\\"dn8ld\\\",\\\"text\\\":\\\"het onderzoek is gebaseerd op meet- en onderzoeksgegevens die niet ouder zijn dan 12 maanden;\\\\n\\\",\\\"type\\\":\\\"ordered-list-item\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[],\\\"entityRanges\\\":[],\\\"data\\\":{}},{\\\"key\\\":\\\"901a\\\",\\\"text\\\":\\\"of gelijkwaardig; de handmatige evaluatie is uitgevoerd overeenkomstig een adequaat gedocumenteerde evaluatiemethode; WCAG-EM     \\\\n\\\\n\\\",\\\"type\\\":\\\"ordered-list-item\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[],\\\"entityRanges\\\":[{\\\"offset\\\":118,\\\"length\\\":14,\\\"key\\\":3}],\\\"data\\\":{}},{\\\"key\\\":\\\"3oern\\\",\\\"text\\\":\\\"Community Group; voor de (semi-)automatische tests is een toetsinstrument ingezet dat is gebaseerd op de algoritmen die zijn gedocumenteerd door de Auto-WCAG     \\\\n\\\\n\\\",\\\"type\\\":\\\"ordered-list-item\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[],\\\"entityRanges\\\":[{\\\"offset\\\":148,\\\"length\\\":16,\\\"key\\\":4}],\\\"data\\\":{}},{\\\"key\\\":\\\"5ppou\\\",\\\"text\\\":\\\"; alle onderzoekresultaten zijn nauwkeurig, eenduidig en op reproduceerbare wijze vastgelegd in een voor mensen leesbaar formaat OFin het machineleesbare formaat EARL     \\\\n\\\\n\\\",\\\"type\\\":\\\"ordered-list-item\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[{\\\"offset\\\":129,\\\"length\\\":2,\\\"style\\\":\\\"BOLD\\\"},{\\\"offset\\\":129,\\\"length\\\":2,\\\"style\\\":\\\"ITALIC\\\"}],\\\"entityRanges\\\":[{\\\"offset\\\":162,\\\"length\\\":11,\\\"key\\\":5}],\\\"data\\\":{}},{\\\"key\\\":\\\"5e0d8\\\",\\\"text\\\":\\\"(zie onder het kopje Afwijkingen)voor elke afzonderlijke afwijking op de eisen die tijdens het onderzoek werd gevonden en die niet kon worden hersteld wordt aangegeven: [referentienummer] [beknopte beschrijving van de afwijking] oorzaak:[reden waarom (nog) niet aan de eis kon worden voldaan] gevolg:[impact van de afwijking voor personen met een functiebeperking] alternatief:[of een toegankelijk alternatief beschikbaar is. En zo ja, welk] maatregel:[te nemen maatregel(en) om de afwijking op te heffen] mogelijkheid om aan te geven of de uitvoering van de maatregel een onevenredige lastmet zich meebrengt: [ja\\/nee] [toelichting als het antwoord op de vraag \'ja\' is] planning:[uiterste datum waarop de afwijking zal zijn hersteld]\\\\n\\\",\\\"type\\\":\\\"ordered-list-item\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[{\\\"offset\\\":229,\\\"length\\\":8,\\\"style\\\":\\\"BOLD\\\"},{\\\"offset\\\":293,\\\"length\\\":7,\\\"style\\\":\\\"BOLD\\\"},{\\\"offset\\\":365,\\\"length\\\":12,\\\"style\\\":\\\"BOLD\\\"},{\\\"offset\\\":442,\\\"length\\\":10,\\\"style\\\":\\\"BOLD\\\"},{\\\"offset\\\":670,\\\"length\\\":9,\\\"style\\\":\\\"BOLD\\\"},{\\\"offset\\\":573,\\\"length\\\":17,\\\"style\\\":\\\"ITALIC\\\"}],\\\"entityRanges\\\":[],\\\"data\\\":{}},{\\\"key\\\":\\\"2buus\\\",\\\"text\\\":\\\"alle evaluatie- en onderzoekresultaten waarop de claim is gebaseerd zijn online beschikbaar (zie onder het kopje Evaluatie- en onderzoekresultaten\\\\n\\\",\\\"type\\\":\\\"ordered-list-item\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[],\\\"entityRanges\\\":[],\\\"data\\\":{}},{\\\"key\\\":\\\"201t\\\",\\\"text\\\":\\\"Afwijkingen\\\\n\\\",\\\"type\\\":\\\"header-four\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[],\\\"entityRanges\\\":[],\\\"data\\\":{}},{\\\"key\\\":\\\"4g70h\\\",\\\"text\\\":\\\"9 (= SC uit Anders (in onderstaand tekstvak benoemen en toelichten)) We hebben geen controle over al onze content oorzaak:Pleio bied mensen de mogelijkheid om deelsites aan te maken gevolg:Daar kunnen mensen content plaatsen, hier hebben we geen controle op. Of attachments toegankelijk zijn is buiten onze controle. Ook kan er custom CSS worden gezet waardoor bijvoorbeeld het contrast wordt verslechterd alternatief:Gebruikers moeten kennis op gaan doen van toegankelijkheid. maatregel:- brengt de uitvoering van de maatregel een onevenredige lastmet zich mee? Nee planning:04-08-2020\\\\n\\\",\\\"type\\\":\\\"ordered-list-item\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[{\\\"offset\\\":114,\\\"length\\\":8,\\\"style\\\":\\\"BOLD\\\"},{\\\"offset\\\":182,\\\"length\\\":7,\\\"style\\\":\\\"BOLD\\\"},{\\\"offset\\\":406,\\\"length\\\":12,\\\"style\\\":\\\"BOLD\\\"},{\\\"offset\\\":478,\\\"length\\\":10,\\\"style\\\":\\\"BOLD\\\"},{\\\"offset\\\":563,\\\"length\\\":13,\\\"style\\\":\\\"BOLD\\\"},{\\\"offset\\\":532,\\\"length\\\":17,\\\"style\\\":\\\"ITALIC\\\"}],\\\"entityRanges\\\":[],\\\"data\\\":{}},{\\\"key\\\":\\\"fbh9e\\\",\\\"text\\\":\\\"1.3.1 (= SC uit 1.3.1 - Info en relaties [niveau A]) Certain ARIA roles must be contained by particular parents oorzaak:button with role tab is not in tablist gevolg:Assistive technologies, like screen readers, can\'t interpret ARIA attributes with invalid values alternatief:nvt maatregel:We gaan de aria rollen kritisch bekijken en waar nodig verbeteren brengt de uitvoering van de maatregel een onevenredige lastmet zich mee? Nee planning:02-12-2019\\\\n\\\",\\\"type\\\":\\\"ordered-list-item\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[{\\\"offset\\\":112,\\\"length\\\":8,\\\"style\\\":\\\"BOLD\\\"},{\\\"offset\\\":159,\\\"length\\\":7,\\\"style\\\":\\\"BOLD\\\"},{\\\"offset\\\":263,\\\"length\\\":12,\\\"style\\\":\\\"BOLD\\\"},{\\\"offset\\\":279,\\\"length\\\":10,\\\"style\\\":\\\"BOLD\\\"},{\\\"offset\\\":428,\\\"length\\\":13,\\\"style\\\":\\\"BOLD\\\"},{\\\"offset\\\":397,\\\"length\\\":17,\\\"style\\\":\\\"ITALIC\\\"}],\\\"entityRanges\\\":[],\\\"data\\\":{}},{\\\"key\\\":\\\"8rl97\\\",\\\"text\\\":\\\"1.3.1 (= SC uit 1.3.1 - Info en relaties [niveau A]) Niet elk formulier veld heeft een label oorzaak:niet alle velden in een formulier hebben een label gevolg:Screenreaders kunnen moeite hebben om de betekenis te geven aan formuliervelden. alternatief:nvt maatregel:We gaan ieder formulier veld van een label voorzien. brengt de uitvoering van de maatregel een onevenredige lastmet zich mee? Nee planning:02-12-2019\\\\n\\\",\\\"type\\\":\\\"ordered-list-item\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[{\\\"offset\\\":93,\\\"length\\\":8,\\\"style\\\":\\\"BOLD\\\"},{\\\"offset\\\":152,\\\"length\\\":7,\\\"style\\\":\\\"BOLD\\\"},{\\\"offset\\\":240,\\\"length\\\":12,\\\"style\\\":\\\"BOLD\\\"},{\\\"offset\\\":256,\\\"length\\\":10,\\\"style\\\":\\\"BOLD\\\"},{\\\"offset\\\":392,\\\"length\\\":13,\\\"style\\\":\\\"BOLD\\\"},{\\\"offset\\\":361,\\\"length\\\":17,\\\"style\\\":\\\"ITALIC\\\"}],\\\"entityRanges\\\":[],\\\"data\\\":{}},{\\\"key\\\":\\\"6h2jv\\\",\\\"text\\\":\\\"1.4.3 (= SC uit 1.4.3 - Contrast (minimum) [niveau AA]) Niet alle tekst op de site heeft voldoende contrast oorzaak:tekst is te licht gevolg:niet alle tekst is goed leesbaar voor alle bezoekers alternatief:nvt maatregel:We gaan het ontwerp aanpassen zodat de tekst, buttons en iconen beter contrast hebben brengt de uitvoering van de maatregel een onevenredige lastmet zich mee? Nee planning:02-12-2019\\\\n\\\",\\\"type\\\":\\\"ordered-list-item\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[{\\\"offset\\\":108,\\\"length\\\":8,\\\"style\\\":\\\"BOLD\\\"},{\\\"offset\\\":134,\\\"length\\\":7,\\\"style\\\":\\\"BOLD\\\"},{\\\"offset\\\":194,\\\"length\\\":12,\\\"style\\\":\\\"BOLD\\\"},{\\\"offset\\\":210,\\\"length\\\":10,\\\"style\\\":\\\"BOLD\\\"},{\\\"offset\\\":379,\\\"length\\\":13,\\\"style\\\":\\\"BOLD\\\"},{\\\"offset\\\":348,\\\"length\\\":17,\\\"style\\\":\\\"ITALIC\\\"}],\\\"entityRanges\\\":[],\\\"data\\\":{}},{\\\"key\\\":\\\"ev67v\\\",\\\"text\\\":\\\"4.1.2 (= SC uit 4.1.2 - Naam, rol, waarde [niveau A]) Niet alle buttons of links hebben een voorleesbare tekst oorzaak:niet alle links of button hebben een tekst, title of value ingevuld gevolg:voor een screenreader is het niet mogelijk om te vertellen waar de link heen gaat of wat de button gaat doen. alternatief:nvt maatregel:We gaan ervoor zorgen dat iedere link en button conformeerd aan deze richtlijn. brengt de uitvoering van de maatregel een onevenredige lastmet zich mee? Nee planning:02-12-2019\\\\n\\\",\\\"type\\\":\\\"ordered-list-item\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[{\\\"offset\\\":111,\\\"length\\\":8,\\\"style\\\":\\\"BOLD\\\"},{\\\"offset\\\":187,\\\"length\\\":7,\\\"style\\\":\\\"BOLD\\\"},{\\\"offset\\\":304,\\\"length\\\":12,\\\"style\\\":\\\"BOLD\\\"},{\\\"offset\\\":320,\\\"length\\\":10,\\\"style\\\":\\\"BOLD\\\"},{\\\"offset\\\":483,\\\"length\\\":13,\\\"style\\\":\\\"BOLD\\\"},{\\\"offset\\\":452,\\\"length\\\":17,\\\"style\\\":\\\"ITALIC\\\"}],\\\"entityRanges\\\":[],\\\"data\\\":{}},{\\\"key\\\":\\\"4k146\\\",\\\"text\\\":\\\"Evaluatie- en onderzoekresultaten\\\\n\\\",\\\"type\\\":\\\"header-four\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[],\\\"entityRanges\\\":[],\\\"data\\\":{}},{\\\"key\\\":\\\"521bv\\\",\\\"text\\\":\\\"of gelijkwaardig: welhttps:\\/\\/support.pleio.nl\\/files\\/view\\/57980637\\/support.pleio.nl-20190917T151248.json  \\\\n\\\\nonderzoeksdatum: 17-09-2019 onderzoeksmethode: automatisch onderzoek de beschikbare informatie is gebaseerd op meet- en onderzoeksgegevens die nietouder zijn dan 12 maanden de handmatige evaluatie is uitgevoerd overeenkomstig een adequaat gedocumenteerde evaluatiemethode; WCAG-EM      \\\\n\\\\n\\\",\\\"type\\\":\\\"unordered-list-item\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[{\\\"offset\\\":18,\\\"length\\\":3,\\\"style\\\":\\\"BOLD\\\"},{\\\"offset\\\":250,\\\"length\\\":4,\\\"style\\\":\\\"BOLD\\\"}],\\\"entityRanges\\\":[{\\\"offset\\\":21,\\\"length\\\":86,\\\"key\\\":6},{\\\"offset\\\":380,\\\"length\\\":15,\\\"key\\\":7}],\\\"data\\\":{}},{\\\"key\\\":\\\"9056q\\\",\\\"text\\\":\\\"Inhoud die buiten de werkingssfeer valt van Tijdelijk besluit digitale toegankelijkheid overheid\\\\n\\\",\\\"type\\\":\\\"header-four\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[],\\\"entityRanges\\\":[],\\\"data\\\":{}},{\\\"key\\\":\\\"e0b33\\\",\\\"text\\\":\\\".Zie Artikel 2, tweede lid van het Tijdelijk besluit digitale toegankelijkheid overheid\\\",\\\"type\\\":\\\"unstyled\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[],\\\"entityRanges\\\":[{\\\"offset\\\":5,\\\"length\\\":82,\\\"key\\\":8}],\\\"data\\\":{}},{\\\"key\\\":\\\"ejaa8\\\",\\\"text\\\":\\\"\\\",\\\"type\\\":\\\"unstyled\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[],\\\"entityRanges\\\":[],\\\"data\\\":{}},{\\\"key\\\":\\\"9iufm\\\",\\\"text\\\":\\\"\\\",\\\"type\\\":\\\"unstyled\\\",\\\"depth\\\":0,\\\"inlineStyleRanges\\\":[],\\\"entityRanges\\\":[],\\\"data\\\":{}}],\\\"entityMap\\\":{\\\"0\\\":{\\\"type\\\":\\\"LINK\\\",\\\"mutability\\\":\\\"MUTABLE\\\",\\\"data\\\":{\\\"href\\\":\\\"https:\\/\\/support.pleio.nl\\/\\\",\\\"url\\\":\\\"https:\\/\\/support.pleio.nl\\/\\\"}},\\\"1\\\":{\\\"type\\\":\\\"LINK\\\",\\\"mutability\\\":\\\"MUTABLE\\\",\\\"data\\\":{\\\"href\\\":\\\"https:\\/\\/support.pleio.nl\\/\\\",\\\"url\\\":\\\"https:\\/\\/support.pleio.nl\\/\\\"}},\\\"2\\\":{\\\"type\\\":\\\"LINK\\\",\\\"mutability\\\":\\\"MUTABLE\\\",\\\"data\\\":{\\\"href\\\":\\\"https:\\/\\/www.nationaleombudsman.nl\\/klacht-indienen\\/uw-klacht\\\",\\\"url\\\":\\\"https:\\/\\/www.nationaleombudsman.nl\\/klacht-indienen\\/uw-klacht\\\"}},\\\"3\\\":{\\\"type\\\":\\\"LINK\\\",\\\"mutability\\\":\\\"MUTABLE\\\",\\\"data\\\":{\\\"href\\\":\\\"https:\\/\\/w3.org\\/TR\\/WCAG-EM\\/\\\",\\\"url\\\":\\\"https:\\/\\/w3.org\\/TR\\/WCAG-EM\\/\\\"}},\\\"4\\\":{\\\"type\\\":\\\"LINK\\\",\\\"mutability\\\":\\\"MUTABLE\\\",\\\"data\\\":{\\\"href\\\":\\\"https:\\/\\/auto-wcag.github.io\\/auto-wcag\\/\\\",\\\"url\\\":\\\"https:\\/\\/auto-wcag.github.io\\/auto-wcag\\/\\\"}},\\\"5\\\":{\\\"type\\\":\\\"LINK\\\",\\\"mutability\\\":\\\"MUTABLE\\\",\\\"data\\\":{\\\"href\\\":\\\"https:\\/\\/www.w3.org\\/WAI\\/standards-guidelines\\/earl\\/\\\",\\\"url\\\":\\\"https:\\/\\/www.w3.org\\/WAI\\/standards-guidelines\\/earl\\/\\\"}},\\\"6\\\":{\\\"type\\\":\\\"LINK\\\",\\\"mutability\\\":\\\"MUTABLE\\\",\\\"data\\\":{\\\"href\\\":\\\"https:\\/\\/support.pleio.nl\\/files\\/view\\/57980637\\/support.pleio.nl-20190917T151248.json\\\",\\\"url\\\":\\\"https:\\/\\/support.pleio.nl\\/files\\/view\\/57980637\\/support.pleio.nl-20190917T151248.json\\\"}},\\\"7\\\":{\\\"type\\\":\\\"LINK\\\",\\\"mutability\\\":\\\"MUTABLE\\\",\\\"data\\\":{\\\"href\\\":\\\"https:\\/\\/w3.org\\/TR\\/WCAG-EM\\/\\\",\\\"url\\\":\\\"https:\\/\\/w3.org\\/TR\\/WCAG-EM\\/\\\"}},\\\"8\\\":{\\\"type\\\":\\\"LINK\\\",\\\"mutability\\\":\\\"MUTABLE\\\",\\\"data\\\":{\\\"href\\\":\\\"https:\\/\\/zoek.officielebekendmakingen.nl\\/stb-2018-141.html#d17e165\\\",\\\"url\\\":\\\"https:\\/\\/zoek.officielebekendmakingen.nl\\/stb-2018-141.html#d17e165\\\"}}}}\"}]'),(3095450913,57978431,'icon_alt','Site icoon'),(3095450915,57978431,'logo_alt','Site logo'),(3095450931,57978431,'initiative_title',''),(3095450933,57978431,'initiative_image_alt','Logo van het initiatief'),(3095450934,57978431,'initiative_description',''),(3095450939,57978431,'show_tag_in_news_card','no'),(3095450950,57978431,'cancel_membership_enabled','yes'),(3095450951,57978431,'achievements_enabled','no'),(3095450952,57978431,'custom_tags_allowed','yes'),(3095450953,57979210,'pleio_token','HurtEVFi3tsbu75rsfHMHf59QI7PzD'),(3095450954,57979211,'elgg:internal:priority','1'),(3095451076,57977371,'te_last_update_nl','1572526440'),(3095451077,57977371,'te_last_update_en','1572526440');
/*!40000 ALTER TABLE `elgg_private_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elgg_push_notifications_count`
--

DROP TABLE IF EXISTS `elgg_push_notifications_count`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elgg_push_notifications_count` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_guid` bigint(20) NOT NULL,
  `site_guid` bigint(20) NOT NULL,
  `container_guid` bigint(20) NOT NULL,
  `count` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_guid` (`user_guid`,`container_guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elgg_push_notifications_count`
--

LOCK TABLES `elgg_push_notifications_count` WRITE;
/*!40000 ALTER TABLE `elgg_push_notifications_count` DISABLE KEYS */;
/*!40000 ALTER TABLE `elgg_push_notifications_count` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elgg_push_notifications_subscriptions`
--

DROP TABLE IF EXISTS `elgg_push_notifications_subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elgg_push_notifications_subscriptions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_guid` bigint(20) NOT NULL,
  `client_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service` enum('gcm','apns','wns') COLLATE utf8mb4_unicode_ci NOT NULL,
  `device_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elgg_push_notifications_subscriptions`
--

LOCK TABLES `elgg_push_notifications_subscriptions` WRITE;
/*!40000 ALTER TABLE `elgg_push_notifications_subscriptions` DISABLE KEYS */;
/*!40000 ALTER TABLE `elgg_push_notifications_subscriptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elgg_river`
--

DROP TABLE IF EXISTS `elgg_river`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elgg_river` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtype` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action_type` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_guid` bigint(20) NOT NULL,
  `access_id` bigint(20) NOT NULL,
  `view` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_guid` bigint(20) NOT NULL,
  `object_guid` bigint(20) NOT NULL,
  `annotation_id` bigint(20) NOT NULL,
  `posted` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type` (`type`),
  KEY `action_type` (`action_type`),
  KEY `access_id` (`access_id`),
  KEY `subject_guid` (`subject_guid`),
  KEY `object_guid` (`object_guid`),
  KEY `annotation_id` (`annotation_id`),
  KEY `posted` (`posted`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `elgg_sites_entity`
--

DROP TABLE IF EXISTS `elgg_sites_entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elgg_sites_entity` (
  `guid` bigint(20) unsigned NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`guid`),
  UNIQUE KEY `url` (`url`),
  FULLTEXT KEY `name` (`name`,`description`,`url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elgg_sites_entity`
--

LOCK TABLES `elgg_sites_entity` WRITE;
/*!40000 ALTER TABLE `elgg_sites_entity` DISABLE KEYS */;
INSERT INTO `elgg_sites_entity` VALUES (57977371,'Development','Development','http://localhost:8090/');
/*!40000 ALTER TABLE `elgg_sites_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elgg_system_log`
--

DROP TABLE IF EXISTS `elgg_system_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elgg_system_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `object_id` bigint(20) NOT NULL,
  `object_class` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `object_type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `object_subtype` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `event` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `performed_by_guid` bigint(20) NOT NULL,
  `owner_guid` bigint(20) NOT NULL,
  `site_guid` bigint(20) DEFAULT NULL,
  `access_id` bigint(20) NOT NULL,
  `enabled` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes',
  `time_created` bigint(20) NOT NULL,
  `ip_address` varchar(46) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `object_id` (`object_id`),
  KEY `object_class` (`object_class`),
  KEY `object_type` (`object_type`),
  KEY `object_subtype` (`object_subtype`),
  KEY `event` (`event`),
  KEY `performed_by_guid` (`performed_by_guid`),
  KEY `access_id` (`access_id`),
  KEY `time_created` (`time_created`),
  KEY `river_key` (`object_type`,`object_subtype`,`event`)
) ENGINE=InnoDB AUTO_INCREMENT=1589239089 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `elgg_users_apisessions`
--

DROP TABLE IF EXISTS `elgg_users_apisessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elgg_users_apisessions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_guid` bigint(20) unsigned NOT NULL,
  `site_guid` bigint(20) unsigned NOT NULL,
  `token` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expires` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_guid` (`user_guid`,`site_guid`),
  KEY `token` (`token`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elgg_users_apisessions`
--

LOCK TABLES `elgg_users_apisessions` WRITE;
/*!40000 ALTER TABLE `elgg_users_apisessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `elgg_users_apisessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elgg_users_entity`
--

DROP TABLE IF EXISTS `elgg_users_entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elgg_users_entity` (
  `guid` bigint(20) unsigned NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `salt` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `password_hash` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `code` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `banned` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `admin` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `last_action` bigint(20) NOT NULL DEFAULT 0,
  `prev_last_action` bigint(20) NOT NULL DEFAULT 0,
  `last_login` bigint(20) NOT NULL DEFAULT 0,
  `prev_last_login` bigint(20) NOT NULL DEFAULT 0,
  `pleio_guid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`guid`),
  UNIQUE KEY `username` (`username`),
  KEY `password` (`password`),
  KEY `email` (`email`(50)),
  KEY `code` (`code`),
  KEY `last_action` (`last_action`),
  KEY `last_login` (`last_login`),
  KEY `admin` (`admin`),
  KEY `pleio_guid` (`pleio_guid`),
  FULLTEXT KEY `name` (`name`),
  FULLTEXT KEY `name_2` (`name`,`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `elgg_users_sessions`
--

DROP TABLE IF EXISTS `elgg_users_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elgg_users_sessions` (
  `session` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ts` bigint(20) unsigned NOT NULL DEFAULT 0,
  `data` mediumblob DEFAULT NULL,
  PRIMARY KEY (`session`),
  KEY `ts` (`ts`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_access_tokens` (
  `access_token` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_id` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `scope` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`access_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_tokens`
--

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_authorization_codes`
--

DROP TABLE IF EXISTS `oauth_authorization_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_authorization_codes` (
  `authorization_code` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_id` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect_uri` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `scope` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`authorization_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_authorization_codes`
--

LOCK TABLES `oauth_authorization_codes` WRITE;
/*!40000 ALTER TABLE `oauth_authorization_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_authorization_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_clients` (
  `client_id` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_secret` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect_uri` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `grant_types` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scope` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gcm_key` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apns_cert` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wns_key` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wns_secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_clients`
--

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_jwt`
--

DROP TABLE IF EXISTS `oauth_jwt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_jwt` (
  `client_id` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `public_key` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_jwt`
--

LOCK TABLES `oauth_jwt` WRITE;
/*!40000 ALTER TABLE `oauth_jwt` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_jwt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_refresh_tokens` (
  `refresh_token` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_id` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `scope` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`refresh_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_tokens`
--

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_scopes`
--

DROP TABLE IF EXISTS `oauth_scopes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_scopes` (
  `scope` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_default` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_scopes`
--

LOCK TABLES `oauth_scopes` WRITE;
/*!40000 ALTER TABLE `oauth_scopes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_scopes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_users`
--

DROP TABLE IF EXISTS `oauth_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_users` (
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_users`
--

LOCK TABLES `oauth_users` WRITE;
/*!40000 ALTER TABLE `oauth_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pleio_request_access`
--

DROP TABLE IF EXISTS `pleio_request_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pleio_request_access` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `guid` bigint(20) unsigned NOT NULL,
  `user` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_created` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `guid` (`guid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pleio_request_access`
--

LOCK TABLES `pleio_request_access` WRITE;
/*!40000 ALTER TABLE `pleio_request_access` DISABLE KEYS */;
/*!40000 ALTER TABLE `pleio_request_access` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-31 14:06:48
