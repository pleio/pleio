#!/bin/sh

echo "[i] Initializing environment..."
/scripts/initialize.sh

echo "[i] Starting apache..."
rm -f /var/run/apache2/apache2.pid

ln -sf /dev/stdout /var/log/apache2/access.log
ln -sf /dev/stderr /var/log/apache2/error.log

apachectl -D FOREGROUND
