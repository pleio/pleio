#!/bin/sh

# Configure default environment variables
export SMTP_HOST=${SMTP_HOST:-mail.pleio.nl}
export SMTP_DOMAIN=${SMTP_DOMAIN:-pleio.nl}
export SMTP_USE_TLS=${SMTP_TLS:-no}

envsubst < /app/docker/ssmtp.conf > /etc/ssmtp/ssmtp.conf
if [ -z "$SMTP_USER" ] || [ "$SMTP_USER" == "none" ]
then
  /bin/sed -i "s/^#*AuthUser=.*$/#AuthUser=/" /etc/ssmtp/ssmtp.conf
  /bin/sed -i "s/^#*AuthPass=.*$/#AuthPass=/" /etc/ssmtp/ssmtp.conf
fi