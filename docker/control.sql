CREATE DATABASE IF NOT EXISTS `pleio-control`;
USE `pleio-control`;

DROP TABLE IF EXISTS `instances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instances` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `host` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT 1,
  `env` varchar(50) DEFAULT 'prod',
  PRIMARY KEY (`id`),
  UNIQUE KEY `host` (`host`)
) ENGINE=InnoDB AUTO_INCREMENT=712 DEFAULT CHARSET=latin1;

LOCK TABLES `instances` WRITE;
INSERT INTO `instances` VALUES (3,'localhost','pleio','root','VRRMrGVJynuXHzM9BKPd',1,'dev');
UNLOCK TABLES;
